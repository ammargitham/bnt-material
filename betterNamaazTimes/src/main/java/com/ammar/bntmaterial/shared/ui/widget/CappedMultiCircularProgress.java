package com.ammar.bntmaterial.shared.ui.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.shared.ui.transitions.MaterialInterpolator;

/**
 * Created by agitham on 8/10/2015.
 */
public class CappedMultiCircularProgress extends View implements ValueAnimator.AnimatorUpdateListener {

    private static final String TAG = "CappedMultiCircularProgress";
    private Paint mPrimaryPaint;
    private Paint mSecondaryPaint;
    private Paint mBackgroundPaint;
    private RectF mRectF;
    private int mSecondaryProgressColor;
    private int mPrimaryProgressColor;
    private int mBackgroundColor;
    private int mStrokeWidth;
    private int mProgress;
    private int mSecondaryProgress;
    private int mPrimaryCapSize;
    private int mSecondaryCapSize;
    private int mPrimarySweepAngle;
    private boolean mIsPrimaryCapVisible;
    private boolean mIsSecondaryCapVisible;
    private Interpolator interpolator;
    private int mWidth = 0;
    private int mHeight = 0;
    private boolean animate = true;
    private int mCurrentPrimarySweepAngle;
    private ValueAnimator mAnimator;

    public CappedMultiCircularProgress(Context context) {
        super(context);
        init(context, null);
    }

    public CappedMultiCircularProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CappedMultiCircularProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    void init(Context context, AttributeSet attrs) {
        TypedArray a;
        if (attrs != null) {
            a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CappedMultiCircularProgress,
                    0, 0);
        }
        else {
            throw new IllegalArgumentException("Must have to pass the attributes");
        }

        try {
            mBackgroundColor = a.getColor(R.styleable.CappedMultiCircularProgress_backgroundColor, ContextCompat.getColor(getContext(), android.R.color.darker_gray));
            mPrimaryProgressColor = a.getColor(R.styleable.CappedMultiCircularProgress_progressColor, ContextCompat.getColor(getContext(), android.R.color.darker_gray));
            mSecondaryProgressColor = a.getColor(R.styleable.CappedMultiCircularProgress_secondaryProgressColor, ContextCompat.getColor(getContext(), android.R.color.black));

            mProgress = a.getInt(R.styleable.CappedMultiCircularProgress_progress, 0);
            mSecondaryProgress = a.getInt(R.styleable.CappedMultiCircularProgress_secondaryProgress, 0);

            mPrimarySweepAngle = (mProgress * 360) / 100;
            mCurrentPrimarySweepAngle = animate ? 0 : mPrimarySweepAngle;

            mStrokeWidth = a.getDimensionPixelSize(R.styleable.CappedMultiCircularProgress_strokeWidth, 20);

            mPrimaryCapSize = a.getDimensionPixelSize(R.styleable.CappedMultiCircularProgress_primaryCapSize, mStrokeWidth / 2);
            mSecondaryCapSize = a.getDimensionPixelSize(R.styleable.CappedMultiCircularProgress_secodaryCapSize, mStrokeWidth / 2);

            mIsPrimaryCapVisible = a.getBoolean(R.styleable.CappedMultiCircularProgress_primaryCapVisibility, true);
            mIsSecondaryCapVisible = a.getBoolean(R.styleable.CappedMultiCircularProgress_secodaryCapVisibility, mSecondaryProgress != 0);
        }
        finally {
            a.recycle();
        }

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setStyle(Paint.Style.STROKE);
        mBackgroundPaint.setStrokeWidth(mStrokeWidth);
        mBackgroundPaint.setColor(mBackgroundColor);

        mPrimaryPaint = new Paint();
        mPrimaryPaint.setAntiAlias(true);
        mPrimaryPaint.setStyle(Paint.Style.STROKE);
        mPrimaryPaint.setStrokeWidth(mStrokeWidth);
        mPrimaryPaint.setColor(mPrimaryProgressColor);

        mSecondaryPaint = new Paint();
        mSecondaryPaint.setAntiAlias(true);
        mSecondaryPaint.setStyle(Paint.Style.STROKE);
        mSecondaryPaint.setStrokeWidth(mStrokeWidth - 2);
        mSecondaryPaint.setColor(mSecondaryProgressColor);

        mRectF = new RectF();

        if (animate) {
            startAnimating();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mRectF.set(getPaddingLeft(), getPaddingTop(), w - getPaddingRight(), h - getPaddingBottom());
        mWidth = w;
        mHeight = h;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPrimaryPaint.setStyle(Paint.Style.STROKE);
        mSecondaryPaint.setStyle(Paint.Style.STROKE);

        double trad;
        int r = (getHeight() - getPaddingLeft() * 2) / 2;      // Calculated from canvas width

        // for drawing a full progress .. The background circle
        canvas.drawArc(mRectF, 0, 360, false, mBackgroundPaint);

        // for drawing a secondary progress circle
        int secondarySweepAngle = (mSecondaryProgress * 360) / 100;
        canvas.drawArc(mRectF, 270, secondarySweepAngle, false, mSecondaryPaint);

        // for cap of secondary progress
        if (mIsSecondaryCapVisible) {
            trad = (secondarySweepAngle - 90) * (Math.PI / 180d); // = 5.1051
            float x = (float) (r * Math.cos(trad));
            float y = (float) (r * Math.sin(trad));
            mSecondaryPaint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(x + (mWidth / 2), y + (mHeight / 2), mSecondaryCapSize, mSecondaryPaint);
        }

        // for drawing a main progress circle
        canvas.drawArc(mRectF, 270, mCurrentPrimarySweepAngle, false, mPrimaryPaint);
        mPrimaryPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle((mWidth / 2), getPaddingTop(), mPrimaryCapSize, mPrimaryPaint);

        // for cap of primary progress
        if (mIsPrimaryCapVisible) {
            trad = (mCurrentPrimarySweepAngle - 90) * (Math.PI / 180d); // = 5.1051
            float x = (float) (r * Math.cos(trad));
            float y = (float) (r * Math.sin(trad));
            canvas.drawCircle(x + (mWidth / 2), y + (mHeight / 2), mPrimaryCapSize, mPrimaryPaint);
        }
    }

    private void startAnimating() {
        mAnimator = ValueAnimator.ofInt(mCurrentPrimarySweepAngle, mPrimarySweepAngle);
        mAnimator.setInterpolator(new MaterialInterpolator());
        mAnimator.setDuration(500);
        mAnimator.setStartDelay(500);
        mAnimator.addUpdateListener(this);
        mAnimator.start();
    }

    /**
     * <p>Notifies the occurrence of another frame of the animation.</p>
     *
     * @param animation The animation which was repeated.
     */
    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        mCurrentPrimarySweepAngle = (int) animation.getAnimatedValue();
        invalidate();
    }

    public void setBackgroundColor(int mBackgroundColor) {
        this.mBackgroundColor = mBackgroundColor;
        invalidate();
    }

    public void setSecondaryProgressColor(int mSecondaryProgressColor) {
        this.mSecondaryProgressColor = mSecondaryProgressColor;
        invalidate();
    }

    public void setPrimaryProgressColor(int mPrimaryProgressColor) {
        this.mPrimaryProgressColor = mPrimaryProgressColor;
        invalidate();
    }

    public void setStrokeWidth(int mStrokeWidth) {
        this.mStrokeWidth = mStrokeWidth;
        invalidate();
    }

    public void setProgress(int mProgress) {
        setProgress(mProgress, 0);
    }

    public void setProgress(int mProgress, int delay) {
        this.mProgress = mProgress;
        mPrimarySweepAngle = (this.mProgress * 360) / 100;
        if (animate && mAnimator != null) {
            if (mAnimator.isRunning()) {
                mAnimator.cancel();
            }
            mAnimator.setIntValues(mCurrentPrimarySweepAngle, mPrimarySweepAngle);
            mAnimator.setStartDelay(delay);
            mAnimator.start();
        }
        else {
            mCurrentPrimarySweepAngle = mPrimarySweepAngle;
            invalidate();
        }
    }

    public void setSecondaryProgress(int mSecondaryProgress) {
        this.mSecondaryProgress = mSecondaryProgress;
        invalidate();
    }

    public void setPrimaryCapSize(int mPrimaryCapSize) {
        this.mPrimaryCapSize = mPrimaryCapSize;
        invalidate();
    }

    public void setSecondaryCapSize(int mSecondaryCapSize) {
        this.mSecondaryCapSize = mSecondaryCapSize;
        invalidate();
    }

    public boolean isPrimaryCapVisible() {
        return mIsPrimaryCapVisible;
    }

    public void setIsPrimaryCapVisible(boolean mIsPrimaryCapVisible) {
        this.mIsPrimaryCapVisible = mIsPrimaryCapVisible;
    }

    public boolean isSecondaryCapVisible() {
        return mIsSecondaryCapVisible;
    }

    public void setIsSecondaryCapVisible(boolean mIsSecondaryCapVisible) {
        this.mIsSecondaryCapVisible = mIsSecondaryCapVisible;
    }

    public int getSecondaryProgressColor() {
        return mSecondaryProgressColor;
    }

    public int getPrimaryProgressColor() {
        return mPrimaryProgressColor;
    }

    public int getProgress() {
        return mProgress;
    }

    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    public int getSecodaryProgress() {
        return mSecondaryProgress;
    }

    public int getPrimaryCapSize() {
        return mPrimaryCapSize;
    }

    public int getSecondaryCapSize() {
        return mSecondaryCapSize;
    }

    public Interpolator getInterpolator() {
        return interpolator;
    }

    public void setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;
    }
}
