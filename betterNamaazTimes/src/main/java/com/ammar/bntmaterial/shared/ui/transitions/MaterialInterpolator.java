package com.ammar.bntmaterial.shared.ui.transitions;

import android.view.animation.Interpolator;

/**
 * Created by Ammar Githam on 25/09/2015.
 */
public class MaterialInterpolator implements Interpolator {

    @Override
    public float getInterpolation(float x) {
        return (float) (6 * Math.pow(x, 2) - 8 * Math.pow(x, 3) + 3 * Math.pow(x, 4));
    }
}