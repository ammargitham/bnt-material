package com.ammar.bntmaterial.shared;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.calendar.HijriCalendarFragment;
import com.ammar.bntmaterial.core.SalaatUtils;
import com.ammar.bntmaterial.home.HomeFragment;
import com.ammar.bntmaterial.home.SalaatTimesFragment;
import com.ammar.bntmaterial.locationchooser.LocationChooserActivity;
import com.ammar.bntmaterial.notifications.NotificationUtils;
import com.ammar.bntmaterial.settings.SettingsActivity;
import com.ammar.bntmaterial.util.Constants;
import com.ammar.bntmaterial.util.PreferenceKeys;
import com.ammar.bntmaterial.util.ThemeHelper;
import com.ammar.bntmaterial.util.Utils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.OnHomeFragmentInteractionListener, HijriCalendarFragment.OnCalendarFragmentInteractionListener {

    private static final long    DRAWER_CLOSE_DELAY_MS = 250;
    private static final String  NAV_ITEM_ID           = "navItemId";
    private static final String  TAG                   = "MainActivity";
    private final        Handler mDrawerActionHandler  = new Handler();
    private DrawerLayout          mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private int                   mNavItemId;
    private double                mLatitude;
    private double                mLongitude;
    private int                   mBackStackCount;
    private Fragment              mCurrentFragment;
    private String                mCurrentFragmentTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = setupActionBar();

        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        String savedLat = sharedPreferences.getString(PreferenceKeys.LATITUDE, "");
        String savedLong = sharedPreferences.getString(PreferenceKeys.LONGITUDE, "");

        boolean isLatLongValid = false;

        try {
            mLatitude = Double.parseDouble(savedLat);
            mLongitude = Double.parseDouble(savedLong);
            isLatLongValid = true;
        }
        catch (NumberFormatException e) {
            Log.d(TAG, "Latitude or Longitude not saved or wrong!\nStarting LocationChooserActivity to capture proper Latitude and Longitude.");
        }

        if (!isLatLongValid) {
            Intent intent = new Intent(this, LocationChooserActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        if (SalaatUtils.isSalaatRecalculationRequired(this)) {
            Log.d(TAG, "onCreate: Recalculation of salaat times required");
            SalaatUtils.calculateSalaatTimesAndSaveToPreferences(this, mLatitude, mLongitude);
        }

        if (savedInstanceState == null) {
            mNavItemId = R.id.item_home;
            //mNavItemId = R.id.item_calendar;
        }
        else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }

        // listen for navigation events
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        // select the correct nav menu item
        navigationView.getMenu().findItem(mNavItemId).setChecked(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // set up the hamburger icon to open and close the drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBackStackCount > 0) {
                    onBackPressed();
                    return;
                }
                if (mCurrentFragment instanceof HijriCalendarFragment) {
                    if (((HijriCalendarFragment) mCurrentFragment).onBackPressed()) {
                        return;
                    }
                }
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int currentBackStackCount = getSupportFragmentManager().getBackStackEntryCount();
                if (currentBackStackCount < mBackStackCount) {
                    ValueAnimator anim = ValueAnimator.ofFloat(1, 0);
                    anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {
                            float slideOffset = (float) valueAnimator.getAnimatedValue();
                            mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
                        }
                    });
                    anim.setInterpolator(new DecelerateInterpolator());
                    anim.setDuration(500);
                    anim.start();
                }
                mBackStackCount = currentBackStackCount;
            }
        });

        navigate(mNavItemId);

        setUpAlarms();
    }

    private void setUpAlarms() {
        boolean isSalaatNotificationAlarmSet = NotificationUtils.checkIfNotificationAlarmIsSet(this, Constants.SALAAT_NOTIFICATION_REQUEST_CODE);
        if (!isSalaatNotificationAlarmSet) {
            NotificationUtils.setNextSalaatAlarm(this);
        }
        else {
            Log.d(TAG, "setUpAlarms: Next Salaat Alarm already active");
        }
        boolean isNextDaySalaatCalculationAlarmSet = NotificationUtils.checkIfDayChangeAlarmIsSet(this);
        if (!isNextDaySalaatCalculationAlarmSet) {
            NotificationUtils.setDayChangeAlarm(this);
        }
        else {
            Log.d(TAG, "setUpAlarms: Day change alarm already active");
        }
    }

    private Toolbar setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        return toolbar;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void navigate(int itemId) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (itemId) {
            case R.id.item_home:
                mCurrentFragment = HomeFragment.newInstance();
                mCurrentFragmentTag = HomeFragment.TAG;
                break;
            case R.id.item_calendar:
                mCurrentFragment = HijriCalendarFragment.newInstance();
                mCurrentFragmentTag = HijriCalendarFragment.TAG;
                mDrawerLayout.setStatusBarBackgroundColor(ThemeHelper.getPrimaryDarkColor(this, R.style.Theme_CalendarTheme));
                break;
            case R.id.item_settings:
                startActivity(new Intent(this, SettingsActivity.class));
        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, mCurrentFragment, mCurrentFragmentTag)
                .commit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        // update highlighted item in the navigation menu
        if (menuItem.getGroupId() != R.id.menu_other) {
            menuItem.setChecked(true);
        }
        mNavItemId = menuItem.getItemId();
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(mNavItemId);
            }
        }, DRAWER_CLOSE_DELAY_MS);

        return true;
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        if (mCurrentFragment instanceof HijriCalendarFragment) {
            if (((HijriCalendarFragment) mCurrentFragment).onBackPressed()) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setElevation(0);
            }
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    @Override
    public void onSalaatTimesCardPressed() {

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        final SalaatTimesFragment fragment = SalaatTimesFragment.newInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Transition transition = new Slide();
            transition.setDuration(getResources().getInteger(R.integer.fragment_transition_duration));
            fragment.setEnterTransition(transition);
            fragment.setReturnTransition(transition);
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) { }

                @Override
                public void onTransitionEnd(Transition transition) {
                    if (getSupportFragmentManager().getBackStackEntryCount() == 0 && getSupportActionBar() != null) {
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, android.R.color.transparent)));
                    }
                }

                @Override
                public void onTransitionCancel(Transition transition) { }

                @Override
                public void onTransitionPause(Transition transition) { }

                @Override
                public void onTransitionResume(Transition transition) { }
            });
        }

        // Delaying fragment transaction to show ripple on salaat card
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ThemeHelper.getPrimaryColor(MainActivity.this, R.style.Theme_AppTheme)));
                    getSupportActionBar().setElevation(Utils.dpToPx(MainActivity.this, 2));
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment, SalaatTimesFragment.TAG)
                        .addToBackStack(SalaatTimesFragment.TAG)
                        .commit();
                ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float slideOffset = (float) valueAnimator.getAnimatedValue();
                        mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
                    }
                });
                anim.setInterpolator(new DecelerateInterpolator());
                anim.setDuration(500);
                anim.start();
            }
        }, 200);
    }

    @Override
    public void onCalendarPanelSlide(float slideOffset) {
        mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
        if (slideOffset == 1.0f) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        else if (slideOffset == 0.0f) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }
}
