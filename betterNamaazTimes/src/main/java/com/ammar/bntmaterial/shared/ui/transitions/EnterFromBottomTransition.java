package com.ammar.bntmaterial.shared.ui.transitions;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;

import com.ammar.bntmaterial.util.Utils;

/**
 * Created by Ammar Githam on 24/09/2015.
 */
public class EnterFromBottomTransition extends Transition {

    // Define a key for storing a property value in
    // TransitionValues.values with the syntax
    // package_name:transition_class:property_name to avoid collisions
    private static final String PROPNAME_Y =
            "com.ammar.bntmaterial:EnterFromBottomTransition:y";
    private static final String TAG = "EnterFromBottomTransition";

    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    private void captureValues(TransitionValues transitionValues) {
        // Get a reference to the view
        View view = transitionValues.view;
        // Store its background property in the values map
        transitionValues.values.put(PROPNAME_Y, view.getY());
    }

    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {

        ObjectAnimator animation = ObjectAnimator.ofFloat(endValues.view, "y", Utils.getScreenHeight(sceneRoot.getContext()) + 50, (float) endValues.values.get(PROPNAME_Y));
        animation.setDuration(500);
        animation.start();
        return animation;
    }
}
