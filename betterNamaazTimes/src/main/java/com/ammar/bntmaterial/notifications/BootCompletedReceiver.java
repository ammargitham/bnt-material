package com.ammar.bntmaterial.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by agitham on 19/11/2015.
 */
public class BootCompletedReceiver extends BroadcastReceiver {

	private static final String TAG = "BootCompletedReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {

		Log.d(TAG, "onReceive: Phone has restarted, set next salaat notification alarm");
		NotificationUtils.setNextSalaatAlarm(context);
		NotificationUtils.setDayChangeAlarm(context);
	}
}
