package com.ammar.bntmaterial.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ammar.bntmaterial.core.SalaatUtils;

/**
 * Created by agitham on 20/11/2015.
 */
public class DayChangeReceiver extends BroadcastReceiver {

    private static final String TAG = "DayChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        // Nisful Layl of Previous Date over. Calculate salaat times of current date and set the next salaat alarm.
        Log.d(TAG, "onReceive: Day Changed!");
        SalaatUtils.calculateSalaatTimesAndSaveToPreferences(context);
        NotificationUtils.setNextSalaatAlarm(context);
        NotificationUtils.setDayChangeAlarm(context);
    }
}
