package com.ammar.bntmaterial.notifications;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.core.SalaatUtils;
import com.ammar.bntmaterial.shared.MainActivity;
import com.ammar.bntmaterial.util.Constants;
import com.ammar.bntmaterial.util.PreferenceKeys;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by agitham on 19/11/2015.
 */
public class NotificationUtils {

	private static final String TAG = "NotificationUtils";

	public static void scheduleNotificationExact(Context context, int notificationId, int notificationRequestCode, Notification notification, DateTime dateTime, boolean setNextSalaatAlarm) {
		Intent notificationIntent = new Intent(context, NotificationPublisher.class);
		notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, notificationId);
		notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
		notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_REQUEST_CODE, notificationRequestCode);
		notificationIntent.putExtra(NotificationPublisher.SET_NEXT_SALAAT_ALARM, setNextSalaatAlarm);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationRequestCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		long futureInMillis = dateTime.withSecondOfMinute(0).getMillis();
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setExact(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
	}

	public static Notification getSalaatNotification(Context context, SalaatUtils.NextSalaat nextSalaat) {
		String title = nextSalaat.salaat;
		String content = DateFormat.getTimeFormat(context).format(nextSalaat.salaatTime.toDate());
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setContentTitle(title);
		builder.setContentText(content);
		builder.setSmallIcon(R.mipmap.ic_launcher);
		builder.setCategory(NotificationCompat.CATEGORY_ALARM);
		builder.setPriority(NotificationCompat.PRIORITY_HIGH);
		builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
		builder.setVibrate(new long[]{0, 300, 300, 300});
		builder.setLights(Color.WHITE, 2000, 3000);
		builder.setShowWhen(false);
		Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		builder.setSound(notificationSound);
		Intent notificationIntent = new Intent(context, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, Constants.SALAAT_NOTIFICATION_CLICK_REQUEST_CODE, notificationIntent, 0);
		builder.setContentIntent(intent);
		builder.setAutoCancel(true);
		return builder.build();
	}

	public static void setNextSalaatAlarm(Context context) {
		SalaatUtils.NextSalaat nextSalaat = SalaatUtils.getNextSalaat(context);
		Notification salaatNotification = NotificationUtils.getSalaatNotification(context, nextSalaat);
		// If current notification is a salaat notification, now set the next salaat notification.
		// If current salaat notification is Nisful Layl, then don't set next salaat as next salaat has not been calculated yet. DayChangeReceiver will set the next salaat alarm.
		// If the current notification is a salaat notification, SET_NEXT_SALAAT_ALARM will be set to true or false.
		boolean setNextSalaatAlarm = true;
		boolean isNisfulLayl = false;
		if (nextSalaat.salaat.equalsIgnoreCase(context.getString(R.string.nisful_layl))) {
			isNisfulLayl = true;
		}
		if (isNisfulLayl) {
			// Check if next DayChangeAlarm is today or not. If today then do not let NotificationPublisher schedule next salaat alarm.
			SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
			DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
			DateTime dayChangeDateTime;
			try {
				dayChangeDateTime = DateTime.parse(sharedPreferences.getString(PreferenceKeys.DAY_CHANGE_DATE, ""), dateTimeFormatter);
			}
			catch (IllegalArgumentException e) {
				dayChangeDateTime = null;
			}
			if (dayChangeDateTime != null && dayChangeDateTime.toLocalDate().equals(nextSalaat.salaatTime.toLocalDate())) {
				setNextSalaatAlarm = false;
			}
		}
		Log.d(TAG, "setNextSalaatAlarm: Next Salaat notification " + nextSalaat.salaat + " at " + nextSalaat.salaatTime);
		NotificationUtils.scheduleNotificationExact(context, Constants.SALAAT_NOTIFICATION_ID, Constants.SALAAT_NOTIFICATION_REQUEST_CODE, salaatNotification, nextSalaat.salaatTime, setNextSalaatAlarm);
	}

	public static boolean checkIfNotificationAlarmIsSet(Context context, int notificationRequestCode) {
		return checkIfAlarmIsSet(context, notificationRequestCode, new Intent(context, NotificationPublisher.class));
	}

	public static boolean checkIfDayChangeAlarmIsSet(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
		DateTime dayChangeDateTime;
		try {
			dayChangeDateTime = DateTime.parse(sharedPreferences.getString(PreferenceKeys.DAY_CHANGE_DATE, ""), dateTimeFormatter);
		}
		catch (IllegalArgumentException e) {
			dayChangeDateTime = null;
		}
		return dayChangeDateTime != null && !dayChangeDateTime.isBefore(new DateTime()) && checkIfAlarmIsSet(context, Constants.DAY_CHANGE_REQUEST_CODE, new Intent(context, DayChangeReceiver.class));
	}

	public static boolean checkIfAlarmIsSet(Context context, int notificationRequestCode, Intent intent) {
		return (PendingIntent.getBroadcast(context, notificationRequestCode, intent, PendingIntent.FLAG_NO_CREATE) != null);
	}

	public static void setDayChangeAlarm(Context context) {

		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
		DateTime nisfulLaylSalaatTime;
		try {
			nisfulLaylSalaatTime = DateTime.parse(sharedPreferences.getString(PreferenceKeys.SALAAT_TIME_NISFUL_LAYL, ""), dateTimeFormatter);
		}
		catch (IllegalArgumentException e) {
			nisfulLaylSalaatTime = null;
		}

		if (nisfulLaylSalaatTime != null) {
			long futureInMillis = nisfulLaylSalaatTime.getMillis();
			Intent intent = new Intent(context, DayChangeReceiver.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.DAY_CHANGE_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Log.d(TAG, "setDayChangeAlarm: Next day change at: " + nisfulLaylSalaatTime);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(PreferenceKeys.DAY_CHANGE_DATE, dateTimeFormatter.print(nisfulLaylSalaatTime));
			editor.apply();
			alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);
		}
	}
}
