package com.ammar.bntmaterial.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ammar.bntmaterial.util.Constants;

public class NotificationPublisher extends BroadcastReceiver {

	public static final  String NOTIFICATION_ID           = "notification-id";
	public static final  String NOTIFICATION              = "notification";
	public static final  String NOTIFICATION_REQUEST_CODE = "notification-request-code";
	public static final  String SET_NEXT_SALAAT_ALARM     = "set-next-salaat-alarm";
	private static final String TAG                       = "NotificationPublisher";

	public void onReceive(Context context, Intent intent) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = intent.getParcelableExtra(NOTIFICATION);
		int id = intent.getIntExtra(NOTIFICATION_ID, 0);
		int requestCode = intent.getIntExtra(NOTIFICATION_REQUEST_CODE, 0);
		notificationManager.notify(id, notification);

		switch (requestCode) {
			case Constants.SALAAT_NOTIFICATION_REQUEST_CODE:

				boolean setNextSalaatAlarm = intent.getBooleanExtra(SET_NEXT_SALAAT_ALARM, true);
				Log.d(TAG, "onReceive: setNextSalaatAlarm: " + setNextSalaatAlarm);
				if (setNextSalaatAlarm) {
					NotificationUtils.setNextSalaatAlarm(context);
				}
				else {
					Log.d(TAG, "onReceive: Not setting next alarm as setNextSalaatAlarm is false");
				}
				break;
			default:
		}
	}
}