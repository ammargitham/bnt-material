package com.ammar.bntmaterial.util;

/**
 * Created by Ammar Githam on 25/09/2015.
 */
public class PreferenceKeys {

	public static final String   LATITUDE                  = "pref_latitude";
	public static final String   SALAAT_DATE               = "pref_salaat_date";
	public static final String   LONGITUDE                 = "pref_longitude";
	public static final String   ADDRESS                   = "pref_address";
	public static final String   LOCATION_AUTO_UPDATE      = "pref_location_auto_update";
	public static final String   LOCATION_LAST_UPDATED     = "pref_location_last_updated";
	public static final String   SALAAT_TIME_SIHORI_END    = "pref_salaat_time_sihori_end";
	public static final String   SALAAT_TIME_SUNRISE       = "pref_salaat_time_sunrise";
	public static final String   SALAAT_TIME_FAJR_START    = "pref_salaat_time_fajr_start";
	public static final String   SALAAT_TIME_FAJR_END      = "pref_salaat_time_fajr_end";
	public static final String   SALAAT_TIME_ZAWAL         = "pref_salaat_time_zawal";
	public static final String   SALAAT_TIME_ZOHR_END      = "pref_salaat_time_zohr_end";
	public static final String   SALAAT_TIME_ASR_END       = "pref_salaat_time_asr_end";
	public static final String   SALAAT_TIME_MAGHRIB       = "pre_salaat_time_maghrib";
	public static final String   SALAAT_TIME_NISFUL_LAYL   = "pref_salaat_time_nisful_layl";
	public static final String   SALAAT_NOTIFY_SIHORI_END  = "pref_salaat_notify_sihori_end";
	public static final String   SALAAT_NOTIFY_FAJR_START  = "pref_salaat_notify_fajr_start";
	public static final String   SALAAT_NOTIFY_FAJR_END    = "pref_salaat_notify_fajr_end";
	public static final String   SALAAT_NOTIFY_ZAWAL       = "pref_salaat_notify_zawal";
	public static final String   SALAAT_NOTIFY_ZOHR_END    = "pref_salaat_notify_zohr_end";
	public static final String   SALAAT_NOTIFY_ASR_END     = "pref_salaat_notify_asr_end";
	public static final String   SALAAT_NOTIFY_MAGHRIB     = "pre_salaat_notify_maghrib";
	public static final String   SALAAT_NOTIFY_NISFUL_LAYL = "pref_salaat_notify_nisful_layl";
	public static final String[] SALAAT_TIME_KEYS          = {SALAAT_TIME_SIHORI_END, SALAAT_TIME_FAJR_START, SALAAT_TIME_FAJR_END,
			SALAAT_TIME_ZAWAL, SALAAT_TIME_ZOHR_END, SALAAT_TIME_ASR_END, SALAAT_TIME_MAGHRIB, SALAAT_TIME_NISFUL_LAYL};
	public static final String[] SALAAT_NOTIFY_KEYS        = {SALAAT_NOTIFY_SIHORI_END, SALAAT_NOTIFY_FAJR_START, SALAAT_NOTIFY_FAJR_END,
			SALAAT_NOTIFY_ZAWAL, SALAAT_NOTIFY_ZOHR_END, SALAAT_NOTIFY_ASR_END, SALAAT_NOTIFY_MAGHRIB, SALAAT_NOTIFY_NISFUL_LAYL};
	public static final String   DAY_CHANGE_DATE           = "pref_day_change_date";
}