package com.ammar.bntmaterial.util;

import android.content.Context;
import android.content.res.TypedArray;

import com.ammar.bntmaterial.R;

/**
 * Created by agitham on 30/7/2015.
 */
public class ThemeHelper {

	public static int getPrimaryColor(Context context, int style) {
		return getThemeAttributeColor(context, style, R.attr.colorPrimary);
	}

	public static int getPrimaryDarkColor(Context context, int style) {
		return getThemeAttributeColor(context, style, R.attr.colorPrimaryDark);
	}

	public static int getAccentColor(Context context, int style) {
		return getThemeAttributeColor(context, style, R.attr.colorAccent);
	}

	public static int getThemeAttributeColor(Context context, int style, int attr) {
		TypedArray a = context.obtainStyledAttributes(style, new int[]{attr});
		int color = a.getColor(0, 0);
		a.recycle();
		return color;
	}
}
