package com.ammar.bntmaterial.util;

/**
 * Created by agitham on 15/9/2015.
 */
public class Constants {

	public static final int    GEOCODER_SUCCESS_RESULT                = 0;
	public static final int    GEOCODER_FAILURE_RESULT                = 1;
	public static final String PACKAGE_NAME                           = "com.ammar.bntmaterial";
	public static final String LOCATION_LATITUDE                      = PACKAGE_NAME + ".LOCATION_LATITUDE";
	public static final String LOCATION_LONGITUDE                     = PACKAGE_NAME + ".LOCATION_LONGITUDE";
	public static final String GEOCODER_RECEIVER                      = PACKAGE_NAME + ".GEOCODER_RECEIVER";
	public static final String GEOCODER_RESULT_DATA_KEY               = PACKAGE_NAME + ".GEOCODER_RESULT_DATA_KEY";
	public static final String GEOCODER_REQUEST_ID                    = PACKAGE_NAME + ".GEOCODER_REQUEST_ID";
	public static final String GEOCODER_RESULT_REQUEST_ID             = ".GEOCODER_RESULT_REQUEST_ID";
	public static final int    MAP_CHOOSER_REQUEST_CODE               = 10;
	public static final int    MAP_CHOOSER_SUCCESS_RESULT             = 0;
	public static final String SALAAT_LOCAL_DATE_PATTERN              = "yyyy-MM-dd";
	public static final String SALAAT_TIMES_LOCAL_TIME_PATTERN        = "yyyy-MM-dd H:mm:ss";
	public static final int    SALAAT_NOTIFICATION_ID                 = 100;
	public static final int    SALAAT_NOTIFICATION_REQUEST_CODE       = 500;
	public static final int    SALAAT_NOTIFICATION_CLICK_REQUEST_CODE = 600;
	public static final int    DAY_CHANGE_REQUEST_CODE                = 510;
}
