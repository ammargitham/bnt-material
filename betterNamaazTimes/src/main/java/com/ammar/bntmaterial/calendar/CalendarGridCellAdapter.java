package com.ammar.bntmaterial.calendar;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.core.HijriDateHelper;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.chrono.GregorianChronology;
import org.joda.time.format.DateTimeFormat;

public class CalendarGridCellAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private static final String TAG                  = "CalendarGridCellAdapter";
	private static final int    VIEW_TYPE_DAY_HEADER = 0;
	private static final int    VIEW_TYPE_DAY        = 1;
	private OnGridItemClickListener mListener;
	private String[]                dayNames;
	private DateTime                tempHijriDateTime;
	private int                     daysBetween;
	private int                     currentActiveMonth;
	private int                     actualDayOfMonth;
	private int                     actualMonth;
	private int                     actualYear;

	public CalendarGridCellAdapter(Context context, DateTime hijriDateTime) {
		dayNames = context.getResources().getStringArray(R.array.day_names_short);
		init(hijriDateTime);
	}

	@Override
	public int getItemViewType(int position) {

		if (position <= 6) {
			return VIEW_TYPE_DAY_HEADER;
		}

		return VIEW_TYPE_DAY;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		switch (viewType) {
			case VIEW_TYPE_DAY_HEADER:
				TextView dayHeaderView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.hijricalendar_grid_day_header_cell, parent, false);
				return new CalendarGridCellAdapter.DayHeaderViewHolder(dayHeaderView);

			case VIEW_TYPE_DAY:
			default:
				TextView textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.hijricalendar_grid_day_cell, parent, false);
				return new MonthDayViewHolder(textView);
		}
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

		if (position <= 6 && viewHolder instanceof DayHeaderViewHolder) {
			((DayHeaderViewHolder) viewHolder).dayHeader.setText(dayNames[position]);
		}
		else {
			int offset = position - dayNames.length;
			MonthDayViewHolder monthDayViewHolder = (MonthDayViewHolder) viewHolder;
			TextView cellTextView = monthDayViewHolder.textView;
			DateTime currentCellHijriDateTime = tempHijriDateTime.plusDays(offset);
			DateTime tempEngDateTime = currentCellHijriDateTime.withChronology(GregorianChronology.getInstance(DateTimeZone.getDefault()));
			String engDate = DateTimeFormat.forPattern("d MMM").print(tempEngDateTime);
			String gridContent = cellTextView.getContext().getString(R.string.calendar_grid_cell_content, currentCellHijriDateTime.getDayOfMonth(), engDate);
			SpannableStringBuilder stringBuilder = new SpannableStringBuilder(gridContent);
			stringBuilder.setSpan(new TypefaceSpan("sans-serif-medium"), 0, gridContent.indexOf(engDate) - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			stringBuilder.setSpan(new AbsoluteSizeSpan(12, true), gridContent.indexOf(engDate), gridContent.indexOf(engDate) + engDate.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			stringBuilder.setSpan(new TypefaceSpan("sans-serif-light"), gridContent.indexOf(engDate), gridContent.indexOf(engDate) + engDate.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			cellTextView.setText(stringBuilder);
			cellTextView.setTag(R.id.calendar_cell_tag_date, currentCellHijriDateTime);
			cellTextView.setClickable(true);

			if (currentCellHijriDateTime.getMonthOfYear() != currentActiveMonth) {
				cellTextView.setAlpha(0.5f);
				cellTextView.setClickable(false);
			}
			else if (currentCellHijriDateTime.getDayOfMonth() == actualDayOfMonth && currentCellHijriDateTime.getMonthOfYear() == actualMonth
					&& currentCellHijriDateTime.getYear() == actualYear) {
				cellTextView.setTag(R.id.calendar_cell_tag_is_today, true);
				cellTextView.setBackgroundResource(R.drawable.calendar_grid_cell_today_background);
				cellTextView.setTextColor(ContextCompat.getColor(cellTextView.getContext(), R.color.calendar_today_cell_text_color));
			}
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return daysBetween + 1 + dayNames.length;
	}

	public void init(DateTime hijriDateTime) {

		currentActiveMonth = hijriDateTime.getMonthOfYear();

		DateTime actualHijriDateTime = HijriDateHelper.getHijriDate();
		actualDayOfMonth = actualHijriDateTime.getDayOfMonth();
		actualMonth = actualHijriDateTime.getMonthOfYear();
		actualYear = actualHijriDateTime.getYear();
		//weeksInMonth = HijriDateTools.weeksInJodaIslamicMonth(hijriDateTime);

		DateTime firstDayInGrid = hijriDateTime.withDayOfMonth(1);

		// If it is not a Sunday then calculate the earliest previous Sunday.
		if (firstDayInGrid.getDayOfWeek() != 7) {
			firstDayInGrid = firstDayInGrid.withDayOfWeek(1).minusDays(1);
		}

		int firstGridHijriDay = firstDayInGrid.getDayOfMonth(); // First Sunday (in the grid)
		int firstGridHijriMonth = firstDayInGrid.getMonthOfYear();
		int firstGridHijriYear = firstDayInGrid.getYear();

		DateTime lastDayInGrid = hijriDateTime.withDayOfMonth(hijriDateTime.dayOfMonth().getMaximumValue()).withDayOfWeek(6);
		int lastGridHijriDay = lastDayInGrid.getDayOfMonth(); // Last Saturday (in the grid)
		int lastGridHijriMonth = lastDayInGrid.getMonthOfYear();
		int lastGridHijriYear = lastDayInGrid.getYear();

		if (lastGridHijriDay < hijriDateTime.dayOfMonth().getMaximumValue() && lastGridHijriDay > 10) {
			lastGridHijriDay = lastDayInGrid.plusDays(7).getDayOfMonth();
			lastGridHijriMonth = lastDayInGrid.plusDays(7).getMonthOfYear();
			lastGridHijriYear = lastDayInGrid.plusDays(7).getYear();
		}

		daysBetween = Days.daysBetween(hijriDateTime.withYear(firstGridHijriYear).withMonthOfYear(firstGridHijriMonth).withDayOfMonth(firstGridHijriDay).toLocalDate(),
				hijriDateTime.withYear(lastGridHijriYear).withMonthOfYear(lastGridHijriMonth).withDayOfMonth(lastGridHijriDay).toLocalDate()).getDays();

		tempHijriDateTime = hijriDateTime.withYear(firstGridHijriYear).withMonthOfYear(firstGridHijriMonth).withDayOfMonth(firstGridHijriDay); // 1st day of month
	}

	public class MonthDayViewHolder extends RecyclerView.ViewHolder {

		public TextView textView;

		public MonthDayViewHolder(TextView textView) {
			super(textView);
			this.textView = textView;

			this.textView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.onGridItemClicked(v);
					}
				}
			});
		}
	}

	public class DayHeaderViewHolder extends RecyclerView.ViewHolder {

		public TextView dayHeader;

		public DayHeaderViewHolder(TextView v) {
			super(v);
			dayHeader = v;
		}
	}

	public void setOnGridItemClickListener(OnGridItemClickListener onGridItemClickListener) {
		this.mListener = onGridItemClickListener;
	}

	public interface OnGridItemClickListener {

		void onGridItemClicked(View view);
	}

	@Override
	public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
		super.onDetachedFromRecyclerView(recyclerView);
		this.mListener = null;
	}
}
