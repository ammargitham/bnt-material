package com.ammar.bntmaterial.calendar;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.core.HijriDateHelper;

import org.joda.time.DateTime;

/**
 * Created by agitham on 27/7/2015.
 */
public class HijriCalendarPageFragment extends Fragment {

	private static final String TAG      = "HijriCalendarPageFragment";
	private static final String ARG_DATE = "date";
	private DateTime mDateTime;
	private View     mCurrentSelectedCell;
	private DateTime mCurrentSelectCellDateTime;
	private boolean mIsCurrentSelectedCellToday = true;
	private OnGridItemClickListener mListener;

	public static HijriCalendarPageFragment newInstance(DateTime dateTime) {

		Bundle args = new Bundle();
		args.putString(ARG_DATE, HijriDateHelper.dateTimeToString(dateTime));

		HijriCalendarPageFragment fragment = new HijriCalendarPageFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		Context themedContext = new ContextThemeWrapper(getContext(), R.style.Theme_CalendarTheme);
		LayoutInflater localInflater = inflater.cloneInContext(themedContext);
		Bundle args = getArguments();
		String dateTimeString = args.getString(ARG_DATE);

		RecyclerView calendarGridRecyclerView = (RecyclerView) localInflater.inflate(R.layout.fragment_hijricalendarpage, container, false);

		if (dateTimeString != null) {
			DateTime hijriDateTime = HijriDateHelper.stringToDateTime(dateTimeString);
			mDateTime = hijriDateTime;
			CalendarGridCellAdapter calendarGridCellAdapter = new CalendarGridCellAdapter(container.getContext(), hijriDateTime);
			calendarGridRecyclerView.setHasFixedSize(true);
			GridLayoutManager gridLayoutManager = new GridLayoutManager(container.getContext(), 7);
			calendarGridRecyclerView.setLayoutManager(gridLayoutManager);
			calendarGridRecyclerView.setAdapter(calendarGridCellAdapter);
			calendarGridRecyclerView.setTag(hijriDateTime);
			calendarGridCellAdapter.setOnGridItemClickListener(new CalendarGridCellAdapter.OnGridItemClickListener() {
				@Override
				public void onGridItemClicked(final View view) {

					if (mCurrentSelectedCell != null && !mIsCurrentSelectedCellToday) {
						mCurrentSelectedCell.setSelected(false);
					}

					mCurrentSelectCellDateTime = (DateTime) view.getTag(R.id.calendar_cell_tag_date);
					mIsCurrentSelectedCellToday = (boolean) (view.getTag(R.id.calendar_cell_tag_is_today) == null ? false : view.getTag(R.id.calendar_cell_tag_is_today));
					mCurrentSelectedCell = view;

					if (!mIsCurrentSelectedCellToday) {
						view.setSelected(true);
					}

					if (mListener != null) {
						mListener.onGridItemClicked(mCurrentSelectCellDateTime);
					}
				}
			});
		}
		return calendarGridRecyclerView;
	}

	@Override
	public String toString() {
		return HijriDateHelper.dateTimeToString(mDateTime);
	}

	public DateTime getHijriDateTime() {
		return mDateTime;
	}

	public void setOnGridItemClickListener(OnGridItemClickListener onGridItemClickListener) {
		this.mListener = onGridItemClickListener;
	}

	public interface OnGridItemClickListener {

		void onGridItemClicked(DateTime currentSelectedDateTime);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
}
