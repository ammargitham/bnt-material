package com.ammar.bntmaterial.calendar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ammar.bntmaterial.shared.ui.widget.FragmentItemIdStatePagerAdapter;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * Created by agitham on 29/7/2015.
 */
public class HijriCalendarPagerAdapter extends FragmentItemIdStatePagerAdapter {

	private static final String TAG = "HijriCalendarPagerAdapter";
	private DateTime                                          currentCenterDateTime;
	private HijriCalendarPageFragment.OnGridItemClickListener onGridItemClickListener;

	public HijriCalendarPagerAdapter(FragmentManager fm, DateTime initDateTime, HijriCalendarPageFragment.OnGridItemClickListener onGridItemClickListener) {
		super(fm);
		this.currentCenterDateTime = initDateTime;
		this.onGridItemClickListener = onGridItemClickListener;
	}

	/**
	 * Return the Fragment associated with a specified position.
	 *
	 * @param position
	 */
	@Override
	public Fragment getItem(int position) {
		int offset = position - 1;
		DateTime dateTime = currentCenterDateTime.plusMonths(offset);
		HijriCalendarPageFragment hijriCalendarPageFragment = HijriCalendarPageFragment.newInstance(dateTime);
		hijriCalendarPageFragment.setOnGridItemClickListener(onGridItemClickListener);
		return hijriCalendarPageFragment;
	}

	/**
	 * Return the number of views available.
	 */
	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public int getItemPosition(Object object) {

		HijriCalendarPageFragment hijriCalendarFragment = (HijriCalendarPageFragment) object;
		DateTime hijriDateTime = hijriCalendarFragment.getHijriDateTime();

		if (Math.abs(hijriDateTime.getMonthOfYear() - currentCenterDateTime.getMonthOfYear()) >= 2) {
			return POSITION_NONE;
		}

		LocalDate firstDate = hijriDateTime.toLocalDate();
		LocalDate secondDate = currentCenterDateTime.toLocalDate();
		int compare = firstDate.compareTo(secondDate);

		if (compare < 0) {
			return 0;
		}

		if (compare > 0) {
			return 2;
		}

		return 1;
	}

	public DateTime onPageChanged(int position) {
		int offset = position - 1;
		currentCenterDateTime = currentCenterDateTime.plusMonths(offset);
		notifyDataSetChanged();
		return currentCenterDateTime;
	}

	@Override
	public long getItemId(int position) {
		int offset = position - 1;
		return currentCenterDateTime.plusMonths(offset).getMillis();
	}
}
