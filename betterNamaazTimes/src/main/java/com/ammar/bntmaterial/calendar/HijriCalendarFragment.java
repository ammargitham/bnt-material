package com.ammar.bntmaterial.calendar;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.core.HijriDateHelper;
import com.ammar.bntmaterial.core.HijriMonthName;
import com.ammar.bntmaterial.core.timescalculator.SalaatTimes;
import com.ammar.bntmaterial.core.timescalculator.TimesCalculator;
import com.ammar.bntmaterial.shared.ui.transitions.MaterialInterpolator;
import com.ammar.bntmaterial.util.PreferenceKeys;
import com.ammar.bntmaterial.util.ThemeHelper;
import com.ammar.bntmaterial.util.Utils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.GregorianChronology;

/**
 * Created by agitham on 1/7/2015.
 */
public class HijriCalendarFragment extends Fragment {

	public static final String  TAG              = "HijriCalendarFragment";
	private             boolean mIsFabAtBottom   = false;
	private             int     mCurrentPosition = 1;
	private DateTime                              mCurrentPageHijriDateTime;
	private DateTime                              mCurrentSelectedHijriDateTime;
	private TextView                              hijriMonthTextView;
	private TextView                              englishDateRangeTextView;
	private SlidingUpPanelLayout                  mSlidingUpPanel;
	private FloatingActionButton                  mCalendarFab;
	private FrameLayout.LayoutParams              mCalendarFabLayoutParams;
	private int                                   mFabCenterMinPixelsFromBottom;
	private boolean                               mHasFabDrawn;
	private OnCalendarFragmentInteractionListener mFragmentInteractionListener;
	private int                                   mHeaderMinPaddingLeft;
	private int                                   mSliderHeaderMaxPaddingLeft;
	private ArgbEvaluator                         mArgbEvaluator;
	private ColorDrawable                         mSliderHeaderBackgroundDrawable;
	private boolean                               mIsFabUpAnimationRunning;
	private int                                   mSlideOffsetPixels;
	private TextView                              mSliderHeaderTextView;
	private int                                   mSliderHeaderDefaultTextColor;
	private LinearLayout                          mSliderContentContainer;
	private LayoutInflater                        mLocalInflater;
	private double                                mLatitude;
	private double                                mLongitude;
	private String[]                              mSalaatNamesArray;
	private int[]                                 mSalaatPrefKeys;

	public static HijriCalendarFragment newInstance() {
		return new HijriCalendarFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mHeaderMinPaddingLeft = Utils.dpToPx(getContext(), 16);
		mSliderHeaderMaxPaddingLeft = Utils.dpToPx(getContext(), 72);
		mArgbEvaluator = new ArgbEvaluator();
		int mPrimaryColor = ThemeHelper.getPrimaryColor(getContext(), R.style.Theme_CalendarTheme);
		mSliderHeaderBackgroundDrawable = new ColorDrawable(mPrimaryColor);
		mSalaatNamesArray = getResources().getStringArray(R.array.salaat_names);
		mSalaatPrefKeys = SalaatTimes.SALAAT_KEYS;
		SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		try {
			mLatitude = Double.parseDouble(sharedPreferences.getString(PreferenceKeys.LATITUDE, ""));
			mLongitude = Double.parseDouble(sharedPreferences.getString(PreferenceKeys.LONGITUDE, ""));
		}
		catch (NumberFormatException e) {
			Log.e(TAG, "onCreate: Latitude or Longitude not saved in preferences");
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		Context themedContext = new ContextThemeWrapper(getContext(), R.style.Theme_CalendarTheme);
		mLocalInflater = inflater.cloneInContext(themedContext);
		View rootView = mLocalInflater.inflate(R.layout.fragment_hijricalendar, container, false);
		hijriMonthTextView = (TextView) rootView.findViewById(R.id.hijri_full_date_tv);
		englishDateRangeTextView = (TextView) rootView.findViewById(R.id.eng_full_date_tv);
		mCurrentPageHijriDateTime = HijriDateHelper.getHijriDate();
		mCurrentSelectedHijriDateTime = mCurrentPageHijriDateTime;
		mSlidingUpPanel = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
		mCalendarFab = (FloatingActionButton) rootView.findViewById(R.id.calendar_fab);
		final int fabMinimumMargin = getResources().getDimensionPixelSize(R.dimen.fab_margin);
		final int collapsedPanelHeight = mSlidingUpPanel.getPanelHeight();

		mCalendarFab.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				mCalendarFab.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				mCalendarFabLayoutParams = (FrameLayout.LayoutParams) mCalendarFab.getLayoutParams();
				mFabCenterMinPixelsFromBottom = fabMinimumMargin + (mCalendarFab.getHeight() / 2);
				mHasFabDrawn = true;
				showPanel();
			}
		});

		mSlidingUpPanel.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
			@Override
			public void onPanelSlide(View panel, float slideOffset, int slideOffsetPixels) {
				mSlideOffsetPixels = slideOffsetPixels;
				if (slideOffsetPixels < 0) {
					slideOffsetPixels = collapsedPanelHeight - Math.abs(slideOffsetPixels);
				}
				if (slideOffsetPixels > mFabCenterMinPixelsFromBottom) {
					if (slideOffsetPixels > mSlidingUpPanel.getHeight() - mCalendarFab.getHeight()) {
						if (!mIsFabAtBottom) {
							animateFabToBottom();
						}
					}
					else {
						if (mIsFabAtBottom) {
							animateFabToTop();
						}
						if (!mIsFabUpAnimationRunning) {
							mCalendarFabLayoutParams.bottomMargin = slideOffsetPixels - mCalendarFab.getHeight() / 2;
							mCalendarFab.setLayoutParams(mCalendarFabLayoutParams);
						}
					}
				}
				if (slideOffset >= 0.0f) {
					updateOffsetDependentSliderViews(slideOffset);
				}
				if (mFragmentInteractionListener != null) {
					mFragmentInteractionListener.onCalendarPanelSlide(slideOffset);
				}
			}

			@Override
			public void onPanelCollapsed(View panel) {

			}

			@Override
			public void onPanelExpanded(View panel) {
			}

			@Override
			public void onPanelAnchored(View panel) {

			}

			@Override
			public void onPanelHidden(View panel) {

			}
		});

		ViewPager calendarViewPager = (ViewPager) rootView.findViewById(R.id.calendar_viewpager);
		final HijriCalendarPagerAdapter hijriCalendarPagerAdapter = new HijriCalendarPagerAdapter(getChildFragmentManager(), mCurrentPageHijriDateTime, new HijriCalendarPageFragment.OnGridItemClickListener() {
			@Override
			public void onGridItemClicked(DateTime currentSelectedDateTime) {
				mCurrentSelectedHijriDateTime = currentSelectedDateTime;
				updateSliderContent();
			}
		});
		calendarViewPager.setOffscreenPageLimit(1);
		calendarViewPager.setAdapter(hijriCalendarPagerAdapter);
		calendarViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				mCurrentPosition = position;
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					mCurrentPageHijriDateTime = hijriCalendarPagerAdapter.onPageChanged(mCurrentPosition);
					currentDateChanged();
				}
			}
		});
		calendarViewPager.setCurrentItem(1);
		currentDateChanged();

		mSliderHeaderTextView = (TextView) rootView.findViewById(R.id.selected_date_tv);
		mSliderHeaderTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleSlidingPanel();
			}
		});
		mSliderHeaderDefaultTextColor = mSliderHeaderTextView.getCurrentTextColor();

		mSliderContentContainer = (LinearLayout) rootView.findViewById(R.id.salaat_container);

		updateSliderContent();
		return rootView;
	}

	private void toggleSlidingPanel() {
		if (mSlidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
			mSlidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
			return;
		}
		mSlidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
	}

	private void animateFabToBottom() {
		ValueAnimator valueAnimator = ValueAnimator.ofInt(mCalendarFabLayoutParams.bottomMargin, mFabCenterMinPixelsFromBottom - mCalendarFab.getHeight() / 2);
		valueAnimator.setInterpolator(new MaterialInterpolator());
		valueAnimator.setDuration(500);
		valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				mCalendarFabLayoutParams.bottomMargin = (int) animation.getAnimatedValue();
				mCalendarFab.setLayoutParams(mCalendarFabLayoutParams);
			}
		});
		valueAnimator.start();
		mIsFabAtBottom = true;
	}

	private void animateFabToTop() {
		mIsFabAtBottom = false;
		ValueAnimator valueAnimator = ValueAnimator.ofInt(mCalendarFabLayoutParams.bottomMargin, mSlideOffsetPixels - mCalendarFab.getHeight() / 2);
		valueAnimator.setInterpolator(new MaterialInterpolator());
		valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				int newBottomMargin = (int) animation.getAnimatedValue();

				if (newBottomMargin > (mSlideOffsetPixels - mCalendarFab.getHeight() / 2)) {
					animation.cancel();
					mCalendarFabLayoutParams.bottomMargin = mSlideOffsetPixels - mCalendarFab.getHeight() / 2;
				}
				else {
					mCalendarFabLayoutParams.bottomMargin = newBottomMargin;
				}

				mCalendarFab.setLayoutParams(mCalendarFabLayoutParams);
			}
		});
		valueAnimator.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
				mIsFabUpAnimationRunning = true;
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				mIsFabUpAnimationRunning = false;
			}

			@Override
			public void onAnimationCancel(Animator animation) {
				mIsFabUpAnimationRunning = false;
			}

			@Override
			public void onAnimationRepeat(Animator animation) { }
		});
		valueAnimator.start();
	}

	private void currentDateChanged() {
		DateTime startGregorianDateTime = mCurrentPageHijriDateTime.withDayOfMonth(1).withChronology(GregorianChronology.getInstance(DateTimeZone.getDefault()));
		DateTime endGregorianDateTime = mCurrentPageHijriDateTime.withDayOfMonth(mCurrentPageHijriDateTime.dayOfMonth().getMaximumValue()).withChronology(GregorianChronology.getInstance(DateTimeZone.getDefault()));
		String startGregorianDate = startGregorianDateTime.toString("d MMM, yyyy");
		String endGregorianDate = endGregorianDateTime.toString("d MMM, yyyy");
		hijriMonthTextView.setText(getString(R.string.calendar_hijri_month, HijriMonthName.getLongNamefromNumber(mCurrentPageHijriDateTime.getMonthOfYear()), mCurrentPageHijriDateTime.getYear()));
		englishDateRangeTextView.setText(getString(R.string.calendar_eng_date_range, startGregorianDate, endGregorianDate));
		showPanel();
	}

	private void showPanel() {
		if (mHasFabDrawn) {
			if (mSlidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN) {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						mSlidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
					}
				}, 800);
			}
		}
	}

	private void updateSliderContent() {
		mSliderHeaderTextView.setText(mCurrentSelectedHijriDateTime.getDayOfMonth() + " " + HijriMonthName.getShortNameFromNumber(mCurrentSelectedHijriDateTime.getMonthOfYear()) + " " + mCurrentSelectedHijriDateTime.getYear());
		mSliderContentContainer.removeAllViews();
		SparseArray<DateTime> salaatDateTimes = TimesCalculator.calculate(mCurrentSelectedHijriDateTime.withChronology(GregorianChronology.getInstance(DateTimeZone.getDefault())), mLatitude, mLongitude);
		for (int i = 0; i < mSalaatPrefKeys.length; i++) {
			View salaatView = mLocalInflater.inflate(R.layout.salaat_times_list_item, mSliderContentContainer, false);
			((TextView) salaatView.findViewById(R.id.salaat_tv)).setText(mSalaatNamesArray[i]);
			((TextView) salaatView.findViewById(R.id.time_tv)).setText(DateFormat.getTimeFormat(getContext()).format(salaatDateTimes.get(mSalaatPrefKeys[i]).toDate()));
			salaatView.findViewById(R.id.notify_iv).setVisibility(View.GONE);
			mSliderContentContainer.addView(salaatView);
		}
	}

	private void updateOffsetDependentSliderViews(float slideOffset) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mSliderHeaderTextView.setElevation(Utils.dpToPx(getContext(), 2) * slideOffset);
		}
		mSliderHeaderBackgroundDrawable.setAlpha((int) (slideOffset * 255));
		mSliderHeaderTextView.setBackground(mSliderHeaderBackgroundDrawable);
		mSliderHeaderTextView.setTextColor((int) mArgbEvaluator.evaluate(slideOffset, mSliderHeaderDefaultTextColor, Color.WHITE));
		int paddingLeft = (int) (mSliderHeaderMaxPaddingLeft * slideOffset);
		if (paddingLeft > mHeaderMinPaddingLeft) {
			mSliderHeaderTextView.setPadding(paddingLeft, mSliderHeaderTextView.getPaddingTop(), mSliderHeaderTextView.getPaddingRight(), mSliderHeaderTextView.getPaddingBottom());
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		try {
			mFragmentInteractionListener = (OnCalendarFragmentInteractionListener) context;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement HijriCalendarFragment.OnCalendarFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mFragmentInteractionListener = null;
	}

	public boolean onBackPressed() {
		if (mSlidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
			mSlidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
			return true;
		}
		return false;
	}

	public interface OnCalendarFragmentInteractionListener {

		void onCalendarPanelSlide(float slideOffset);
	}
}