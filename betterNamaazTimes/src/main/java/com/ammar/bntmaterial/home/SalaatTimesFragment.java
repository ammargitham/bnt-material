package com.ammar.bntmaterial.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.shared.ui.widget.VerticalSpaceItemDecoration;
import com.ammar.bntmaterial.util.Constants;
import com.ammar.bntmaterial.util.PreferenceKeys;
import com.ammar.bntmaterial.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SalaatTimesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SalaatTimesFragment extends Fragment {

	public static final String TAG = "SalaatTimesFragment";

	//private OnFragmentInteractionListener mListener;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @return A new instance of fragment SalaatTimesFragment.
	 */
	public static SalaatTimesFragment newInstance() {
		SalaatTimesFragment fragment = new SalaatTimesFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public SalaatTimesFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		final RecyclerView rootView = (RecyclerView) inflater.inflate(R.layout.fragment_salaat_times, container, false);
		final SparseArray<String> salaatTextSparseArray = new SparseArray<>();
		final SparseArray<String> salaatTimeSparseArray = new SparseArray<>();
		final SparseArray<Boolean> salaatNotifyEnabledArray = new SparseArray<>();
		rootView.setHasFixedSize(true);
		final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		rootView.setLayoutManager(linearLayoutManager);
		final SalaatTimesRecyclerViewAdapter adapter = new SalaatTimesRecyclerViewAdapter(salaatTextSparseArray, salaatTimeSparseArray, salaatNotifyEnabledArray);

		final SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		final String[] salaatNotifyPrefKeys = PreferenceKeys.SALAAT_NOTIFY_KEYS;

		adapter.setOnNotifyClickListener(new SalaatTimesRecyclerViewAdapter.OnNotifyClickListener() {
			@Override
			public void onClick(int position, boolean isActive) {
				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putBoolean(salaatNotifyPrefKeys[position], !isActive);
				editor.apply();
			}
		});

		rootView.setAdapter(adapter);
		rootView.addItemDecoration(new VerticalSpaceItemDecoration(Utils.dpToPx(getContext(), 1)));

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				String[] salaatTimePrefKeys = PreferenceKeys.SALAAT_TIME_KEYS;
				String[] salaatNamesArray = getResources().getStringArray(R.array.salaat_names);
				DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
				for (int i = 0; i < salaatTimePrefKeys.length; i++) {
					DateTime salaatDateTime = DateTime.parse(sharedPreferences.getString(salaatTimePrefKeys[i], ""), dateTimeFormatter);
					salaatTextSparseArray.put(i, salaatNamesArray[i]);
					salaatTimeSparseArray.put(i, DateFormat.getTimeFormat(getActivity()).format(salaatDateTime.toDate()));
					salaatNotifyEnabledArray.put(i, sharedPreferences.getBoolean(salaatNotifyPrefKeys[i], true));
				}
				rootView.getAdapter().notifyDataSetChanged();
			}
		}, 200);

		return rootView;
	}
}
