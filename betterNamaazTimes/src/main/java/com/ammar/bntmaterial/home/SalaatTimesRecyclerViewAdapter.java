package com.ammar.bntmaterial.home;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.util.ThemeHelper;

import java.util.List;

/**
 * Created by agitham on 6/10/2015.
 */
public class SalaatTimesRecyclerViewAdapter extends RecyclerView.Adapter<SalaatTimesRecyclerViewAdapter.SalaatTimeViewHolder> {

	private static final String TAG = "SalaatTimesRecyclerViewAdapter";
	private SparseArray<String>  mSalaatTextArray;
	private SparseArray<String>  mSalaatTimeArray;
	private SparseArray<Boolean> mSalaatNotifyEnabledArray;
	private int mLastPosition         = -1;
	private int mLastAnimatedPosition = 0;
	private OnNotifyClickListener mOnNotifyClickListener;

	public SalaatTimesRecyclerViewAdapter(SparseArray<String> salaatTextArray, SparseArray<String> salaatTimeArray, SparseArray<Boolean> salaatNotifyEnabledArray) {
		this.mSalaatTextArray = salaatTextArray;
		this.mSalaatTimeArray = salaatTimeArray;
		this.mSalaatNotifyEnabledArray = salaatNotifyEnabledArray;
	}

	/**
	 * Called when RecyclerView needs a new {@link RecyclerView.ViewHolder} of the given type to represent
	 * an item.
	 * <p/>
	 * This new ViewHolder should be constructed with a new View that can represent the items
	 * of the given type. You can either create a new View manually or inflate it from an XML
	 * layout file.
	 * <p/>
	 * The new ViewHolder will be used to display items of the adapter using
	 * {@link #onBindViewHolder(RecyclerView.ViewHolder, int, List)}. Since it will be re-used to display
	 * different items in the data set, it is a good idea to cache references to sub views of
	 * the View to avoid unnecessary {@link View#findViewById(int)} calls.
	 *
	 * @param parent   The ViewGroup into which the new View will be added after it is bound to
	 *                 an adapter position.
	 * @param viewType The view type of the new View.
	 * @return A new ViewHolder that holds a View of the given view type.
	 * @see #getItemViewType(int)
	 * @see #onBindViewHolder(RecyclerView.ViewHolder, int)
	 */
	@Override
	public SalaatTimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.salaat_times_list_item, parent, false);
		return new SalaatTimeViewHolder(view);
	}

	/**
	 * Called by RecyclerView to display the data at the specified position. This method should
	 * update the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at the given
	 * position.
	 * <p/>
	 * Note that unlike {@link ListView}, RecyclerView will not call this method
	 * again if the position of the item changes in the data set unless the item itself is
	 * invalidated or the new position cannot be determined. For this reason, you should only
	 * use the <code>position</code> parameter while acquiring the related data item inside
	 * this method and should not keep a copy of it. If you need the position of an item later
	 * on (e.g. in a click listener), use {@link RecyclerView.ViewHolder#getAdapterPosition()} which will
	 * have the updated adapter position.
	 * <p/>
	 * Override {@link #onBindViewHolder(RecyclerView.ViewHolder, int, List)} instead if Adapter can
	 * handle effcient partial bind.
	 *
	 * @param holder   The ViewHolder which should be updated to represent the contents of the
	 *                 item at the given position in the data set.
	 * @param position The position of the item within the adapter's data set.
	 */
	@Override
	public void onBindViewHolder(final SalaatTimeViewHolder holder, final int position) {

		holder.salaatTextView.setText(mSalaatTextArray.get(position));
		holder.timeTextView.setText(mSalaatTimeArray.get(position));

		setNotifyDrawable(holder.notifyImageView, mSalaatNotifyEnabledArray.get(position));

		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				holder.notifyImageView.callOnClick();
			}
		});

		holder.notifyImageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mOnNotifyClickListener != null) {
					mOnNotifyClickListener.onClick(position, (boolean) v.getTag());
				}
				setNotifyDrawable((ImageView) v, !(boolean) v.getTag());
			}
		});

		// If the bound view wasn't previously displayed on screen, it's animated
		if (position > mLastPosition) {
			holder.itemView.setVisibility(View.GONE);
			final Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.slide_up_fade_in);
			animation.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) { }

				@Override
				public void onAnimationEnd(Animation animation) {
					mLastAnimatedPosition = position;
				}

				@Override
				public void onAnimationRepeat(Animation animation) { }
			});
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					holder.itemView.setVisibility(View.VISIBLE);
					holder.itemView.startAnimation(animation);
				}
			}, (position - mLastAnimatedPosition) * 20);
			mLastPosition = position;
		}
	}

	/**
	 * Returns the total number of items in the data set hold by the adapter.
	 *
	 * @return The total number of items in this adapter.
	 */
	@Override
	public int getItemCount() {
		return mSalaatTextArray.size();
	}

	class SalaatTimeViewHolder extends RecyclerView.ViewHolder {

		TextView  salaatTextView;
		TextView  timeTextView;
		ImageView notifyImageView;

		public SalaatTimeViewHolder(View itemView) {
			super(itemView);
			salaatTextView = (TextView) itemView.findViewById(R.id.salaat_tv);
			timeTextView = (TextView) itemView.findViewById(R.id.time_tv);
			notifyImageView = (ImageView) itemView.findViewById(R.id.notify_iv);
		}
	}

	private void setNotifyDrawable(ImageView view, boolean state) {
		if (state) {
			view.setTag(true);
			view.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_notifications_active_black_24dp));
			view.setColorFilter(ThemeHelper.getPrimaryColor(view.getContext(), R.style.Theme_AppTheme));
		}
		else {
			view.setTag(false);
			view.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.ic_notifications_off_black_24dp));
			view.setColorFilter(ContextCompat.getColor(view.getContext(), android.R.color.transparent));
		}
	}

	public void setOnNotifyClickListener(OnNotifyClickListener onNotifyClickListener) {
		this.mOnNotifyClickListener = onNotifyClickListener;
	}

	public interface OnNotifyClickListener {

		void onClick(int position, boolean isActive);
	}
}
