package com.ammar.bntmaterial.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.core.HijriDateHelper;
import com.ammar.bntmaterial.core.HijriMonthName;
import com.ammar.bntmaterial.core.SalaatUtils;
import com.ammar.bntmaterial.locationchooser.LocationChooserActivity;
import com.ammar.bntmaterial.shared.MainActivity;
import com.ammar.bntmaterial.shared.ui.transitions.MaterialInterpolator;
import com.ammar.bntmaterial.shared.ui.widget.CappedMultiCircularProgress;
import com.ammar.bntmaterial.util.PreferenceKeys;
import com.ammar.bntmaterial.util.ThemeHelper;
import com.ammar.bntmaterial.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

/**
 * Created by agitham on 29/6/2015.
 */
public class HomeFragment extends Fragment {

	public static final String TAG = "HomeFragment";
	private OnHomeFragmentInteractionListener mListener;
	private BroadcastReceiver                 mTickReceiver;
	private TextView                          mNextSalaatTextView;
	private CappedMultiCircularProgress       mProgressView;
	private TextView                          mHijriDateTextView;
	private TextView                          mGregorianDateTextView;

	/**
	 * @return New instance of HomeFragment
	 */
	public static HomeFragment newInstance() {
		return new HomeFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mTickReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
					DateTime gregorianDateTime = new DateTime();
					updateDateViews(gregorianDateTime);
					updateNextSalaatView(gregorianDateTime, 0);
				}
			}
		};
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		DrawerLayout mDrawerLayout = ((MainActivity) getActivity()).getDrawerLayout();
		mDrawerLayout.setStatusBarBackgroundColor(ThemeHelper.getPrimaryColor(getContext(), R.style.Theme_AppTheme));

		View rootView = inflater.inflate(R.layout.fragment_home, container, false);
		mHijriDateTextView = (TextView) rootView.findViewById(R.id.hijri_full_date_tv);
		mGregorianDateTextView = (TextView) rootView.findViewById(R.id.eng_full_date_tv);
		View addressCardView = rootView.findViewById(R.id.address_cardview);
		TextView addressTextView = (TextView) addressCardView.findViewById(R.id.address_tv);
		View salaatTimesCardView = rootView.findViewById(R.id.salaattimes_cardview);
		mNextSalaatTextView = (TextView) salaatTimesCardView.findViewById(R.id.salaat_tv);
		mProgressView = (CappedMultiCircularProgress) rootView.findViewById(R.id.progress);

		DateTime gregorianDateTime = new DateTime();
		updateDateViews(gregorianDateTime);

		addressCardView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getContext(), LocationChooserActivity.class));
			}
		});

		salaatTimesCardView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mListener.onSalaatTimesCardPressed();
			}
		});

		SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		String address = sharedPreferences.getString(PreferenceKeys.ADDRESS, "");

		if (Utils.isStringNullOrWhitespace(address)) {
			String latLong = sharedPreferences.getString(PreferenceKeys.LATITUDE, "") + ", " + sharedPreferences.getString(PreferenceKeys.LONGITUDE, "");
			addressTextView.setText(latLong);
		}
		else {
			addressTextView.setText(address);
		}

		Drawable[] drawables = addressTextView.getCompoundDrawables();
		if (drawables.length > 0) {
			Drawable drawableLeft = drawables[0];
			if (drawableLeft != null) {
				// TODO Change pixels to dps
				drawableLeft = Utils.scaleDrawable(drawableLeft, 50, 50);
				drawableLeft.setAlpha(137);
			}
			addressTextView.setCompoundDrawables(drawableLeft, null, null, null);
		}

		updateNextSalaatView(gregorianDateTime, 800);

		Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_from_0_anim);
		animation.setDuration(800);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			animation.setInterpolator(getContext(), android.R.interpolator.fast_out_slow_in);
		}
		else {
			animation.setInterpolator(new MaterialInterpolator());
		}
		addressCardView.setAnimation(animation);

		return rootView;
	}

	private void updateDateViews(DateTime englishDateTime) {
		DateTime hijriDateTime = HijriDateHelper.getHijriDate(englishDateTime);
		String hijriDateFormatted = String.format(getResources().getString(R.string.and_hijri_full_date), hijriDateTime.getDayOfMonth(),
				HijriMonthName.getLongNamefromNumber(hijriDateTime.getMonthOfYear()), hijriDateTime.getYear());
		mHijriDateTextView.setText(hijriDateFormatted);

		String englishDateFormatted = String.format(getResources().getString(R.string.and_eng_full_date), englishDateTime.getDayOfMonth(),
				englishDateTime.toString("MMMM"), englishDateTime.getYear());
		mGregorianDateTextView.setText(englishDateFormatted);
	}

	private void updateNextSalaatView(DateTime gregorianDateTime, int delay) {
		SalaatUtils.NextSalaat nextSalaat = SalaatUtils.getNextSalaat(getContext());
		String nextSalaatTime = DateFormat.getTimeFormat(getContext()).format(nextSalaat.salaatTime.toDate());

		int minsBetweenPreviousAndNextSalaat = Minutes.minutesBetween(nextSalaat.previousSalaatTime.withSecondOfMinute(0),
				nextSalaat.salaatTime.withSecondOfMinute(0)).getMinutes();
		int minsSincePreviousSalaat = Minutes.minutesBetween(nextSalaat.previousSalaatTime.withSecondOfMinute(0), gregorianDateTime.withSecondOfMinute(0)).getMinutes();
		int progress = (int) (((float) minsSincePreviousSalaat / (float) minsBetweenPreviousAndNextSalaat) * 100);
		mProgressView.setProgress(progress, delay);

		// Adding 1 to include end minute.
		int minsToNextSalaat = Minutes.minutesBetween(gregorianDateTime.withSecondOfMinute(0), nextSalaat.salaatTime.withSecondOfMinute(0)).getMinutes() + 1;
		boolean isMins = true;

		// Converting isMins to hours if greater than 60
		if (minsToNextSalaat >= 60) {
			isMins = false;
			minsToNextSalaat /= 60;
		}
		String minsToNextSalaatString = getResources().getQuantityString(
				isMins ? R.plurals.and_min_quant_to_next_salaat : R.plurals.and_hour_quant_to_next_salaat,
				minsToNextSalaat, minsToNextSalaat,
				nextSalaat.salaat,
				nextSalaatTime);
		SpannableStringBuilder builder = new SpannableStringBuilder(minsToNextSalaatString);
		builder.setSpan(
				new StyleSpan(Typeface.BOLD),
				minsToNextSalaatString.indexOf(Integer.toString(minsToNextSalaat)),
				minsToNextSalaatString.indexOf(Integer.toString(minsToNextSalaat)) + Integer.toString(minsToNextSalaat).length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
		);
		builder.setSpan(
				new AbsoluteSizeSpan(24, true),
				minsToNextSalaatString.indexOf(Integer.toString(minsToNextSalaat)),
				minsToNextSalaatString.indexOf(Integer.toString(minsToNextSalaat)) + Integer.toString(minsToNextSalaat).length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
		);
		builder.setSpan(
				new AbsoluteSizeSpan(26, true),
				minsToNextSalaatString.indexOf(nextSalaat.salaat),
				minsToNextSalaatString.indexOf(nextSalaat.salaat) + nextSalaat.salaat.length(),
				Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
		);
		mNextSalaatTextView.setText(builder);
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.home, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			default:
				return false;
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		try {
			mListener = (OnHomeFragmentInteractionListener) context;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onResume() {
		super.onResume();
		getContext().registerReceiver(mTickReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mTickReceiver != null) {
			getContext().unregisterReceiver(mTickReceiver);
		}
	}

	public interface OnHomeFragmentInteractionListener {

		void onSalaatTimesCardPressed();
	}
}
