package com.ammar.bntmaterial.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.SparseArray;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.core.timescalculator.SalaatTimes;
import com.ammar.bntmaterial.core.timescalculator.TimesCalculator;
import com.ammar.bntmaterial.util.Constants;
import com.ammar.bntmaterial.util.PreferenceKeys;
import com.ammar.bntmaterial.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by agitham on 19/11/2015.
 */
public class SalaatUtils {

	private static final String TAG = "SalaatUtils";

	public static NextSalaat getNextSalaat(Context context) {

		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		String[] salaatPrefKeys = PreferenceKeys.SALAAT_TIME_KEYS;
		DateTime currentDateTime = new DateTime();
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
		String salaat = null;
		DateTime salaatTime = null;
		String previousSalaat = null;
		DateTime previousSalaatTime = null;
		DateTime nisfulLaylSalaatTime;

		boolean gotNextSalaat = false;
		// First check if calculated NisfulLayl is over or not.
		try {
			nisfulLaylSalaatTime = DateTime.parse(sharedPreferences.getString(PreferenceKeys.SALAAT_TIME_NISFUL_LAYL, ""), dateTimeFormatter);
			nisfulLaylSalaatTime = nisfulLaylSalaatTime.withSecondOfMinute(0).withDayOfMonth(currentDateTime.getDayOfMonth()).withMonthOfYear(currentDateTime.getMonthOfYear()).withYear(currentDateTime.getYear());

			if (nisfulLaylSalaatTime.getDayOfMonth() == currentDateTime.getDayOfMonth() && nisfulLaylSalaatTime.isAfter(currentDateTime)) {
				gotNextSalaat = true;
				salaat = context.getString(R.string.nisful_layl);
				salaatTime = nisfulLaylSalaatTime;
				previousSalaat = context.getString(R.string.maghrib);
				try {
					previousSalaatTime = DateTime.parse(sharedPreferences.getString(PreferenceKeys.SALAAT_TIME_MAGHRIB, ""), dateTimeFormatter).withDayOfMonth(nisfulLaylSalaatTime.getDayOfMonth() - 1);
					previousSalaatTime = previousSalaatTime.withSecondOfMinute(0);
				}
				catch (IllegalArgumentException e) {
					previousSalaatTime = null;
					Log.d(TAG, "getNextSalaat: Unable to parse SALAAT_TIME_MAGHRIB salaat time: " + e.getLocalizedMessage());
				}
			}
		}
		catch (IllegalArgumentException e) {
			Log.d(TAG, "getNextSalaat: Unable to parse NisfulLayl salaat time: " + e.getLocalizedMessage());
		}

		if (!gotNextSalaat) {
			for (int i = 0; i < salaatPrefKeys.length; i++) {
				salaatTime = DateTime.parse(sharedPreferences.getString(salaatPrefKeys[i], ""), dateTimeFormatter);
				salaatTime = salaatTime.withSecondOfMinute(0);
				if (salaatTime.isAfter(currentDateTime)) {
					salaat = context.getResources().getStringArray(R.array.salaat_names)[i];
					previousSalaat = context.getResources().getStringArray(R.array.salaat_names)[i - 1 == -1 ? salaatPrefKeys.length - 1 : i - 1];
					try {
						previousSalaatTime = DateTime.parse(sharedPreferences.getString(salaatPrefKeys[i - 1 == -1 ? salaatPrefKeys.length - 1 : i - 1], ""), dateTimeFormatter);
						previousSalaatTime = previousSalaatTime.withSecondOfMinute(0);
						if (previousSalaatTime.isAfter(salaatTime)) {
							previousSalaatTime = previousSalaatTime.minusDays(1);
						}
					}
					catch (IllegalArgumentException e) {
						Log.d(TAG, "getNextSalaat: Unable to parse " + salaatPrefKeys[i - 1 == -1 ? salaatPrefKeys.length - 1 : i - 1] + " salaat time: " + e.getLocalizedMessage());
					}
					break;
				}
			}
		}

		NextSalaat nextSalaat = new NextSalaat();
		nextSalaat.salaat = salaat;
		nextSalaat.salaatTime = salaatTime;
		nextSalaat.previousSalaat = previousSalaat;
		nextSalaat.previousSalaatTime = previousSalaatTime;
		return nextSalaat;
	}

	public static void calculateSalaatTimesAndSaveToPreferences(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		String savedLat = sharedPreferences.getString(PreferenceKeys.LATITUDE, "");
		String savedLong = sharedPreferences.getString(PreferenceKeys.LONGITUDE, "");

		boolean isLatLongValid = false;
		double latitude = 0;
		double longitude = 0;
		try {
			latitude = Double.parseDouble(savedLat);
			longitude = Double.parseDouble(savedLong);
			isLatLongValid = true;
		}
		catch (NumberFormatException e) {
			Log.d(TAG, "Latitude or Longitude not saved or wrong!");
		}

		if (isLatLongValid) {
			calculateSalaatTimesAndSaveToPreferences(context, latitude, longitude);
		}
	}

	public static void calculateSalaatTimesAndSaveToPreferences(Context context, double latitude, double longitude) {
		DateTime gregorianDateTime = new DateTime().withSecondOfMinute(0);
		Log.d(TAG, "Calculating salaat times for date: " + gregorianDateTime);
		SparseArray<DateTime> salaatDateTimeMap = TimesCalculator.calculate(gregorianDateTime, latitude, longitude);
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(PreferenceKeys.SALAAT_DATE, DateTimeFormat.forPattern(Constants.SALAAT_LOCAL_DATE_PATTERN).print(new LocalDate()));
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
		editor.putString(PreferenceKeys.SALAAT_TIME_SUNRISE, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_SUNRISE)));
		editor.putString(PreferenceKeys.SALAAT_TIME_SIHORI_END, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_SIHORI_END)));
		editor.putString(PreferenceKeys.SALAAT_TIME_FAJR_START, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_FAJR_START)));
		editor.putString(PreferenceKeys.SALAAT_TIME_FAJR_END, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_FAJR_END)));
		editor.putString(PreferenceKeys.SALAAT_TIME_ZAWAL, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_ZAWAL)));
		editor.putString(PreferenceKeys.SALAAT_TIME_ZOHR_END, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_ZOHR_END)));
		editor.putString(PreferenceKeys.SALAAT_TIME_ASR_END, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_ASR_END)));
		editor.putString(PreferenceKeys.SALAAT_TIME_MAGHRIB, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_MAGHRIB)));
		editor.putString(PreferenceKeys.SALAAT_TIME_NISFUL_LAYL, dateTimeFormatter.print(salaatDateTimeMap.get(SalaatTimes.KEY_NISFUL_LAYL)));
		editor.apply();
	}

	public static boolean isSalaatRecalculationRequired(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
		String savedSalaatDate = sharedPreferences.getString(PreferenceKeys.SALAAT_DATE, "");
		LocalDate savedSalaatLocalDate = null;

		if (!Utils.isStringNullOrWhitespace(savedSalaatDate)) {
			savedSalaatLocalDate = LocalDate.parse(savedSalaatDate, DateTimeFormat.forPattern(Constants.SALAAT_LOCAL_DATE_PATTERN));
		}

		if (savedSalaatLocalDate == null) {
			return true;
		}

		// Salaat was calculated for a date.
		// Now check if Nisful layl is over or not. Calculate the salaat for the new date only if Nisful layl is over.
		DateTime currentDateTime = new DateTime();
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.SALAAT_TIMES_LOCAL_TIME_PATTERN);
		DateTime nisfulLaylSalaatTime = null;
		try {
			nisfulLaylSalaatTime = DateTime.parse(sharedPreferences.getString(PreferenceKeys.SALAAT_TIME_NISFUL_LAYL, ""), dateTimeFormatter);
		}
		catch (IllegalArgumentException e) {
			nisfulLaylSalaatTime = null;
		}

		return nisfulLaylSalaatTime == null || nisfulLaylSalaatTime.isBefore(currentDateTime);
	}

	public static class NextSalaat {

		public String   salaat;
		public DateTime salaatTime;
		public String   previousSalaat;
		public DateTime previousSalaatTime;

		@Override
		public String toString() {
			return "NextSalaat{" +
					"salaat='" + salaat + '\'' +
					", salaatTime=" + salaatTime +
					", previousSalaat='" + previousSalaat + '\'' +
					", previousSalaatTime=" + previousSalaatTime +
					'}';
		}
	}
}
