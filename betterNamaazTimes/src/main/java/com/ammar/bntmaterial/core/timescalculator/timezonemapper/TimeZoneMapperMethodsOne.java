package com.ammar.bntmaterial.core.timescalculator.timezonemapper;


public class TimeZoneMapperMethodsOne {

    public static int kdLookup0(double lat, double lng)
    {
        if (lng < -168.75) {
            if (lat < 56.25) {
                if (lng < -174.5) {
                    return 159;
                } else {
                    if (lat < 50.5) {
                        return 0;
                    } else {
                        if (lng < -171.75) {
                            return 0;
                        } else {
                            if (lat < 53.25) {
                                if (lng < -170.25) {
                                    return 159;
                                } else {
                                    if (lat < 51.75) {
                                        return 0;
                                    } else {
                                        if (lng < -169.5) {
                                            return 159;
                                        } else {
                                            if (lat < 52.5) {
                                                return 0;
                                            } else {
                                                if (lng < -169.25) {
                                                    return 159;
                                                } else {
                                                    return 135;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 159;
                            }
                        }
                    }
                }
            } else {
                if (lng < -174.5) {
                    return 366;
                } else {
                    if (lat < 61.75) {
                        return 135;
                    } else {
                        if (lng < -171.75) {
                            if (lat < 64.5) {
                                if (lng < -173.25) {
                                    return 366;
                                } else {
                                    if (lat < 63.0) {
                                        return 0;
                                    } else {
                                        if (lng < -172.5) {
                                            return 366;
                                        } else {
                                            if (lat < 63.75) {
                                                return 135;
                                            } else {
                                                return 366;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 366;
                            }
                        } else {
                            if (lat < 64.5) {
                                return 135;
                            } else {
                                return 366;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 56.25) {
                if (lng < -163.25) {
                    return 135;
                } else {
                    if (lat < 50.5) {
                        return 0;
                    } else {
                        if (lng < -160.5) {
                            if (lat < 53.25) {
                                return 0;
                            } else {
                                if (lat < 54.75) {
                                    return 135;
                                } else {
                                    if (lng < -162.0) {
                                        return 135;
                                    } else {
                                        return 371;
                                    }
                                }
                            }
                        } else {
                            return 371;
                        }
                    }
                }
            } else {
                if (lng < -163.25) {
                    return 135;
                } else {
                    if (lat < 61.75) {
                        if (lng < -161.75) {
                            if (lat < 59.0) {
                                return 371;
                            } else {
                                if (lat < 60.25) {
                                    if (lng < -162.25) {
                                        return 135;
                                    } else {
                                        return 371;
                                    }
                                } else {
                                    if (lng < -162.0) {
                                        return 135;
                                    } else {
                                        return 371;
                                    }
                                }
                            }
                        } else {
                            return 371;
                        }
                    } else {
                        if (lng < -161.0) {
                            if (lat < 64.5) {
                                if (lat < 63.0) {
                                    if (lng < -162.0) {
                                        return 135;
                                    } else {
                                        return 371;
                                    }
                                } else {
                                    if (lng < -162.25) {
                                        return 135;
                                    } else {
                                        if (lat < 63.75) {
                                            if (lng < -161.75) {
                                                if (lat < 63.25) {
                                                    if (lng < -162.0) {
                                                        return 135;
                                                    } else {
                                                        return 371;
                                                    }
                                                } else {
                                                    if (lng < -162.0) {
                                                        return 135;
                                                    } else {
                                                        return 371;
                                                    }
                                                }
                                            } else {
                                                return 371;
                                            }
                                        } else {
                                            if (lng < -161.75) {
                                                return 135;
                                            } else {
                                                return 371;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 66.0) {
                                    if (lng < -162.0) {
                                        return 135;
                                    } else {
                                        return 371;
                                    }
                                } else {
                                    if (lng < -162.25) {
                                        return 135;
                                    } else {
                                        if (lat < 66.75) {
                                            if (lng < -161.75) {
                                                if (lat < 66.25) {
                                                    if (lng < -162.0) {
                                                        return 135;
                                                    } else {
                                                        return 371;
                                                    }
                                                } else {
                                                    if (lng < -162.0) {
                                                        return 135;
                                                    } else {
                                                        return 371;
                                                    }
                                                }
                                            } else {
                                                return 371;
                                            }
                                        } else {
                                            if (lng < -162.0) {
                                                return 135;
                                            } else {
                                                return 371;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            return 371;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup1(double lat, double lng)
    {
        if (lat < 61.75) {
            if (lng < -138.0) {
                if (lat < 59.0) {
                    return 0;
                } else {
                    if (lng < -139.5) {
                        if (lat < 60.25) {
                            return 98;
                        } else {
                            if (lng < -140.25) {
                                if (lat < 60.5) {
                                    return 98;
                                } else {
                                    return 325;
                                }
                            } else {
                                if (lat < 60.5) {
                                    if (lng < -140.0) {
                                        return 98;
                                    } else {
                                        if (lng < -139.75) {
                                            return 325;
                                        } else {
                                            return 98;
                                        }
                                    }
                                } else {
                                    return 325;
                                }
                            }
                        }
                    } else {
                        if (lat < 60.25) {
                            if (lng < -138.75) {
                                if (lat < 59.5) {
                                    return 98;
                                } else {
                                    if (lng < -139.0) {
                                        return 98;
                                    } else {
                                        if (lat < 60.0) {
                                            return 98;
                                        } else {
                                            return 325;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 59.75) {
                                    return 98;
                                } else {
                                    if (lng < -138.5) {
                                        if (lat < 60.0) {
                                            return 98;
                                        } else {
                                            return 325;
                                        }
                                    } else {
                                        if (lng < -138.25) {
                                            if (lat < 60.0) {
                                                return 45;
                                            } else {
                                                return 325;
                                            }
                                        } else {
                                            if (lat < 60.0) {
                                                return 45;
                                            } else {
                                                return 325;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -139.0) {
                                if (lat < 60.5) {
                                    return 98;
                                } else {
                                    return 325;
                                }
                            } else {
                                return 325;
                            }
                        }
                    }
                }
            } else {
                if (lat < 59.0) {
                    if (lng < -136.5) {
                        return 360;
                    } else {
                        if (lat < 57.5) {
                            return 23;
                        } else {
                            if (lng < -135.75) {
                                if (lat < 58.0) {
                                    return 23;
                                } else {
                                    return 360;
                                }
                            } else {
                                if (lat < 58.0) {
                                    return 23;
                                } else {
                                    return 360;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -136.5) {
                        if (lat < 60.0) {
                            if (lng < -137.25) {
                                if (lat < 59.5) {
                                    if (lng < -137.5) {
                                        return 98;
                                    } else {
                                        if (lat < 59.25) {
                                            return 98;
                                        } else {
                                            return 45;
                                        }
                                    }
                                } else {
                                    return 45;
                                }
                            } else {
                                if (lat < 59.25) {
                                    return 360;
                                } else {
                                    return 45;
                                }
                            }
                        } else {
                            return 325;
                        }
                    } else {
                        if (lat < 60.0) {
                            if (lng < -135.75) {
                                if (lat < 59.5) {
                                    return 360;
                                } else {
                                    if (lng < -136.25) {
                                        return 45;
                                    } else {
                                        if (lng < -136.0) {
                                            if (lat < 59.75) {
                                                return 360;
                                            } else {
                                                return 45;
                                            }
                                        } else {
                                            if (lat < 59.75) {
                                                return 360;
                                            } else {
                                                return 45;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 59.75) {
                                    return 360;
                                } else {
                                    if (lng < -135.5) {
                                        return 45;
                                    } else {
                                        if (lng < -135.25) {
                                            return 360;
                                        } else {
                                            return 45;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 325;
                        }
                    }
                }
            }
        } else {
            if (lng < -138.0) {
                if (lat < 64.5) {
                    if (lng < -140.75) {
                        if (lat < 64.25) {
                            return 325;
                        } else {
                            return 371;
                        }
                    } else {
                        return 325;
                    }
                } else {
                    if (lng < -140.75) {
                        return 371;
                    } else {
                        return 325;
                    }
                }
            } else {
                if (lat < 67.25) {
                    return 325;
                } else {
                    if (lng < -136.0) {
                        return 325;
                    } else {
                        return 143;
                    }
                }
            }
        }
    }

    public static int kdLookup2(double lat, double lng)
    {
        if (lng < -115.5) {
            if (lat < 30.75) {
                return 368;
            } else {
                if (lat < 32.25) {
                    if (lng < -117.0) {
                        return 0;
                    } else {
                        if (lng < -116.25) {
                            if (lat < 31.5) {
                                return 368;
                            } else {
                                if (lng < -116.75) {
                                    return 195;
                                } else {
                                    if (lat < 32.0) {
                                        return 368;
                                    } else {
                                        return 195;
                                    }
                                }
                            }
                        } else {
                            return 368;
                        }
                    }
                } else {
                    if (lng < -117.0) {
                        return 39;
                    } else {
                        if (lng < -116.25) {
                            if (lat < 32.75) {
                                return 195;
                            } else {
                                return 39;
                            }
                        } else {
                            if (lat < 32.75) {
                                if (lng < -116.0) {
                                    if (lat < 32.5) {
                                        return 368;
                                    } else {
                                        return 195;
                                    }
                                } else {
                                    if (lng < -115.75) {
                                        if (lat < 32.5) {
                                            return 368;
                                        } else {
                                            return 195;
                                        }
                                    } else {
                                        if (lat < 32.5) {
                                            return 368;
                                        } else {
                                            return 195;
                                        }
                                    }
                                }
                            } else {
                                return 39;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 30.75) {
                if (lng < -114.0) {
                    if (lat < 29.25) {
                        if (lng < -114.75) {
                            if (lat < 28.5) {
                                if (lng < -115.0) {
                                    return 368;
                                } else {
                                    if (lat < 28.25) {
                                        return 408;
                                    } else {
                                        return 368;
                                    }
                                }
                            } else {
                                return 368;
                            }
                        } else {
                            if (lat < 28.25) {
                                return 408;
                            } else {
                                return 368;
                            }
                        }
                    } else {
                        return 368;
                    }
                } else {
                    if (lat < 29.25) {
                        if (lng < -113.25) {
                            if (lat < 28.25) {
                                return 408;
                            } else {
                                return 368;
                            }
                        } else {
                            return 368;
                        }
                    } else {
                        if (lng < -113.25) {
                            return 368;
                        } else {
                            if (lat < 29.75) {
                                return 368;
                            } else {
                                return 403;
                            }
                        }
                    }
                }
            } else {
                if (lng < -114.0) {
                    if (lat < 32.25) {
                        if (lng < -114.75) {
                            if (lat < 32.0) {
                                return 368;
                            } else {
                                if (lng < -115.0) {
                                    return 368;
                                } else {
                                    return 403;
                                }
                            }
                        } else {
                            if (lat < 31.5) {
                                return 368;
                            } else {
                                if (lng < -114.5) {
                                    if (lat < 31.75) {
                                        return 368;
                                    } else {
                                        return 403;
                                    }
                                } else {
                                    return 403;
                                }
                            }
                        }
                    } else {
                        if (lng < -114.75) {
                            if (lat < 32.75) {
                                if (lng < -115.25) {
                                    if (lat < 32.5) {
                                        return 368;
                                    } else {
                                        return 195;
                                    }
                                } else {
                                    if (lng < -115.0) {
                                        if (lat < 32.5) {
                                            return 368;
                                        } else {
                                            return 195;
                                        }
                                    } else {
                                        if (lat < 32.5) {
                                            return 368;
                                        } else {
                                            return 195;
                                        }
                                    }
                                }
                            } else {
                                return 39;
                            }
                        } else {
                            if (lat < 33.0) {
                                if (lng < -114.5) {
                                    if (lat < 32.5) {
                                        return 403;
                                    } else {
                                        if (lat < 32.75) {
                                            return 9;
                                        } else {
                                            return 39;
                                        }
                                    }
                                } else {
                                    if (lat < 32.5) {
                                        return 403;
                                    } else {
                                        return 9;
                                    }
                                }
                            } else {
                                if (lng < -114.5) {
                                    return 39;
                                } else {
                                    if (lat < 33.25) {
                                        if (lng < -114.25) {
                                            return 39;
                                        } else {
                                            return 9;
                                        }
                                    } else {
                                        return 9;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 32.25) {
                        if (lng < -113.0) {
                            return 403;
                        } else {
                            if (lat < 32.0) {
                                return 403;
                            } else {
                                return 9;
                            }
                        }
                    } else {
                        return 9;
                    }
                }
            }
        }
    }

    public static int kdLookup3(double lat, double lng)
    {
        if (lng < -101.25) {
            if (lat < 11.25) {
                return 0;
            } else {
                if (lng < -107.0) {
                    return 408;
                } else {
                    if (lat < 16.75) {
                        return 0;
                    } else {
                        if (lng < -104.25) {
                            if (lat < 19.5) {
                                return 193;
                            } else {
                                if (lat < 21.0) {
                                    if (lng < -105.75) {
                                        return 0;
                                    } else {
                                        if (lng < -105.0) {
                                            if (lat < 20.75) {
                                                return 193;
                                            } else {
                                                return 408;
                                            }
                                        } else {
                                            return 193;
                                        }
                                    }
                                } else {
                                    if (lng < -105.75) {
                                        return 0;
                                    } else {
                                        if (lng < -104.75) {
                                            return 408;
                                        } else {
                                            if (lat < 21.25) {
                                                if (lng < -104.5) {
                                                    return 193;
                                                } else {
                                                    return 408;
                                                }
                                            } else {
                                                return 408;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 17.5) {
                                return 0;
                            } else {
                                if (lat < 21.0) {
                                    return 193;
                                } else {
                                    if (lng < -104.0) {
                                        if (lat < 22.0) {
                                            return 408;
                                        } else {
                                            return 193;
                                        }
                                    } else {
                                        return 193;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 11.25) {
                return 114;
            } else {
                if (lng < -95.75) {
                    return 193;
                } else {
                    if (lat < 16.75) {
                        if (lng < -92.0) {
                            return 193;
                        } else {
                            if (lat < 14.0) {
                                return 393;
                            } else {
                                if (lat < 15.75) {
                                    return 393;
                                } else {
                                    if (lng < -91.0) {
                                        if (lng < -91.5) {
                                            if (lat < 16.25) {
                                                if (lng < -91.75) {
                                                    return 193;
                                                } else {
                                                    return 393;
                                                }
                                            } else {
                                                return 193;
                                            }
                                        } else {
                                            if (lat < 16.25) {
                                                return 393;
                                            } else {
                                                return 193;
                                            }
                                        }
                                    } else {
                                        if (lng < -90.5) {
                                            if (lat < 16.25) {
                                                return 393;
                                            } else {
                                                return 193;
                                            }
                                        } else {
                                            if (lat < 16.25) {
                                                return 393;
                                            } else {
                                                if (lng < -90.25) {
                                                    if (lat < 16.5) {
                                                        return 193;
                                                    } else {
                                                        return 393;
                                                    }
                                                } else {
                                                    return 393;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -93.0) {
                            return 193;
                        } else {
                            if (lat < 19.5) {
                                if (lng < -91.5) {
                                    if (lat < 18.0) {
                                        return 193;
                                    } else {
                                        if (lng < -92.25) {
                                            return 193;
                                        } else {
                                            if (lat < 18.75) {
                                                if (lng < -92.0) {
                                                    return 193;
                                                } else {
                                                    if (lat < 18.25) {
                                                        if (lng < -91.75) {
                                                            return 193;
                                                        } else {
                                                            return 32;
                                                        }
                                                    } else {
                                                        return 32;
                                                    }
                                                }
                                            } else {
                                                if (lng < -92.0) {
                                                    return 193;
                                                } else {
                                                    return 32;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 18.0) {
                                        if (lng < -90.75) {
                                            if (lat < 17.25) {
                                                if (lng < -91.0) {
                                                    return 193;
                                                } else {
                                                    if (lat < 17.0) {
                                                        return 193;
                                                    } else {
                                                        return 393;
                                                    }
                                                }
                                            } else {
                                                if (lng < -91.25) {
                                                    return 193;
                                                } else {
                                                    if (lat < 17.5) {
                                                        return 393;
                                                    } else {
                                                        return 193;
                                                    }
                                                }
                                            }
                                        } else {
                                            return 393;
                                        }
                                    } else {
                                        if (lng < -91.0) {
                                            if (lat < 18.75) {
                                                if (lat < 18.25) {
                                                    return 193;
                                                } else {
                                                    return 32;
                                                }
                                            } else {
                                                return 32;
                                            }
                                        } else {
                                            return 32;
                                        }
                                    }
                                }
                            } else {
                                return 32;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup4(double lat, double lng)
    {
        if (lat < 28.0) {
            if (lng < -109.75) {
                if (lat < 25.25) {
                    return 408;
                } else {
                    if (lng < -111.25) {
                        return 408;
                    } else {
                        if (lat < 26.5) {
                            return 408;
                        } else {
                            if (lng < -111.0) {
                                return 408;
                            } else {
                                return 403;
                            }
                        }
                    }
                }
            } else {
                if (lat < 25.25) {
                    return 408;
                } else {
                    if (lng < -108.5) {
                        if (lat < 26.5) {
                            return 408;
                        } else {
                            if (lat < 27.25) {
                                if (lng < -108.75) {
                                    return 403;
                                } else {
                                    if (lat < 26.75) {
                                        return 408;
                                    } else {
                                        return 403;
                                    }
                                }
                            } else {
                                if (lng < -108.75) {
                                    return 403;
                                } else {
                                    if (lat < 27.75) {
                                        return 403;
                                    } else {
                                        return 186;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 26.5) {
                            if (lng < -107.75) {
                                return 408;
                            } else {
                                if (lat < 26.0) {
                                    return 408;
                                } else {
                                    if (lng < -107.5) {
                                        if (lat < 26.25) {
                                            return 408;
                                        } else {
                                            return 186;
                                        }
                                    } else {
                                        if (lng < -107.25) {
                                            if (lat < 26.25) {
                                                return 408;
                                            } else {
                                                return 186;
                                            }
                                        } else {
                                            return 186;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -107.75) {
                                if (lat < 27.25) {
                                    if (lng < -108.25) {
                                        if (lat < 27.0) {
                                            return 408;
                                        } else {
                                            return 186;
                                        }
                                    } else {
                                        if (lat < 27.0) {
                                            return 408;
                                        } else {
                                            if (lng < -108.0) {
                                                return 408;
                                            } else {
                                                return 186;
                                            }
                                        }
                                    }
                                } else {
                                    return 186;
                                }
                            } else {
                                return 186;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 30.75) {
                if (lng < -109.75) {
                    return 403;
                } else {
                    if (lng < -108.5) {
                        if (lat < 28.5) {
                            if (lng < -109.0) {
                                return 403;
                            } else {
                                if (lng < -108.75) {
                                    return 186;
                                } else {
                                    if (lat < 28.25) {
                                        return 186;
                                    } else {
                                        return 403;
                                    }
                                }
                            }
                        } else {
                            return 403;
                        }
                    } else {
                        return 186;
                    }
                }
            } else {
                if (lng < -109.75) {
                    if (lat < 32.0) {
                        if (lng < -111.25) {
                            if (lng < -112.0) {
                                if (lat < 31.75) {
                                    return 403;
                                } else {
                                    if (lng < -112.25) {
                                        return 403;
                                    } else {
                                        return 9;
                                    }
                                }
                            } else {
                                if (lat < 31.5) {
                                    return 403;
                                } else {
                                    if (lng < -111.75) {
                                        if (lat < 31.75) {
                                            return 403;
                                        } else {
                                            return 9;
                                        }
                                    } else {
                                        if (lng < -111.5) {
                                            if (lat < 31.75) {
                                                return 403;
                                            } else {
                                                return 9;
                                            }
                                        } else {
                                            return 9;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -110.5) {
                                if (lat < 31.5) {
                                    return 403;
                                } else {
                                    return 9;
                                }
                            } else {
                                if (lat < 31.5) {
                                    return 403;
                                } else {
                                    return 9;
                                }
                            }
                        }
                    } else {
                        return 9;
                    }
                } else {
                    if (lat < 32.25) {
                        if (lng < -108.5) {
                            if (lat < 31.5) {
                                if (lng < -108.75) {
                                    return 403;
                                } else {
                                    if (lat < 31.0) {
                                        return 403;
                                    } else {
                                        if (lat < 31.25) {
                                            return 186;
                                        } else {
                                            return 134;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -109.0) {
                                    return 9;
                                } else {
                                    return 274;
                                }
                            }
                        } else {
                            if (lng < -107.75) {
                                if (lat < 31.5) {
                                    if (lng < -108.25) {
                                        if (lat < 31.25) {
                                            return 186;
                                        } else {
                                            return 134;
                                        }
                                    } else {
                                        if (lat < 31.25) {
                                            return 186;
                                        } else {
                                            return 134;
                                        }
                                    }
                                } else {
                                    if (lng < -108.0) {
                                        return 274;
                                    } else {
                                        if (lat < 32.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 31.75) {
                                    return 186;
                                } else {
                                    if (lng < -107.5) {
                                        if (lat < 32.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        if (lng < -107.25) {
                                            if (lat < 32.0) {
                                                return 134;
                                            } else {
                                                return 274;
                                            }
                                        } else {
                                            if (lat < 32.0) {
                                                return 134;
                                            } else {
                                                return 274;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -109.0) {
                            return 9;
                        } else {
                            return 274;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup5(double lat, double lng)
    {
        if (lng < -104.25) {
            if (lat < 30.75) {
                if (lng < -105.25) {
                    return 186;
                } else {
                    if (lat < 29.5) {
                        return 186;
                    } else {
                        if (lat < 30.0) {
                            if (lng < -104.75) {
                                return 186;
                            } else {
                                if (lng < -104.5) {
                                    if (lat < 29.75) {
                                        return 186;
                                    } else {
                                        return 134;
                                    }
                                } else {
                                    if (lat < 29.75) {
                                        return 134;
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        } else {
                            if (lng < -104.75) {
                                if (lat < 30.25) {
                                    return 186;
                                } else {
                                    if (lng < -105.0) {
                                        if (lat < 30.5) {
                                            return 186;
                                        } else {
                                            return 134;
                                        }
                                    } else {
                                        return 134;
                                    }
                                }
                            } else {
                                if (lat < 30.25) {
                                    if (lng < -104.5) {
                                        return 134;
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    if (lng < -104.5) {
                                        if (lat < 30.5) {
                                            return 134;
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 32.25) {
                    if (lng < -105.75) {
                        if (lat < 31.5) {
                            if (lng < -106.25) {
                                return 186;
                            } else {
                                if (lat < 31.25) {
                                    return 186;
                                } else {
                                    return 134;
                                }
                            }
                        } else {
                            if (lng < -106.5) {
                                if (lat < 31.75) {
                                    return 186;
                                } else {
                                    if (lng < -106.75) {
                                        if (lat < 32.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        if (lat < 32.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -106.25) {
                                    if (lat < 32.0) {
                                        return 134;
                                    } else {
                                        return 274;
                                    }
                                } else {
                                    if (lat < 31.75) {
                                        if (lng < -106.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        return 274;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -105.0) {
                            if (lat < 31.25) {
                                if (lng < -105.5) {
                                    if (lat < 31.0) {
                                        return 186;
                                    } else {
                                        return 134;
                                    }
                                } else {
                                    if (lng < -105.25) {
                                        if (lat < 31.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        if (lat < 31.0) {
                                            return 134;
                                        } else {
                                            return 274;
                                        }
                                    }
                                }
                            } else {
                                return 274;
                            }
                        } else {
                            if (lat < 31.5) {
                                if (lng < -104.75) {
                                    return 274;
                                } else {
                                    return 161;
                                }
                            } else {
                                if (lng < -104.75) {
                                    return 274;
                                } else {
                                    return 161;
                                }
                            }
                        }
                    }
                } else {
                    return 274;
                }
            }
        } else {
            if (lat < 30.75) {
                if (lng < -102.75) {
                    if (lat < 29.25) {
                        if (lng < -103.5) {
                            if (lat < 28.5) {
                                if (lng < -103.75) {
                                    return 186;
                                } else {
                                    if (lat < 28.25) {
                                        return 385;
                                    } else {
                                        return 186;
                                    }
                                }
                            } else {
                                if (lng < -103.75) {
                                    return 186;
                                } else {
                                    if (lat < 29.0) {
                                        return 186;
                                    } else {
                                        return 134;
                                    }
                                }
                            }
                        } else {
                            if (lat < 28.75) {
                                return 385;
                            } else {
                                if (lng < -103.25) {
                                    if (lat < 29.0) {
                                        return 186;
                                    } else {
                                        return 134;
                                    }
                                } else {
                                    if (lng < -103.0) {
                                        if (lat < 29.0) {
                                            return 375;
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        if (lat < 29.0) {
                                            return 385;
                                        } else {
                                            return 375;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -103.75) {
                            if (lat < 29.75) {
                                if (lng < -104.0) {
                                    return 134;
                                } else {
                                    if (lat < 29.5) {
                                        return 134;
                                    } else {
                                        return 161;
                                    }
                                }
                            } else {
                                return 161;
                            }
                        } else {
                            return 161;
                        }
                    }
                } else {
                    if (lat < 29.25) {
                        if (lng < -102.5) {
                            if (lat < 29.0) {
                                return 385;
                            } else {
                                return 375;
                            }
                        } else {
                            return 385;
                        }
                    } else {
                        if (lng < -102.0) {
                            if (lat < 30.0) {
                                if (lng < -102.5) {
                                    if (lat < 29.75) {
                                        return 375;
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    if (lat < 29.5) {
                                        return 385;
                                    } else {
                                        if (lng < -102.25) {
                                            return 375;
                                        } else {
                                            if (lat < 29.75) {
                                                return 385;
                                            } else {
                                                return 375;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 161;
                            }
                        } else {
                            if (lat < 30.0) {
                                if (lng < -101.75) {
                                    if (lat < 29.75) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                } else {
                                    if (lat < 29.5) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                }
                            } else {
                                return 161;
                            }
                        }
                    }
                }
            } else {
                if (lng < -103.0) {
                    if (lat < 32.25) {
                        return 161;
                    } else {
                        return 274;
                    }
                } else {
                    return 161;
                }
            }
        }
    }

    public static int kdLookup6(double lat, double lng)
    {
        if (lng < -107.0) {
            return kdLookup4(lat,lng);
        } else {
            if (lat < 28.0) {
                if (lng < -104.25) {
                    if (lat < 25.25) {
                        if (lng < -105.75) {
                            if (lat < 23.75) {
                                return 408;
                            } else {
                                if (lat < 24.5) {
                                    if (lng < -106.0) {
                                        return 408;
                                    } else {
                                        if (lat < 24.25) {
                                            return 408;
                                        } else {
                                            return 385;
                                        }
                                    }
                                } else {
                                    if (lng < -106.5) {
                                        if (lat < 24.75) {
                                            return 408;
                                        } else {
                                            if (lng < -106.75) {
                                                if (lat < 25.0) {
                                                    return 408;
                                                } else {
                                                    return 385;
                                                }
                                            } else {
                                                return 385;
                                            }
                                        }
                                    } else {
                                        return 385;
                                    }
                                }
                            }
                        } else {
                            if (lat < 23.75) {
                                if (lng < -105.0) {
                                    if (lat < 23.25) {
                                        return 408;
                                    } else {
                                        if (lng < -105.5) {
                                            return 408;
                                        } else {
                                            return 385;
                                        }
                                    }
                                } else {
                                    if (lat < 23.0) {
                                        if (lng < -104.75) {
                                            return 408;
                                        } else {
                                            if (lng < -104.5) {
                                                if (lat < 22.75) {
                                                    return 408;
                                                } else {
                                                    return 385;
                                                }
                                            } else {
                                                return 385;
                                            }
                                        }
                                    } else {
                                        return 385;
                                    }
                                }
                            } else {
                                return 385;
                            }
                        }
                    } else {
                        if (lng < -105.75) {
                            if (lat < 26.5) {
                                if (lng < -106.5) {
                                    if (lat < 25.75) {
                                        return 385;
                                    } else {
                                        return 186;
                                    }
                                } else {
                                    if (lat < 26.25) {
                                        return 385;
                                    } else {
                                        if (lng < -106.25) {
                                            return 186;
                                        } else {
                                            return 385;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 27.0) {
                                    if (lng < -106.0) {
                                        return 186;
                                    } else {
                                        return 385;
                                    }
                                } else {
                                    return 186;
                                }
                            }
                        } else {
                            if (lat < 26.5) {
                                return 385;
                            } else {
                                if (lng < -105.25) {
                                    if (lat < 26.75) {
                                        return 385;
                                    } else {
                                        return 186;
                                    }
                                } else {
                                    return 186;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 25.25) {
                        if (lng < -102.75) {
                            if (lat < 23.75) {
                                if (lng < -104.0) {
                                    if (lat < 22.75) {
                                        return 193;
                                    } else {
                                        return 385;
                                    }
                                } else {
                                    return 193;
                                }
                            } else {
                                if (lng < -103.5) {
                                    if (lat < 24.25) {
                                        if (lng < -103.75) {
                                            return 385;
                                        } else {
                                            return 193;
                                        }
                                    } else {
                                        return 385;
                                    }
                                } else {
                                    if (lat < 24.5) {
                                        return 193;
                                    } else {
                                        return 385;
                                    }
                                }
                            }
                        } else {
                            if (lat < 24.5) {
                                return 193;
                            } else {
                                if (lng < -102.0) {
                                    if (lng < -102.5) {
                                        return 385;
                                    } else {
                                        return 193;
                                    }
                                } else {
                                    if (lng < -101.75) {
                                        return 193;
                                    } else {
                                        if (lat < 25.0) {
                                            return 193;
                                        } else {
                                            return 385;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -103.5) {
                            if (lat < 26.75) {
                                return 385;
                            } else {
                                if (lat < 27.25) {
                                    return 186;
                                } else {
                                    if (lng < -103.75) {
                                        return 186;
                                    } else {
                                        return 385;
                                    }
                                }
                            }
                        } else {
                            return 385;
                        }
                    }
                }
            } else {
                return kdLookup5(lat,lng);
            }
        }
    }

    public static int kdLookup7(double lat, double lng)
    {
        if (lng < -107.0) {
            if (lat < 37.25) {
                if (lng < -109.75) {
                    if (lat < 35.5) {
                        if (lng < -111.0) {
                            return 9;
                        } else {
                            if (lat < 35.25) {
                                return 9;
                            } else {
                                return 274;
                            }
                        }
                    } else {
                        if (lng < -111.25) {
                            if (lat < 36.25) {
                                if (lng < -111.5) {
                                    return 9;
                                } else {
                                    if (lat < 36.0) {
                                        return 9;
                                    } else {
                                        return 274;
                                    }
                                }
                            } else {
                                if (lng < -111.75) {
                                    return 9;
                                } else {
                                    if (lat < 36.75) {
                                        return 274;
                                    } else {
                                        if (lng < -111.5) {
                                            return 9;
                                        } else {
                                            if (lat < 37.0) {
                                                return 274;
                                            } else {
                                                return 9;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 36.25) {
                                if (lng < -110.5) {
                                    if (lng < -111.0) {
                                        if (lat < 35.75) {
                                            return 9;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        if (lat < 35.75) {
                                            return 274;
                                        } else {
                                            return 9;
                                        }
                                    }
                                } else {
                                    if (lng < -110.25) {
                                        if (lat < 35.75) {
                                            return 274;
                                        } else {
                                            return 9;
                                        }
                                    } else {
                                        if (lat < 35.75) {
                                            return 274;
                                        } else {
                                            if (lng < -110.0) {
                                                if (lat < 36.0) {
                                                    return 9;
                                                } else {
                                                    return 274;
                                                }
                                            } else {
                                                return 274;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -110.5) {
                                    if (lat < 36.75) {
                                        if (lng < -111.0) {
                                            return 274;
                                        } else {
                                            if (lng < -110.75) {
                                                return 9;
                                            } else {
                                                if (lat < 36.5) {
                                                    return 9;
                                                } else {
                                                    return 274;
                                                }
                                            }
                                        }
                                    } else {
                                        return 274;
                                    }
                                } else {
                                    if (lat < 36.5) {
                                        if (lng < -110.25) {
                                            return 9;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        return 274;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 35.5) {
                        if (lng < -109.0) {
                            if (lat < 35.25) {
                                return 9;
                            } else {
                                if (lng < -109.5) {
                                    return 274;
                                } else {
                                    if (lng < -109.25) {
                                        return 9;
                                    } else {
                                        return 274;
                                    }
                                }
                            }
                        } else {
                            return 274;
                        }
                    } else {
                        return 274;
                    }
                }
            } else {
                if (lat < 42.0) {
                    return 274;
                } else {
                    if (lng < -111.0) {
                        if (lat < 43.5) {
                            if (lng < -111.75) {
                                if (lat < 42.25) {
                                    if (lng < -112.25) {
                                        return 274;
                                    } else {
                                        return 27;
                                    }
                                } else {
                                    return 27;
                                }
                            } else {
                                if (lat < 42.25) {
                                    return 274;
                                } else {
                                    return 27;
                                }
                            }
                        } else {
                            if (lng < -111.75) {
                                if (lat < 44.5) {
                                    return 27;
                                } else {
                                    if (lng < -112.25) {
                                        return 274;
                                    } else {
                                        if (lng < -112.0) {
                                            if (lat < 44.75) {
                                                return 27;
                                            } else {
                                                return 274;
                                            }
                                        } else {
                                            if (lat < 44.75) {
                                                return 27;
                                            } else {
                                                return 274;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 44.75) {
                                    return 27;
                                } else {
                                    return 274;
                                }
                            }
                        }
                    } else {
                        return 274;
                    }
                }
            }
        } else {
            if (lat < 38.75) {
                if (lng < -103.0) {
                    return 274;
                } else {
                    if (lat < 37.0) {
                        return 161;
                    } else {
                        if (lng < -102.0) {
                            return 274;
                        } else {
                            if (lat < 37.75) {
                                return 161;
                            } else {
                                if (lat < 38.25) {
                                    if (lng < -101.5) {
                                        return 274;
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    if (lng < -101.5) {
                                        return 274;
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 44.75) {
                    if (lat < 44.5) {
                        if (lng < -102.0) {
                            return 274;
                        } else {
                            if (lat < 44.25) {
                                if (lat < 44.0) {
                                    if (lat < 43.75) {
                                        if (lat < 43.5) {
                                            if (lat < 43.25) {
                                                if (lat < 43.0) {
                                                    if (lat < 42.75) {
                                                        if (lat < 42.5) {
                                                            if (lat < 42.25) {
                                                                if (lat < 42.0) {
                                                                    if (lat < 41.75) {
                                                                        if (lat < 41.5) {
                                                                            if (lat < 40.0) {
                                                                                if (lat < 39.75) {
                                                                                    return 274;
                                                                                } else {
                                                                                    return 161;
                                                                                }
                                                                            } else {
                                                                                if (lat < 40.25) {
                                                                                    return 161;
                                                                                } else {
                                                                                    return 274;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            return 274;
                                                                        }
                                                                    } else {
                                                                        return 274;
                                                                    }
                                                                } else {
                                                                    return 274;
                                                                }
                                                            } else {
                                                                return 274;
                                                            }
                                                        } else {
                                                            return 274;
                                                        }
                                                    } else {
                                                        return 274;
                                                    }
                                                } else {
                                                    return 274;
                                                }
                                            } else {
                                                return 274;
                                            }
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        return 274;
                                    }
                                } else {
                                    return 274;
                                }
                            } else {
                                return 274;
                            }
                        }
                    } else {
                        return 274;
                    }
                } else {
                    return 274;
                }
            }
        }
    }

    public static int kdLookup8(double lat, double lng)
    {
        if (lat < 25.25) {
            if (lng < -99.0) {
                if (lat < 23.75) {
                    if (lng < -100.25) {
                        return 193;
                    } else {
                        if (lng < -99.75) {
                            if (lat < 23.25) {
                                return 193;
                            } else {
                                return 385;
                            }
                        } else {
                            if (lat < 22.75) {
                                if (lng < -99.25) {
                                    return 193;
                                } else {
                                    return 385;
                                }
                            } else {
                                return 385;
                            }
                        }
                    }
                } else {
                    if (lng < -100.25) {
                        if (lat < 24.5) {
                            if (lng < -100.5) {
                                return 193;
                            } else {
                                if (lat < 24.0) {
                                    return 193;
                                } else {
                                    return 385;
                                }
                            }
                        } else {
                            if (lng < -100.75) {
                                if (lat < 24.75) {
                                    return 193;
                                } else {
                                    if (lng < -101.0) {
                                        if (lat < 25.0) {
                                            return 193;
                                        } else {
                                            return 385;
                                        }
                                    } else {
                                        return 385;
                                    }
                                }
                            } else {
                                return 385;
                            }
                        }
                    } else {
                        return 385;
                    }
                }
            } else {
                return 385;
            }
        } else {
            if (lng < -99.0) {
                if (lat < 26.5) {
                    if (lng < -99.25) {
                        return 385;
                    } else {
                        if (lat < 26.25) {
                            return 385;
                        } else {
                            return 375;
                        }
                    }
                } else {
                    if (lng < -100.0) {
                        return 385;
                    } else {
                        if (lat < 27.25) {
                            if (lng < -99.5) {
                                return 385;
                            } else {
                                if (lat < 26.75) {
                                    if (lng < -99.25) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                } else {
                                    if (lng < -99.25) {
                                        return 375;
                                    } else {
                                        if (lat < 27.0) {
                                            return 375;
                                        } else {
                                            return 161;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -99.5) {
                                if (lat < 27.5) {
                                    if (lng < -99.75) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                } else {
                                    if (lng < -99.75) {
                                        if (lat < 27.75) {
                                            return 385;
                                        } else {
                                            return 375;
                                        }
                                    } else {
                                        if (lat < 27.75) {
                                            return 375;
                                        } else {
                                            return 161;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 27.5) {
                                    if (lng < -99.25) {
                                        return 375;
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    return 161;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 26.5) {
                    if (lng < -98.0) {
                        if (lat < 26.0) {
                            return 385;
                        } else {
                            if (lng < -98.5) {
                                if (lng < -98.75) {
                                    if (lat < 26.25) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                } else {
                                    if (lat < 26.25) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                }
                            } else {
                                if (lng < -98.25) {
                                    if (lat < 26.25) {
                                        return 375;
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    if (lat < 26.25) {
                                        return 375;
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -97.5) {
                            if (lat < 25.75) {
                                return 385;
                            } else {
                                if (lat < 26.0) {
                                    if (lng < -97.75) {
                                        return 385;
                                    } else {
                                        return 375;
                                    }
                                } else {
                                    if (lng < -97.75) {
                                        if (lat < 26.25) {
                                            return 375;
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        if (lat < 26.25) {
                                            return 375;
                                        } else {
                                            return 161;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 25.75) {
                                return 385;
                            } else {
                                if (lng < -97.25) {
                                    if (lat < 26.0) {
                                        return 375;
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    if (lat < 26.0) {
                                        return 375;
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 161;
                }
            }
        }
    }

    public static int kdLookup9(double lat, double lng)
    {
        if (lng < -112.5) {
            if (lat < 22.5) {
                return 0;
            } else {
                if (lng < -123.75) {
                    return 39;
                } else {
                    if (lat < 33.75) {
                        if (lng < -118.25) {
                            return 39;
                        } else {
                            if (lat < 28.0) {
                                return 408;
                            } else {
                                return kdLookup2(lat,lng);
                            }
                        }
                    } else {
                        if (lng < -118.0) {
                            return 39;
                        } else {
                            if (lat < 39.25) {
                                if (lng < -114.5) {
                                    return 39;
                                } else {
                                    if (lat < 36.5) {
                                        if (lat < 36.25) {
                                            if (lat < 36.0) {
                                                if (lat < 35.75) {
                                                    if (lng < -114.0) {
                                                        if (lat < 35.5) {
                                                            if (lat < 34.5) {
                                                                if (lat < 34.0) {
                                                                    return 9;
                                                                } else {
                                                                    if (lng < -114.25) {
                                                                        return 39;
                                                                    } else {
                                                                        if (lat < 34.25) {
                                                                            return 9;
                                                                        } else {
                                                                            return 39;
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                if (lat < 34.75) {
                                                                    if (lng < -114.25) {
                                                                        return 39;
                                                                    } else {
                                                                        return 9;
                                                                    }
                                                                } else {
                                                                    return 9;
                                                                }
                                                            }
                                                        } else {
                                                            return 9;
                                                        }
                                                    } else {
                                                        return 9;
                                                    }
                                                } else {
                                                    return 9;
                                                }
                                            } else {
                                                return 9;
                                            }
                                        } else {
                                            if (lng < -114.0) {
                                                return 39;
                                            } else {
                                                return 9;
                                            }
                                        }
                                    } else {
                                        if (lat < 37.75) {
                                            if (lng < -113.5) {
                                                if (lat < 37.0) {
                                                    if (lng < -114.0) {
                                                        return 39;
                                                    } else {
                                                        return 9;
                                                    }
                                                } else {
                                                    if (lng < -114.0) {
                                                        return 39;
                                                    } else {
                                                        if (lat < 37.25) {
                                                            return 9;
                                                        } else {
                                                            return 274;
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (lat < 37.25) {
                                                    return 9;
                                                } else {
                                                    return 274;
                                                }
                                            }
                                        } else {
                                            if (lng < -114.0) {
                                                return 39;
                                            } else {
                                                return 274;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 42.0) {
                                    if (lng < -114.0) {
                                        return 39;
                                    } else {
                                        return 274;
                                    }
                                } else {
                                    if (lng < -115.25) {
                                        if (lat < 43.5) {
                                            if (lng < -116.75) {
                                                if (lat < 42.5) {
                                                    if (lng < -117.0) {
                                                        return 39;
                                                    } else {
                                                        if (lat < 42.25) {
                                                            return 39;
                                                        } else {
                                                            return 27;
                                                        }
                                                    }
                                                } else {
                                                    return 27;
                                                }
                                            } else {
                                                return 27;
                                            }
                                        } else {
                                            if (lng < -117.0) {
                                                if (lat < 44.5) {
                                                    return 27;
                                                } else {
                                                    return 39;
                                                }
                                            } else {
                                                return 27;
                                            }
                                        }
                                    } else {
                                        if (lat < 44.5) {
                                            return 27;
                                        } else {
                                            if (lng < -113.0) {
                                                return 27;
                                            } else {
                                                return 274;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 22.5) {
                return kdLookup3(lat,lng);
            } else {
                if (lng < -101.25) {
                    if (lat < 33.75) {
                        return kdLookup6(lat,lng);
                    } else {
                        return kdLookup7(lat,lng);
                    }
                } else {
                    if (lat < 33.75) {
                        if (lng < -96.75) {
                            if (lat < 28.0) {
                                return kdLookup8(lat,lng);
                            } else {
                                if (lat < 29.75) {
                                    if (lng < -99.75) {
                                        if (lat < 28.75) {
                                            if (lng < -100.5) {
                                                return 385;
                                            } else {
                                                if (lng < -100.25) {
                                                    if (lat < 28.25) {
                                                        return 385;
                                                    } else {
                                                        return 375;
                                                    }
                                                } else {
                                                    if (lat < 28.25) {
                                                        return 375;
                                                    } else {
                                                        return 161;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lng < -100.5) {
                                                if (lat < 29.25) {
                                                    if (lng < -100.75) {
                                                        return 385;
                                                    } else {
                                                        return 375;
                                                    }
                                                } else {
                                                    if (lng < -101.0) {
                                                        return 375;
                                                    } else {
                                                        if (lng < -100.75) {
                                                            if (lat < 29.5) {
                                                                return 375;
                                                            } else {
                                                                return 161;
                                                            }
                                                        } else {
                                                            return 161;
                                                        }
                                                    }
                                                }
                                            } else {
                                                return 161;
                                            }
                                        }
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    return 161;
                                }
                            }
                        } else {
                            return 161;
                        }
                    } else {
                        if (lng < -100.25) {
                            if (lat < 40.75) {
                                return 161;
                            } else {
                                if (lat < 42.75) {
                                    if (lat < 41.75) {
                                        if (lng < -101.0) {
                                            if (lat < 41.25) {
                                                return 274;
                                            } else {
                                                return 161;
                                            }
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        if (lng < -100.75) {
                                            return 274;
                                        } else {
                                            return 161;
                                        }
                                    }
                                } else {
                                    if (lat < 43.75) {
                                        if (lng < -100.75) {
                                            if (lat < 43.25) {
                                                if (lng < -101.0) {
                                                    return 274;
                                                } else {
                                                    if (lat < 43.0) {
                                                        return 274;
                                                    } else {
                                                        return 161;
                                                    }
                                                }
                                            } else {
                                                if (lng < -101.0) {
                                                    return 274;
                                                } else {
                                                    return 161;
                                                }
                                            }
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        if (lat < 44.25) {
                                            if (lng < -101.0) {
                                                return 274;
                                            } else {
                                                return 161;
                                            }
                                        } else {
                                            if (lng < -100.5) {
                                                return 274;
                                            } else {
                                                if (lat < 44.5) {
                                                    return 274;
                                                } else {
                                                    return 161;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            return 161;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup10(double lat, double lng)
    {
        if (lat < 61.75) {
            if (lng < -132.25) {
                if (lat < 59.0) {
                    if (lng < -133.75) {
                        if (lat < 57.5) {
                            if (lng < -134.5) {
                                return 23;
                            } else {
                                if (lat < 57.25) {
                                    return 23;
                                } else {
                                    return 360;
                                }
                            }
                        } else {
                            if (lat < 57.75) {
                                if (lng < -134.5) {
                                    return 23;
                                } else {
                                    return 360;
                                }
                            } else {
                                return 360;
                            }
                        }
                    } else {
                        if (lat < 57.5) {
                            if (lng < -133.0) {
                                if (lat < 57.25) {
                                    return 23;
                                } else {
                                    return 360;
                                }
                            } else {
                                return 23;
                            }
                        } else {
                            if (lng < -133.0) {
                                if (lat < 58.25) {
                                    return 360;
                                } else {
                                    if (lng < -133.5) {
                                        if (lat < 58.75) {
                                            return 360;
                                        } else {
                                            return 45;
                                        }
                                    } else {
                                        if (lat < 58.5) {
                                            if (lng < -133.25) {
                                                return 360;
                                            } else {
                                                return 45;
                                            }
                                        } else {
                                            return 45;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 58.0) {
                                    if (lng < -132.75) {
                                        return 360;
                                    } else {
                                        if (lng < -132.5) {
                                            if (lat < 57.75) {
                                                return 360;
                                            } else {
                                                return 45;
                                            }
                                        } else {
                                            return 45;
                                        }
                                    }
                                } else {
                                    return 45;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -133.75) {
                        if (lat < 60.0) {
                            if (lng < -134.5) {
                                if (lat < 59.5) {
                                    return 360;
                                } else {
                                    return 45;
                                }
                            } else {
                                if (lat < 59.25) {
                                    if (lng < -134.25) {
                                        return 360;
                                    } else {
                                        return 45;
                                    }
                                } else {
                                    return 45;
                                }
                            }
                        } else {
                            return 325;
                        }
                    } else {
                        if (lat < 60.0) {
                            return 45;
                        } else {
                            return 325;
                        }
                    }
                }
            } else {
                if (lat < 59.0) {
                    if (lng < -131.0) {
                        if (lat < 57.25) {
                            if (lng < -131.75) {
                                if (lat < 57.0) {
                                    return 23;
                                } else {
                                    if (lng < -132.0) {
                                        return 23;
                                    } else {
                                        return 45;
                                    }
                                }
                            } else {
                                if (lat < 56.75) {
                                    if (lng < -131.25) {
                                        return 23;
                                    } else {
                                        if (lat < 56.5) {
                                            return 23;
                                        } else {
                                            return 45;
                                        }
                                    }
                                } else {
                                    return 45;
                                }
                            }
                        } else {
                            return 45;
                        }
                    } else {
                        if (lat < 56.5) {
                            if (lng < -130.5) {
                                return 23;
                            } else {
                                return 45;
                            }
                        } else {
                            return 45;
                        }
                    }
                } else {
                    if (lng < -131.0) {
                        if (lat < 60.0) {
                            return 45;
                        } else {
                            return 325;
                        }
                    } else {
                        if (lat < 60.0) {
                            return 45;
                        } else {
                            return 325;
                        }
                    }
                }
            }
        } else {
            if (lat < 64.5) {
                if (lng < -130.75) {
                    return 325;
                } else {
                    if (lat < 63.25) {
                        return 325;
                    } else {
                        if (lng < -130.25) {
                            if (lat < 64.0) {
                                return 325;
                            } else {
                                return 143;
                            }
                        } else {
                            if (lat < 63.75) {
                                if (lng < -130.0) {
                                    return 325;
                                } else {
                                    if (lng < -129.75) {
                                        if (lat < 63.5) {
                                            return 143;
                                        } else {
                                            return 325;
                                        }
                                    } else {
                                        return 143;
                                    }
                                }
                            } else {
                                if (lng < -130.0) {
                                    if (lat < 64.0) {
                                        return 325;
                                    } else {
                                        return 143;
                                    }
                                } else {
                                    return 143;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -132.25) {
                    if (lat < 66.0) {
                        if (lng < -132.5) {
                            return 325;
                        } else {
                            if (lat < 65.25) {
                                return 325;
                            } else {
                                if (lat < 65.5) {
                                    return 143;
                                } else {
                                    return 325;
                                }
                            }
                        }
                    } else {
                        if (lng < -133.75) {
                            if (lat < 67.25) {
                                return 325;
                            } else {
                                return 143;
                            }
                        } else {
                            if (lng < -133.0) {
                                if (lat < 66.75) {
                                    if (lng < -133.5) {
                                        return 325;
                                    } else {
                                        if (lat < 66.25) {
                                            if (lng < -133.25) {
                                                return 143;
                                            } else {
                                                return 325;
                                            }
                                        } else {
                                            return 143;
                                        }
                                    }
                                } else {
                                    return 143;
                                }
                            } else {
                                if (lat < 66.25) {
                                    if (lng < -132.75) {
                                        return 325;
                                    } else {
                                        return 143;
                                    }
                                } else {
                                    return 143;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 65.0) {
                        if (lng < -131.5) {
                            if (lng < -132.0) {
                                return 325;
                            } else {
                                if (lng < -131.75) {
                                    if (lat < 64.75) {
                                        return 325;
                                    } else {
                                        return 143;
                                    }
                                } else {
                                    if (lat < 64.75) {
                                        return 325;
                                    } else {
                                        return 143;
                                    }
                                }
                            }
                        } else {
                            return 143;
                        }
                    } else {
                        return 143;
                    }
                }
            }
        }
    }

    public static int kdLookup11(double lat, double lng)
    {
        if (lat < 56.25) {
            if (lng < -129.5) {
                if (lat < 50.5) {
                    return 0;
                } else {
                    if (lat < 53.25) {
                        return 45;
                    } else {
                        if (lng < -132.25) {
                            if (lat < 54.75) {
                                if (lng < -133.75) {
                                    return 0;
                                } else {
                                    if (lng < -133.0) {
                                        return 45;
                                    } else {
                                        if (lat < 54.5) {
                                            return 45;
                                        } else {
                                            if (lng < -132.75) {
                                                return 45;
                                            } else {
                                                return 23;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 23;
                            }
                        } else {
                            if (lat < 54.75) {
                                if (lng < -131.0) {
                                    if (lat < 54.0) {
                                        return 45;
                                    } else {
                                        if (lng < -131.75) {
                                            if (lat < 54.5) {
                                                return 45;
                                            } else {
                                                return 23;
                                            }
                                        } else {
                                            return 45;
                                        }
                                    }
                                } else {
                                    return 45;
                                }
                            } else {
                                if (lng < -130.5) {
                                    if (lng < -131.5) {
                                        return 23;
                                    } else {
                                        if (lat < 55.5) {
                                            if (lng < -131.25) {
                                                return 196;
                                            } else {
                                                return 23;
                                            }
                                        } else {
                                            return 23;
                                        }
                                    }
                                } else {
                                    if (lat < 55.5) {
                                        if (lng < -130.0) {
                                            if (lat < 55.0) {
                                                return 45;
                                            } else {
                                                return 23;
                                            }
                                        } else {
                                            return 45;
                                        }
                                    } else {
                                        if (lng < -130.0) {
                                            return 23;
                                        } else {
                                            return 45;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 48.75) {
                    if (lng < -126.75) {
                        return 0;
                    } else {
                        if (lat < 48.25) {
                            return 39;
                        } else {
                            if (lng < -125.25) {
                                return 0;
                            } else {
                                if (lng < -124.5) {
                                    if (lng < -125.0) {
                                        return 0;
                                    } else {
                                        if (lng < -124.75) {
                                            return 45;
                                        } else {
                                            return 39;
                                        }
                                    }
                                } else {
                                    if (lng < -124.25) {
                                        return 39;
                                    } else {
                                        if (lng < -124.0) {
                                            if (lat < 48.5) {
                                                return 39;
                                            } else {
                                                return 45;
                                            }
                                        } else {
                                            return 45;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 45;
                }
            }
        } else {
            if (lng < -129.5) {
                return kdLookup10(lat,lng);
            } else {
                if (lat < 61.75) {
                    if (lng < -126.75) {
                        if (lat < 60.0) {
                            return 45;
                        } else {
                            if (lng < -127.25) {
                                return 325;
                            } else {
                                if (lat < 61.25) {
                                    return 325;
                                } else {
                                    if (lng < -127.0) {
                                        if (lat < 61.5) {
                                            return 325;
                                        } else {
                                            return 143;
                                        }
                                    } else {
                                        return 143;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 60.0) {
                            return 45;
                        } else {
                            if (lng < -125.25) {
                                if (lat < 61.0) {
                                    return 325;
                                } else {
                                    return 143;
                                }
                            } else {
                                if (lat < 60.75) {
                                    if (lng < -124.25) {
                                        return 325;
                                    } else {
                                        if (lat < 60.25) {
                                            if (lng < -124.0) {
                                                return 325;
                                            } else {
                                                return 143;
                                            }
                                        } else {
                                            if (lng < -124.0) {
                                                if (lat < 60.5) {
                                                    return 325;
                                                } else {
                                                    return 143;
                                                }
                                            } else {
                                                return 143;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -124.5) {
                                        if (lat < 61.0) {
                                            return 325;
                                        } else {
                                            return 143;
                                        }
                                    } else {
                                        return 143;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -128.0) {
                        if (lat < 62.75) {
                            if (lng < -128.75) {
                                if (lat < 62.25) {
                                    return 325;
                                } else {
                                    if (lng < -129.25) {
                                        return 325;
                                    } else {
                                        if (lng < -129.0) {
                                            if (lat < 62.5) {
                                                return 143;
                                            } else {
                                                return 325;
                                            }
                                        } else {
                                            return 143;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 62.25) {
                                    if (lng < -128.25) {
                                        return 325;
                                    } else {
                                        if (lat < 62.0) {
                                            return 325;
                                        } else {
                                            return 143;
                                        }
                                    }
                                } else {
                                    return 143;
                                }
                            }
                        } else {
                            return 143;
                        }
                    } else {
                        return 143;
                    }
                }
            }
        }
    }

    public static int kdLookup12(double lat, double lng)
    {
        if (lat < 50.5) {
            if (lng < -121.0) {
                if (lat < 48.5) {
                    return 39;
                } else {
                    if (lng < -122.5) {
                        if (lat < 49.25) {
                            if (lng < -123.0) {
                                return 45;
                            } else {
                                if (lat < 48.75) {
                                    return 39;
                                } else {
                                    if (lng < -122.75) {
                                        return 45;
                                    } else {
                                        return 39;
                                    }
                                }
                            }
                        } else {
                            return 45;
                        }
                    } else {
                        if (lat < 49.25) {
                            return 39;
                        } else {
                            return 45;
                        }
                    }
                }
            } else {
                if (lat < 49.0) {
                    return 39;
                } else {
                    if (lng < -119.75) {
                        if (lat < 49.25) {
                            if (lng < -120.75) {
                                return 45;
                            } else {
                                return 39;
                            }
                        } else {
                            return 45;
                        }
                    } else {
                        if (lng < -119.0) {
                            if (lat < 49.25) {
                                return 39;
                            } else {
                                return 45;
                            }
                        } else {
                            if (lat < 49.25) {
                                return 39;
                            } else {
                                return 45;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 53.25) {
                if (lng < -118.5) {
                    return 45;
                } else {
                    if (lat < 52.25) {
                        return 45;
                    } else {
                        if (lat < 52.75) {
                            if (lat < 52.5) {
                                return 228;
                            } else {
                                return 45;
                            }
                        } else {
                            if (lat < 53.0) {
                                return 45;
                            } else {
                                return 228;
                            }
                        }
                    }
                }
            } else {
                if (lng < -121.0) {
                    if (lat < 54.75) {
                        return 45;
                    } else {
                        if (lng < -122.5) {
                            if (lat < 55.5) {
                                return 45;
                            } else {
                                if (lng < -123.25) {
                                    return 45;
                                } else {
                                    if (lng < -123.0) {
                                        if (lat < 55.75) {
                                            return 45;
                                        } else {
                                            return 116;
                                        }
                                    } else {
                                        if (lat < 55.75) {
                                            if (lng < -122.75) {
                                                return 45;
                                            } else {
                                                return 116;
                                            }
                                        } else {
                                            return 116;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -121.75) {
                                if (lat < 55.5) {
                                    if (lng < -122.25) {
                                        return 45;
                                    } else {
                                        if (lat < 55.25) {
                                            return 45;
                                        } else {
                                            return 116;
                                        }
                                    }
                                } else {
                                    return 116;
                                }
                            } else {
                                if (lat < 55.0) {
                                    if (lng < -121.25) {
                                        return 45;
                                    } else {
                                        return 116;
                                    }
                                } else {
                                    return 116;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 54.75) {
                        if (lng < -119.75) {
                            if (lat < 54.0) {
                                return 45;
                            } else {
                                if (lng < -120.5) {
                                    if (lat < 54.5) {
                                        return 45;
                                    } else {
                                        return 116;
                                    }
                                } else {
                                    if (lng < -120.25) {
                                        if (lat < 54.25) {
                                            return 45;
                                        } else {
                                            return 116;
                                        }
                                    } else {
                                        if (lat < 54.25) {
                                            if (lng < -120.0) {
                                                return 116;
                                            } else {
                                                return 228;
                                            }
                                        } else {
                                            if (lng < -120.0) {
                                                return 116;
                                            } else {
                                                return 228;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -119.25) {
                                if (lat < 53.5) {
                                    return 45;
                                } else {
                                    return 228;
                                }
                            } else {
                                return 228;
                            }
                        }
                    } else {
                        if (lng < -120.0) {
                            return 116;
                        } else {
                            return 228;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup13(double lat, double lng)
    {
        if (lng < -115.5) {
            if (lat < 47.75) {
                if (lng < -116.75) {
                    return 39;
                } else {
                    if (lat < 46.0) {
                        if (lng < -116.25) {
                            if (lat < 45.5) {
                                if (lng < -116.5) {
                                    if (lat < 45.25) {
                                        return 27;
                                    } else {
                                        return 39;
                                    }
                                } else {
                                    return 27;
                                }
                            } else {
                                if (lng < -116.5) {
                                    return 39;
                                } else {
                                    return 27;
                                }
                            }
                        } else {
                            if (lat < 45.5) {
                                return 27;
                            } else {
                                return 39;
                            }
                        }
                    } else {
                        return 39;
                    }
                }
            } else {
                if (lng < -117.0) {
                    if (lat < 49.25) {
                        return 39;
                    } else {
                        return 45;
                    }
                } else {
                    if (lat < 49.0) {
                        if (lng < -116.0) {
                            return 39;
                        } else {
                            if (lat < 48.0) {
                                if (lng < -115.75) {
                                    return 39;
                                } else {
                                    return 274;
                                }
                            } else {
                                return 274;
                            }
                        }
                    } else {
                        if (lng < -116.25) {
                            if (lat < 49.75) {
                                if (lng < -116.75) {
                                    return 45;
                                } else {
                                    if (lat < 49.25) {
                                        return 39;
                                    } else {
                                        if (lng < -116.5) {
                                            if (lat < 49.5) {
                                                return 45;
                                            } else {
                                                return 254;
                                            }
                                        } else {
                                            if (lat < 49.5) {
                                                return 254;
                                            } else {
                                                return 228;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -116.75) {
                                    return 45;
                                } else {
                                    if (lat < 50.0) {
                                        if (lng < -116.5) {
                                            return 254;
                                        } else {
                                            return 228;
                                        }
                                    } else {
                                        if (lng < -116.5) {
                                            return 45;
                                        } else {
                                            if (lat < 50.25) {
                                                return 45;
                                            } else {
                                                return 228;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 49.5) {
                                if (lng < -116.0) {
                                    if (lat < 49.25) {
                                        return 39;
                                    } else {
                                        return 254;
                                    }
                                } else {
                                    if (lng < -115.75) {
                                        if (lat < 49.25) {
                                            return 274;
                                        } else {
                                            return 228;
                                        }
                                    } else {
                                        if (lat < 49.25) {
                                            return 274;
                                        } else {
                                            return 228;
                                        }
                                    }
                                }
                            } else {
                                return 228;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 47.75) {
                if (lng < -114.0) {
                    if (lat < 46.25) {
                        if (lng < -114.75) {
                            if (lat < 45.5) {
                                return 27;
                            } else {
                                if (lng < -115.25) {
                                    return 39;
                                } else {
                                    if (lat < 45.75) {
                                        if (lng < -115.0) {
                                            return 27;
                                        } else {
                                            return 39;
                                        }
                                    } else {
                                        return 39;
                                    }
                                }
                            }
                        } else {
                            if (lat < 45.5) {
                                return 27;
                            } else {
                                if (lng < -114.5) {
                                    return 39;
                                } else {
                                    if (lat < 45.75) {
                                        return 27;
                                    } else {
                                        if (lng < -114.25) {
                                            if (lat < 46.0) {
                                                return 274;
                                            } else {
                                                return 39;
                                            }
                                        } else {
                                            return 274;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -114.75) {
                            if (lat < 47.0) {
                                return 39;
                            } else {
                                if (lng < -115.25) {
                                    if (lat < 47.5) {
                                        return 39;
                                    } else {
                                        return 274;
                                    }
                                } else {
                                    if (lat < 47.25) {
                                        if (lng < -115.0) {
                                            return 39;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        return 274;
                                    }
                                }
                            }
                        } else {
                            if (lat < 46.75) {
                                if (lng < -114.25) {
                                    return 39;
                                } else {
                                    return 274;
                                }
                            } else {
                                return 274;
                            }
                        }
                    }
                } else {
                    if (lat < 45.75) {
                        if (lng < -113.25) {
                            if (lng < -113.75) {
                                return 27;
                            } else {
                                if (lat < 45.25) {
                                    return 27;
                                } else {
                                    if (lng < -113.5) {
                                        if (lat < 45.5) {
                                            return 27;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        return 274;
                                    }
                                }
                            }
                        } else {
                            return 274;
                        }
                    } else {
                        return 274;
                    }
                }
            } else {
                if (lng < -114.0) {
                    if (lat < 49.0) {
                        return 274;
                    } else {
                        if (lng < -115.25) {
                            if (lat < 49.25) {
                                return 274;
                            } else {
                                return 228;
                            }
                        } else {
                            return 228;
                        }
                    }
                } else {
                    if (lat < 49.0) {
                        return 274;
                    } else {
                        return 228;
                    }
                }
            }
        }
    }

    public static int kdLookup14(double lat, double lng)
    {
        if (lat < 56.25) {
            if (lng < -118.25) {
                return kdLookup12(lat,lng);
            } else {
                if (lat < 50.5) {
                    return kdLookup13(lat,lng);
                } else {
                    if (lng < -116.5) {
                        if (lat < 52.0) {
                            if (lng < -117.5) {
                                if (lat < 51.5) {
                                    return 45;
                                } else {
                                    if (lng < -118.0) {
                                        return 45;
                                    } else {
                                        if (lng < -117.75) {
                                            if (lat < 51.75) {
                                                return 45;
                                            } else {
                                                return 228;
                                            }
                                        } else {
                                            return 228;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 51.25) {
                                    if (lng < -117.0) {
                                        return 45;
                                    } else {
                                        if (lat < 50.75) {
                                            return 45;
                                        } else {
                                            if (lng < -116.75) {
                                                if (lat < 51.0) {
                                                    return 45;
                                                } else {
                                                    return 228;
                                                }
                                            } else {
                                                return 228;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -117.25) {
                                        if (lat < 51.5) {
                                            return 45;
                                        } else {
                                            return 228;
                                        }
                                    } else {
                                        return 228;
                                    }
                                }
                            }
                        } else {
                            return 228;
                        }
                    } else {
                        return 228;
                    }
                }
            }
        } else {
            if (lng < -118.25) {
                if (lat < 60.0) {
                    if (lng < -121.0) {
                        if (lat < 57.25) {
                            if (lng < -123.5) {
                                if (lat < 56.75) {
                                    return 45;
                                } else {
                                    if (lat < 57.0) {
                                        return 116;
                                    } else {
                                        return 45;
                                    }
                                }
                            } else {
                                if (lng < -122.25) {
                                    return 116;
                                } else {
                                    if (lng < -121.75) {
                                        if (lat < 57.0) {
                                            return 116;
                                        } else {
                                            return 45;
                                        }
                                    } else {
                                        return 116;
                                    }
                                }
                            }
                        } else {
                            return 45;
                        }
                    } else {
                        if (lat < 58.0) {
                            if (lng < -120.0) {
                                if (lat < 57.25) {
                                    return 116;
                                } else {
                                    if (lng < -120.5) {
                                        if (lat < 57.5) {
                                            return 116;
                                        } else {
                                            return 45;
                                        }
                                    } else {
                                        if (lat < 57.5) {
                                            if (lng < -120.25) {
                                                return 116;
                                            } else {
                                                return 45;
                                            }
                                        } else {
                                            return 45;
                                        }
                                    }
                                }
                            } else {
                                return 228;
                            }
                        } else {
                            if (lng < -120.0) {
                                return 45;
                            } else {
                                return 228;
                            }
                        }
                    }
                } else {
                    return 143;
                }
            } else {
                if (lat < 61.75) {
                    if (lng < -115.5) {
                        if (lat < 60.0) {
                            return 228;
                        } else {
                            return 143;
                        }
                    } else {
                        if (lat < 60.0) {
                            return 228;
                        } else {
                            return 143;
                        }
                    }
                } else {
                    if (lng < -115.5) {
                        if (lat < 66.5) {
                            return 143;
                        } else {
                            if (lng < -117.0) {
                                if (lng < -117.75) {
                                    if (lat < 67.25) {
                                        return 143;
                                    } else {
                                        return 108;
                                    }
                                } else {
                                    if (lat < 67.0) {
                                        return 143;
                                    } else {
                                        if (lng < -117.5) {
                                            if (lat < 67.25) {
                                                return 143;
                                            } else {
                                                return 108;
                                            }
                                        } else {
                                            return 108;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -116.25) {
                                    if (lat < 66.75) {
                                        return 143;
                                    } else {
                                        return 108;
                                    }
                                } else {
                                    return 108;
                                }
                            }
                        }
                    } else {
                        if (lat < 65.5) {
                            return 143;
                        } else {
                            if (lng < -114.0) {
                                if (lat < 66.25) {
                                    if (lng < -114.75) {
                                        return 143;
                                    } else {
                                        if (lng < -114.5) {
                                            if (lat < 66.0) {
                                                return 143;
                                            } else {
                                                return 108;
                                            }
                                        } else {
                                            if (lat < 66.0) {
                                                return 143;
                                            } else {
                                                return 108;
                                            }
                                        }
                                    }
                                } else {
                                    return 108;
                                }
                            } else {
                                if (lat < 65.75) {
                                    if (lng < -113.5) {
                                        return 143;
                                    } else {
                                        return 108;
                                    }
                                } else {
                                    return 108;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup15(double lat, double lng)
    {
        if (lat < 67.5) {
            if (lng < -123.75) {
                return kdLookup11(lat,lng);
            } else {
                return kdLookup14(lat,lng);
            }
        } else {
            if (lng < -123.75) {
                return 143;
            } else {
                if (lat < 78.75) {
                    if (lng < -118.25) {
                        if (lat < 73.0) {
                            if (lng < -121.0) {
                                if (lat < 70.25) {
                                    if (lng < -122.5) {
                                        return 143;
                                    } else {
                                        if (lat < 68.75) {
                                            if (lng < -121.75) {
                                                return 143;
                                            } else {
                                                if (lat < 68.5) {
                                                    return 143;
                                                } else {
                                                    return 108;
                                                }
                                            }
                                        } else {
                                            if (lng < -121.75) {
                                                return 143;
                                            } else {
                                                if (lat < 69.75) {
                                                    return 108;
                                                } else {
                                                    return 143;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 143;
                                }
                            } else {
                                if (lat < 70.25) {
                                    if (lng < -119.75) {
                                        if (lat < 68.75) {
                                            if (lng < -120.5) {
                                                if (lat < 68.25) {
                                                    return 143;
                                                } else {
                                                    return 108;
                                                }
                                            } else {
                                                if (lat < 68.0) {
                                                    return 143;
                                                } else {
                                                    return 108;
                                                }
                                            }
                                        } else {
                                            return 108;
                                        }
                                    } else {
                                        if (lat < 68.75) {
                                            if (lng < -119.0) {
                                                if (lat < 67.75) {
                                                    return 143;
                                                } else {
                                                    return 108;
                                                }
                                            } else {
                                                return 108;
                                            }
                                        } else {
                                            return 108;
                                        }
                                    }
                                } else {
                                    return 143;
                                }
                            }
                        } else {
                            return 143;
                        }
                    } else {
                        if (lat < 70.5) {
                            if (lng < -115.5) {
                                if (lat < 69.25) {
                                    return 108;
                                } else {
                                    if (lng < -117.0) {
                                        if (lng < -117.75) {
                                            return 108;
                                        } else {
                                            return 143;
                                        }
                                    } else {
                                        if (lng < -116.25) {
                                            if (lat < 69.75) {
                                                if (lng < -116.75) {
                                                    return 143;
                                                } else {
                                                    return 108;
                                                }
                                            } else {
                                                if (lng < -116.75) {
                                                    if (lat < 70.0) {
                                                        return 143;
                                                    } else {
                                                        return 108;
                                                    }
                                                } else {
                                                    return 108;
                                                }
                                            }
                                        } else {
                                            return 108;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -114.0) {
                                    if (lat < 69.0) {
                                        return 108;
                                    } else {
                                        if (lng < -114.75) {
                                            if (lat < 70.25) {
                                                return 108;
                                            } else {
                                                return 143;
                                            }
                                        } else {
                                            if (lat < 69.75) {
                                                return 108;
                                            } else {
                                                if (lng < -114.5) {
                                                    if (lat < 70.25) {
                                                        return 108;
                                                    } else {
                                                        return 143;
                                                    }
                                                } else {
                                                    if (lat < 70.25) {
                                                        return 108;
                                                    } else {
                                                        return 143;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 70.0) {
                                        return 108;
                                    } else {
                                        if (lng < -113.25) {
                                            if (lng < -113.75) {
                                                if (lat < 70.25) {
                                                    return 108;
                                                } else {
                                                    return 143;
                                                }
                                            } else {
                                                if (lng < -113.5) {
                                                    if (lat < 70.25) {
                                                        return 108;
                                                    } else {
                                                        return 143;
                                                    }
                                                } else {
                                                    if (lat < 70.25) {
                                                        return 108;
                                                    } else {
                                                        return 143;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lng < -113.0) {
                                                if (lat < 70.25) {
                                                    return 108;
                                                } else {
                                                    return 143;
                                                }
                                            } else {
                                                if (lng < -112.75) {
                                                    return 108;
                                                } else {
                                                    return 143;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            return 143;
                        }
                    }
                } else {
                    return 0;
                }
            }
        }
    }

    public static int kdLookup16(double lat, double lng)
    {
        if (lat < 50.5) {
            if (lng < -104.25) {
                if (lat < 49.0) {
                    return 274;
                } else {
                    return 309;
                }
            } else {
                if (lat < 47.75) {
                    if (lng < -102.0) {
                        return 274;
                    } else {
                        if (lat < 46.75) {
                            return 274;
                        } else {
                            if (lat < 47.25) {
                                if (lng < -101.75) {
                                    if (lat < 47.0) {
                                        return 52;
                                    } else {
                                        return 71;
                                    }
                                } else {
                                    if (lng < -101.5) {
                                        if (lat < 47.0) {
                                            return 52;
                                        } else {
                                            return 105;
                                        }
                                    } else {
                                        if (lat < 47.0) {
                                            return 52;
                                        } else {
                                            return 105;
                                        }
                                    }
                                }
                            } else {
                                return 71;
                            }
                        }
                    }
                } else {
                    if (lng < -102.75) {
                        if (lat < 49.0) {
                            if (lng < -103.75) {
                                if (lat < 48.25) {
                                    if (lng < -104.0) {
                                        return 274;
                                    } else {
                                        if (lat < 48.0) {
                                            return 274;
                                        } else {
                                            return 161;
                                        }
                                    }
                                } else {
                                    if (lat < 48.5) {
                                        if (lng < -104.0) {
                                            return 274;
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        if (lng < -104.0) {
                                            return 274;
                                        } else {
                                            return 161;
                                        }
                                    }
                                }
                            } else {
                                return 161;
                            }
                        } else {
                            return 309;
                        }
                    } else {
                        if (lat < 49.0) {
                            return 161;
                        } else {
                            return 309;
                        }
                    }
                }
            }
        } else {
            if (lng < -102.5) {
                return 309;
            } else {
                if (lat < 53.25) {
                    if (lat < 51.75) {
                        if (lng < -101.5) {
                            return 309;
                        } else {
                            if (lat < 50.75) {
                                return 309;
                            } else {
                                return 285;
                            }
                        }
                    } else {
                        if (lat < 52.5) {
                            if (lng < -101.5) {
                                return 309;
                            } else {
                                return 285;
                            }
                        } else {
                            if (lng < -101.5) {
                                return 309;
                            } else {
                                return 285;
                            }
                        }
                    }
                } else {
                    if (lat < 54.75) {
                        if (lat < 54.0) {
                            if (lng < -101.75) {
                                return 309;
                            } else {
                                if (lat < 53.5) {
                                    if (lng < -101.5) {
                                        return 309;
                                    } else {
                                        return 285;
                                    }
                                } else {
                                    if (lng < -101.5) {
                                        if (lat < 53.75) {
                                            return 309;
                                        } else {
                                            return 285;
                                        }
                                    } else {
                                        return 285;
                                    }
                                }
                            }
                        } else {
                            if (lng < -102.0) {
                                if (lat < 54.25) {
                                    return 309;
                                } else {
                                    if (lng < -102.25) {
                                        if (lat < 54.5) {
                                            return 309;
                                        } else {
                                            return 285;
                                        }
                                    } else {
                                        return 285;
                                    }
                                }
                            } else {
                                if (lng < -101.75) {
                                    if (lat < 54.25) {
                                        return 309;
                                    } else {
                                        return 285;
                                    }
                                } else {
                                    return 285;
                                }
                            }
                        }
                    } else {
                        if (lat < 55.5) {
                            if (lng < -102.0) {
                                if (lat < 55.0) {
                                    return 285;
                                } else {
                                    if (lng < -102.25) {
                                        return 309;
                                    } else {
                                        if (lat < 55.25) {
                                            return 285;
                                        } else {
                                            return 309;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -101.75) {
                                    if (lat < 55.25) {
                                        return 285;
                                    } else {
                                        return 309;
                                    }
                                } else {
                                    return 285;
                                }
                            }
                        } else {
                            if (lng < -102.0) {
                                return 309;
                            } else {
                                if (lng < -101.75) {
                                    if (lat < 56.0) {
                                        return 309;
                                    } else {
                                        return 285;
                                    }
                                } else {
                                    return 285;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup17(double lat, double lng)
    {
        if (lat < 56.25) {
            if (lng < -107.0) {
                if (lat < 50.5) {
                    if (lng < -109.75) {
                        if (lat < 49.0) {
                            return 274;
                        } else {
                            if (lng < -111.25) {
                                return 228;
                            } else {
                                if (lng < -110.5) {
                                    if (lat < 49.25) {
                                        return 274;
                                    } else {
                                        return 228;
                                    }
                                } else {
                                    if (lat < 49.75) {
                                        if (lng < -110.0) {
                                            return 228;
                                        } else {
                                            return 309;
                                        }
                                    } else {
                                        if (lng < -110.0) {
                                            return 228;
                                        } else {
                                            return 309;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 49.0) {
                            return 274;
                        } else {
                            return 309;
                        }
                    }
                } else {
                    if (lat < 53.25) {
                        if (lng < -109.75) {
                            if (lng < -110.0) {
                                return 228;
                            } else {
                                if (lat < 52.75) {
                                    return 309;
                                } else {
                                    return 228;
                                }
                            }
                        } else {
                            if (lng < -108.75) {
                                if (lat < 52.75) {
                                    return 309;
                                } else {
                                    if (lng < -109.25) {
                                        return 228;
                                    } else {
                                        if (lng < -109.0) {
                                            if (lat < 53.0) {
                                                return 309;
                                            } else {
                                                return 228;
                                            }
                                        } else {
                                            if (lat < 53.0) {
                                                return 309;
                                            } else {
                                                return 228;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 309;
                            }
                        }
                    } else {
                        if (lng < -109.75) {
                            if (lat < 54.75) {
                                if (lng < -110.0) {
                                    return 228;
                                } else {
                                    if (lat < 53.75) {
                                        return 228;
                                    } else {
                                        return 309;
                                    }
                                }
                            } else {
                                if (lng < -110.0) {
                                    return 228;
                                } else {
                                    if (lat < 55.75) {
                                        return 309;
                                    } else {
                                        if (lat < 56.0) {
                                            return 228;
                                        } else {
                                            return 309;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 53.75) {
                                if (lng < -109.0) {
                                    if (lng < -109.5) {
                                        return 228;
                                    } else {
                                        if (lng < -109.25) {
                                            if (lat < 53.5) {
                                                return 228;
                                            } else {
                                                return 309;
                                            }
                                        } else {
                                            if (lat < 53.5) {
                                                return 228;
                                            } else {
                                                return 309;
                                            }
                                        }
                                    }
                                } else {
                                    return 309;
                                }
                            } else {
                                return 309;
                            }
                        }
                    }
                }
            } else {
                return kdLookup16(lat,lng);
            }
        } else {
            if (lng < -107.0) {
                if (lat < 61.75) {
                    if (lng < -109.75) {
                        if (lat < 59.0) {
                            if (lng < -110.0) {
                                return 228;
                            } else {
                                return 309;
                            }
                        } else {
                            if (lng < -111.25) {
                                if (lat < 60.0) {
                                    return 228;
                                } else {
                                    return 143;
                                }
                            } else {
                                if (lat < 60.0) {
                                    if (lng < -110.0) {
                                        return 228;
                                    } else {
                                        return 309;
                                    }
                                } else {
                                    return 143;
                                }
                            }
                        }
                    } else {
                        if (lat < 60.0) {
                            return 309;
                        } else {
                            return 143;
                        }
                    }
                } else {
                    if (lat < 64.75) {
                        return 143;
                    } else {
                        if (lng < -110.25) {
                            if (lat < 65.5) {
                                if (lng < -111.0) {
                                    return 143;
                                } else {
                                    if (lng < -110.75) {
                                        if (lat < 65.25) {
                                            return 143;
                                        } else {
                                            return 108;
                                        }
                                    } else {
                                        if (lat < 65.0) {
                                            return 143;
                                        } else {
                                            return 108;
                                        }
                                    }
                                }
                            } else {
                                return 108;
                            }
                        } else {
                            return 108;
                        }
                    }
                }
            } else {
                if (lat < 61.75) {
                    if (lng < -104.25) {
                        if (lat < 60.0) {
                            return 309;
                        } else {
                            return 143;
                        }
                    } else {
                        if (lat < 59.0) {
                            if (lng < -101.75) {
                                return 309;
                            } else {
                                return 285;
                            }
                        } else {
                            if (lng < -102.75) {
                                if (lat < 60.0) {
                                    return 309;
                                } else {
                                    return 143;
                                }
                            } else {
                                if (lat < 60.25) {
                                    if (lng < -102.0) {
                                        if (lat < 60.0) {
                                            return 309;
                                        } else {
                                            return 143;
                                        }
                                    } else {
                                        if (lat < 60.0) {
                                            return 285;
                                        } else {
                                            return 308;
                                        }
                                    }
                                } else {
                                    if (lng < -102.0) {
                                        return 143;
                                    } else {
                                        return 308;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -104.25) {
                        if (lat < 64.5) {
                            return 143;
                        } else {
                            if (lat < 64.75) {
                                if (lng < -106.75) {
                                    return 143;
                                } else {
                                    return 108;
                                }
                            } else {
                                return 108;
                            }
                        }
                    } else {
                        if (lat < 64.5) {
                            if (lng < -102.0) {
                                return 143;
                            } else {
                                return 308;
                            }
                        } else {
                            if (lng < -102.0) {
                                return 108;
                            } else {
                                if (lat < 67.0) {
                                    return 308;
                                } else {
                                    return 108;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup18(double lat, double lng)
    {
        if (lng < -95.75) {
            if (lat < 49.0) {
                if (lng < -100.25) {
                    if (lat < 47.0) {
                        if (lat < 46.0) {
                            return 274;
                        } else {
                            if (lng < -100.75) {
                                if (lat < 46.5) {
                                    if (lng < -101.0) {
                                        if (lat < 46.25) {
                                            return 161;
                                        } else {
                                            return 274;
                                        }
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    return 52;
                                }
                            } else {
                                if (lat < 46.5) {
                                    return 161;
                                } else {
                                    if (lng < -100.5) {
                                        if (lat < 46.75) {
                                            return 52;
                                        } else {
                                            return 161;
                                        }
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 47.5) {
                            if (lng < -100.75) {
                                return 105;
                            } else {
                                return 161;
                            }
                        } else {
                            return 161;
                        }
                    }
                } else {
                    return 161;
                }
            } else {
                return 285;
            }
        } else {
            if (lat < 49.5) {
                if (lng < -93.0) {
                    if (lat < 48.75) {
                        return 161;
                    } else {
                        if (lng < -94.5) {
                            if (lng < -95.25) {
                                if (lat < 49.0) {
                                    return 161;
                                } else {
                                    return 285;
                                }
                            } else {
                                if (lng < -95.0) {
                                    if (lat < 49.0) {
                                        return 161;
                                    } else {
                                        return 285;
                                    }
                                } else {
                                    if (lat < 49.25) {
                                        return 161;
                                    } else {
                                        if (lng < -94.75) {
                                            return 161;
                                        } else {
                                            return 285;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 285;
                        }
                    }
                } else {
                    if (lat < 47.25) {
                        return 161;
                    } else {
                        if (lng < -91.5) {
                            if (lat < 48.25) {
                                return 161;
                            } else {
                                if (lng < -92.25) {
                                    if (lat < 48.75) {
                                        if (lng < -92.5) {
                                            return 161;
                                        } else {
                                            if (lat < 48.5) {
                                                return 161;
                                            } else {
                                                return 285;
                                            }
                                        }
                                    } else {
                                        return 285;
                                    }
                                } else {
                                    if (lat < 48.75) {
                                        if (lng < -92.0) {
                                            if (lat < 48.5) {
                                                return 161;
                                            } else {
                                                return 285;
                                            }
                                        } else {
                                            return 224;
                                        }
                                    } else {
                                        if (lng < -91.75) {
                                            return 285;
                                        } else {
                                            if (lat < 49.0) {
                                                return 224;
                                            } else {
                                                return 285;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 48.25) {
                                return 161;
                            } else {
                                if (lng < -90.75) {
                                    if (lat < 49.0) {
                                        return 224;
                                    } else {
                                        if (lng < -91.0) {
                                            return 285;
                                        } else {
                                            return 239;
                                        }
                                    }
                                } else {
                                    if (lat < 48.5) {
                                        return 224;
                                    } else {
                                        return 239;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 56.0) {
                    if (lat < 55.75) {
                        if (lat < 55.5) {
                            if (lat < 55.25) {
                                if (lng < -90.75) {
                                    return 285;
                                } else {
                                    if (lat < 55.0) {
                                        if (lat < 54.75) {
                                            if (lat < 54.5) {
                                                if (lat < 54.25) {
                                                    if (lat < 51.75) {
                                                        if (lat < 51.0) {
                                                            return 285;
                                                        } else {
                                                            if (lng < -90.5) {
                                                                if (lat < 51.25) {
                                                                    return 285;
                                                                } else {
                                                                    return 239;
                                                                }
                                                            } else {
                                                                return 239;
                                                            }
                                                        }
                                                    } else {
                                                        if (lat < 52.0) {
                                                            if (lng < -90.5) {
                                                                return 285;
                                                            } else {
                                                                return 239;
                                                            }
                                                        } else {
                                                            return 285;
                                                        }
                                                    }
                                                } else {
                                                    return 285;
                                                }
                                            } else {
                                                return 285;
                                            }
                                        } else {
                                            return 285;
                                        }
                                    } else {
                                        return 285;
                                    }
                                }
                            } else {
                                return 285;
                            }
                        } else {
                            return 285;
                        }
                    } else {
                        return 285;
                    }
                } else {
                    return 285;
                }
            }
        }
    }

    public static int kdLookup19(double lat, double lng)
    {
        if (lng < -95.75) {
            if (lat < 73.0) {
                if (lng < -98.5) {
                    return 108;
                } else {
                    if (lat < 71.25) {
                        return 108;
                    } else {
                        if (lng < -97.25) {
                            return 108;
                        } else {
                            if (lat < 72.0) {
                                return 108;
                            } else {
                                if (lng < -96.5) {
                                    if (lat < 72.75) {
                                        return 108;
                                    } else {
                                        return 308;
                                    }
                                } else {
                                    return 108;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 75.75) {
                    if (lng < -98.5) {
                        if (lng < -100.0) {
                            if (lat < 73.25) {
                                return 108;
                            } else {
                                return 308;
                            }
                        } else {
                            if (lat < 73.25) {
                                if (lng < -99.0) {
                                    return 108;
                                } else {
                                    return 308;
                                }
                            } else {
                                return 308;
                            }
                        }
                    } else {
                        if (lng < -97.25) {
                            return 308;
                        } else {
                            if (lat < 74.25) {
                                return 308;
                            } else {
                                if (lng < -96.5) {
                                    return 308;
                                } else {
                                    if (lat < 75.0) {
                                        return 154;
                                    } else {
                                        if (lng < -96.25) {
                                            if (lat < 75.25) {
                                                return 154;
                                            } else {
                                                return 308;
                                            }
                                        } else {
                                            if (lat < 75.5) {
                                                return 154;
                                            } else {
                                                return 308;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 308;
                }
            }
        } else {
            if (lat < 73.0) {
                if (lng < -93.0) {
                    if (lat < 71.75) {
                        return 108;
                    } else {
                        if (lng < -94.5) {
                            if (lng < -95.25) {
                                return 308;
                            } else {
                                if (lat < 72.5) {
                                    return 108;
                                } else {
                                    return 308;
                                }
                            }
                        } else {
                            if (lng < -93.75) {
                                if (lat < 72.5) {
                                    return 108;
                                } else {
                                    return 308;
                                }
                            } else {
                                if (lat < 72.25) {
                                    return 108;
                                } else {
                                    return 308;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 70.25) {
                        return 108;
                    } else {
                        if (lng < -91.5) {
                            if (lat < 71.5) {
                                return 108;
                            } else {
                                if (lng < -92.75) {
                                    if (lat < 72.25) {
                                        return 108;
                                    } else {
                                        return 308;
                                    }
                                } else {
                                    return 308;
                                }
                            }
                        } else {
                            if (lat < 71.5) {
                                return 108;
                            } else {
                                return 308;
                            }
                        }
                    }
                }
            } else {
                if (lng < -93.0) {
                    if (lat < 75.75) {
                        if (lng < -94.5) {
                            if (lat < 74.5) {
                                return 308;
                            } else {
                                return 154;
                            }
                        } else {
                            if (lat < 74.5) {
                                return 308;
                            } else {
                                return 154;
                            }
                        }
                    } else {
                        if (lat < 77.25) {
                            if (lng < -94.5) {
                                if (lat < 76.25) {
                                    if (lng < -95.25) {
                                        return 154;
                                    } else {
                                        if (lng < -95.0) {
                                            if (lat < 76.0) {
                                                return 154;
                                            } else {
                                                return 308;
                                            }
                                        } else {
                                            if (lng < -94.75) {
                                                if (lat < 76.0) {
                                                    return 154;
                                                } else {
                                                    return 308;
                                                }
                                            } else {
                                                return 154;
                                            }
                                        }
                                    }
                                } else {
                                    return 308;
                                }
                            } else {
                                if (lng < -94.0) {
                                    if (lat < 76.25) {
                                        return 154;
                                    } else {
                                        return 308;
                                    }
                                } else {
                                    return 308;
                                }
                            }
                        } else {
                            return 308;
                        }
                    }
                } else {
                    return 308;
                }
            }
        }
    }

    public static int kdLookup20(double lat, double lng)
    {
        if (lat < 67.5) {
            if (lng < -101.25) {
                return kdLookup17(lat,lng);
            } else {
                if (lat < 56.25) {
                    return kdLookup18(lat,lng);
                } else {
                    if (lng < -95.75) {
                        if (lat < 61.75) {
                            if (lng < -98.5) {
                                if (lat < 60.0) {
                                    return 285;
                                } else {
                                    return 308;
                                }
                            } else {
                                if (lat < 60.0) {
                                    return 285;
                                } else {
                                    return 308;
                                }
                            }
                        } else {
                            if (lat < 67.0) {
                                return 308;
                            } else {
                                return 108;
                            }
                        }
                    } else {
                        if (lat < 61.75) {
                            if (lng < -93.0) {
                                if (lat < 59.25) {
                                    return 285;
                                } else {
                                    if (lng < -94.5) {
                                        if (lat < 60.0) {
                                            return 285;
                                        } else {
                                            return 308;
                                        }
                                    } else {
                                        return 308;
                                    }
                                }
                            } else {
                                return 285;
                            }
                        } else {
                            if (lng < -93.0) {
                                if (lat < 67.0) {
                                    return 308;
                                } else {
                                    return 108;
                                }
                            } else {
                                if (lat < 64.5) {
                                    return 308;
                                } else {
                                    if (lng < -91.5) {
                                        if (lat < 67.0) {
                                            return 308;
                                        } else {
                                            return 108;
                                        }
                                    } else {
                                        if (lat < 67.0) {
                                            return 308;
                                        } else {
                                            return 108;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -101.25) {
                if (lat < 78.75) {
                    if (lng < -107.0) {
                        if (lat < 73.0) {
                            if (lng < -109.75) {
                                if (lat < 70.25) {
                                    if (lng < -111.25) {
                                        if (lat < 70.0) {
                                            return 108;
                                        } else {
                                            if (lng < -112.25) {
                                                return 143;
                                            } else {
                                                return 108;
                                            }
                                        }
                                    } else {
                                        return 108;
                                    }
                                } else {
                                    return 143;
                                }
                            } else {
                                return 108;
                            }
                        } else {
                            if (lat < 75.75) {
                                if (lng < -109.75) {
                                    return 143;
                                } else {
                                    return 108;
                                }
                            } else {
                                if (lng < -109.75) {
                                    return 143;
                                } else {
                                    return 108;
                                }
                            }
                        }
                    } else {
                        if (lat < 73.0) {
                            return 108;
                        } else {
                            if (lng < -104.25) {
                                return 108;
                            } else {
                                if (lat < 75.75) {
                                    if (lng < -102.75) {
                                        return 108;
                                    } else {
                                        if (lat < 74.25) {
                                            if (lng < -102.0) {
                                                return 108;
                                            } else {
                                                if (lat < 73.5) {
                                                    if (lng < -101.75) {
                                                        return 108;
                                                    } else {
                                                        if (lng < -101.5) {
                                                            return 108;
                                                        } else {
                                                            if (lat < 73.25) {
                                                                return 108;
                                                            } else {
                                                                return 308;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 308;
                                                }
                                            }
                                        } else {
                                            return 308;
                                        }
                                    }
                                } else {
                                    if (lng < -102.75) {
                                        return 108;
                                    } else {
                                        if (lat < 77.25) {
                                            if (lng < -102.0) {
                                                if (lat < 76.5) {
                                                    if (lng < -102.25) {
                                                        return 108;
                                                    } else {
                                                        if (lat < 76.0) {
                                                            return 308;
                                                        } else {
                                                            if (lat < 76.25) {
                                                                return 108;
                                                            } else {
                                                                return 308;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 108;
                                                }
                                            } else {
                                                return 308;
                                            }
                                        } else {
                                            if (lng < -102.0) {
                                                return 108;
                                            } else {
                                                return 308;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -107.0) {
                        if (lat < 84.25) {
                            if (lng < -109.75) {
                                return 143;
                            } else {
                                return 108;
                            }
                        } else {
                            return 0;
                        }
                    } else {
                        if (lat < 84.25) {
                            if (lng < -104.25) {
                                return 108;
                            } else {
                                if (lat < 81.5) {
                                    if (lng < -102.75) {
                                        return 108;
                                    } else {
                                        if (lat < 80.0) {
                                            if (lng < -102.0) {
                                                return 108;
                                            } else {
                                                return 308;
                                            }
                                        } else {
                                            return 0;
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        } else {
                            return 0;
                        }
                    }
                }
            } else {
                if (lat < 78.75) {
                    return kdLookup19(lat,lng);
                } else {
                    return 308;
                }
            }
        }
    }

    public static int kdLookup21(double lat, double lng)
    {
        if (lat < 0.0) {
            if (lng < -135.0) {
                if (lat < -45.0) {
                    return 122;
                } else {
                    if (lng < -157.5) {
                        if (lat < -22.5) {
                            return 59;
                        } else {
                            if (lng < -168.75) {
                                if (lat < -11.25) {
                                    if (lng < -174.5) {
                                        return 148;
                                    } else {
                                        if (lat < -17.0) {
                                            return 0;
                                        } else {
                                            if (lng < -171.75) {
                                                return 200;
                                            } else {
                                                if (lat < -14.25) {
                                                    return 44;
                                                } else {
                                                    if (lng < -170.25) {
                                                        return 200;
                                                    } else {
                                                        return 44;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            } else {
                                return 361;
                            }
                        }
                    } else {
                        if (lat < -22.5) {
                            return 0;
                        } else {
                            if (lng < -146.25) {
                                return 313;
                            } else {
                                if (lat < -11.25) {
                                    return 313;
                                } else {
                                    return 102;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < -45.0) {
                    return 122;
                } else {
                    return 114;
                }
            }
        } else {
            if (lng < -135.0) {
                if (lat < 45.0) {
                    if (lng < -157.5) {
                        return 245;
                    } else {
                        if (lat < 22.5) {
                            if (lng < -146.25) {
                                if (lat < 11.25) {
                                    return 238;
                                } else {
                                    return 245;
                                }
                            } else {
                                return 0;
                            }
                        } else {
                            return 0;
                        }
                    }
                } else {
                    if (lng < -157.5) {
                        if (lat < 67.5) {
                            if (lng < -168.75) {
                                if (lat < 56.25) {
                                    if (lng < -174.5) {
                                        return 159;
                                    } else {
                                        if (lat < 50.5) {
                                            return 0;
                                        } else {
                                            if (lng < -171.75) {
                                                return 0;
                                            } else {
                                                if (lat < 53.25) {
                                                    if (lng < -170.25) {
                                                        return 159;
                                                    } else {
                                                        if (lat < 51.75) {
                                                            return 0;
                                                        } else {
                                                            if (lng < -169.5) {
                                                                return 159;
                                                            } else {
                                                                if (lat < 52.5) {
                                                                    return 0;
                                                                } else {
                                                                    if (lng < -169.25) {
                                                                        return 159;
                                                                    } else {
                                                                        return 135;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 159;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -174.5) {
                                        return 366;
                                    } else {
                                        if (lat < 61.75) {
                                            return 135;
                                        } else {
                                            if (lng < -171.75) {
                                                if (lat < 64.5) {
                                                    if (lng < -173.25) {
                                                        return 366;
                                                    } else {
                                                        if (lat < 63.0) {
                                                            return 0;
                                                        } else {
                                                            if (lng < -172.5) {
                                                                return 366;
                                                            } else {
                                                                if (lat < 63.75) {
                                                                    return 135;
                                                                } else {
                                                                    return 366;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 366;
                                                }
                                            } else {
                                                if (lat < 64.5) {
                                                    return 135;
                                                } else {
                                                    return 366;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 56.25) {
                                    if (lng < -163.25) {
                                        return 135;
                                    } else {
                                        if (lat < 50.5) {
                                            return 0;
                                        } else {
                                            if (lng < -160.5) {
                                                if (lat < 53.25) {
                                                    return 0;
                                                } else {
                                                    if (lat < 54.75) {
                                                        return 135;
                                                    } else {
                                                        if (lng < -162.0) {
                                                            return 135;
                                                        } else {
                                                            return 371;
                                                        }
                                                    }
                                                }
                                            } else {
                                                return 371;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -163.25) {
                                        return 135;
                                    } else {
                                        if (lat < 61.75) {
                                            if (lng < -161.75) {
                                                if (lat < 59.0) {
                                                    return 371;
                                                } else {
                                                    if (lat < 60.25) {
                                                        if (lng < -162.25) {
                                                            return 135;
                                                        } else {
                                                            return 371;
                                                        }
                                                    } else {
                                                        if (lng < -162.0) {
                                                            return 135;
                                                        } else {
                                                            return 371;
                                                        }
                                                    }
                                                }
                                            } else {
                                                return 371;
                                            }
                                        } else {
                                            if (lng < -161.0) {
                                                if (lat < 64.5) {
                                                    if (lat < 63.0) {
                                                        if (lng < -162.0) {
                                                            return 135;
                                                        } else {
                                                            return 371;
                                                        }
                                                    } else {
                                                        if (lng < -162.25) {
                                                            return 135;
                                                        } else {
                                                            if (lat < 63.75) {
                                                                if (lng < -161.75) {
                                                                    if (lat < 63.25) {
                                                                        if (lng < -162.0) {
                                                                            return 135;
                                                                        } else {
                                                                            return 371;
                                                                        }
                                                                    } else {
                                                                        if (lng < -162.0) {
                                                                            return 135;
                                                                        } else {
                                                                            return 371;
                                                                        }
                                                                    }
                                                                } else {
                                                                    return 371;
                                                                }
                                                            } else {
                                                                if (lng < -161.75) {
                                                                    return 135;
                                                                } else {
                                                                    return 371;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (lat < 66.0) {
                                                        if (lng < -162.0) {
                                                            return 135;
                                                        } else {
                                                            return 371;
                                                        }
                                                    } else {
                                                        if (lng < -162.25) {
                                                            return 135;
                                                        } else {
                                                            if (lat < 66.75) {
                                                                if (lng < -161.75) {
                                                                    if (lat < 66.25) {
                                                                        if (lng < -162.0) {
                                                                            return 135;
                                                                        } else {
                                                                            return 371;
                                                                        }
                                                                    } else {
                                                                        if (lng < -162.0) {
                                                                            return 135;
                                                                        } else {
                                                                            return 371;
                                                                        }
                                                                    }
                                                                } else {
                                                                    return 371;
                                                                }
                                                            } else {
                                                                if (lng < -162.0) {
                                                                    return 135;
                                                                } else {
                                                                    return 371;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                return 371;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -168.75) {
                                return 366;
                            } else {
                                if (lat < 78.75) {
                                    if (lng < -163.25) {
                                        return 135;
                                    } else {
                                        if (lat < 73.0) {
                                            if (lng < -160.5) {
                                                if (lat < 70.25) {
                                                    if (lng < -162.0) {
                                                        return 135;
                                                    } else {
                                                        return 371;
                                                    }
                                                } else {
                                                    if (lng < -162.0) {
                                                        return 135;
                                                    } else {
                                                        return 371;
                                                    }
                                                }
                                            } else {
                                                return 371;
                                            }
                                        } else {
                                            return 0;
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        }
                    } else {
                        if (lat < 67.5) {
                            if (lng < -141.0) {
                                return 371;
                            } else {
                                if (lat < 56.25) {
                                    return 0;
                                } else {
                                    return kdLookup1(lat,lng);
                                }
                            }
                        } else {
                            if (lng < -146.25) {
                                return 371;
                            } else {
                                if (lat < 78.75) {
                                    if (lng < -140.75) {
                                        return 371;
                                    } else {
                                        if (lat < 73.0) {
                                            if (lng < -138.0) {
                                                return 325;
                                            } else {
                                                if (lat < 70.25) {
                                                    if (lng < -136.5) {
                                                        return 325;
                                                    } else {
                                                        if (lat < 68.75) {
                                                            if (lng < -136.0) {
                                                                if (lat < 68.0) {
                                                                    if (lng < -136.25) {
                                                                        return 325;
                                                                    } else {
                                                                        if (lat < 67.75) {
                                                                            return 325;
                                                                        } else {
                                                                            return 143;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (lat < 68.25) {
                                                                        if (lng < -136.25) {
                                                                            return 325;
                                                                        } else {
                                                                            return 143;
                                                                        }
                                                                    } else {
                                                                        if (lng < -136.25) {
                                                                            return 325;
                                                                        } else {
                                                                            return 143;
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                return 143;
                                                            }
                                                        } else {
                                                            if (lng < -135.75) {
                                                                if (lat < 69.5) {
                                                                    if (lng < -136.25) {
                                                                        return 325;
                                                                    } else {
                                                                        return 143;
                                                                    }
                                                                } else {
                                                                    return 143;
                                                                }
                                                            } else {
                                                                return 143;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 0;
                                                }
                                            }
                                        } else {
                                            return 0;
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 45.0) {
                    return kdLookup9(lat,lng);
                } else {
                    if (lng < -112.5) {
                        return kdLookup15(lat,lng);
                    } else {
                        return kdLookup20(lat,lng);
                    }
                }
            }
        }
    }

    public static int kdLookup22(double lat, double lng)
    {
        if (lat < -50.75) {
            if (lng < -70.5) {
                if (lat < -53.5) {
                    return 31;
                } else {
                    if (lng < -72.0) {
                        if (lat < -51.5) {
                            return 31;
                        } else {
                            if (lng < -72.25) {
                                return 31;
                            } else {
                                if (lat < -51.0) {
                                    return 85;
                                } else {
                                    return 31;
                                }
                            }
                        }
                    } else {
                        if (lat < -52.0) {
                            return 31;
                        } else {
                            if (lng < -71.25) {
                                if (lat < -51.75) {
                                    if (lng < -71.5) {
                                        return 31;
                                    } else {
                                        return 85;
                                    }
                                } else {
                                    return 85;
                                }
                            } else {
                                if (lat < -51.75) {
                                    return 31;
                                } else {
                                    return 85;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < -53.5) {
                    if (lng < -68.5) {
                        return 31;
                    } else {
                        if (lat < -55.0) {
                            return 31;
                        } else {
                            if (lat < -54.75) {
                                return 31;
                            } else {
                                return 288;
                            }
                        }
                    }
                } else {
                    if (lng < -69.0) {
                        if (lat < -52.25) {
                            return 31;
                        } else {
                            if (lng < -69.75) {
                                if (lat < -52.0) {
                                    return 31;
                                } else {
                                    return 85;
                                }
                            } else {
                                if (lat < -52.0) {
                                    return 31;
                                } else {
                                    return 85;
                                }
                            }
                        }
                    } else {
                        if (lat < -52.25) {
                            if (lng < -68.25) {
                                if (lat < -53.0) {
                                    if (lng < -68.5) {
                                        return 31;
                                    } else {
                                        return 288;
                                    }
                                } else {
                                    if (lng < -68.75) {
                                        return 31;
                                    } else {
                                        if (lat < -52.75) {
                                            if (lng < -68.5) {
                                                return 31;
                                            } else {
                                                return 288;
                                            }
                                        } else {
                                            if (lng < -68.5) {
                                                return 31;
                                            } else {
                                                return 85;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 288;
                            }
                        } else {
                            if (lng < -68.25) {
                                if (lat < -51.5) {
                                    if (lng < -68.75) {
                                        if (lat < -52.0) {
                                            return 31;
                                        } else {
                                            return 85;
                                        }
                                    } else {
                                        return 85;
                                    }
                                } else {
                                    return 85;
                                }
                            } else {
                                return 85;
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -70.5) {
                if (lat < -48.0) {
                    if (lng < -72.25) {
                        if (lat < -49.5) {
                            if (lat < -50.5) {
                                return 31;
                            } else {
                                return 85;
                            }
                        } else {
                            if (lat < -48.75) {
                                if (lng < -72.75) {
                                    if (lat < -49.0) {
                                        return 85;
                                    } else {
                                        return 31;
                                    }
                                } else {
                                    return 85;
                                }
                            } else {
                                if (lng < -72.5) {
                                    return 31;
                                } else {
                                    if (lat < -48.25) {
                                        return 85;
                                    } else {
                                        return 31;
                                    }
                                }
                            }
                        }
                    } else {
                        return 85;
                    }
                } else {
                    if (lat < -46.5) {
                        if (lng < -72.0) {
                            if (lat < -47.25) {
                                if (lng < -72.25) {
                                    return 31;
                                } else {
                                    return 85;
                                }
                            } else {
                                return 31;
                            }
                        } else {
                            if (lng < -71.5) {
                                if (lat < -47.0) {
                                    return 85;
                                } else {
                                    if (lng < -71.75) {
                                        return 31;
                                    } else {
                                        if (lat < -46.75) {
                                            return 85;
                                        } else {
                                            return 31;
                                        }
                                    }
                                }
                            } else {
                                return 85;
                            }
                        }
                    } else {
                        if (lng < -71.75) {
                            return 31;
                        } else {
                            if (lat < -45.75) {
                                if (lng < -71.5) {
                                    return 31;
                                } else {
                                    return 85;
                                }
                            } else {
                                if (lng < -71.25) {
                                    if (lat < -45.5) {
                                        return 88;
                                    } else {
                                        return 31;
                                    }
                                } else {
                                    return 88;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < -48.0) {
                    return 85;
                } else {
                    if (lng < -69.0) {
                        if (lat < -45.75) {
                            return 85;
                        } else {
                            return 88;
                        }
                    } else {
                        if (lat < -45.75) {
                            return 85;
                        } else {
                            return 88;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup23(double lat, double lng)
    {
        if (lng < -70.5) {
            if (lat < -36.75) {
                if (lng < -71.5) {
                    return 31;
                } else {
                    if (lat < -38.25) {
                        if (lat < -39.0) {
                            if (lng < -71.25) {
                                if (lat < -39.25) {
                                    return 128;
                                } else {
                                    return 31;
                                }
                            } else {
                                return 128;
                            }
                        } else {
                            if (lng < -71.0) {
                                if (lat < -38.75) {
                                    if (lng < -71.25) {
                                        return 31;
                                    } else {
                                        return 128;
                                    }
                                } else {
                                    return 31;
                                }
                            } else {
                                if (lat < -38.75) {
                                    return 128;
                                } else {
                                    if (lng < -70.75) {
                                        return 31;
                                    } else {
                                        return 128;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -37.5) {
                            if (lng < -71.0) {
                                return 31;
                            } else {
                                return 128;
                            }
                        } else {
                            if (lng < -71.0) {
                                return 31;
                            } else {
                                return 128;
                            }
                        }
                    }
                }
            } else {
                if (lat < -36.25) {
                    if (lng < -72.0) {
                        return 31;
                    } else {
                        if (lng < -71.0) {
                            return 31;
                        } else {
                            return 128;
                        }
                    }
                } else {
                    return 31;
                }
            }
        } else {
            if (lat < -36.75) {
                if (lng < -69.0) {
                    if (lat < -37.0) {
                        return 128;
                    } else {
                        if (lng < -69.75) {
                            return 128;
                        } else {
                            return 14;
                        }
                    }
                } else {
                    if (lat < -37.5) {
                        return 128;
                    } else {
                        if (lng < -68.25) {
                            if (lng < -68.75) {
                                if (lat < -37.25) {
                                    return 128;
                                } else {
                                    return 14;
                                }
                            } else {
                                if (lat < -37.25) {
                                    return 128;
                                } else {
                                    return 14;
                                }
                            }
                        } else {
                            if (lng < -68.0) {
                                return 14;
                            } else {
                                return 128;
                            }
                        }
                    }
                }
            } else {
                if (lng < -69.0) {
                    if (lat < -35.25) {
                        if (lng < -69.75) {
                            if (lat < -36.0) {
                                if (lng < -70.25) {
                                    return 128;
                                } else {
                                    if (lat < -36.5) {
                                        return 128;
                                    } else {
                                        if (lng < -70.0) {
                                            if (lat < -36.25) {
                                                return 128;
                                            } else {
                                                return 14;
                                            }
                                        } else {
                                            return 14;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -70.25) {
                                    return 31;
                                } else {
                                    return 14;
                                }
                            }
                        } else {
                            return 14;
                        }
                    } else {
                        if (lng < -69.75) {
                            if (lat < -34.5) {
                                if (lng < -70.25) {
                                    if (lat < -35.0) {
                                        return 14;
                                    } else {
                                        return 31;
                                    }
                                } else {
                                    return 14;
                                }
                            } else {
                                if (lng < -70.0) {
                                    return 31;
                                } else {
                                    if (lat < -34.25) {
                                        return 14;
                                    } else {
                                        return 31;
                                    }
                                }
                            }
                        } else {
                            return 14;
                        }
                    }
                } else {
                    if (lat < -35.75) {
                        if (lng < -68.25) {
                            return 14;
                        } else {
                            if (lat < -36.25) {
                                if (lng < -68.0) {
                                    return 14;
                                } else {
                                    return 128;
                                }
                            } else {
                                if (lng < -68.0) {
                                    if (lat < -36.0) {
                                        return 14;
                                    } else {
                                        return 128;
                                    }
                                } else {
                                    if (lng < -67.75) {
                                        if (lat < -36.0) {
                                            return 128;
                                        } else {
                                            return 14;
                                        }
                                    } else {
                                        if (lat < -36.0) {
                                            return 128;
                                        } else {
                                            return 14;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        return 14;
                    }
                }
            }
        }
    }

    public static int kdLookup24(double lat, double lng)
    {
        if (lat < -28.25) {
            if (lng < -70.5) {
                return 31;
            } else {
                if (lat < -31.0) {
                    if (lng < -69.0) {
                        if (lat < -32.5) {
                            if (lng < -69.75) {
                                if (lat < -33.25) {
                                    return 31;
                                } else {
                                    if (lng < -70.0) {
                                        return 31;
                                    } else {
                                        return 14;
                                    }
                                }
                            } else {
                                return 14;
                            }
                        } else {
                            if (lng < -69.75) {
                                if (lat < -31.75) {
                                    if (lng < -70.25) {
                                        return 31;
                                    } else {
                                        if (lat < -32.25) {
                                            if (lng < -70.0) {
                                                return 31;
                                            } else {
                                                return 14;
                                            }
                                        } else {
                                            if (lng < -70.0) {
                                                if (lat < -32.0) {
                                                    return 347;
                                                } else {
                                                    return 31;
                                                }
                                            } else {
                                                return 347;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -70.25) {
                                        if (lat < -31.5) {
                                            return 31;
                                        } else {
                                            return 347;
                                        }
                                    } else {
                                        return 347;
                                    }
                                }
                            } else {
                                if (lat < -31.75) {
                                    if (lng < -69.5) {
                                        if (lat < -32.25) {
                                            return 14;
                                        } else {
                                            return 347;
                                        }
                                    } else {
                                        if (lat < -32.0) {
                                            return 14;
                                        } else {
                                            if (lng < -69.25) {
                                                return 347;
                                            } else {
                                                return 14;
                                            }
                                        }
                                    }
                                } else {
                                    return 347;
                                }
                            }
                        }
                    } else {
                        if (lat < -32.25) {
                            return 14;
                        } else {
                            if (lng < -68.25) {
                                if (lat < -32.0) {
                                    return 14;
                                } else {
                                    return 347;
                                }
                            } else {
                                if (lat < -32.0) {
                                    if (lng < -67.75) {
                                        return 14;
                                    } else {
                                        return 347;
                                    }
                                } else {
                                    return 347;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -69.0) {
                        if (lat < -29.75) {
                            if (lng < -69.75) {
                                if (lat < -30.5) {
                                    if (lng < -70.25) {
                                        return 31;
                                    } else {
                                        return 347;
                                    }
                                } else {
                                    if (lng < -70.0) {
                                        return 31;
                                    } else {
                                        if (lat < -30.25) {
                                            return 347;
                                        } else {
                                            return 31;
                                        }
                                    }
                                }
                            } else {
                                return 347;
                            }
                        } else {
                            if (lng < -69.75) {
                                return 31;
                            } else {
                                if (lat < -28.75) {
                                    return 347;
                                } else {
                                    if (lng < -69.5) {
                                        return 31;
                                    } else {
                                        if (lng < -69.25) {
                                            return 347;
                                        } else {
                                            if (lat < -28.5) {
                                                return 347;
                                            } else {
                                                return 250;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -29.75) {
                            if (lng < -67.75) {
                                return 347;
                            } else {
                                if (lat < -30.0) {
                                    return 347;
                                } else {
                                    return 250;
                                }
                            }
                        } else {
                            if (lng < -68.25) {
                                if (lat < -29.0) {
                                    if (lng < -68.75) {
                                        if (lat < -29.5) {
                                            return 347;
                                        } else {
                                            if (lat < -29.25) {
                                                return 250;
                                            } else {
                                                return 347;
                                            }
                                        }
                                    } else {
                                        if (lat < -29.5) {
                                            return 347;
                                        } else {
                                            return 250;
                                        }
                                    }
                                } else {
                                    if (lng < -68.75) {
                                        if (lat < -28.75) {
                                            return 347;
                                        } else {
                                            return 250;
                                        }
                                    } else {
                                        return 250;
                                    }
                                }
                            } else {
                                return 250;
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -70.5) {
                return 31;
            } else {
                if (lat < -25.5) {
                    if (lng < -69.0) {
                        if (lat < -28.0) {
                            if (lng < -69.25) {
                                return 31;
                            } else {
                                return 250;
                            }
                        } else {
                            return 31;
                        }
                    } else {
                        if (lat < -27.0) {
                            if (lng < -68.25) {
                                if (lat < -27.75) {
                                    return 250;
                                } else {
                                    if (lng < -68.75) {
                                        if (lat < -27.5) {
                                            return 88;
                                        } else {
                                            return 31;
                                        }
                                    } else {
                                        if (lat < -27.5) {
                                            if (lng < -68.5) {
                                                return 88;
                                            } else {
                                                return 250;
                                            }
                                        } else {
                                            return 88;
                                        }
                                    }
                                }
                            } else {
                                if (lat < -28.0) {
                                    if (lng < -67.75) {
                                        return 250;
                                    } else {
                                        return 88;
                                    }
                                } else {
                                    return 88;
                                }
                            }
                        } else {
                            if (lng < -68.25) {
                                if (lat < -25.75) {
                                    if (lat < -26.5) {
                                        return 31;
                                    } else {
                                        if (lng < -68.5) {
                                            return 31;
                                        } else {
                                            if (lat < -26.25) {
                                                return 88;
                                            } else {
                                                return 31;
                                            }
                                        }
                                    }
                                } else {
                                    return 31;
                                }
                            } else {
                                return 88;
                            }
                        }
                    }
                } else {
                    if (lng < -68.5) {
                        return 31;
                    } else {
                        if (lat < -24.0) {
                            if (lat < -24.75) {
                                if (lng < -68.0) {
                                    if (lat < -25.0) {
                                        return 88;
                                    } else {
                                        if (lng < -68.25) {
                                            return 31;
                                        } else {
                                            return 128;
                                        }
                                    }
                                } else {
                                    if (lat < -25.25) {
                                        return 88;
                                    } else {
                                        if (lng < -67.75) {
                                            if (lat < -25.0) {
                                                return 88;
                                            } else {
                                                return 128;
                                            }
                                        } else {
                                            return 128;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -68.0) {
                                    if (lat < -24.5) {
                                        return 128;
                                    } else {
                                        if (lng < -68.25) {
                                            return 31;
                                        } else {
                                            if (lat < -24.25) {
                                                return 128;
                                            } else {
                                                return 31;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < -24.25) {
                                        return 128;
                                    } else {
                                        if (lng < -67.75) {
                                            return 31;
                                        } else {
                                            return 128;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -22.75) {
                                return 31;
                            } else {
                                if (lng < -67.75) {
                                    return 31;
                                } else {
                                    return 191;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup25(double lat, double lng)
    {
        if (lat < -17.0) {
            if (lng < -70.5) {
                return 165;
            } else {
                if (lat < -19.75) {
                    if (lng < -69.0) {
                        return 31;
                    } else {
                        if (lat < -21.25) {
                            if (lng < -68.0) {
                                return 31;
                            } else {
                                if (lat < -22.0) {
                                    if (lng < -67.75) {
                                        return 31;
                                    } else {
                                        return 191;
                                    }
                                } else {
                                    return 191;
                                }
                            }
                        } else {
                            if (lng < -68.25) {
                                if (lat < -20.5) {
                                    if (lng < -68.5) {
                                        return 31;
                                    } else {
                                        if (lat < -20.75) {
                                            return 31;
                                        } else {
                                            return 191;
                                        }
                                    }
                                } else {
                                    if (lng < -68.5) {
                                        return 31;
                                    } else {
                                        return 191;
                                    }
                                }
                            } else {
                                if (lat < -21.0) {
                                    if (lng < -68.0) {
                                        return 31;
                                    } else {
                                        return 191;
                                    }
                                } else {
                                    return 191;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -69.0) {
                        if (lat < -18.25) {
                            return 31;
                        } else {
                            if (lng < -69.75) {
                                return 165;
                            } else {
                                if (lat < -17.75) {
                                    return 31;
                                } else {
                                    if (lng < -69.5) {
                                        if (lat < -17.5) {
                                            return 31;
                                        } else {
                                            return 165;
                                        }
                                    } else {
                                        if (lat < -17.5) {
                                            if (lng < -69.25) {
                                                return 31;
                                            } else {
                                                return 191;
                                            }
                                        } else {
                                            if (lng < -69.25) {
                                                if (lat < -17.25) {
                                                    return 165;
                                                } else {
                                                    return 191;
                                                }
                                            } else {
                                                return 191;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -18.5) {
                            if (lng < -68.25) {
                                if (lat < -19.25) {
                                    if (lng < -68.5) {
                                        return 31;
                                    } else {
                                        if (lat < -19.5) {
                                            return 191;
                                        } else {
                                            return 31;
                                        }
                                    }
                                } else {
                                    if (lng < -68.75) {
                                        return 31;
                                    } else {
                                        if (lat < -19.0) {
                                            if (lng < -68.5) {
                                                return 31;
                                            } else {
                                                return 191;
                                            }
                                        } else {
                                            return 191;
                                        }
                                    }
                                }
                            } else {
                                return 191;
                            }
                        } else {
                            return 191;
                        }
                    }
                }
            }
        } else {
            if (lng < -69.25) {
                return 165;
            } else {
                if (lat < -14.25) {
                    if (lat < -15.25) {
                        if (lng < -68.75) {
                            if (lat < -16.25) {
                                if (lat < -16.75) {
                                    return 191;
                                } else {
                                    if (lng < -69.0) {
                                        return 165;
                                    } else {
                                        return 191;
                                    }
                                }
                            } else {
                                if (lat < -16.0) {
                                    return 165;
                                } else {
                                    return 191;
                                }
                            }
                        } else {
                            return 191;
                        }
                    } else {
                        if (lng < -69.0) {
                            if (lat < -14.5) {
                                if (lat < -15.0) {
                                    return 165;
                                } else {
                                    if (lat < -14.75) {
                                        return 191;
                                    } else {
                                        return 165;
                                    }
                                }
                            } else {
                                return 165;
                            }
                        } else {
                            return 191;
                        }
                    }
                } else {
                    if (lat < -12.75) {
                        if (lng < -68.75) {
                            if (lat < -13.0) {
                                if (lat < -13.75) {
                                    return 165;
                                } else {
                                    if (lat < -13.5) {
                                        if (lng < -69.0) {
                                            return 165;
                                        } else {
                                            return 191;
                                        }
                                    } else {
                                        return 165;
                                    }
                                }
                            } else {
                                return 165;
                            }
                        } else {
                            return 191;
                        }
                    } else {
                        if (lng < -68.5) {
                            if (lat < -12.0) {
                                if (lng < -68.75) {
                                    return 165;
                                } else {
                                    if (lat < -12.5) {
                                        return 191;
                                    } else {
                                        if (lat < -12.25) {
                                            return 165;
                                        } else {
                                            return 191;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -69.0) {
                                    return 165;
                                } else {
                                    if (lat < -11.75) {
                                        if (lng < -68.75) {
                                            return 165;
                                        } else {
                                            return 191;
                                        }
                                    } else {
                                        return 191;
                                    }
                                }
                            }
                        } else {
                            return 191;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup26(double lat, double lng)
    {
        if (lat < -4.5) {
            if (lat < -8.0) {
                if (lng < -73.5) {
                    return 165;
                } else {
                    if (lat < -8.25) {
                        return 165;
                    } else {
                        return 181;
                    }
                }
            } else {
                if (lng < -73.75) {
                    return 165;
                } else {
                    if (lat < -6.5) {
                        if (lat < -7.25) {
                            if (lat < -7.75) {
                                if (lng < -73.5) {
                                    return 165;
                                } else {
                                    return 181;
                                }
                            } else {
                                if (lng < -73.5) {
                                    if (lat < -7.5) {
                                        return 165;
                                    } else {
                                        return 181;
                                    }
                                } else {
                                    return 181;
                                }
                            }
                        } else {
                            if (lat < -7.0) {
                                if (lng < -73.5) {
                                    return 165;
                                } else {
                                    return 242;
                                }
                            } else {
                                if (lng < -73.5) {
                                    if (lat < -6.75) {
                                        return 242;
                                    } else {
                                        return 165;
                                    }
                                } else {
                                    return 242;
                                }
                            }
                        }
                    } else {
                        return 165;
                    }
                }
            }
        } else {
            if (lng < -76.0) {
                if (lat < -2.25) {
                    if (lng < -77.5) {
                        if (lat < -3.5) {
                            if (lng < -78.25) {
                                if (lat < -4.0) {
                                    if (lng < -78.5) {
                                        return 382;
                                    } else {
                                        return 165;
                                    }
                                } else {
                                    if (lng < -78.5) {
                                        return 382;
                                    } else {
                                        if (lat < -3.75) {
                                            return 165;
                                        } else {
                                            return 382;
                                        }
                                    }
                                }
                            } else {
                                return 165;
                            }
                        } else {
                            if (lng < -78.25) {
                                return 382;
                            } else {
                                if (lat < -3.0) {
                                    if (lng < -78.0) {
                                        if (lat < -3.25) {
                                            return 165;
                                        } else {
                                            return 382;
                                        }
                                    } else {
                                        return 165;
                                    }
                                } else {
                                    if (lng < -77.75) {
                                        return 382;
                                    } else {
                                        if (lat < -2.75) {
                                            return 165;
                                        } else {
                                            return 382;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -2.75) {
                            return 165;
                        } else {
                            if (lng < -76.75) {
                                if (lng < -77.0) {
                                    return 382;
                                } else {
                                    if (lat < -2.5) {
                                        return 165;
                                    } else {
                                        return 382;
                                    }
                                }
                            } else {
                                if (lng < -76.5) {
                                    if (lat < -2.5) {
                                        return 165;
                                    } else {
                                        return 382;
                                    }
                                } else {
                                    return 165;
                                }
                            }
                        }
                    }
                } else {
                    return 382;
                }
            } else {
                if (lat < -2.0) {
                    return 165;
                } else {
                    if (lng < -74.75) {
                        if (lat < -1.0) {
                            if (lng < -75.5) {
                                if (lat < -1.75) {
                                    if (lng < -75.75) {
                                        return 382;
                                    } else {
                                        return 165;
                                    }
                                } else {
                                    return 382;
                                }
                            } else {
                                if (lat < -1.25) {
                                    return 165;
                                } else {
                                    if (lng < -75.25) {
                                        return 382;
                                    } else {
                                        return 165;
                                    }
                                }
                            }
                        } else {
                            if (lng < -75.5) {
                                return 382;
                            } else {
                                if (lat < -0.5) {
                                    if (lng < -75.25) {
                                        return 382;
                                    } else {
                                        return 165;
                                    }
                                } else {
                                    if (lng < -75.25) {
                                        if (lat < -0.25) {
                                            return 382;
                                        } else {
                                            return 165;
                                        }
                                    } else {
                                        return 165;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -1.0) {
                            if (lng < -73.5) {
                                return 165;
                            } else {
                                if (lat < -1.75) {
                                    return 165;
                                } else {
                                    return 391;
                                }
                            }
                        } else {
                            if (lng < -74.25) {
                                if (lat < -0.25) {
                                    return 165;
                                } else {
                                    return 391;
                                }
                            } else {
                                return 391;
                            }
                        }
                    }
                }
            }
        }
    }
}
