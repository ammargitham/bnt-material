package com.ammar.bntmaterial.core.timescalculator.timezonemapper;

public class TimeZoneMapperMethodsTwo {

    public static int kdLookup27(double lat, double lng)
    {
        if (lat < -8.5) {
            if (lng < -69.0) {
                if (lat < -10.0) {
                    if (lng < -69.75) {
                        if (lat < -10.75) {
                            if (lng < -70.25) {
                                return 165;
                            } else {
                                if (lng < -70.0) {
                                    if (lat < -11.0) {
                                        return 165;
                                    } else {
                                        return 181;
                                    }
                                } else {
                                    return 165;
                                }
                            }
                        } else {
                            return 181;
                        }
                    } else {
                        if (lat < -10.75) {
                            if (lng < -69.5) {
                                return 165;
                            } else {
                                if (lng < -69.25) {
                                    if (lat < -11.0) {
                                        return 165;
                                    } else {
                                        return 191;
                                    }
                                } else {
                                    return 191;
                                }
                            }
                        } else {
                            return 181;
                        }
                    }
                } else {
                    if (lng < -69.25) {
                        return 181;
                    } else {
                        if (lat < -8.75) {
                            return 181;
                        } else {
                            return 242;
                        }
                    }
                }
            } else {
                if (lat < -10.0) {
                    if (lng < -68.25) {
                        if (lat < -10.75) {
                            if (lng < -68.75) {
                                return 191;
                            } else {
                                if (lng < -68.5) {
                                    if (lat < -11.0) {
                                        return 191;
                                    } else {
                                        return 181;
                                    }
                                } else {
                                    if (lat < -11.0) {
                                        return 191;
                                    } else {
                                        return 181;
                                    }
                                }
                            }
                        } else {
                            return 181;
                        }
                    } else {
                        if (lat < -10.75) {
                            return 191;
                        } else {
                            if (lng < -68.0) {
                                return 181;
                            } else {
                                if (lat < -10.5) {
                                    return 191;
                                } else {
                                    return 181;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -68.25) {
                        if (lat < -9.0) {
                            return 181;
                        } else {
                            if (lng < -68.75) {
                                if (lat < -8.75) {
                                    return 181;
                                } else {
                                    return 242;
                                }
                            } else {
                                return 242;
                            }
                        }
                    } else {
                        if (lat < -9.25) {
                            return 181;
                        } else {
                            if (lng < -68.0) {
                                if (lat < -9.0) {
                                    return 181;
                                } else {
                                    return 242;
                                }
                            } else {
                                if (lat < -9.0) {
                                    return 242;
                                } else {
                                    if (lng < -67.75) {
                                        return 242;
                                    } else {
                                        return 7;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -69.0) {
                if (lat < -8.0) {
                    if (lng < -69.75) {
                        if (lng < -70.0) {
                            return 181;
                        } else {
                            if (lat < -8.25) {
                                return 181;
                            } else {
                                return 242;
                            }
                        }
                    } else {
                        if (lng < -69.5) {
                            if (lat < -8.25) {
                                return 181;
                            } else {
                                return 242;
                            }
                        } else {
                            return 242;
                        }
                    }
                } else {
                    return 242;
                }
            } else {
                if (lat < -7.25) {
                    if (lng < -68.25) {
                        return 242;
                    } else {
                        if (lat < -8.0) {
                            if (lng < -68.0) {
                                return 242;
                            } else {
                                return 7;
                            }
                        } else {
                            if (lng < -68.0) {
                                if (lat < -7.75) {
                                    return 242;
                                } else {
                                    return 7;
                                }
                            } else {
                                return 7;
                            }
                        }
                    }
                } else {
                    if (lng < -68.5) {
                        if (lat < -6.5) {
                            if (lat < -6.75) {
                                return 242;
                            } else {
                                if (lng < -68.75) {
                                    return 242;
                                } else {
                                    return 7;
                                }
                            }
                        } else {
                            if (lat < -6.25) {
                                if (lng < -68.75) {
                                    return 242;
                                } else {
                                    return 7;
                                }
                            } else {
                                return 7;
                            }
                        }
                    } else {
                        return 7;
                    }
                }
            }
        }
    }

    public static int kdLookup28(double lat, double lng)
    {
        if (lng < -70.5) {
            if (lat < -3.0) {
                if (lng < -72.0) {
                    if (lat < -4.75) {
                        if (lng < -72.75) {
                            if (lat < -5.5) {
                                if (lng < -73.0) {
                                    return 165;
                                } else {
                                    return 242;
                                }
                            } else {
                                return 165;
                            }
                        } else {
                            if (lat < -5.0) {
                                return 242;
                            } else {
                                if (lng < -72.5) {
                                    return 165;
                                } else {
                                    return 242;
                                }
                            }
                        }
                    } else {
                        return 165;
                    }
                } else {
                    if (lat < -4.5) {
                        return 242;
                    } else {
                        if (lng < -71.25) {
                            if (lat < -4.25) {
                                if (lng < -71.75) {
                                    return 165;
                                } else {
                                    return 242;
                                }
                            } else {
                                return 165;
                            }
                        } else {
                            if (lat < -4.0) {
                                if (lng < -71.0) {
                                    if (lat < -4.25) {
                                        return 242;
                                    } else {
                                        return 165;
                                    }
                                } else {
                                    if (lng < -70.75) {
                                        if (lat < -4.25) {
                                            return 242;
                                        } else {
                                            return 165;
                                        }
                                    } else {
                                        return 242;
                                    }
                                }
                            } else {
                                return 165;
                            }
                        }
                    }
                }
            } else {
                if (lat < -1.75) {
                    if (lng < -72.0) {
                        if (lng < -72.75) {
                            if (lat < -2.25) {
                                return 165;
                            } else {
                                if (lng < -73.0) {
                                    return 165;
                                } else {
                                    return 391;
                                }
                            }
                        } else {
                            if (lat < -2.25) {
                                return 165;
                            } else {
                                return 391;
                            }
                        }
                    } else {
                        if (lng < -71.25) {
                            if (lat < -2.25) {
                                return 165;
                            } else {
                                if (lng < -71.75) {
                                    return 391;
                                } else {
                                    if (lng < -71.5) {
                                        if (lat < -2.0) {
                                            return 165;
                                        } else {
                                            return 391;
                                        }
                                    } else {
                                        if (lat < -2.0) {
                                            return 165;
                                        } else {
                                            return 391;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -2.25) {
                                return 165;
                            } else {
                                return 391;
                            }
                        }
                    }
                } else {
                    return 391;
                }
            }
        } else {
            if (lat < -3.0) {
                if (lng < -69.25) {
                    if (lat < -4.5) {
                        if (lng < -69.5) {
                            return 242;
                        } else {
                            if (lat < -5.0) {
                                return 242;
                            } else {
                                return 7;
                            }
                        }
                    } else {
                        if (lat < -3.75) {
                            if (lng < -70.0) {
                                if (lat < -4.25) {
                                    return 242;
                                } else {
                                    if (lng < -70.25) {
                                        if (lat < -4.0) {
                                            return 242;
                                        } else {
                                            return 165;
                                        }
                                    } else {
                                        return 165;
                                    }
                                }
                            } else {
                                if (lng < -69.75) {
                                    if (lat < -4.25) {
                                        return 242;
                                    } else {
                                        if (lat < -4.0) {
                                            return 165;
                                        } else {
                                            return 391;
                                        }
                                    }
                                } else {
                                    return 7;
                                }
                            }
                        } else {
                            if (lng < -70.0) {
                                if (lat < -3.25) {
                                    return 391;
                                } else {
                                    if (lng < -70.25) {
                                        return 165;
                                    } else {
                                        return 391;
                                    }
                                }
                            } else {
                                if (lng < -69.75) {
                                    return 391;
                                } else {
                                    return 7;
                                }
                            }
                        }
                    }
                } else {
                    return 7;
                }
            } else {
                if (lng < -69.25) {
                    if (lat < -1.5) {
                        if (lat < -2.25) {
                            if (lng < -70.0) {
                                if (lat < -2.5) {
                                    return 165;
                                } else {
                                    if (lng < -70.25) {
                                        return 165;
                                    } else {
                                        return 391;
                                    }
                                }
                            } else {
                                if (lng < -69.5) {
                                    return 391;
                                } else {
                                    return 7;
                                }
                            }
                        } else {
                            if (lng < -69.5) {
                                return 391;
                            } else {
                                if (lat < -1.75) {
                                    return 7;
                                } else {
                                    return 391;
                                }
                            }
                        }
                    } else {
                        if (lat < -0.75) {
                            return 391;
                        } else {
                            if (lng < -69.75) {
                                return 391;
                            } else {
                                if (lat < -0.5) {
                                    if (lng < -69.5) {
                                        return 391;
                                    } else {
                                        return 7;
                                    }
                                } else {
                                    if (lng < -69.5) {
                                        if (lat < -0.25) {
                                            return 391;
                                        } else {
                                            return 7;
                                        }
                                    } else {
                                        return 7;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 7;
                }
            }
        }
    }

    public static int kdLookup29(double lat, double lng)
    {
        if (lng < -78.75) {
            if (lat < -11.25) {
                return 0;
            } else {
                if (lng < -84.5) {
                    return 114;
                } else {
                    if (lat < -5.75) {
                        return 165;
                    } else {
                        if (lng < -81.75) {
                            return 0;
                        } else {
                            if (lat < -3.25) {
                                if (lng < -80.25) {
                                    return 165;
                                } else {
                                    if (lat < -4.5) {
                                        if (lng < -79.25) {
                                            return 165;
                                        } else {
                                            if (lat < -4.75) {
                                                return 165;
                                            } else {
                                                return 382;
                                            }
                                        }
                                    } else {
                                        if (lng < -79.5) {
                                            if (lat < -4.0) {
                                                if (lng < -80.0) {
                                                    if (lat < -4.25) {
                                                        return 165;
                                                    } else {
                                                        return 382;
                                                    }
                                                } else {
                                                    if (lng < -79.75) {
                                                        if (lat < -4.25) {
                                                            return 165;
                                                        } else {
                                                            return 382;
                                                        }
                                                    } else {
                                                        if (lat < -4.25) {
                                                            return 165;
                                                        } else {
                                                            return 382;
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (lng < -80.0) {
                                                    if (lat < -3.75) {
                                                        return 382;
                                                    } else {
                                                        return 165;
                                                    }
                                                } else {
                                                    return 382;
                                                }
                                            }
                                        } else {
                                            return 382;
                                        }
                                    }
                                }
                            } else {
                                return 382;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -11.25) {
                if (lng < -73.25) {
                    return 165;
                } else {
                    return TimeZoneMapperMethodsOne.kdLookup25(lat,lng);
                }
            } else {
                if (lng < -73.25) {
                    return TimeZoneMapperMethodsOne.kdLookup26(lat,lng);
                } else {
                    if (lat < -5.75) {
                        if (lng < -70.5) {
                            if (lat < -8.5) {
                                if (lng < -72.0) {
                                    if (lat < -9.75) {
                                        return 165;
                                    } else {
                                        if (lng < -72.75) {
                                            if (lat < -9.25) {
                                                return 165;
                                            } else {
                                                if (lat < -9.0) {
                                                    if (lng < -73.0) {
                                                        return 165;
                                                    } else {
                                                        return 181;
                                                    }
                                                } else {
                                                    if (lng < -73.0) {
                                                        return 165;
                                                    } else {
                                                        if (lat < -8.75) {
                                                            return 165;
                                                        } else {
                                                            return 181;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lat < -9.25) {
                                                if (lng < -72.25) {
                                                    return 165;
                                                } else {
                                                    return 181;
                                                }
                                            } else {
                                                return 181;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < -10.0) {
                                        return 165;
                                    } else {
                                        if (lng < -71.25) {
                                            if (lat < -9.75) {
                                                if (lng < -71.5) {
                                                    return 181;
                                                } else {
                                                    return 165;
                                                }
                                            } else {
                                                return 181;
                                            }
                                        } else {
                                            if (lat < -9.5) {
                                                if (lng < -71.0) {
                                                    if (lat < -9.75) {
                                                        return 165;
                                                    } else {
                                                        return 181;
                                                    }
                                                } else {
                                                    if (lng < -70.75) {
                                                        if (lat < -9.75) {
                                                            return 165;
                                                        } else {
                                                            return 181;
                                                        }
                                                    } else {
                                                        return 165;
                                                    }
                                                }
                                            } else {
                                                return 181;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -72.0) {
                                    if (lat < -7.25) {
                                        if (lng < -72.75) {
                                            return 181;
                                        } else {
                                            if (lat < -7.5) {
                                                return 181;
                                            } else {
                                                return 242;
                                            }
                                        }
                                    } else {
                                        if (lat < -6.5) {
                                            return 242;
                                        } else {
                                            if (lng < -73.0) {
                                                return 165;
                                            } else {
                                                return 242;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < -7.75) {
                                        if (lng < -70.75) {
                                            return 181;
                                        } else {
                                            if (lat < -8.0) {
                                                return 181;
                                            } else {
                                                return 242;
                                            }
                                        }
                                    } else {
                                        return 242;
                                    }
                                }
                            }
                        } else {
                            return kdLookup27(lat,lng);
                        }
                    } else {
                        return kdLookup28(lat,lng);
                    }
                }
            }
        }
    }

    public static int kdLookup30(double lat, double lng)
    {
        if (lat < -39.5) {
            if (lng < -64.75) {
                if (lat < -42.25) {
                    return 88;
                } else {
                    if (lng < -66.25) {
                        if (lat < -41.75) {
                            return 88;
                        } else {
                            return 128;
                        }
                    } else {
                        if (lat < -41.75) {
                            return 88;
                        } else {
                            return 128;
                        }
                    }
                }
            } else {
                if (lat < -42.25) {
                    return 88;
                } else {
                    if (lng < -63.5) {
                        if (lat < -41.25) {
                            return 88;
                        } else {
                            return 128;
                        }
                    } else {
                        if (lat < -41.0) {
                            if (lng < -62.75) {
                                if (lat < -41.75) {
                                    return 88;
                                } else {
                                    return 128;
                                }
                            } else {
                                return 227;
                            }
                        } else {
                            if (lng < -62.75) {
                                if (lat < -40.25) {
                                    if (lng < -63.25) {
                                        return 128;
                                    } else {
                                        if (lat < -40.75) {
                                            return 128;
                                        } else {
                                            if (lng < -63.0) {
                                                if (lat < -40.5) {
                                                    return 128;
                                                } else {
                                                    return 227;
                                                }
                                            } else {
                                                return 227;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -63.25) {
                                        return 128;
                                    } else {
                                        return 227;
                                    }
                                }
                            } else {
                                return 227;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -36.75) {
                if (lng < -63.25) {
                    return 128;
                } else {
                    return 227;
                }
            } else {
                if (lng < -64.75) {
                    if (lat < -35.25) {
                        if (lng < -66.25) {
                            if (lat < -36.0) {
                                return 128;
                            } else {
                                if (lng < -67.0) {
                                    return 14;
                                } else {
                                    if (lng < -66.75) {
                                        if (lat < -35.75) {
                                            return 128;
                                        } else {
                                            return 14;
                                        }
                                    } else {
                                        if (lat < -35.75) {
                                            if (lng < -66.5) {
                                                return 128;
                                            } else {
                                                return 315;
                                            }
                                        } else {
                                            if (lng < -66.5) {
                                                return 14;
                                            } else {
                                                return 315;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -65.5) {
                                if (lat < -36.0) {
                                    return 128;
                                } else {
                                    if (lng < -66.0) {
                                        return 315;
                                    } else {
                                        if (lat < -35.75) {
                                            if (lng < -65.75) {
                                                return 128;
                                            } else {
                                                return 315;
                                            }
                                        } else {
                                            return 315;
                                        }
                                    }
                                }
                            } else {
                                if (lat < -36.0) {
                                    return 128;
                                } else {
                                    if (lng < -65.0) {
                                        return 315;
                                    } else {
                                        return 128;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -66.25) {
                            if (lat < -34.5) {
                                if (lng < -66.5) {
                                    return 14;
                                } else {
                                    return 315;
                                }
                            } else {
                                if (lng < -66.75) {
                                    return 14;
                                } else {
                                    return 315;
                                }
                            }
                        } else {
                            if (lng < -65.0) {
                                return 315;
                            } else {
                                if (lat < -34.75) {
                                    return 128;
                                } else {
                                    return 279;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < -35.25) {
                        if (lng < -63.25) {
                            return 128;
                        } else {
                            return 227;
                        }
                    } else {
                        if (lng < -63.5) {
                            if (lat < -35.0) {
                                return 128;
                            } else {
                                return 279;
                            }
                        } else {
                            if (lng < -62.75) {
                                if (lat < -34.5) {
                                    if (lng < -63.25) {
                                        if (lat < -35.0) {
                                            return 128;
                                        } else {
                                            return 279;
                                        }
                                    } else {
                                        return 227;
                                    }
                                } else {
                                    if (lng < -63.25) {
                                        return 279;
                                    } else {
                                        if (lat < -34.25) {
                                            return 227;
                                        } else {
                                            return 279;
                                        }
                                    }
                                }
                            } else {
                                if (lat < -34.25) {
                                    return 227;
                                } else {
                                    return 279;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup31(double lat, double lng)
    {
        if (lng < -64.75) {
            if (lat < -24.0) {
                if (lng < -66.25) {
                    if (lat < -25.25) {
                        return 88;
                    } else {
                        return 128;
                    }
                } else {
                    if (lng < -65.5) {
                        return 128;
                    } else {
                        if (lat < -24.5) {
                            return 128;
                        } else {
                            if (lng < -65.25) {
                                if (lat < -24.25) {
                                    return 128;
                                } else {
                                    return 160;
                                }
                            } else {
                                if (lng < -65.0) {
                                    if (lat < -24.25) {
                                        return 128;
                                    } else {
                                        return 160;
                                    }
                                } else {
                                    return 160;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -66.25) {
                    if (lat < -23.25) {
                        if (lng < -67.0) {
                            if (lat < -23.75) {
                                if (lng < -67.25) {
                                    return 31;
                                } else {
                                    return 128;
                                }
                            } else {
                                return 31;
                            }
                        } else {
                            if (lng < -66.75) {
                                if (lat < -23.75) {
                                    return 128;
                                } else {
                                    return 160;
                                }
                            } else {
                                return 160;
                            }
                        }
                    } else {
                        if (lng < -67.0) {
                            if (lat < -22.75) {
                                return 31;
                            } else {
                                return 191;
                            }
                        } else {
                            if (lng < -66.75) {
                                if (lat < -23.0) {
                                    return 160;
                                } else {
                                    if (lat < -22.75) {
                                        return 31;
                                    } else {
                                        return 160;
                                    }
                                }
                            } else {
                                return 160;
                            }
                        }
                    }
                } else {
                    if (lng < -65.5) {
                        if (lat < -23.25) {
                            if (lng < -66.0) {
                                return 128;
                            } else {
                                if (lat < -23.75) {
                                    if (lng < -65.75) {
                                        return 128;
                                    } else {
                                        return 160;
                                    }
                                } else {
                                    return 160;
                                }
                            }
                        } else {
                            return 160;
                        }
                    } else {
                        if (lat < -23.25) {
                            return 160;
                        } else {
                            if (lng < -65.25) {
                                return 160;
                            } else {
                                if (lat < -23.0) {
                                    if (lng < -65.0) {
                                        return 160;
                                    } else {
                                        return 128;
                                    }
                                } else {
                                    if (lng < -65.0) {
                                        if (lat < -22.75) {
                                            return 160;
                                        } else {
                                            return 128;
                                        }
                                    } else {
                                        return 128;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -24.0) {
                if (lng < -63.5) {
                    if (lat < -24.5) {
                        return 128;
                    } else {
                        if (lng < -64.25) {
                            if (lng < -64.5) {
                                return 160;
                            } else {
                                if (lat < -24.25) {
                                    return 128;
                                } else {
                                    return 160;
                                }
                            }
                        } else {
                            if (lng < -64.0) {
                                if (lat < -24.25) {
                                    return 128;
                                } else {
                                    return 160;
                                }
                            } else {
                                return 128;
                            }
                        }
                    }
                } else {
                    if (lng < -62.75) {
                        if (lat < -25.0) {
                            if (lng < -63.25) {
                                return 128;
                            } else {
                                if (lng < -63.0) {
                                    if (lat < -25.25) {
                                        return 279;
                                    } else {
                                        return 128;
                                    }
                                } else {
                                    return 279;
                                }
                            }
                        } else {
                            return 128;
                        }
                    } else {
                        if (lat < -24.75) {
                            return 279;
                        } else {
                            if (lng < -62.5) {
                                return 128;
                            } else {
                                if (lat < -24.5) {
                                    return 279;
                                } else {
                                    if (lng < -62.25) {
                                        return 128;
                                    } else {
                                        return 279;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -63.5) {
                    if (lat < -23.25) {
                        if (lng < -64.25) {
                            if (lat < -23.5) {
                                return 160;
                            } else {
                                if (lng < -64.5) {
                                    return 160;
                                } else {
                                    return 128;
                                }
                            }
                        } else {
                            if (lng < -64.0) {
                                if (lat < -23.5) {
                                    return 160;
                                } else {
                                    return 128;
                                }
                            } else {
                                return 128;
                            }
                        }
                    } else {
                        return 128;
                    }
                } else {
                    if (lng < -62.25) {
                        return 128;
                    } else {
                        return 279;
                    }
                }
            }
        }
    }

    public static int kdLookup32(double lat, double lng)
    {
        if (lat < -28.25) {
            if (lng < -64.75) {
                if (lat < -31.0) {
                    if (lng < -66.25) {
                        if (lat < -32.5) {
                            if (lng < -67.0) {
                                return 14;
                            } else {
                                if (lat < -33.5) {
                                    if (lng < -66.75) {
                                        return 14;
                                    } else {
                                        return 315;
                                    }
                                } else {
                                    return 315;
                                }
                            }
                        } else {
                            if (lat < -31.75) {
                                if (lng < -67.25) {
                                    if (lat < -32.25) {
                                        return 14;
                                    } else {
                                        return 347;
                                    }
                                } else {
                                    return 315;
                                }
                            } else {
                                if (lng < -67.0) {
                                    return 347;
                                } else {
                                    if (lng < -66.75) {
                                        if (lat < -31.5) {
                                            return 347;
                                        } else {
                                            return 250;
                                        }
                                    } else {
                                        return 250;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -32.5) {
                            if (lng < -65.0) {
                                return 315;
                            } else {
                                if (lat < -32.75) {
                                    return 279;
                                } else {
                                    return 315;
                                }
                            }
                        } else {
                            if (lng < -65.5) {
                                if (lat < -31.75) {
                                    return 315;
                                } else {
                                    if (lng < -65.75) {
                                        return 250;
                                    } else {
                                        return 279;
                                    }
                                }
                            } else {
                                if (lat < -31.75) {
                                    if (lng < -65.25) {
                                        return 315;
                                    } else {
                                        if (lat < -32.25) {
                                            return 315;
                                        } else {
                                            if (lng < -65.0) {
                                                if (lat < -32.0) {
                                                    return 315;
                                                } else {
                                                    return 279;
                                                }
                                            } else {
                                                return 279;
                                            }
                                        }
                                    }
                                } else {
                                    return 279;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -66.25) {
                        if (lat < -30.25) {
                            if (lng < -67.0) {
                                if (lat < -30.5) {
                                    return 347;
                                } else {
                                    if (lng < -67.25) {
                                        return 347;
                                    } else {
                                        return 250;
                                    }
                                }
                            } else {
                                return 250;
                            }
                        } else {
                            return 250;
                        }
                    } else {
                        if (lat < -29.75) {
                            if (lng < -65.5) {
                                return 250;
                            } else {
                                if (lat < -30.25) {
                                    return 279;
                                } else {
                                    if (lng < -65.25) {
                                        return 250;
                                    } else {
                                        if (lng < -65.0) {
                                            if (lat < -30.0) {
                                                return 279;
                                            } else {
                                                return 250;
                                            }
                                        } else {
                                            return 279;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -65.5) {
                                if (lat < -29.0) {
                                    if (lng < -65.75) {
                                        return 250;
                                    } else {
                                        if (lat < -29.25) {
                                            return 250;
                                        } else {
                                            return 88;
                                        }
                                    }
                                } else {
                                    if (lng < -66.0) {
                                        if (lat < -28.75) {
                                            return 250;
                                        } else {
                                            return 88;
                                        }
                                    } else {
                                        return 88;
                                    }
                                }
                            } else {
                                if (lat < -29.0) {
                                    if (lng < -65.25) {
                                        if (lat < -29.5) {
                                            return 250;
                                        } else {
                                            return 88;
                                        }
                                    } else {
                                        if (lat < -29.25) {
                                            return 88;
                                        } else {
                                            if (lng < -65.0) {
                                                return 88;
                                            } else {
                                                return 279;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -65.0) {
                                        return 88;
                                    } else {
                                        return 279;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                return 279;
            }
        } else {
            if (lat < -25.5) {
                if (lng < -64.75) {
                    if (lng < -66.25) {
                        if (lat < -27.0) {
                            if (lng < -67.25) {
                                if (lat < -28.0) {
                                    return 250;
                                } else {
                                    return 88;
                                }
                            } else {
                                return 88;
                            }
                        } else {
                            if (lat < -26.25) {
                                return 88;
                            } else {
                                if (lng < -66.75) {
                                    return 88;
                                } else {
                                    if (lat < -26.0) {
                                        if (lng < -66.5) {
                                            return 88;
                                        } else {
                                            return 128;
                                        }
                                    } else {
                                        if (lng < -66.5) {
                                            if (lat < -25.75) {
                                                return 88;
                                            } else {
                                                return 128;
                                            }
                                        } else {
                                            return 128;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -27.0) {
                            if (lng < -65.5) {
                                if (lat < -27.75) {
                                    return 88;
                                } else {
                                    if (lng < -66.0) {
                                        return 88;
                                    } else {
                                        if (lat < -27.5) {
                                            if (lng < -65.75) {
                                                return 88;
                                            } else {
                                                return 130;
                                            }
                                        } else {
                                            if (lng < -65.75) {
                                                if (lat < -27.25) {
                                                    return 88;
                                                } else {
                                                    return 130;
                                                }
                                            } else {
                                                return 130;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < -27.75) {
                                    if (lng < -65.0) {
                                        return 88;
                                    } else {
                                        return 279;
                                    }
                                } else {
                                    if (lng < -65.0) {
                                        return 130;
                                    } else {
                                        if (lat < -27.25) {
                                            return 279;
                                        } else {
                                            return 130;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -65.5) {
                                if (lat < -26.25) {
                                    if (lng < -66.0) {
                                        return 88;
                                    } else {
                                        if (lat < -26.75) {
                                            if (lng < -65.75) {
                                                return 88;
                                            } else {
                                                return 130;
                                            }
                                        } else {
                                            if (lng < -65.75) {
                                                if (lat < -26.5) {
                                                    return 88;
                                                } else {
                                                    return 130;
                                                }
                                            } else {
                                                return 130;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -66.0) {
                                        if (lat < -26.0) {
                                            return 88;
                                        } else {
                                            return 128;
                                        }
                                    } else {
                                        return 128;
                                    }
                                }
                            } else {
                                if (lat < -26.25) {
                                    return 130;
                                } else {
                                    if (lng < -65.25) {
                                        if (lat < -26.0) {
                                            return 130;
                                        } else {
                                            return 128;
                                        }
                                    } else {
                                        if (lat < -26.0) {
                                            if (lng < -65.0) {
                                                return 130;
                                            } else {
                                                return 128;
                                            }
                                        } else {
                                            return 128;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -64.25) {
                        if (lat < -27.0) {
                            return 279;
                        } else {
                            if (lat < -26.25) {
                                if (lat < -26.75) {
                                    if (lng < -64.5) {
                                        return 130;
                                    } else {
                                        return 279;
                                    }
                                } else {
                                    if (lng < -64.5) {
                                        return 130;
                                    } else {
                                        return 279;
                                    }
                                }
                            } else {
                                if (lat < -26.0) {
                                    return 130;
                                } else {
                                    return 128;
                                }
                            }
                        }
                    } else {
                        return 279;
                    }
                }
            } else {
                return kdLookup31(lat,lng);
            }
        }
    }

    public static int kdLookup33(double lat, double lng)
    {
        if (lat < -28.25) {
            if (lng < -58.25) {
                if (lat < -33.25) {
                    if (lng < -60.25) {
                        if (lng < -61.0) {
                            return 279;
                        } else {
                            if (lng < -60.75) {
                                if (lat < -33.5) {
                                    return 227;
                                } else {
                                    return 279;
                                }
                            } else {
                                if (lng < -60.5) {
                                    if (lat < -33.5) {
                                        return 227;
                                    } else {
                                        return 279;
                                    }
                                } else {
                                    if (lat < -33.5) {
                                        return 227;
                                    } else {
                                        return 279;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -59.25) {
                            if (lng < -59.75) {
                                return 227;
                            } else {
                                if (lng < -59.5) {
                                    if (lat < -33.5) {
                                        return 227;
                                    } else {
                                        return 279;
                                    }
                                } else {
                                    if (lat < -33.5) {
                                        return 227;
                                    } else {
                                        return 279;
                                    }
                                }
                            }
                        } else {
                            return 279;
                        }
                    }
                } else {
                    return 279;
                }
            } else {
                if (lat < -31.0) {
                    if (lat < -32.5) {
                        if (lng < -58.0) {
                            if (lat < -33.0) {
                                return 246;
                            } else {
                                return 279;
                            }
                        } else {
                            return 246;
                        }
                    } else {
                        if (lng < -57.75) {
                            if (lat < -31.75) {
                                if (lat < -32.25) {
                                    if (lng < -58.0) {
                                        return 279;
                                    } else {
                                        return 246;
                                    }
                                } else {
                                    if (lng < -58.0) {
                                        return 279;
                                    } else {
                                        return 246;
                                    }
                                }
                            } else {
                                if (lat < -31.5) {
                                    if (lng < -58.0) {
                                        return 279;
                                    } else {
                                        return 246;
                                    }
                                } else {
                                    if (lng < -58.0) {
                                        return 279;
                                    } else {
                                        if (lat < -31.25) {
                                            return 246;
                                        } else {
                                            return 279;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 246;
                        }
                    }
                } else {
                    if (lat < -29.75) {
                        if (lng < -57.25) {
                            if (lat < -30.5) {
                                if (lng < -57.75) {
                                    return 279;
                                } else {
                                    return 246;
                                }
                            } else {
                                if (lng < -57.75) {
                                    return 279;
                                } else {
                                    if (lat < -30.25) {
                                        return 246;
                                    } else {
                                        if (lng < -57.5) {
                                            return 279;
                                        } else {
                                            if (lat < -30.0) {
                                                return 330;
                                            } else {
                                                return 279;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -30.25) {
                                return 246;
                            } else {
                                if (lng < -56.75) {
                                    if (lng < -57.0) {
                                        return 330;
                                    } else {
                                        if (lat < -30.0) {
                                            return 246;
                                        } else {
                                            return 330;
                                        }
                                    }
                                } else {
                                    if (lng < -56.5) {
                                        if (lat < -30.0) {
                                            return 246;
                                        } else {
                                            return 330;
                                        }
                                    } else {
                                        return 330;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -57.0) {
                            return 279;
                        } else {
                            if (lat < -29.0) {
                                if (lng < -56.75) {
                                    if (lat < -29.5) {
                                        return 330;
                                    } else {
                                        return 279;
                                    }
                                } else {
                                    if (lat < -29.25) {
                                        return 330;
                                    } else {
                                        if (lng < -56.5) {
                                            return 279;
                                        } else {
                                            return 330;
                                        }
                                    }
                                }
                            } else {
                                return 279;
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -59.25) {
                if (lat < -24.25) {
                    return 279;
                } else {
                    if (lng < -60.75) {
                        if (lat < -23.5) {
                            if (lng < -61.0) {
                                return 279;
                            } else {
                                if (lat < -23.75) {
                                    return 279;
                                } else {
                                    return 220;
                                }
                            }
                        } else {
                            if (lng < -61.5) {
                                if (lat < -23.0) {
                                    return 279;
                                } else {
                                    return 220;
                                }
                            } else {
                                if (lat < -23.25) {
                                    if (lng < -61.25) {
                                        return 279;
                                    } else {
                                        return 220;
                                    }
                                } else {
                                    return 220;
                                }
                            }
                        }
                    } else {
                        if (lat < -23.75) {
                            if (lng < -60.0) {
                                if (lng < -60.25) {
                                    return 279;
                                } else {
                                    if (lat < -24.0) {
                                        return 279;
                                    } else {
                                        return 220;
                                    }
                                }
                            } else {
                                if (lng < -59.75) {
                                    if (lat < -24.0) {
                                        return 279;
                                    } else {
                                        return 220;
                                    }
                                } else {
                                    if (lng < -59.5) {
                                        if (lat < -24.0) {
                                            return 279;
                                        } else {
                                            return 220;
                                        }
                                    } else {
                                        return 220;
                                    }
                                }
                            }
                        } else {
                            return 220;
                        }
                    }
                }
            } else {
                if (lat < -25.5) {
                    if (lng < -57.75) {
                        if (lat < -27.0) {
                            if (lng < -58.5) {
                                return 279;
                            } else {
                                if (lat < -27.25) {
                                    return 279;
                                } else {
                                    return 220;
                                }
                            }
                        } else {
                            if (lng < -58.25) {
                                return 279;
                            } else {
                                if (lat < -26.25) {
                                    if (lat < -26.75) {
                                        return 220;
                                    } else {
                                        if (lng < -58.0) {
                                            return 279;
                                        } else {
                                            return 220;
                                        }
                                    }
                                } else {
                                    if (lat < -26.0) {
                                        if (lng < -58.0) {
                                            return 279;
                                        } else {
                                            return 220;
                                        }
                                    } else {
                                        return 279;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -27.25) {
                            if (lng < -57.0) {
                                return 279;
                            } else {
                                if (lat < -27.5) {
                                    return 279;
                                } else {
                                    return 220;
                                }
                            }
                        } else {
                            return 220;
                        }
                    }
                } else {
                    if (lng < -57.75) {
                        if (lat < -24.5) {
                            if (lng < -58.5) {
                                if (lat < -24.75) {
                                    return 279;
                                } else {
                                    if (lng < -58.75) {
                                        return 279;
                                    } else {
                                        return 220;
                                    }
                                }
                            } else {
                                if (lat < -25.0) {
                                    return 279;
                                } else {
                                    if (lng < -58.25) {
                                        if (lat < -24.75) {
                                            return 279;
                                        } else {
                                            return 220;
                                        }
                                    } else {
                                        if (lng < -58.0) {
                                            if (lat < -24.75) {
                                                return 279;
                                            } else {
                                                return 220;
                                            }
                                        } else {
                                            return 220;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 220;
                        }
                    } else {
                        if (lat < -25.0) {
                            if (lng < -57.5) {
                                return 279;
                            } else {
                                return 220;
                            }
                        } else {
                            return 220;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup34(double lat, double lng)
    {
        if (lat < -25.5) {
            if (lng < -53.5) {
                if (lng < -55.0) {
                    if (lat < -27.0) {
                        if (lng < -55.75) {
                            if (lat < -27.25) {
                                return 279;
                            } else {
                                return 220;
                            }
                        } else {
                            if (lat < -27.75) {
                                if (lng < -55.5) {
                                    if (lat < -28.0) {
                                        return 330;
                                    } else {
                                        return 279;
                                    }
                                } else {
                                    if (lng < -55.25) {
                                        if (lat < -28.0) {
                                            return 330;
                                        } else {
                                            return 279;
                                        }
                                    } else {
                                        return 330;
                                    }
                                }
                            } else {
                                if (lng < -55.5) {
                                    if (lat < -27.25) {
                                        return 279;
                                    } else {
                                        return 220;
                                    }
                                } else {
                                    return 279;
                                }
                            }
                        }
                    } else {
                        if (lat < -26.75) {
                            if (lng < -55.25) {
                                return 220;
                            } else {
                                return 279;
                            }
                        } else {
                            return 220;
                        }
                    }
                } else {
                    if (lat < -27.0) {
                        if (lng < -54.25) {
                            if (lat < -27.75) {
                                return 330;
                            } else {
                                if (lng < -54.75) {
                                    return 279;
                                } else {
                                    if (lat < -27.5) {
                                        return 330;
                                    } else {
                                        if (lng < -54.5) {
                                            return 279;
                                        } else {
                                            if (lat < -27.25) {
                                                return 330;
                                            } else {
                                                return 279;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -27.25) {
                                return 330;
                            } else {
                                if (lng < -54.0) {
                                    return 279;
                                } else {
                                    return 330;
                                }
                            }
                        }
                    } else {
                        if (lng < -54.25) {
                            if (lat < -26.25) {
                                if (lng < -54.75) {
                                    if (lat < -26.75) {
                                        return 279;
                                    } else {
                                        return 220;
                                    }
                                } else {
                                    return 279;
                                }
                            } else {
                                if (lng < -54.5) {
                                    return 220;
                                } else {
                                    return 279;
                                }
                            }
                        } else {
                            if (lat < -26.0) {
                                return 279;
                            } else {
                                if (lng < -53.75) {
                                    return 279;
                                } else {
                                    return 330;
                                }
                            }
                        }
                    }
                }
            } else {
                return 330;
            }
        } else {
            if (lng < -53.5) {
                if (lat < -24.0) {
                    if (lng < -54.5) {
                        return 220;
                    } else {
                        if (lat < -24.75) {
                            if (lng < -54.25) {
                                if (lat < -25.25) {
                                    return 330;
                                } else {
                                    return 220;
                                }
                            } else {
                                return 330;
                            }
                        } else {
                            if (lng < -54.25) {
                                return 220;
                            } else {
                                return 330;
                            }
                        }
                    }
                } else {
                    if (lng < -55.0) {
                        if (lat < -23.25) {
                            if (lng < -55.5) {
                                return 220;
                            } else {
                                if (lat < -23.75) {
                                    return 220;
                                } else {
                                    if (lng < -55.25) {
                                        if (lat < -23.5) {
                                            return 220;
                                        } else {
                                            return 318;
                                        }
                                    } else {
                                        return 318;
                                    }
                                }
                            }
                        } else {
                            if (lng < -55.5) {
                                return 220;
                            } else {
                                return 318;
                            }
                        }
                    } else {
                        if (lng < -54.25) {
                            if (lat < -23.75) {
                                return 220;
                            } else {
                                return 318;
                            }
                        } else {
                            if (lat < -23.25) {
                                if (lng < -54.0) {
                                    return 318;
                                } else {
                                    if (lat < -23.5) {
                                        return 330;
                                    } else {
                                        if (lng < -53.75) {
                                            return 318;
                                        } else {
                                            return 330;
                                        }
                                    }
                                }
                            } else {
                                return 318;
                            }
                        }
                    }
                }
            } else {
                if (lat < -22.75) {
                    return 330;
                } else {
                    if (lng < -53.25) {
                        return 318;
                    } else {
                        return 330;
                    }
                }
            }
        }
    }

    public static int kdLookup35(double lat, double lng)
    {
        if (lng < -56.25) {
            if (lat < -33.75) {
                if (lng < -62.0) {
                    return kdLookup30(lat,lng);
                } else {
                    if (lat < -39.5) {
                        return 227;
                    } else {
                        if (lng < -59.25) {
                            if (lat < -36.75) {
                                return 227;
                            } else {
                                if (lat < -34.25) {
                                    return 227;
                                } else {
                                    if (lng < -61.25) {
                                        if (lng < -61.5) {
                                            return 279;
                                        } else {
                                            if (lat < -34.0) {
                                                return 227;
                                            } else {
                                                return 279;
                                            }
                                        }
                                    } else {
                                        return 227;
                                    }
                                }
                            }
                        } else {
                            if (lat < -36.75) {
                                return 227;
                            } else {
                                if (lng < -57.75) {
                                    if (lat < -34.5) {
                                        return 227;
                                    } else {
                                        if (lng < -58.5) {
                                            return 227;
                                        } else {
                                            if (lng < -58.25) {
                                                if (lat < -34.0) {
                                                    return 227;
                                                } else {
                                                    return 279;
                                                }
                                            } else {
                                                if (lat < -34.25) {
                                                    if (lng < -58.0) {
                                                        return 227;
                                                    } else {
                                                        return 246;
                                                    }
                                                } else {
                                                    return 246;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < -35.25) {
                                        return 227;
                                    } else {
                                        if (lng < -57.0) {
                                            if (lat < -34.5) {
                                                return 227;
                                            } else {
                                                return 246;
                                            }
                                        } else {
                                            return 246;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -62.0) {
                    return kdLookup32(lat,lng);
                } else {
                    return kdLookup33(lat,lng);
                }
            }
        } else {
            if (lat < -33.75) {
                return 246;
            } else {
                if (lng < -50.75) {
                    if (lat < -28.25) {
                        if (lng < -53.5) {
                            if (lat < -31.0) {
                                if (lng < -55.0) {
                                    if (lat < -31.25) {
                                        return 246;
                                    } else {
                                        if (lng < -55.25) {
                                            return 246;
                                        } else {
                                            return 330;
                                        }
                                    }
                                } else {
                                    if (lat < -32.0) {
                                        return 246;
                                    } else {
                                        if (lng < -54.25) {
                                            if (lat < -31.5) {
                                                return 246;
                                            } else {
                                                if (lng < -54.75) {
                                                    if (lat < -31.25) {
                                                        return 246;
                                                    } else {
                                                        return 330;
                                                    }
                                                } else {
                                                    if (lng < -54.5) {
                                                        if (lat < -31.25) {
                                                            return 246;
                                                        } else {
                                                            return 330;
                                                        }
                                                    } else {
                                                        return 330;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lat < -31.75) {
                                                if (lng < -53.75) {
                                                    return 246;
                                                } else {
                                                    return 330;
                                                }
                                            } else {
                                                return 330;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -55.25) {
                                    if (lat < -29.75) {
                                        if (lat < -30.5) {
                                            if (lng < -55.75) {
                                                if (lng < -56.0) {
                                                    return 246;
                                                } else {
                                                    return 330;
                                                }
                                            } else {
                                                if (lng < -55.5) {
                                                    if (lat < -30.75) {
                                                        return 246;
                                                    } else {
                                                        return 330;
                                                    }
                                                } else {
                                                    if (lat < -30.75) {
                                                        return 246;
                                                    } else {
                                                        return 330;
                                                    }
                                                }
                                            }
                                        } else {
                                            return 330;
                                        }
                                    } else {
                                        if (lat < -28.75) {
                                            return 330;
                                        } else {
                                            if (lng < -55.75) {
                                                if (lng < -56.0) {
                                                    return 279;
                                                } else {
                                                    if (lat < -28.5) {
                                                        return 330;
                                                    } else {
                                                        return 279;
                                                    }
                                                }
                                            } else {
                                                return 330;
                                            }
                                        }
                                    }
                                } else {
                                    return 330;
                                }
                            }
                        } else {
                            if (lat < -31.5) {
                                if (lng < -52.25) {
                                    if (lat < -32.75) {
                                        if (lng < -53.25) {
                                            if (lat < -33.25) {
                                                if (lat < -33.5) {
                                                    return 246;
                                                } else {
                                                    return 330;
                                                }
                                            } else {
                                                if (lat < -33.0) {
                                                    return 330;
                                                } else {
                                                    return 246;
                                                }
                                            }
                                        } else {
                                            return 330;
                                        }
                                    } else {
                                        if (lng < -53.0) {
                                            if (lat < -32.25) {
                                                if (lng < -53.25) {
                                                    return 246;
                                                } else {
                                                    if (lat < -32.5) {
                                                        return 246;
                                                    } else {
                                                        return 330;
                                                    }
                                                }
                                            } else {
                                                return 330;
                                            }
                                        } else {
                                            return 330;
                                        }
                                    }
                                } else {
                                    return 330;
                                }
                            } else {
                                return 330;
                            }
                        }
                    } else {
                        return kdLookup34(lat,lng);
                    }
                } else {
                    return 330;
                }
            }
        }
    }

    public static int kdLookup36(double lat, double lng)
    {
        if (lat < -17.0) {
            if (lng < -64.75) {
                if (lat < -21.75) {
                    if (lng < -66.25) {
                        if (lng < -66.75) {
                            return 191;
                        } else {
                            if (lat < -22.25) {
                                return 160;
                            } else {
                                if (lng < -66.5) {
                                    return 191;
                                } else {
                                    if (lat < -22.0) {
                                        return 160;
                                    } else {
                                        return 191;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -65.5) {
                            if (lng < -65.75) {
                                return 160;
                            } else {
                                if (lat < -22.0) {
                                    return 160;
                                } else {
                                    return 191;
                                }
                            }
                        } else {
                            if (lng < -65.25) {
                                if (lat < -22.0) {
                                    return 160;
                                } else {
                                    return 191;
                                }
                            } else {
                                if (lat < -22.25) {
                                    return 128;
                                } else {
                                    if (lng < -65.0) {
                                        if (lat < -22.0) {
                                            return 160;
                                        } else {
                                            return 191;
                                        }
                                    } else {
                                        if (lat < -22.0) {
                                            return 128;
                                        } else {
                                            return 191;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 191;
                }
            } else {
                if (lat < -20.5) {
                    if (lng < -63.5) {
                        if (lat < -22.0) {
                            if (lng < -64.25) {
                                if (lng < -64.5) {
                                    return 128;
                                } else {
                                    if (lat < -22.25) {
                                        return 128;
                                    } else {
                                        return 191;
                                    }
                                }
                            } else {
                                if (lng < -64.0) {
                                    return 191;
                                } else {
                                    return 128;
                                }
                            }
                        } else {
                            return 191;
                        }
                    } else {
                        if (lat < -21.5) {
                            if (lng < -62.75) {
                                if (lat < -22.0) {
                                    return 128;
                                } else {
                                    return 191;
                                }
                            } else {
                                if (lat < -22.0) {
                                    if (lng < -62.5) {
                                        return 128;
                                    } else {
                                        if (lng < -62.25) {
                                            if (lat < -22.25) {
                                                return 128;
                                            } else {
                                                return 220;
                                            }
                                        } else {
                                            return 220;
                                        }
                                    }
                                } else {
                                    if (lng < -62.5) {
                                        return 191;
                                    } else {
                                        if (lng < -62.25) {
                                            if (lat < -21.75) {
                                                return 220;
                                            } else {
                                                return 191;
                                            }
                                        } else {
                                            return 220;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -62.25) {
                                return 191;
                            } else {
                                return 220;
                            }
                        }
                    }
                } else {
                    return 191;
                }
            }
        } else {
            if (lat < -13.0) {
                return 191;
            } else {
                if (lng < -64.75) {
                    if (lng < -65.25) {
                        return 191;
                    } else {
                        if (lat < -11.75) {
                            return 191;
                        } else {
                            if (lng < -65.0) {
                                if (lat < -11.5) {
                                    return 191;
                                } else {
                                    return 342;
                                }
                            } else {
                                return 342;
                            }
                        }
                    }
                } else {
                    if (lng < -63.5) {
                        if (lat < -12.25) {
                            if (lng < -64.0) {
                                return 191;
                            } else {
                                if (lat < -12.5) {
                                    return 191;
                                } else {
                                    if (lng < -63.75) {
                                        return 342;
                                    } else {
                                        return 191;
                                    }
                                }
                            }
                        } else {
                            if (lng < -64.25) {
                                if (lat < -12.0) {
                                    return 191;
                                } else {
                                    return 342;
                                }
                            } else {
                                return 342;
                            }
                        }
                    } else {
                        if (lat < -12.5) {
                            if (lng < -62.75) {
                                if (lng < -63.0) {
                                    return 191;
                                } else {
                                    if (lat < -12.75) {
                                        return 191;
                                    } else {
                                        return 342;
                                    }
                                }
                            } else {
                                if (lng < -62.5) {
                                    if (lat < -12.75) {
                                        return 191;
                                    } else {
                                        return 342;
                                    }
                                } else {
                                    return 342;
                                }
                            }
                        } else {
                            return 342;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup37(double lat, double lng)
    {
        if (lat < -17.0) {
            if (lng < -59.25) {
                if (lat < -19.75) {
                    if (lng < -61.75) {
                        if (lat < -20.0) {
                            return 220;
                        } else {
                            return 191;
                        }
                    } else {
                        return 220;
                    }
                } else {
                    if (lng < -60.75) {
                        if (lat < -19.5) {
                            if (lng < -61.75) {
                                return 191;
                            } else {
                                return 220;
                            }
                        } else {
                            return 191;
                        }
                    } else {
                        if (lat < -19.25) {
                            return 220;
                        } else {
                            return 191;
                        }
                    }
                }
            } else {
                if (lat < -19.75) {
                    if (lng < -57.75) {
                        if (lat < -20.5) {
                            return 220;
                        } else {
                            if (lng < -58.0) {
                                return 220;
                            } else {
                                if (lat < -20.0) {
                                    return 318;
                                } else {
                                    return 191;
                                }
                            }
                        }
                    } else {
                        if (lat < -22.0) {
                            return 220;
                        } else {
                            return 318;
                        }
                    }
                } else {
                    if (lng < -57.75) {
                        if (lat < -18.5) {
                            if (lng < -58.5) {
                                if (lat < -19.25) {
                                    if (lng < -58.75) {
                                        return 220;
                                    } else {
                                        if (lat < -19.5) {
                                            return 220;
                                        } else {
                                            return 191;
                                        }
                                    }
                                } else {
                                    return 191;
                                }
                            } else {
                                if (lat < -19.25) {
                                    if (lng < -58.25) {
                                        if (lat < -19.5) {
                                            return 220;
                                        } else {
                                            return 191;
                                        }
                                    } else {
                                        if (lng < -58.0) {
                                            return 191;
                                        } else {
                                            return 318;
                                        }
                                    }
                                } else {
                                    return 191;
                                }
                            }
                        } else {
                            if (lng < -58.25) {
                                return 191;
                            } else {
                                if (lat < -17.5) {
                                    return 191;
                                } else {
                                    if (lng < -58.0) {
                                        if (lat < -17.25) {
                                            return 191;
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        return 364;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -18.5) {
                            if (lng < -57.5) {
                                if (lat < -19.0) {
                                    return 318;
                                } else {
                                    return 191;
                                }
                            } else {
                                return 318;
                            }
                        } else {
                            if (lng < -57.0) {
                                if (lat < -17.75) {
                                    if (lng < -57.5) {
                                        return 191;
                                    } else {
                                        return 318;
                                    }
                                } else {
                                    if (lng < -57.5) {
                                        if (lat < -17.5) {
                                            return 191;
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        return 364;
                                    }
                                }
                            } else {
                                if (lat < -17.5) {
                                    return 318;
                                } else {
                                    if (lng < -56.75) {
                                        return 364;
                                    } else {
                                        if (lng < -56.5) {
                                            if (lat < -17.25) {
                                                return 318;
                                            } else {
                                                return 364;
                                            }
                                        } else {
                                            if (lat < -17.25) {
                                                return 318;
                                            } else {
                                                return 364;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -59.25) {
                if (lat < -14.25) {
                    if (lng < -60.25) {
                        return 191;
                    } else {
                        if (lat < -15.75) {
                            if (lat < -16.25) {
                                return 191;
                            } else {
                                if (lng < -60.0) {
                                    return 191;
                                } else {
                                    return 364;
                                }
                            }
                        } else {
                            if (lat < -15.25) {
                                if (lng < -60.0) {
                                    return 191;
                                } else {
                                    return 364;
                                }
                            } else {
                                return 364;
                            }
                        }
                    }
                } else {
                    if (lat < -12.75) {
                        if (lng < -60.75) {
                            if (lat < -13.5) {
                                return 191;
                            } else {
                                if (lng < -61.75) {
                                    if (lat < -13.25) {
                                        return 191;
                                    } else {
                                        return 342;
                                    }
                                } else {
                                    return 342;
                                }
                            }
                        } else {
                            if (lng < -60.0) {
                                if (lat < -13.5) {
                                    if (lng < -60.5) {
                                        return 191;
                                    } else {
                                        if (lat < -14.0) {
                                            if (lng < -60.25) {
                                                return 191;
                                            } else {
                                                return 364;
                                            }
                                        } else {
                                            if (lng < -60.25) {
                                                if (lat < -13.75) {
                                                    return 191;
                                                } else {
                                                    return 364;
                                                }
                                            } else {
                                                return 364;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -60.5) {
                                        return 342;
                                    } else {
                                        if (lat < -13.25) {
                                            return 364;
                                        } else {
                                            if (lng < -60.25) {
                                                return 342;
                                            } else {
                                                if (lat < -13.0) {
                                                    return 364;
                                                } else {
                                                    return 342;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 364;
                            }
                        }
                    } else {
                        if (lng < -60.0) {
                            return 342;
                        } else {
                            if (lat < -12.0) {
                                if (lng < -59.75) {
                                    if (lat < -12.5) {
                                        return 364;
                                    } else {
                                        return 342;
                                    }
                                } else {
                                    return 364;
                                }
                            } else {
                                if (lng < -59.75) {
                                    if (lat < -11.75) {
                                        return 342;
                                    } else {
                                        if (lat < -11.5) {
                                            return 364;
                                        } else {
                                            return 342;
                                        }
                                    }
                                } else {
                                    return 364;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < -16.25) {
                    if (lng < -58.25) {
                        return 191;
                    } else {
                        return 364;
                    }
                } else {
                    return 364;
                }
            }
        }
    }

    public static int kdLookup38(double lat, double lng)
    {
        if (lng < -59.25) {
            if (lat < -8.5) {
                if (lng < -60.75) {
                    if (lat < -10.0) {
                        if (lng < -61.5) {
                            return 342;
                        } else {
                            if (lat < -10.75) {
                                if (lng < -61.25) {
                                    if (lat < -11.0) {
                                        return 342;
                                    } else {
                                        return 364;
                                    }
                                } else {
                                    if (lng < -61.0) {
                                        if (lat < -11.0) {
                                            return 342;
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        return 342;
                                    }
                                }
                            } else {
                                if (lng < -61.25) {
                                    if (lat < -10.25) {
                                        return 342;
                                    } else {
                                        return 364;
                                    }
                                } else {
                                    return 364;
                                }
                            }
                        }
                    } else {
                        if (lat < -9.25) {
                            if (lng < -61.5) {
                                return 342;
                            } else {
                                return 364;
                            }
                        } else {
                            if (lng < -61.5) {
                                if (lat < -8.75) {
                                    return 342;
                                } else {
                                    if (lng < -61.75) {
                                        return 7;
                                    } else {
                                        return 342;
                                    }
                                }
                            } else {
                                if (lng < -61.25) {
                                    if (lat < -8.75) {
                                        return 364;
                                    } else {
                                        return 7;
                                    }
                                } else {
                                    if (lat < -8.75) {
                                        return 364;
                                    } else {
                                        return 7;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < -10.0) {
                        if (lng < -60.0) {
                            if (lat < -11.0) {
                                return 342;
                            } else {
                                return 364;
                            }
                        } else {
                            if (lat < -11.0) {
                                if (lng < -59.75) {
                                    return 342;
                                } else {
                                    return 364;
                                }
                            } else {
                                return 364;
                            }
                        }
                    } else {
                        if (lng < -60.0) {
                            if (lat < -8.75) {
                                return 364;
                            } else {
                                return 7;
                            }
                        } else {
                            if (lat < -8.75) {
                                return 364;
                            } else {
                                return 7;
                            }
                        }
                    }
                }
            } else {
                return 7;
            }
        } else {
            if (lat < -8.5) {
                if (lng < -57.75) {
                    if (lat < -8.75) {
                        return 364;
                    } else {
                        if (lng < -59.0) {
                            return 7;
                        } else {
                            return 364;
                        }
                    }
                } else {
                    if (lat < -9.25) {
                        return 364;
                    } else {
                        if (lng < -57.0) {
                            if (lng < -57.5) {
                                return 364;
                            } else {
                                if (lat < -8.75) {
                                    return 364;
                                } else {
                                    return 311;
                                }
                            }
                        } else {
                            if (lng < -56.75) {
                                if (lat < -9.0) {
                                    return 364;
                                } else {
                                    return 311;
                                }
                            } else {
                                return 311;
                            }
                        }
                    }
                }
            } else {
                if (lng < -57.75) {
                    if (lat < -7.25) {
                        if (lng < -58.25) {
                            return 7;
                        } else {
                            if (lat < -7.5) {
                                return 364;
                            } else {
                                if (lng < -58.0) {
                                    return 7;
                                } else {
                                    return 364;
                                }
                            }
                        }
                    } else {
                        if (lng < -58.25) {
                            return 7;
                        } else {
                            if (lat < -6.5) {
                                if (lat < -7.0) {
                                    if (lng < -58.0) {
                                        return 7;
                                    } else {
                                        return 311;
                                    }
                                } else {
                                    return 311;
                                }
                            } else {
                                if (lat < -6.25) {
                                    return 311;
                                } else {
                                    if (lng < -58.0) {
                                        return 7;
                                    } else {
                                        return 311;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < -8.0) {
                        if (lng < -57.5) {
                            return 364;
                        } else {
                            return 311;
                        }
                    } else {
                        return 311;
                    }
                }
            }
        }
    }

    public static int kdLookup39(double lat, double lng)
    {
        if (lng < -62.0) {
            if (lat < -5.75) {
                if (lng < -64.75) {
                    if (lat < -9.25) {
                        if (lng < -66.25) {
                            if (lat < -10.25) {
                                return 191;
                            } else {
                                if (lng < -67.0) {
                                    if (lat < -9.5) {
                                        return 181;
                                    } else {
                                        return 7;
                                    }
                                } else {
                                    if (lat < -9.75) {
                                        if (lng < -66.75) {
                                            if (lat < -10.0) {
                                                return 191;
                                            } else {
                                                return 181;
                                            }
                                        } else {
                                            return 191;
                                        }
                                    } else {
                                        if (lng < -66.5) {
                                            return 7;
                                        } else {
                                            if (lat < -9.5) {
                                                return 342;
                                            } else {
                                                return 7;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -10.25) {
                                if (lng < -65.25) {
                                    return 191;
                                } else {
                                    return 342;
                                }
                            } else {
                                if (lng < -65.5) {
                                    if (lat < -9.75) {
                                        return 191;
                                    } else {
                                        if (lng < -65.75) {
                                            return 342;
                                        } else {
                                            if (lat < -9.5) {
                                                return 342;
                                            } else {
                                                return 7;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < -9.75) {
                                        if (lng < -65.25) {
                                            return 191;
                                        } else {
                                            return 342;
                                        }
                                    } else {
                                        if (lng < -65.25) {
                                            if (lat < -9.5) {
                                                return 191;
                                            } else {
                                                return 342;
                                            }
                                        } else {
                                            return 342;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        return 7;
                    }
                } else {
                    if (lat < -8.5) {
                        if (lng < -64.0) {
                            if (lat < -8.75) {
                                return 342;
                            } else {
                                return 7;
                            }
                        } else {
                            return 342;
                        }
                    } else {
                        if (lng < -63.5) {
                            if (lat < -8.25) {
                                if (lng < -63.75) {
                                    return 7;
                                } else {
                                    return 342;
                                }
                            } else {
                                return 7;
                            }
                        } else {
                            if (lat < -8.0) {
                                if (lng < -62.5) {
                                    return 342;
                                } else {
                                    if (lng < -62.25) {
                                        if (lat < -8.25) {
                                            return 342;
                                        } else {
                                            return 7;
                                        }
                                    } else {
                                        return 7;
                                    }
                                }
                            } else {
                                return 7;
                            }
                        }
                    }
                }
            } else {
                if (lat < -0.75) {
                    return 7;
                } else {
                    if (lng < -62.5) {
                        return 7;
                    } else {
                        if (lat < -0.5) {
                            return 350;
                        } else {
                            if (lng < -62.25) {
                                return 7;
                            } else {
                                return 350;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -5.75) {
                return kdLookup38(lat,lng);
            } else {
                if (lng < -59.25) {
                    if (lat < -1.25) {
                        return 7;
                    } else {
                        if (lng < -60.75) {
                            if (lng < -61.5) {
                                if (lat < -1.0) {
                                    if (lng < -61.75) {
                                        return 7;
                                    } else {
                                        return 350;
                                    }
                                } else {
                                    return 350;
                                }
                            } else {
                                if (lat < -0.5) {
                                    return 7;
                                } else {
                                    return 350;
                                }
                            }
                        } else {
                            if (lng < -60.25) {
                                if (lat < -0.75) {
                                    return 7;
                                } else {
                                    return 350;
                                }
                            } else {
                                return 7;
                            }
                        }
                    }
                } else {
                    if (lat < -3.0) {
                        if (lng < -57.75) {
                            return 7;
                        } else {
                            if (lat < -4.5) {
                                if (lng < -57.25) {
                                    if (lat < -5.25) {
                                        return 311;
                                    } else {
                                        if (lat < -5.0) {
                                            if (lng < -57.5) {
                                                return 7;
                                            } else {
                                                return 311;
                                            }
                                        } else {
                                            if (lng < -57.5) {
                                                return 7;
                                            } else {
                                                if (lat < -4.75) {
                                                    return 311;
                                                } else {
                                                    return 7;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 311;
                                }
                            } else {
                                if (lng < -57.0) {
                                    if (lat < -4.0) {
                                        if (lng < -57.25) {
                                            return 7;
                                        } else {
                                            return 311;
                                        }
                                    } else {
                                        return 7;
                                    }
                                } else {
                                    if (lat < -3.5) {
                                        return 311;
                                    } else {
                                        if (lng < -56.75) {
                                            return 7;
                                        } else {
                                            return 311;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -57.75) {
                            if (lat < -1.25) {
                                return 7;
                            } else {
                                if (lng < -58.5) {
                                    if (lat < -0.25) {
                                        return 7;
                                    } else {
                                        if (lng < -58.75) {
                                            return 7;
                                        } else {
                                            return 311;
                                        }
                                    }
                                } else {
                                    if (lat < -0.75) {
                                        if (lng < -58.25) {
                                            return 7;
                                        } else {
                                            if (lng < -58.0) {
                                                if (lat < -1.0) {
                                                    return 7;
                                                } else {
                                                    return 311;
                                                }
                                            } else {
                                                return 311;
                                            }
                                        }
                                    } else {
                                        return 311;
                                    }
                                }
                            }
                        } else {
                            if (lat < -1.5) {
                                if (lng < -57.0) {
                                    if (lat < -1.75) {
                                        return 7;
                                    } else {
                                        if (lng < -57.25) {
                                            return 7;
                                        } else {
                                            return 311;
                                        }
                                    }
                                } else {
                                    if (lat < -2.25) {
                                        if (lng < -56.5) {
                                            return 7;
                                        } else {
                                            if (lat < -2.5) {
                                                return 311;
                                            } else {
                                                return 7;
                                            }
                                        }
                                    } else {
                                        if (lng < -56.75) {
                                            if (lat < -1.75) {
                                                return 7;
                                            } else {
                                                return 311;
                                            }
                                        } else {
                                            if (lat < -2.0) {
                                                return 7;
                                            } else {
                                                return 311;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 311;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup40(double lat, double lng)
    {
        if (lng < -53.5) {
            if (lat < -19.75) {
                if (lng < -55.5) {
                    if (lat < -22.0) {
                        if (lng < -56.0) {
                            return 220;
                        } else {
                            if (lng < -55.75) {
                                if (lat < -22.25) {
                                    return 220;
                                } else {
                                    return 318;
                                }
                            } else {
                                if (lat < -22.25) {
                                    return 220;
                                } else {
                                    return 318;
                                }
                            }
                        }
                    } else {
                        return 318;
                    }
                } else {
                    return 318;
                }
            } else {
                if (lng < -55.0) {
                    if (lat < -17.5) {
                        return 318;
                    } else {
                        if (lng < -55.75) {
                            if (lng < -56.0) {
                                return 318;
                            } else {
                                if (lat < -17.25) {
                                    return 318;
                                } else {
                                    return 364;
                                }
                            }
                        } else {
                            if (lng < -55.5) {
                                if (lat < -17.25) {
                                    return 318;
                                } else {
                                    return 364;
                                }
                            } else {
                                return 364;
                            }
                        }
                    }
                } else {
                    if (lat < -18.0) {
                        return 318;
                    } else {
                        if (lng < -54.25) {
                            if (lat < -17.5) {
                                return 318;
                            } else {
                                return 364;
                            }
                        } else {
                            if (lat < -17.5) {
                                if (lng < -53.75) {
                                    return 318;
                                } else {
                                    return 364;
                                }
                            } else {
                                if (lng < -54.0) {
                                    return 364;
                                } else {
                                    if (lng < -53.75) {
                                        if (lat < -17.25) {
                                            return 318;
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        if (lat < -17.25) {
                                            return 318;
                                        } else {
                                            return 364;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -19.75) {
                if (lng < -52.25) {
                    if (lat < -22.0) {
                        if (lng < -52.75) {
                            return 318;
                        } else {
                            if (lng < -52.5) {
                                if (lat < -22.25) {
                                    return 330;
                                } else {
                                    return 318;
                                }
                            } else {
                                return 330;
                            }
                        }
                    } else {
                        return 318;
                    }
                } else {
                    if (lat < -21.25) {
                        if (lng < -51.75) {
                            if (lat < -21.75) {
                                return 330;
                            } else {
                                if (lng < -52.0) {
                                    return 318;
                                } else {
                                    if (lat < -21.5) {
                                        return 330;
                                    } else {
                                        return 318;
                                    }
                                }
                            }
                        } else {
                            return 330;
                        }
                    } else {
                        if (lng < -51.5) {
                            if (lat < -21.0) {
                                if (lng < -51.75) {
                                    return 318;
                                } else {
                                    return 330;
                                }
                            } else {
                                return 318;
                            }
                        } else {
                            if (lat < -20.5) {
                                return 330;
                            } else {
                                if (lng < -51.25) {
                                    return 318;
                                } else {
                                    if (lat < -20.25) {
                                        return 330;
                                    } else {
                                        if (lng < -51.0) {
                                            return 318;
                                        } else {
                                            if (lat < -20.0) {
                                                return 330;
                                            } else {
                                                return 318;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -52.25) {
                    if (lat < -18.5) {
                        return 318;
                    } else {
                        if (lat < -17.75) {
                            if (lng < -53.0) {
                                if (lat < -18.0) {
                                    return 318;
                                } else {
                                    if (lng < -53.25) {
                                        return 364;
                                    } else {
                                        return 318;
                                    }
                                }
                            } else {
                                if (lng < -52.75) {
                                    if (lat < -18.25) {
                                        return 318;
                                    } else {
                                        return 330;
                                    }
                                } else {
                                    return 330;
                                }
                            }
                        } else {
                            if (lng < -53.0) {
                                return 364;
                            } else {
                                return 330;
                            }
                        }
                    }
                } else {
                    if (lat < -18.75) {
                        if (lng < -51.5) {
                            if (lat < -19.0) {
                                return 318;
                            } else {
                                if (lng < -51.75) {
                                    return 318;
                                } else {
                                    return 330;
                                }
                            }
                        } else {
                            if (lat < -19.25) {
                                if (lng < -51.0) {
                                    return 318;
                                } else {
                                    if (lat < -19.5) {
                                        return 330;
                                    } else {
                                        return 318;
                                    }
                                }
                            } else {
                                if (lng < -51.25) {
                                    if (lat < -19.0) {
                                        return 318;
                                    } else {
                                        return 330;
                                    }
                                } else {
                                    return 330;
                                }
                            }
                        }
                    } else {
                        return 330;
                    }
                }
            }
        }
    }

    public static int kdLookup41(double lat, double lng)
    {
        if (lng < -50.75) {
            if (lat < -17.0) {
                return kdLookup40(lat,lng);
            } else {
                if (lat < -14.5) {
                    if (lng < -53.0) {
                        return 364;
                    } else {
                        if (lat < -15.75) {
                            if (lng < -52.25) {
                                if (lat < -16.5) {
                                    if (lng < -52.75) {
                                        if (lat < -16.75) {
                                            return 330;
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        return 330;
                                    }
                                } else {
                                    if (lng < -52.5) {
                                        return 364;
                                    } else {
                                        if (lat < -16.25) {
                                            return 330;
                                        } else {
                                            return 364;
                                        }
                                    }
                                }
                            } else {
                                return 330;
                            }
                        } else {
                            if (lng < -51.75) {
                                return 364;
                            } else {
                                if (lat < -15.25) {
                                    if (lng < -51.5) {
                                        if (lat < -15.5) {
                                            return 330;
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        return 330;
                                    }
                                } else {
                                    if (lng < -51.25) {
                                        if (lat < -15.0) {
                                            if (lng < -51.5) {
                                                return 364;
                                            } else {
                                                return 330;
                                            }
                                        } else {
                                            return 364;
                                        }
                                    } else {
                                        if (lat < -15.0) {
                                            return 330;
                                        } else {
                                            if (lng < -51.0) {
                                                return 364;
                                            } else {
                                                return 330;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 364;
                }
            }
        } else {
            if (lat < -15.0) {
                return 330;
            } else {
                if (lng < -48.0) {
                    if (lat < -13.25) {
                        if (lng < -50.5) {
                            if (lat < -13.5) {
                                return 330;
                            } else {
                                return 364;
                            }
                        } else {
                            return 330;
                        }
                    } else {
                        if (lng < -49.5) {
                            if (lat < -12.25) {
                                if (lng < -50.25) {
                                    if (lat < -12.75) {
                                        if (lng < -50.5) {
                                            return 364;
                                        } else {
                                            return 330;
                                        }
                                    } else {
                                        if (lng < -50.5) {
                                            return 364;
                                        } else {
                                            return 61;
                                        }
                                    }
                                } else {
                                    if (lat < -12.75) {
                                        return 330;
                                    } else {
                                        return 61;
                                    }
                                }
                            } else {
                                if (lng < -50.5) {
                                    return 364;
                                } else {
                                    return 61;
                                }
                            }
                        } else {
                            if (lat < -12.5) {
                                if (lng < -48.75) {
                                    if (lng < -49.25) {
                                        if (lat < -13.0) {
                                            return 330;
                                        } else {
                                            return 61;
                                        }
                                    } else {
                                        if (lat < -12.75) {
                                            return 330;
                                        } else {
                                            if (lng < -49.0) {
                                                return 61;
                                            } else {
                                                return 330;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -48.5) {
                                        if (lat < -13.0) {
                                            return 330;
                                        } else {
                                            return 61;
                                        }
                                    } else {
                                        if (lat < -13.0) {
                                            return 330;
                                        } else {
                                            return 61;
                                        }
                                    }
                                }
                            } else {
                                return 61;
                            }
                        }
                    }
                } else {
                    if (lat < -13.25) {
                        if (lng < -46.25) {
                            return 330;
                        } else {
                            if (lat < -14.25) {
                                if (lng < -45.75) {
                                    return 330;
                                } else {
                                    if (lng < -45.5) {
                                        return 172;
                                    } else {
                                        if (lat < -14.75) {
                                            return 330;
                                        } else {
                                            return 172;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -46.0) {
                                    if (lat < -13.75) {
                                        return 330;
                                    } else {
                                        if (lat < -13.5) {
                                            return 172;
                                        } else {
                                            return 330;
                                        }
                                    }
                                } else {
                                    return 172;
                                }
                            }
                        }
                    } else {
                        if (lng < -46.5) {
                            if (lat < -12.75) {
                                if (lng < -47.25) {
                                    return 61;
                                } else {
                                    if (lng < -47.0) {
                                        if (lat < -13.0) {
                                            return 330;
                                        } else {
                                            return 61;
                                        }
                                    } else {
                                        if (lng < -46.75) {
                                            if (lat < -13.0) {
                                                return 330;
                                            } else {
                                                return 61;
                                            }
                                        } else {
                                            return 330;
                                        }
                                    }
                                }
                            } else {
                                return 61;
                            }
                        } else {
                            if (lat < -12.25) {
                                if (lng < -46.0) {
                                    if (lat < -12.75) {
                                        return 330;
                                    } else {
                                        return 61;
                                    }
                                } else {
                                    return 172;
                                }
                            } else {
                                if (lng < -46.0) {
                                    if (lat < -11.75) {
                                        if (lng < -46.25) {
                                            return 61;
                                        } else {
                                            if (lat < -12.0) {
                                                return 172;
                                            } else {
                                                return 61;
                                            }
                                        }
                                    } else {
                                        if (lng < -46.25) {
                                            return 61;
                                        } else {
                                            return 172;
                                        }
                                    }
                                } else {
                                    return 172;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup42(double lat, double lng)
    {
        if (lat < -5.75) {
            if (lng < -53.5) {
                if (lat < -9.25) {
                    if (lng < -55.0) {
                        if (lat < -9.5) {
                            return 364;
                        } else {
                            if (lng < -56.0) {
                                return 364;
                            } else {
                                return 311;
                            }
                        }
                    } else {
                        if (lat < -9.5) {
                            return 364;
                        } else {
                            return 311;
                        }
                    }
                } else {
                    return 311;
                }
            } else {
                if (lat < -8.5) {
                    if (lng < -52.25) {
                        if (lat < -9.5) {
                            return 364;
                        } else {
                            return 311;
                        }
                    } else {
                        if (lat < -9.75) {
                            return 364;
                        } else {
                            if (lng < -52.0) {
                                if (lat < -9.25) {
                                    if (lat < -9.5) {
                                        return 364;
                                    } else {
                                        return 311;
                                    }
                                } else {
                                    if (lat < -8.75) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                }
                            } else {
                                return 401;
                            }
                        }
                    }
                } else {
                    if (lng < -52.25) {
                        if (lat < -7.25) {
                            if (lng < -52.75) {
                                return 311;
                            } else {
                                if (lat < -8.0) {
                                    if (lng < -52.5) {
                                        if (lat < -8.25) {
                                            return 311;
                                        } else {
                                            return 401;
                                        }
                                    } else {
                                        return 401;
                                    }
                                } else {
                                    if (lat < -7.5) {
                                        return 401;
                                    } else {
                                        if (lng < -52.5) {
                                            return 311;
                                        } else {
                                            return 401;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -6.5) {
                                if (lng < -52.5) {
                                    return 311;
                                } else {
                                    if (lat < -7.0) {
                                        return 401;
                                    } else {
                                        return 311;
                                    }
                                }
                            } else {
                                if (lng < -52.5) {
                                    return 311;
                                } else {
                                    if (lat < -6.0) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < -6.75) {
                            return 401;
                        } else {
                            if (lng < -51.75) {
                                if (lat < -6.5) {
                                    return 311;
                                } else {
                                    return 401;
                                }
                            } else {
                                return 401;
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -3.0) {
                if (lng < -52.75) {
                    return 311;
                } else {
                    if (lat < -4.5) {
                        if (lng < -52.5) {
                            if (lat < -5.25) {
                                return 311;
                            } else {
                                return 401;
                            }
                        } else {
                            return 401;
                        }
                    } else {
                        if (lng < -51.75) {
                            if (lat < -3.75) {
                                if (lng < -52.5) {
                                    return 311;
                                } else {
                                    return 401;
                                }
                            } else {
                                if (lng < -52.25) {
                                    return 311;
                                } else {
                                    if (lat < -3.25) {
                                        return 401;
                                    } else {
                                        return 311;
                                    }
                                }
                            }
                        } else {
                            return 401;
                        }
                    }
                }
            } else {
                if (lng < -52.75) {
                    return 311;
                } else {
                    if (lat < -1.5) {
                        if (lng < -51.75) {
                            if (lat < -2.25) {
                                if (lng < -52.0) {
                                    return 311;
                                } else {
                                    if (lat < -2.75) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                }
                            } else {
                                if (lng < -52.25) {
                                    return 311;
                                } else {
                                    if (lat < -2.0) {
                                        if (lng < -52.0) {
                                            return 311;
                                        } else {
                                            return 401;
                                        }
                                    } else {
                                        if (lng < -52.0) {
                                            if (lat < -1.75) {
                                                return 311;
                                            } else {
                                                return 401;
                                            }
                                        } else {
                                            return 401;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 401;
                        }
                    } else {
                        if (lng < -51.75) {
                            if (lat < -0.75) {
                                if (lng < -52.25) {
                                    return 311;
                                } else {
                                    if (lat < -1.0) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                }
                            } else {
                                if (lng < -52.5) {
                                    if (lat < -0.25) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                } else {
                                    return 401;
                                }
                            }
                        } else {
                            return 401;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup43(double lat, double lng)
    {
        if (lng < -48.0) {
            if (lat < -8.5) {
                if (lng < -49.5) {
                    if (lat < -10.0) {
                        if (lng < -50.25) {
                            if (lat < -10.75) {
                                if (lng < -50.5) {
                                    return 364;
                                } else {
                                    return 61;
                                }
                            } else {
                                if (lat < -10.5) {
                                    if (lng < -50.5) {
                                        return 364;
                                    } else {
                                        return 61;
                                    }
                                } else {
                                    if (lng < -50.5) {
                                        return 364;
                                    } else {
                                        if (lat < -10.25) {
                                            return 61;
                                        } else {
                                            return 364;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 61;
                        }
                    } else {
                        if (lat < -9.25) {
                            if (lng < -50.25) {
                                if (lat < -9.75) {
                                    return 364;
                                } else {
                                    return 401;
                                }
                            } else {
                                if (lng < -50.0) {
                                    if (lat < -9.75) {
                                        return 61;
                                    } else {
                                        return 401;
                                    }
                                } else {
                                    return 61;
                                }
                            }
                        } else {
                            if (lng < -49.75) {
                                return 401;
                            } else {
                                if (lat < -8.75) {
                                    return 61;
                                } else {
                                    return 401;
                                }
                            }
                        }
                    }
                } else {
                    return 61;
                }
            } else {
                if (lng < -49.25) {
                    return 401;
                } else {
                    if (lat < -7.25) {
                        if (lng < -49.0) {
                            if (lat < -8.0) {
                                return 61;
                            } else {
                                if (lat < -7.5) {
                                    return 401;
                                } else {
                                    return 61;
                                }
                            }
                        } else {
                            return 61;
                        }
                    } else {
                        if (lat < -6.5) {
                            if (lng < -48.75) {
                                if (lat < -7.0) {
                                    if (lng < -49.0) {
                                        return 401;
                                    } else {
                                        return 61;
                                    }
                                } else {
                                    if (lng < -49.0) {
                                        return 401;
                                    } else {
                                        if (lat < -6.75) {
                                            return 61;
                                        } else {
                                            return 401;
                                        }
                                    }
                                }
                            } else {
                                return 61;
                            }
                        } else {
                            if (lng < -48.5) {
                                return 401;
                            } else {
                                if (lat < -6.25) {
                                    return 61;
                                } else {
                                    if (lng < -48.25) {
                                        return 401;
                                    } else {
                                        return 61;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < -8.5) {
                if (lng < -46.5) {
                    if (lat < -9.25) {
                        return 61;
                    } else {
                        if (lng < -47.0) {
                            return 61;
                        } else {
                            if (lat < -9.0) {
                                if (lng < -46.75) {
                                    return 61;
                                } else {
                                    return 144;
                                }
                            } else {
                                if (lng < -46.75) {
                                    if (lat < -8.75) {
                                        return 144;
                                    } else {
                                        return 61;
                                    }
                                } else {
                                    return 144;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < -10.0) {
                        if (lng < -45.75) {
                            if (lat < -10.75) {
                                if (lng < -46.25) {
                                    if (lat < -11.0) {
                                        return 172;
                                    } else {
                                        return 61;
                                    }
                                } else {
                                    return 172;
                                }
                            } else {
                                if (lng < -46.25) {
                                    return 61;
                                } else {
                                    if (lat < -10.5) {
                                        return 172;
                                    } else {
                                        if (lng < -46.0) {
                                            return 61;
                                        } else {
                                            if (lat < -10.25) {
                                                return 61;
                                            } else {
                                                return 144;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -10.75) {
                                return 172;
                            } else {
                                if (lng < -45.5) {
                                    if (lat < -10.25) {
                                        return 172;
                                    } else {
                                        return 144;
                                    }
                                } else {
                                    if (lat < -10.5) {
                                        if (lng < -45.25) {
                                            return 172;
                                        } else {
                                            return 144;
                                        }
                                    } else {
                                        if (lng < -45.25) {
                                            if (lat < -10.25) {
                                                return 172;
                                            } else {
                                                return 144;
                                            }
                                        } else {
                                            return 144;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -46.25) {
                            if (lat < -9.75) {
                                return 61;
                            } else {
                                return 144;
                            }
                        } else {
                            return 144;
                        }
                    }
                }
            } else {
                if (lng < -46.5) {
                    if (lat < -7.25) {
                        if (lng < -47.25) {
                            return 61;
                        } else {
                            if (lat < -8.0) {
                                if (lng < -46.75) {
                                    return 61;
                                } else {
                                    if (lat < -8.25) {
                                        return 144;
                                    } else {
                                        return 61;
                                    }
                                }
                            } else {
                                if (lng < -47.0) {
                                    if (lat < -7.5) {
                                        return 61;
                                    } else {
                                        return 144;
                                    }
                                } else {
                                    if (lat < -7.75) {
                                        if (lng < -46.75) {
                                            return 144;
                                        } else {
                                            return 61;
                                        }
                                    } else {
                                        return 144;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -47.25) {
                            if (lat < -6.75) {
                                if (lng < -47.5) {
                                    return 61;
                                } else {
                                    return 144;
                                }
                            } else {
                                return 61;
                            }
                        } else {
                            return 144;
                        }
                    }
                } else {
                    if (lat < -6.0) {
                        if (lat < -6.25) {
                            if (lat < -6.5) {
                                if (lat < -6.75) {
                                    if (lat < -7.0) {
                                        if (lng < -46.25) {
                                            if (lat < -7.25) {
                                                if (lat < -8.0) {
                                                    return 144;
                                                } else {
                                                    if (lat < -7.75) {
                                                        return 61;
                                                    } else {
                                                        return 144;
                                                    }
                                                }
                                            } else {
                                                return 144;
                                            }
                                        } else {
                                            return 144;
                                        }
                                    } else {
                                        return 144;
                                    }
                                } else {
                                    return 144;
                                }
                            } else {
                                return 144;
                            }
                        } else {
                            return 144;
                        }
                    } else {
                        return 144;
                    }
                }
            }
        }
    }

    public static int kdLookup44(double lat, double lng)
    {
        if (lng < -67.5) {
            if (lat < -22.5) {
                if (lng < -78.75) {
                    return 31;
                } else {
                    if (lat < -33.75) {
                        if (lng < -73.25) {
                            return 31;
                        } else {
                            if (lat < -39.5) {
                                if (lng < -70.5) {
                                    if (lat < -42.25) {
                                        if (lng < -72.0) {
                                            return 31;
                                        } else {
                                            if (lat < -43.75) {
                                                if (lng < -71.25) {
                                                    if (lat < -44.5) {
                                                        if (lng < -71.5) {
                                                            return 31;
                                                        } else {
                                                            return 88;
                                                        }
                                                    } else {
                                                        if (lng < -71.75) {
                                                            return 31;
                                                        } else {
                                                            if (lat < -44.25) {
                                                                return 31;
                                                            } else {
                                                                if (lng < -71.5) {
                                                                    if (lat < -44.0) {
                                                                        return 88;
                                                                    } else {
                                                                        return 31;
                                                                    }
                                                                } else {
                                                                    return 88;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (lat < -44.5) {
                                                        if (lng < -71.0) {
                                                            if (lat < -44.75) {
                                                                return 88;
                                                            } else {
                                                                return 31;
                                                            }
                                                        } else {
                                                            return 88;
                                                        }
                                                    } else {
                                                        if (lng < -71.0) {
                                                            if (lat < -44.25) {
                                                                return 31;
                                                            } else {
                                                                return 88;
                                                            }
                                                        } else {
                                                            return 88;
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (lng < -71.5) {
                                                    if (lat < -43.0) {
                                                        if (lat < -43.5) {
                                                            return 31;
                                                        } else {
                                                            if (lng < -71.75) {
                                                                return 31;
                                                            } else {
                                                                if (lat < -43.25) {
                                                                    return 88;
                                                                } else {
                                                                    return 31;
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        return 88;
                                                    }
                                                } else {
                                                    return 88;
                                                }
                                            }
                                        }
                                    } else {
                                        if (lng < -72.0) {
                                            return 31;
                                        } else {
                                            if (lat < -41.0) {
                                                if (lng < -71.25) {
                                                    if (lat < -41.75) {
                                                        if (lng < -71.75) {
                                                            if (lat < -42.0) {
                                                                return 88;
                                                            } else {
                                                                return 31;
                                                            }
                                                        } else {
                                                            return 88;
                                                        }
                                                    } else {
                                                        if (lng < -71.75) {
                                                            return 31;
                                                        } else {
                                                            return 128;
                                                        }
                                                    }
                                                } else {
                                                    if (lat < -41.75) {
                                                        return 88;
                                                    } else {
                                                        return 128;
                                                    }
                                                }
                                            } else {
                                                if (lng < -71.5) {
                                                    if (lat < -40.25) {
                                                        if (lat < -40.75) {
                                                            if (lng < -71.75) {
                                                                return 31;
                                                            } else {
                                                                return 128;
                                                            }
                                                        } else {
                                                            if (lng < -71.75) {
                                                                return 31;
                                                            } else {
                                                                return 128;
                                                            }
                                                        }
                                                    } else {
                                                        if (lat < -40.0) {
                                                            if (lng < -71.75) {
                                                                return 31;
                                                            } else {
                                                                return 128;
                                                            }
                                                        } else {
                                                            return 31;
                                                        }
                                                    }
                                                } else {
                                                    return 128;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < -41.75) {
                                        return 88;
                                    } else {
                                        return 128;
                                    }
                                }
                            } else {
                                return TimeZoneMapperMethodsOne.kdLookup23(lat,lng);
                            }
                        }
                    } else {
                        if (lng < -73.25) {
                            return 0;
                        } else {
                            return TimeZoneMapperMethodsOne.kdLookup24(lat,lng);
                        }
                    }
                }
            } else {
                return kdLookup29(lat,lng);
            }
        } else {
            if (lat < -22.5) {
                return kdLookup35(lat,lng);
            } else {
                if (lng < -56.25) {
                    if (lat < -11.25) {
                        if (lng < -62.0) {
                            return kdLookup36(lat,lng);
                        } else {
                            return kdLookup37(lat,lng);
                        }
                    } else {
                        return kdLookup39(lat,lng);
                    }
                } else {
                    if (lat < -11.25) {
                        return kdLookup41(lat,lng);
                    } else {
                        if (lng < -50.75) {
                            return kdLookup42(lat,lng);
                        } else {
                            if (lat < -5.75) {
                                return kdLookup43(lat,lng);
                            } else {
                                if (lng < -48.0) {
                                    if (lat < -4.75) {
                                        if (lng < -48.5) {
                                            return 401;
                                        } else {
                                            if (lat < -5.25) {
                                                if (lng < -48.25) {
                                                    return 401;
                                                } else {
                                                    return 61;
                                                }
                                            } else {
                                                if (lng < -48.25) {
                                                    if (lat < -5.0) {
                                                        return 61;
                                                    } else {
                                                        return 401;
                                                    }
                                                } else {
                                                    if (lat < -5.0) {
                                                        return 61;
                                                    } else {
                                                        return 144;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        return 401;
                                    }
                                } else {
                                    if (lat < -3.0) {
                                        if (lng < -46.75) {
                                            if (lat < -4.5) {
                                                if (lng < -47.5) {
                                                    if (lat < -5.25) {
                                                        return 61;
                                                    } else {
                                                        if (lat < -5.0) {
                                                            if (lng < -47.75) {
                                                                return 61;
                                                            } else {
                                                                return 144;
                                                            }
                                                        } else {
                                                            return 144;
                                                        }
                                                    }
                                                } else {
                                                    if (lat < -5.5) {
                                                        if (lng < -47.25) {
                                                            return 61;
                                                        } else {
                                                            return 144;
                                                        }
                                                    } else {
                                                        return 144;
                                                    }
                                                }
                                            } else {
                                                if (lat < -3.75) {
                                                    if (lng < -47.5) {
                                                        return 401;
                                                    } else {
                                                        if (lng < -47.25) {
                                                            if (lat < -4.25) {
                                                                return 144;
                                                            } else {
                                                                return 401;
                                                            }
                                                        } else {
                                                            if (lat < -4.0) {
                                                                return 144;
                                                            } else {
                                                                if (lng < -47.0) {
                                                                    return 401;
                                                                } else {
                                                                    return 144;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (lng < -47.0) {
                                                        return 401;
                                                    } else {
                                                        if (lat < -3.5) {
                                                            return 144;
                                                        } else {
                                                            return 401;
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            return 144;
                                        }
                                    } else {
                                        if (lng < -46.5) {
                                            return 401;
                                        } else {
                                            if (lat < -1.5) {
                                                if (lng < -46.0) {
                                                    if (lat < -2.25) {
                                                        if (lat < -2.5) {
                                                            return 144;
                                                        } else {
                                                            if (lng < -46.25) {
                                                                return 401;
                                                            } else {
                                                                return 144;
                                                            }
                                                        }
                                                    } else {
                                                        if (lat < -2.0) {
                                                            if (lng < -46.25) {
                                                                return 401;
                                                            } else {
                                                                return 144;
                                                            }
                                                        } else {
                                                            if (lng < -46.25) {
                                                                return 401;
                                                            } else {
                                                                if (lat < -1.75) {
                                                                    return 401;
                                                                } else {
                                                                    return 144;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 144;
                                                }
                                            } else {
                                                if (lng < -45.75) {
                                                    if (lat < -0.75) {
                                                        if (lng < -46.0) {
                                                            return 401;
                                                        } else {
                                                            return 144;
                                                        }
                                                    } else {
                                                        return 401;
                                                    }
                                                } else {
                                                    return 144;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup45(double lat, double lng)
    {
        if (lng < -39.5) {
            if (lat < -17.0) {
                if (lng < -40.75) {
                    return 330;
                } else {
                    if (lat < -19.75) {
                        return 330;
                    } else {
                        if (lat < -18.25) {
                            return 330;
                        } else {
                            if (lng < -40.25) {
                                if (lat < -17.25) {
                                    return 330;
                                } else {
                                    if (lng < -40.5) {
                                        return 330;
                                    } else {
                                        return 172;
                                    }
                                }
                            } else {
                                if (lat < -17.75) {
                                    if (lng < -40.0) {
                                        return 330;
                                    } else {
                                        if (lng < -39.75) {
                                            if (lat < -18.0) {
                                                return 330;
                                            } else {
                                                return 172;
                                            }
                                        } else {
                                            return 172;
                                        }
                                    }
                                } else {
                                    if (lng < -40.0) {
                                        if (lat < -17.5) {
                                            return 330;
                                        } else {
                                            return 172;
                                        }
                                    } else {
                                        return 172;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < -14.25) {
                    if (lng < -42.25) {
                        if (lng < -43.75) {
                            if (lat < -14.5) {
                                return 330;
                            } else {
                                if (lng < -44.75) {
                                    return 172;
                                } else {
                                    return 330;
                                }
                            }
                        } else {
                            if (lat < -14.75) {
                                return 330;
                            } else {
                                if (lng < -43.0) {
                                    if (lng < -43.5) {
                                        if (lat < -14.5) {
                                            return 330;
                                        } else {
                                            return 172;
                                        }
                                    } else {
                                        if (lng < -43.25) {
                                            return 172;
                                        } else {
                                            if (lat < -14.5) {
                                                return 330;
                                            } else {
                                                return 172;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -42.75) {
                                        if (lat < -14.5) {
                                            return 330;
                                        } else {
                                            return 172;
                                        }
                                    } else {
                                        return 172;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -41.0) {
                            if (lat < -15.75) {
                                return 330;
                            } else {
                                if (lat < -15.0) {
                                    if (lng < -41.5) {
                                        return 330;
                                    } else {
                                        if (lat < -15.5) {
                                            if (lng < -41.25) {
                                                return 330;
                                            } else {
                                                return 172;
                                            }
                                        } else {
                                            if (lng < -41.25) {
                                                if (lat < -15.25) {
                                                    return 330;
                                                } else {
                                                    return 172;
                                                }
                                            } else {
                                                return 172;
                                            }
                                        }
                                    }
                                } else {
                                    return 172;
                                }
                            }
                        } else {
                            if (lat < -15.75) {
                                if (lng < -40.25) {
                                    if (lat < -16.75) {
                                        if (lng < -40.5) {
                                            return 330;
                                        } else {
                                            return 172;
                                        }
                                    } else {
                                        return 330;
                                    }
                                } else {
                                    if (lat < -16.5) {
                                        return 172;
                                    } else {
                                        if (lng < -40.0) {
                                            return 330;
                                        } else {
                                            if (lat < -16.25) {
                                                return 172;
                                            } else {
                                                if (lng < -39.75) {
                                                    return 330;
                                                } else {
                                                    return 172;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -40.5) {
                                    if (lat < -15.5) {
                                        return 330;
                                    } else {
                                        return 172;
                                    }
                                } else {
                                    return 172;
                                }
                            }
                        }
                    }
                } else {
                    return 172;
                }
            }
        } else {
            if (lat < -17.0) {
                if (lng < -36.75) {
                    if (lat < -19.75) {
                        return 0;
                    } else {
                        if (lng < -38.25) {
                            if (lat < -18.5) {
                                return 330;
                            } else {
                                if (lat < -17.75) {
                                    if (lng < -39.0) {
                                        if (lat < -18.25) {
                                            return 330;
                                        } else {
                                            return 172;
                                        }
                                    } else {
                                        return 0;
                                    }
                                } else {
                                    return 172;
                                }
                            }
                        } else {
                            return 0;
                        }
                    }
                } else {
                    return 0;
                }
            } else {
                if (lng < -36.75) {
                    if (lat < -14.25) {
                        return 172;
                    } else {
                        if (lat < -12.75) {
                            return 172;
                        } else {
                            if (lng < -37.75) {
                                return 172;
                            } else {
                                if (lat < -12.0) {
                                    return 172;
                                } else {
                                    if (lng < -37.25) {
                                        if (lat < -11.75) {
                                            return 172;
                                        } else {
                                            if (lng < -37.5) {
                                                if (lat < -11.5) {
                                                    return 172;
                                                } else {
                                                    return 326;
                                                }
                                            } else {
                                                return 326;
                                            }
                                        }
                                    } else {
                                        return 326;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 0;
                }
            }
        }
    }

    public static int kdLookup46(double lat, double lng)
    {
        if (lng < -36.75) {
            if (lat < -8.5) {
                if (lng < -38.25) {
                    if (lat < -8.75) {
                        return 172;
                    } else {
                        if (lng < -39.0) {
                            return 172;
                        } else {
                            return 192;
                        }
                    }
                } else {
                    if (lat < -10.0) {
                        if (lng < -37.75) {
                            if (lat < -10.25) {
                                if (lat < -10.5) {
                                    if (lat < -11.0) {
                                        return 172;
                                    } else {
                                        if (lng < -38.0) {
                                            return 172;
                                        } else {
                                            if (lat < -10.75) {
                                                return 326;
                                            } else {
                                                return 172;
                                            }
                                        }
                                    }
                                } else {
                                    return 172;
                                }
                            } else {
                                return 172;
                            }
                        } else {
                            return 326;
                        }
                    } else {
                        if (lng < -37.5) {
                            if (lat < -9.25) {
                                if (lng < -38.0) {
                                    return 172;
                                } else {
                                    if (lat < -9.75) {
                                        if (lng < -37.75) {
                                            return 172;
                                        } else {
                                            return 326;
                                        }
                                    } else {
                                        return 326;
                                    }
                                }
                            } else {
                                if (lng < -38.0) {
                                    return 192;
                                } else {
                                    if (lat < -9.0) {
                                        return 326;
                                    } else {
                                        if (lng < -37.75) {
                                            return 192;
                                        } else {
                                            if (lat < -8.75) {
                                                return 326;
                                            } else {
                                                return 192;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < -9.25) {
                                return 326;
                            } else {
                                if (lng < -37.25) {
                                    if (lat < -8.75) {
                                        return 326;
                                    } else {
                                        return 192;
                                    }
                                } else {
                                    if (lat < -9.0) {
                                        if (lng < -37.0) {
                                            return 326;
                                        } else {
                                            return 192;
                                        }
                                    } else {
                                        return 192;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -38.25) {
                    if (lat < -7.25) {
                        if (lng < -39.0) {
                            if (lat < -7.5) {
                                return 192;
                            } else {
                                if (lng < -39.25) {
                                    return 192;
                                } else {
                                    return 144;
                                }
                            }
                        } else {
                            if (lat < -7.75) {
                                return 192;
                            } else {
                                if (lng < -38.75) {
                                    return 144;
                                } else {
                                    if (lng < -38.5) {
                                        if (lat < -7.5) {
                                            return 192;
                                        } else {
                                            return 144;
                                        }
                                    } else {
                                        return 144;
                                    }
                                }
                            }
                        }
                    } else {
                        return 144;
                    }
                } else {
                    if (lat < -7.25) {
                        if (lng < -37.5) {
                            if (lat < -7.75) {
                                return 192;
                            } else {
                                if (lng < -37.75) {
                                    return 144;
                                } else {
                                    if (lat < -7.5) {
                                        return 192;
                                    } else {
                                        return 144;
                                    }
                                }
                            }
                        } else {
                            if (lat < -8.0) {
                                if (lng < -37.0) {
                                    return 192;
                                } else {
                                    if (lat < -8.25) {
                                        return 192;
                                    } else {
                                        return 144;
                                    }
                                }
                            } else {
                                if (lng < -37.0) {
                                    return 192;
                                } else {
                                    if (lat < -7.5) {
                                        return 144;
                                    } else {
                                        return 192;
                                    }
                                }
                            }
                        }
                    } else {
                        return 144;
                    }
                }
            }
        } else {
            if (lat < -8.5) {
                if (lng < -35.25) {
                    if (lat < -10.0) {
                        return 326;
                    } else {
                        if (lng < -36.0) {
                            if (lat < -9.25) {
                                return 326;
                            } else {
                                if (lng < -36.5) {
                                    return 192;
                                } else {
                                    if (lat < -9.0) {
                                        return 326;
                                    } else {
                                        return 192;
                                    }
                                }
                            }
                        } else {
                            if (lat < -9.25) {
                                return 326;
                            } else {
                                if (lng < -35.75) {
                                    if (lat < -8.75) {
                                        return 326;
                                    } else {
                                        return 192;
                                    }
                                } else {
                                    if (lat < -8.75) {
                                        return 326;
                                    } else {
                                        return 192;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < -10.0) {
                        return 0;
                    } else {
                        if (lng < -34.5) {
                            if (lat < -9.25) {
                                return 0;
                            } else {
                                if (lng < -35.0) {
                                    if (lat < -8.75) {
                                        return 326;
                                    } else {
                                        return 192;
                                    }
                                } else {
                                    if (lat < -9.0) {
                                        return 0;
                                    } else {
                                        if (lng < -34.75) {
                                            if (lat < -8.75) {
                                                return 326;
                                            } else {
                                                return 192;
                                            }
                                        } else {
                                            return 0;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 0;
                        }
                    }
                }
            } else {
                if (lng < -35.25) {
                    if (lat < -7.25) {
                        if (lng < -36.0) {
                            if (lat < -8.0) {
                                return 192;
                            } else {
                                if (lng < -36.5) {
                                    return 144;
                                } else {
                                    if (lat < -7.75) {
                                        return 192;
                                    } else {
                                        return 144;
                                    }
                                }
                            }
                        } else {
                            if (lat < -7.75) {
                                return 192;
                            } else {
                                if (lng < -35.75) {
                                    return 144;
                                } else {
                                    if (lng < -35.5) {
                                        if (lat < -7.5) {
                                            return 192;
                                        } else {
                                            return 144;
                                        }
                                    } else {
                                        return 192;
                                    }
                                }
                            }
                        }
                    } else {
                        return 144;
                    }
                } else {
                    if (lat < -7.25) {
                        return 192;
                    } else {
                        return 144;
                    }
                }
            }
        }
    }

    public static int kdLookup47(double lat, double lng)
    {
        if (lat < -45.0) {
            if (lng < -22.5) {
                if (lat < -67.5) {
                    return 122;
                } else {
                    return 69;
                }
            } else {
                return 122;
            }
        } else {
            if (lng < -22.5) {
                if (lat < -22.5) {
                    return 330;
                } else {
                    if (lng < -33.75) {
                        if (lat < -11.25) {
                            return kdLookup45(lat,lng);
                        } else {
                            if (lng < -39.5) {
                                if (lat < -5.75) {
                                    if (lng < -42.25) {
                                        if (lat < -9.25) {
                                            if (lng < -43.75) {
                                                if (lat < -10.25) {
                                                    if (lng < -44.5) {
                                                        if (lat < -10.75) {
                                                            return 172;
                                                        } else {
                                                            return 144;
                                                        }
                                                    } else {
                                                        if (lat < -10.5) {
                                                            return 172;
                                                        } else {
                                                            if (lng < -44.0) {
                                                                return 144;
                                                            } else {
                                                                return 172;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 144;
                                                }
                                            } else {
                                                if (lat < -10.0) {
                                                    return 172;
                                                } else {
                                                    if (lng < -43.0) {
                                                        if (lng < -43.5) {
                                                            if (lat < -9.5) {
                                                                return 144;
                                                            } else {
                                                                return 172;
                                                            }
                                                        } else {
                                                            return 172;
                                                        }
                                                    } else {
                                                        if (lng < -42.75) {
                                                            return 172;
                                                        } else {
                                                            if (lat < -9.5) {
                                                                return 172;
                                                            } else {
                                                                return 144;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            return 144;
                                        }
                                    } else {
                                        if (lat < -8.5) {
                                            if (lng < -41.0) {
                                                if (lat < -9.25) {
                                                    return 172;
                                                } else {
                                                    if (lng < -41.75) {
                                                        if (lat < -9.0) {
                                                            if (lng < -42.0) {
                                                                return 144;
                                                            } else {
                                                                return 172;
                                                            }
                                                        } else {
                                                            return 144;
                                                        }
                                                    } else {
                                                        if (lng < -41.5) {
                                                            if (lat < -9.0) {
                                                                return 172;
                                                            } else {
                                                                return 144;
                                                            }
                                                        } else {
                                                            if (lat < -8.75) {
                                                                return 172;
                                                            } else {
                                                                if (lng < -41.25) {
                                                                    return 144;
                                                                } else {
                                                                    return 172;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (lat < -9.25) {
                                                    return 172;
                                                } else {
                                                    if (lng < -40.25) {
                                                        if (lng < -40.75) {
                                                            if (lat < -8.75) {
                                                                return 172;
                                                            } else {
                                                                return 192;
                                                            }
                                                        } else {
                                                            if (lat < -9.0) {
                                                                if (lng < -40.5) {
                                                                    return 172;
                                                                } else {
                                                                    return 192;
                                                                }
                                                            } else {
                                                                return 192;
                                                            }
                                                        }
                                                    } else {
                                                        if (lng < -40.0) {
                                                            if (lat < -9.0) {
                                                                return 172;
                                                            } else {
                                                                return 192;
                                                            }
                                                        } else {
                                                            if (lat < -9.0) {
                                                                return 172;
                                                            } else {
                                                                if (lng < -39.75) {
                                                                    return 192;
                                                                } else {
                                                                    if (lat < -8.75) {
                                                                        return 172;
                                                                    } else {
                                                                        return 192;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lng < -41.0) {
                                                return 144;
                                            } else {
                                                if (lat < -7.25) {
                                                    if (lng < -40.5) {
                                                        if (lat < -8.0) {
                                                            if (lng < -40.75) {
                                                                if (lat < -8.25) {
                                                                    return 192;
                                                                } else {
                                                                    return 144;
                                                                }
                                                            } else {
                                                                return 192;
                                                            }
                                                        } else {
                                                            return 144;
                                                        }
                                                    } else {
                                                        return 192;
                                                    }
                                                } else {
                                                    return 144;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 144;
                                }
                            } else {
                                if (lat < -5.75) {
                                    return kdLookup46(lat,lng);
                                } else {
                                    return 144;
                                }
                            }
                        }
                    } else {
                        return 0;
                    }
                }
            } else {
                return 157;
            }
        }
    }

    public static int kdLookup48(double lat, double lng)
    {
        if (lat < 16.75) {
            if (lng < -87.25) {
                if (lat < 14.0) {
                    if (lng < -87.75) {
                        return 259;
                    } else {
                        if (lat < 12.5) {
                            return 0;
                        } else {
                            if (lat < 13.25) {
                                return 260;
                            } else {
                                if (lat < 13.5) {
                                    return 259;
                                } else {
                                    if (lng < -87.5) {
                                        return 259;
                                    } else {
                                        return 146;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -88.75) {
                        if (lat < 15.25) {
                            if (lng < -89.5) {
                                if (lat < 14.25) {
                                    if (lng < -89.75) {
                                        return 393;
                                    } else {
                                        return 259;
                                    }
                                } else {
                                    return 393;
                                }
                            } else {
                                if (lat < 14.5) {
                                    return 259;
                                } else {
                                    if (lng < -89.25) {
                                        return 393;
                                    } else {
                                        if (lat < 14.75) {
                                            return 146;
                                        } else {
                                            if (lng < -89.0) {
                                                return 393;
                                            } else {
                                                return 146;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 16.0) {
                                return 393;
                            } else {
                                if (lng < -89.0) {
                                    return 393;
                                } else {
                                    return 41;
                                }
                            }
                        }
                    } else {
                        if (lat < 15.25) {
                            if (lng < -88.5) {
                                if (lat < 14.25) {
                                    return 259;
                                } else {
                                    return 146;
                                }
                            } else {
                                return 146;
                            }
                        } else {
                            if (lng < -88.0) {
                                if (lat < 16.0) {
                                    if (lng < -88.5) {
                                        if (lat < 15.5) {
                                            return 146;
                                        } else {
                                            return 393;
                                        }
                                    } else {
                                        if (lat < 15.5) {
                                            return 146;
                                        } else {
                                            if (lng < -88.25) {
                                                return 393;
                                            } else {
                                                return 146;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -88.5) {
                                        return 41;
                                    } else {
                                        if (lat < 16.25) {
                                            return 393;
                                        } else {
                                            return 41;
                                        }
                                    }
                                }
                            } else {
                                return 146;
                            }
                        }
                    }
                }
            } else {
                if (lat < 14.0) {
                    if (lng < -86.5) {
                        if (lat < 12.5) {
                            return 260;
                        } else {
                            if (lat < 13.25) {
                                if (lng < -87.0) {
                                    if (lat < 13.0) {
                                        return 260;
                                    } else {
                                        return 146;
                                    }
                                } else {
                                    return 260;
                                }
                            } else {
                                if (lng < -86.75) {
                                    return 146;
                                } else {
                                    if (lat < 13.5) {
                                        return 260;
                                    } else {
                                        if (lat < 13.75) {
                                            return 146;
                                        } else {
                                            return 260;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        return 260;
                    }
                } else {
                    if (lng < -86.0) {
                        return 146;
                    } else {
                        if (lat < 15.25) {
                            if (lng < -85.25) {
                                if (lat < 14.25) {
                                    if (lng < -85.5) {
                                        return 146;
                                    } else {
                                        return 260;
                                    }
                                } else {
                                    return 146;
                                }
                            } else {
                                if (lat < 14.5) {
                                    return 260;
                                } else {
                                    if (lng < -85.0) {
                                        return 146;
                                    } else {
                                        if (lat < 14.75) {
                                            return 260;
                                        } else {
                                            return 146;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 146;
                        }
                    }
                }
            }
        } else {
            if (lat < 19.5) {
                if (lng < -87.25) {
                    if (lng < -88.75) {
                        if (lat < 18.0) {
                            if (lng < -89.0) {
                                return 393;
                            } else {
                                return 41;
                            }
                        } else {
                            if (lat < 18.75) {
                                if (lng < -89.25) {
                                    return 32;
                                } else {
                                    return 402;
                                }
                            } else {
                                if (lng < -89.25) {
                                    return 32;
                                } else {
                                    return 402;
                                }
                            }
                        }
                    } else {
                        if (lat < 18.0) {
                            return 41;
                        } else {
                            if (lng < -88.0) {
                                if (lat < 18.75) {
                                    if (lng < -88.5) {
                                        if (lat < 18.25) {
                                            return 41;
                                        } else {
                                            return 402;
                                        }
                                    } else {
                                        if (lat < 18.5) {
                                            return 41;
                                        } else {
                                            return 402;
                                        }
                                    }
                                } else {
                                    return 402;
                                }
                            } else {
                                if (lat < 18.5) {
                                    if (lng < -87.75) {
                                        return 41;
                                    } else {
                                        return 402;
                                    }
                                } else {
                                    return 402;
                                }
                            }
                        }
                    }
                } else {
                    return 0;
                }
            } else {
                if (lng < -87.25) {
                    if (lat < 21.0) {
                        if (lng < -88.75) {
                            if (lat < 20.0) {
                                if (lng < -89.25) {
                                    return 32;
                                } else {
                                    return 402;
                                }
                            } else {
                                return 32;
                            }
                        } else {
                            if (lng < -88.0) {
                                if (lat < 20.25) {
                                    return 402;
                                } else {
                                    if (lng < -88.25) {
                                        return 32;
                                    } else {
                                        if (lat < 20.5) {
                                            return 402;
                                        } else {
                                            return 32;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 20.75) {
                                    return 402;
                                } else {
                                    if (lng < -87.5) {
                                        return 32;
                                    } else {
                                        return 402;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -88.75) {
                            return 32;
                        } else {
                            if (lng < -88.0) {
                                return 32;
                            } else {
                                if (lat < 21.75) {
                                    if (lng < -87.5) {
                                        return 32;
                                    } else {
                                        return 402;
                                    }
                                } else {
                                    return 32;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 21.0) {
                        return 402;
                    } else {
                        if (lng < -86.0) {
                            return 402;
                        } else {
                            return 377;
                        }
                    }
                }
            }
        }
    }
}