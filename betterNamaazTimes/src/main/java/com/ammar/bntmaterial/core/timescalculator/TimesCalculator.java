package com.ammar.bntmaterial.core.timescalculator;

import android.util.SparseArray;

import com.ammar.bntmaterial.core.timescalculator.timezonemapper.TimeZoneMapper;
import com.ammar.bntmaterial.core.timescalculator.zmanim.GeoLocation;
import com.ammar.bntmaterial.core.timescalculator.zmanim.SunTimesCalculator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by agitham on 10/9/2015.
 */
public class TimesCalculator {

	private static final String TAG = "TimesCalculator";

	/**
	 * @param calendar
	 * @param latitude
	 * @param longitude
	 * @param timeZoneId
	 * @param zenith
	 * @param localOffset
	 * @return sunrise in HH:mm format
	 */
	public static String getLocalSunrise(Calendar calendar, double latitude, double longitude, String timeZoneId, double zenith, double localOffset) {

		SunTimesCalculator cal = new SunTimesCalculator();
		GeoLocation loc = new GeoLocation();
		loc.setLatitude(latitude);
		loc.setLongitude(longitude);
		loc.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		double utcSunrise = cal.getUTCSunrise(calendar, loc, zenith, false);
		Double localSunrise = utcSunrise + localOffset;
		if (localSunrise < 0) {
			localSunrise = 24.0 + localSunrise;
		}
		else if (localSunrise > 24) {
			localSunrise = localSunrise - 24.0;
		}
		int hour = localSunrise.intValue();
		int minutes = (int) Math.round((localSunrise - hour) * 60);
		if (minutes == 60) {
			hour++;
			minutes = 0;
		}
		DecimalFormat df = new DecimalFormat("00"); // "0" means don't omit leading zero
		return (df.format(hour) + ":" + df.format(minutes));
	}

	/**
	 * @param calendar
	 * @param latitude
	 * @param longitude
	 * @param timeZoneId
	 * @param zenith
	 * @param localOffset
	 * @return sunset in HH:mm format
	 */
	public static String getLocalSunset(Calendar calendar, double latitude, double longitude, String timeZoneId, double zenith, double localOffset) {

		SunTimesCalculator cal = new SunTimesCalculator();
		GeoLocation loc = new GeoLocation();
		loc.setLatitude(latitude);
		loc.setLongitude(longitude);
		loc.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		double utcSunset = cal.getUTCSunset(calendar, loc, zenith, false);
		Double localSunset = utcSunset + localOffset;
		if (localSunset < 0) {
			localSunset = 24.0 + localSunset;
		}
		else if (localSunset > 24) {
			localSunset = localSunset - 24.0;
		}
		int hour = localSunset.intValue();
		int minutes = (int) Math.round((localSunset - hour) * 60);
		if (minutes == 60) {
			hour++;
			minutes = 0;
		}
		DecimalFormat df = new DecimalFormat("00"); // "0" means don't omit leading zero
		return (df.format(hour) + ":" + df.format(minutes));
	}

	public static SparseArray<DateTime> calculate(DateTime gregorianDateTime, double latitude, double longitude) {

		SparseArray<DateTime> salaatTimesMap = new SparseArray<>(9);
		String timeZoneId = TimeZoneMapper.latLngToTimezoneString(latitude, longitude);
		DateTimeZone dateTimeZone = DateTimeZone.forID(timeZoneId);
		Calendar gregCalendar = gregorianDateTime.toCalendar(Locale.getDefault());
		long offsetInMilliseconds = dateTimeZone.getOffset(gregorianDateTime);
		double localOffsetInHours = (double) offsetInMilliseconds / 1000 / 3600;
		String sunriseTime = getLocalSunrise(gregCalendar, latitude, longitude, timeZoneId, 90.83333, localOffsetInHours); // 90.83333 = zenith - official
		String sunsetTime = getLocalSunset(gregCalendar, latitude, longitude, timeZoneId, 90.83333, localOffsetInHours);
		DateTimeFormatter timeParser = DateTimeFormat.forPattern("HH:mm");
		LocalTime sunriseLocalTime = timeParser.parseLocalTime(sunriseTime);
		DateTime sunrise = gregorianDateTime.withTime(sunriseLocalTime).withSecondOfMinute(0);
		LocalTime sunsetLocalTime = timeParser.parseLocalTime(sunsetTime);
		DateTime sunset = gregorianDateTime.withTime(sunsetLocalTime).withSecondOfMinute(0);
		salaatTimesMap.put(SalaatTimes.KEY_SUNRISE, sunrise);

		/*
		    The daylight time is divided by 12 to determine a day-ghari. The day-ghari and night-ghari add up to 2 hours, so the night-ghari is
			determined by subtracting the day- ghari from 120 minutes.
		*/

		long daylightDurationMillis = sunset.getMillis() - sunrise.getMillis();
		long dayGhariDurationMillis = daylightDurationMillis / 12;
		long nightGhariDurationMillis = 7200000 - dayGhariDurationMillis; // 2 hours in milliseconds = 7200000

		/*
		    Shafa-witr namaz may be offered immediately after nisful layl. The last time for shafa-witr, which is also the last time for sihori, is
			when the sun's light spreads over the whole of the eastern horizon. This occurs sometime soon after Nautical Twilight, defined as when the sun's
			centre is 12 degrees below the horizon. Syedna Taher Saifuddin Saheb (AQ) pronounced 1 hr and 15 minutes before sunrise to be a rule-of-thumb for
			the last sihori time in Bombay. This '75 minute rule' has been applied by mumineen worldwide and is used in the DBNet Salaat Calculations since
			it is a safe time for most regions of the world. In cases where the Nautical Twilight occurs earlier than 75 minutes before sunrise, the
			Nautical Twilight time has been used. This occurs in locations above latitude 45 N or below latitude 45 S.
		*/

		DateTime sihoriEnd = sunrise.minusMinutes(75);
		salaatTimesMap.put(SalaatTimes.KEY_SIHORI_END, sihoriEnd);
		//Log.d(TAG, "Sihori End : " + sihoriEnd);

		/*
		    Fajr namaz time begins immediately after the sihori time and <b>lasts until sunrise</b>. However, since the sihori time above is a 'safe'
			rather than accurate time, there is a gap between sihori and the earliest fajr time. On mumineen.org, this calculated time corresponds to the point
			when the sun is 10 degrees below the horizon, when its light becomes visible over the whole of the eastern horizon. Again, this is a 'safe' time
			and the actual start of fajr is earlier than this in most regions. In Mumbai and Surat, it is a practice to subtract 1 night ghari from sunrise
			to determine the earliest fajr time. This is acceptable in areas where fajr calculated in this way does not come too close to the sihori
			time. In all circumstances, however, it is safer to use the late time as calculated here.
			In some very northern areas, where the phenomena of Nautical Twilight and 10 degrees depression do not occur, the program has not
			calculated the times. In such cases, ask your Amilsaheb for more information.
		*/

		DateTime fajrStart = sunrise.minus(nightGhariDurationMillis);
		salaatTimesMap.put(SalaatTimes.KEY_FAJR_START, fajrStart);
		//Log.d(TAG, "Fajr Start: " + fajrStart);

		salaatTimesMap.put(SalaatTimes.KEY_FAJR_END, sunrise);
		//Log.d(TAG, "Fajr End: " + sunrise);

		// Zawal is the mid-point of the daylight hours (between sunrise and sunset).
		DateTime zawal = sunrise.plus(daylightDurationMillis / 2);
		salaatTimesMap.put(SalaatTimes.KEY_ZAWAL, zawal);
		//Log.d(TAG, "Zawal: " + zawal);

		// Zuhr namaz time starts at zawal and ends after 2 day-gharis.
		DateTime zohrEnd = zawal.plus(2 * dayGhariDurationMillis);
		salaatTimesMap.put(SalaatTimes.KEY_ZOHR_END, zohrEnd);
		//Log.d(TAG, "Zohr end: " + zohrEnd);

		// Asr namaz time starts immediately after completing zuhr and ends 4 daygharis after zawal.
		DateTime asrEnd = zawal.plus(4 * dayGhariDurationMillis);
		salaatTimesMap.put(SalaatTimes.KEY_ASR_END, asrEnd);
		//Log.d(TAG, "Asr end: " + asrEnd);

		/*
		Maghrib namaz time begins at sunset. There is no end time for maghrib. The length of the namaz time is about as long as it takes one to
		do wudhu and offer farizat and sunnat prayers.
		*/
		salaatTimesMap.put(SalaatTimes.KEY_MAGHRIB, sunset);
		//Log.d(TAG, "Maghrib: " + sunset);

		// Isha namaz time begins after completing maghrib, and ends at nisful layl. Nisful layl and zawal are separated by exactly 12 hours, so
		// they are the same except that one is AM and the other PM.
		DateTime nisfullayl = zawal.plusHours(12);
		salaatTimesMap.put(SalaatTimes.KEY_NISFUL_LAYL, nisfullayl);
		//Log.d(TAG, "Nisful Layl: " + nisfullayl);

		return salaatTimesMap;
	}
}