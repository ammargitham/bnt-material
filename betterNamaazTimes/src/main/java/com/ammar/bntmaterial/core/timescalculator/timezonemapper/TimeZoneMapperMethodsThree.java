package com.ammar.bntmaterial.core.timescalculator.timezonemapper;

/**
 * Created by agitham on 2/9/2014.
 */
public class TimeZoneMapperMethodsThree {

    public static int kdLookup49(double lat, double lng)
    {
        if (lat < 5.5) {
            if (lng < -70.0) {
                return 391;
            } else {
                if (lat < 2.75) {
                    if (lat < 1.25) {
                        if (lng < -69.0) {
                            if (lat < 0.75) {
                                return 7;
                            } else {
                                return 391;
                            }
                        } else {
                            return 7;
                        }
                    } else {
                        if (lng < -68.75) {
                            if (lat < 2.0) {
                                if (lng < -69.5) {
                                    if (lat < 1.5) {
                                        if (lng < -69.75) {
                                            return 391;
                                        } else {
                                            return 7;
                                        }
                                    } else {
                                        if (lng < -69.75) {
                                            return 391;
                                        } else {
                                            if (lat < 1.75) {
                                                return 7;
                                            } else {
                                                return 391;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -69.25) {
                                        return 7;
                                    } else {
                                        if (lat < 1.75) {
                                            return 7;
                                        } else {
                                            return 391;
                                        }
                                    }
                                }
                            } else {
                                return 391;
                            }
                        } else {
                            if (lat < 2.0) {
                                if (lng < -68.25) {
                                    if (lat < 1.75) {
                                        return 7;
                                    } else {
                                        return 391;
                                    }
                                } else {
                                    if (lng < -68.0) {
                                        if (lat < 1.75) {
                                            return 7;
                                        } else {
                                            return 391;
                                        }
                                    } else {
                                        return 7;
                                    }
                                }
                            } else {
                                return 391;
                            }
                        }
                    }
                } else {
                    if (lat < 4.25) {
                        return 391;
                    } else {
                        if (lng < -67.75) {
                            return 391;
                        } else {
                            return 398;
                        }
                    }
                }
            }
        } else {
            if (lng < -70.5) {
                if (lat < 8.25) {
                    if (lng < -72.0) {
                        if (lat < 7.5) {
                            return 391;
                        } else {
                            if (lng < -72.25) {
                                return 391;
                            } else {
                                return 398;
                            }
                        }
                    } else {
                        if (lat < 7.0) {
                            return 391;
                        } else {
                            if (lng < -71.25) {
                                if (lat < 7.25) {
                                    return 391;
                                } else {
                                    return 398;
                                }
                            } else {
                                if (lat < 7.25) {
                                    if (lng < -71.0) {
                                        return 391;
                                    } else {
                                        if (lng < -70.75) {
                                            return 398;
                                        } else {
                                            return 391;
                                        }
                                    }
                                } else {
                                    return 398;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 9.75) {
                        if (lng < -72.25) {
                            if (lat < 9.0) {
                                if (lng < -72.5) {
                                    return 391;
                                } else {
                                    if (lat < 8.5) {
                                        return 391;
                                    } else {
                                        return 398;
                                    }
                                }
                            } else {
                                if (lng < -72.75) {
                                    if (lat < 9.25) {
                                        return 391;
                                    } else {
                                        if (lng < -73.0) {
                                            if (lat < 9.5) {
                                                return 398;
                                            } else {
                                                return 391;
                                            }
                                        } else {
                                            if (lat < 9.5) {
                                                return 391;
                                            } else {
                                                return 398;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 9.25) {
                                        if (lng < -72.5) {
                                            return 391;
                                        } else {
                                            return 398;
                                        }
                                    } else {
                                        return 398;
                                    }
                                }
                            }
                        } else {
                            return 398;
                        }
                    } else {
                        if (lng < -72.5) {
                            if (lat < 10.5) {
                                if (lng < -73.0) {
                                    return 391;
                                } else {
                                    if (lat < 10.0) {
                                        return 398;
                                    } else {
                                        if (lng < -72.75) {
                                            return 391;
                                        } else {
                                            return 398;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -72.75) {
                                    return 391;
                                } else {
                                    if (lat < 10.75) {
                                        return 398;
                                    } else {
                                        return 391;
                                    }
                                }
                            }
                        } else {
                            return 398;
                        }
                    }
                }
            } else {
                if (lat < 8.25) {
                    if (lng < -69.0) {
                        if (lat < 6.75) {
                            if (lng < -69.5) {
                                return 391;
                            } else {
                                if (lat < 6.25) {
                                    return 391;
                                } else {
                                    return 398;
                                }
                            }
                        } else {
                            if (lng < -69.75) {
                                if (lat < 7.25) {
                                    if (lng < -70.25) {
                                        return 391;
                                    } else {
                                        if (lng < -70.0) {
                                            if (lat < 7.0) {
                                                return 391;
                                            } else {
                                                return 398;
                                            }
                                        } else {
                                            if (lat < 7.0) {
                                                return 391;
                                            } else {
                                                return 398;
                                            }
                                        }
                                    }
                                } else {
                                    return 398;
                                }
                            } else {
                                return 398;
                            }
                        }
                    } else {
                        if (lat < 6.5) {
                            if (lng < -68.25) {
                                if (lat < 6.25) {
                                    return 391;
                                } else {
                                    return 398;
                                }
                            } else {
                                if (lat < 6.25) {
                                    return 391;
                                } else {
                                    if (lng < -67.75) {
                                        return 398;
                                    } else {
                                        return 391;
                                    }
                                }
                            }
                        } else {
                            return 398;
                        }
                    }
                } else {
                    return 398;
                }
            }
        }
    }

    public static int kdLookup50(double lat, double lng)
    {
        if (lat < 11.25) {
            if (lng < -73.25) {
                if (lat < 1.5) {
                    if (lng < -76.0) {
                        if (lng < -77.5) {
                            if (lat < 1.0) {
                                return 382;
                            } else {
                                if (lng < -78.25) {
                                    if (lng < -78.5) {
                                        return 382;
                                    } else {
                                        if (lat < 1.25) {
                                            return 382;
                                        } else {
                                            return 391;
                                        }
                                    }
                                } else {
                                    if (lng < -78.0) {
                                        if (lat < 1.25) {
                                            return 382;
                                        } else {
                                            return 391;
                                        }
                                    } else {
                                        return 391;
                                    }
                                }
                            }
                        } else {
                            if (lng < -76.75) {
                                if (lat < 0.75) {
                                    if (lng < -77.25) {
                                        return 382;
                                    } else {
                                        if (lat < 0.5) {
                                            return 382;
                                        } else {
                                            return 391;
                                        }
                                    }
                                } else {
                                    return 391;
                                }
                            } else {
                                if (lat < 0.5) {
                                    if (lng < -76.5) {
                                        return 382;
                                    } else {
                                        if (lng < -76.25) {
                                            if (lat < 0.25) {
                                                return 382;
                                            } else {
                                                return 391;
                                            }
                                        } else {
                                            return 382;
                                        }
                                    }
                                } else {
                                    return 391;
                                }
                            }
                        }
                    } else {
                        if (lng < -75.5) {
                            if (lat < 0.5) {
                                if (lng < -75.75) {
                                    return 382;
                                } else {
                                    if (lat < 0.25) {
                                        return 382;
                                    } else {
                                        return 391;
                                    }
                                }
                            } else {
                                return 391;
                            }
                        } else {
                            return 391;
                        }
                    }
                } else {
                    if (lat < 7.5) {
                        return 391;
                    } else {
                        if (lng < -76.0) {
                            if (lat < 9.25) {
                                if (lng < -77.5) {
                                    return 22;
                                } else {
                                    if (lat < 8.25) {
                                        if (lng < -77.0) {
                                            if (lat < 7.75) {
                                                return 391;
                                            } else {
                                                if (lng < -77.25) {
                                                    return 22;
                                                } else {
                                                    if (lat < 8.0) {
                                                        return 391;
                                                    } else {
                                                        return 22;
                                                    }
                                                }
                                            }
                                        } else {
                                            return 391;
                                        }
                                    } else {
                                        if (lng < -76.75) {
                                            if (lat < 8.75) {
                                                if (lng < -77.25) {
                                                    return 22;
                                                } else {
                                                    return 391;
                                                }
                                            } else {
                                                return 22;
                                            }
                                        } else {
                                            return 391;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -77.5) {
                                    return 22;
                                } else {
                                    return 391;
                                }
                            }
                        } else {
                            return 391;
                        }
                    }
                }
            } else {
                return kdLookup49(lat,lng);
            }
        } else {
            if (lng < -73.25) {
                if (lat < 16.75) {
                    return 391;
                } else {
                    if (lat < 19.5) {
                        if (lng < -76.0) {
                            return 72;
                        } else {
                            return 19;
                        }
                    } else {
                        if (lng < -76.0) {
                            return 377;
                        } else {
                            if (lat < 21.0) {
                                if (lng < -74.5) {
                                    return 377;
                                } else {
                                    if (lat < 20.25) {
                                        if (lng < -74.0) {
                                            return 377;
                                        } else {
                                            return 19;
                                        }
                                    } else {
                                        if (lng < -74.0) {
                                            return 377;
                                        } else {
                                            if (lng < -73.75) {
                                                return 377;
                                            } else {
                                                return 282;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -74.75) {
                                    return 377;
                                } else {
                                    return 282;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 16.75) {
                    if (lng < -70.5) {
                        if (lat < 14.0) {
                            if (lng < -72.0) {
                                return 391;
                            } else {
                                if (lat < 12.5) {
                                    if (lng < -71.25) {
                                        if (lat < 11.75) {
                                            return 398;
                                        } else {
                                            if (lng < -71.5) {
                                                return 391;
                                            } else {
                                                if (lat < 12.0) {
                                                    return 398;
                                                } else {
                                                    return 391;
                                                }
                                            }
                                        }
                                    } else {
                                        if (lat < 11.75) {
                                            return 398;
                                        } else {
                                            return 391;
                                        }
                                    }
                                } else {
                                    return 391;
                                }
                            }
                        } else {
                            return 0;
                        }
                    } else {
                        if (lat < 14.0) {
                            if (lng < -69.0) {
                                if (lat < 12.5) {
                                    return 398;
                                } else {
                                    return 235;
                                }
                            } else {
                                return 398;
                            }
                        } else {
                            return 0;
                        }
                    }
                } else {
                    if (lng < -70.5) {
                        if (lat < 19.5) {
                            if (lng < -72.0) {
                                return 19;
                            } else {
                                if (lat < 18.0) {
                                    return 230;
                                } else {
                                    if (lng < -71.5) {
                                        if (lat < 18.75) {
                                            if (lat < 18.25) {
                                                return 19;
                                            } else {
                                                if (lng < -71.75) {
                                                    return 19;
                                                } else {
                                                    if (lat < 18.5) {
                                                        return 19;
                                                    } else {
                                                        return 230;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lat < 19.0) {
                                                return 19;
                                            } else {
                                                if (lng < -71.75) {
                                                    return 19;
                                                } else {
                                                    if (lat < 19.25) {
                                                        return 230;
                                                    } else {
                                                        return 19;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        return 230;
                                    }
                                }
                            }
                        } else {
                            if (lat < 21.0) {
                                if (lng < -72.0) {
                                    if (lat < 20.25) {
                                        return 19;
                                    } else {
                                        return 282;
                                    }
                                } else {
                                    if (lng < -71.25) {
                                        if (lat < 20.25) {
                                            if (lng < -71.75) {
                                                return 19;
                                            } else {
                                                if (lat < 19.75) {
                                                    if (lng < -71.5) {
                                                        return 19;
                                                    } else {
                                                        return 230;
                                                    }
                                                } else {
                                                    return 230;
                                                }
                                            }
                                        } else {
                                            return 0;
                                        }
                                    } else {
                                        return 230;
                                    }
                                }
                            } else {
                                if (lng < -72.0) {
                                    return 282;
                                } else {
                                    return 103;
                                }
                            }
                        }
                    } else {
                        return 230;
                    }
                }
            }
        }
    }

    public static int kdLookup51(double lat, double lng)
    {
        if (lat < 36.5) {
            if (lng < -85.5) {
                return 161;
            } else {
                if (lat < 35.0) {
                    if (lat < 34.25) {
                        if (lng < -85.25) {
                            return 161;
                        } else {
                            return 166;
                        }
                    } else {
                        if (lng < -85.25) {
                            if (lat < 34.5) {
                                return 161;
                            } else {
                                return 166;
                            }
                        } else {
                            return 166;
                        }
                    }
                } else {
                    if (lat < 35.75) {
                        if (lng < -85.0) {
                            if (lat < 35.25) {
                                if (lng < -85.25) {
                                    return 161;
                                } else {
                                    return 166;
                                }
                            } else {
                                if (lng < -85.25) {
                                    return 161;
                                } else {
                                    if (lat < 35.5) {
                                        return 166;
                                    } else {
                                        return 161;
                                    }
                                }
                            }
                        } else {
                            return 166;
                        }
                    } else {
                        if (lng < -84.75) {
                            return 161;
                        } else {
                            if (lat < 36.0) {
                                return 166;
                            } else {
                                if (lat < 36.25) {
                                    return 161;
                                } else {
                                    return 166;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lng < -86.0) {
                if (lat < 38.0) {
                    return 161;
                } else {
                    if (lng < -86.75) {
                        if (lat < 38.5) {
                            if (lng < -87.0) {
                                if (lat < 38.25) {
                                    return 161;
                                } else {
                                    return 54;
                                }
                            } else {
                                if (lat < 38.25) {
                                    return 161;
                                } else {
                                    return 310;
                                }
                            }
                        } else {
                            if (lat < 38.75) {
                                if (lng < -87.0) {
                                    return 54;
                                } else {
                                    return 310;
                                }
                            } else {
                                if (lng < -87.0) {
                                    if (lat < 39.0) {
                                        return 310;
                                    } else {
                                        return 89;
                                    }
                                } else {
                                    if (lat < 39.0) {
                                        return 310;
                                    } else {
                                        return 89;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 38.5) {
                            if (lng < -86.5) {
                                if (lat < 38.25) {
                                    return 90;
                                } else {
                                    return 310;
                                }
                            } else {
                                if (lng < -86.25) {
                                    if (lat < 38.25) {
                                        return 161;
                                    } else {
                                        return 389;
                                    }
                                } else {
                                    if (lat < 38.25) {
                                        return 166;
                                    } else {
                                        return 131;
                                    }
                                }
                            }
                        } else {
                            if (lng < -86.5) {
                                if (lat < 39.0) {
                                    return 310;
                                } else {
                                    return 89;
                                }
                            } else {
                                return 89;
                            }
                        }
                    }
                }
            } else {
                if (lat < 37.75) {
                    if (lng < -85.25) {
                        if (lat < 37.5) {
                            return 161;
                        } else {
                            return 166;
                        }
                    } else {
                        if (lat < 37.0) {
                            if (lng < -85.0) {
                                return 161;
                            } else {
                                if (lng < -84.75) {
                                    if (lat < 36.75) {
                                        return 161;
                                    } else {
                                        return 110;
                                    }
                                } else {
                                    if (lat < 36.75) {
                                        return 161;
                                    } else {
                                        return 110;
                                    }
                                }
                            }
                        } else {
                            if (lng < -85.0) {
                                if (lat < 37.5) {
                                    return 161;
                                } else {
                                    return 166;
                                }
                            } else {
                                if (lat < 37.25) {
                                    if (lng < -84.75) {
                                        return 161;
                                    } else {
                                        return 166;
                                    }
                                } else {
                                    return 166;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -85.25) {
                        if (lat < 38.5) {
                            if (lng < -85.75) {
                                if (lat < 38.0) {
                                    return 166;
                                } else {
                                    return 131;
                                }
                            } else {
                                return 166;
                            }
                        } else {
                            if (lng < -85.75) {
                                return 89;
                            } else {
                                if (lat < 38.75) {
                                    return 131;
                                } else {
                                    return 89;
                                }
                            }
                        }
                    } else {
                        if (lat < 38.75) {
                            return 166;
                        } else {
                            if (lng < -85.0) {
                                return 89;
                            } else {
                                return 166;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup52(double lat, double lng)
    {
        if (lng < -84.5) {
            if (lat < 39.25) {
                if (lng < -87.25) {
                    if (lat < 38.5) {
                        return 161;
                    } else {
                        if (lng < -87.5) {
                            return 161;
                        } else {
                            if (lat < 39.0) {
                                return 310;
                            } else {
                                return 89;
                            }
                        }
                    }
                } else {
                    return kdLookup51(lat,lng);
                }
            } else {
                if (lat < 42.0) {
                    if (lng < -87.25) {
                        if (lng < -87.5) {
                            return 161;
                        } else {
                            if (lat < 40.75) {
                                return 89;
                            } else {
                                return 161;
                            }
                        }
                    } else {
                        if (lng < -86.0) {
                            if (lat < 40.75) {
                                return 89;
                            } else {
                                if (lng < -86.75) {
                                    if (lat < 41.0) {
                                        if (lng < -87.0) {
                                            return 161;
                                        } else {
                                            return 89;
                                        }
                                    } else {
                                        return 161;
                                    }
                                } else {
                                    if (lat < 41.25) {
                                        if (lng < -86.5) {
                                            if (lat < 41.0) {
                                                return 89;
                                            } else {
                                                return 249;
                                            }
                                        } else {
                                            if (lng < -86.25) {
                                                if (lat < 41.0) {
                                                    return 89;
                                                } else {
                                                    return 249;
                                                }
                                            } else {
                                                return 89;
                                            }
                                        }
                                    } else {
                                        if (lng < -86.5) {
                                            if (lat < 41.5) {
                                                return 258;
                                            } else {
                                                return 161;
                                            }
                                        } else {
                                            if (lat < 41.5) {
                                                if (lng < -86.25) {
                                                    return 258;
                                                } else {
                                                    return 89;
                                                }
                                            } else {
                                                return 89;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 40.5) {
                                if (lng < -85.0) {
                                    return 89;
                                } else {
                                    if (lat < 39.75) {
                                        if (lng < -84.75) {
                                            if (lat < 39.5) {
                                                return 166;
                                            } else {
                                                return 89;
                                            }
                                        } else {
                                            return 166;
                                        }
                                    } else {
                                        if (lat < 40.0) {
                                            if (lng < -84.75) {
                                                return 89;
                                            } else {
                                                return 166;
                                            }
                                        } else {
                                            if (lng < -84.75) {
                                                return 89;
                                            } else {
                                                return 166;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -84.75) {
                                    return 89;
                                } else {
                                    if (lat < 41.75) {
                                        return 166;
                                    } else {
                                        return 93;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -87.0) {
                        return 161;
                    } else {
                        return 93;
                    }
                }
            }
        } else {
            if (lat < 41.75) {
                return 166;
            } else {
                if (lng < -81.75) {
                    if (lat < 43.25) {
                        if (lng < -83.0) {
                            return 93;
                        } else {
                            if (lat < 42.5) {
                                if (lng < -82.5) {
                                    if (lat < 42.0) {
                                        return 166;
                                    } else {
                                        return 239;
                                    }
                                } else {
                                    if (lng < -82.0) {
                                        return 239;
                                    } else {
                                        if (lat < 42.0) {
                                            return 166;
                                        } else {
                                            return 239;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -82.5) {
                                    return 93;
                                } else {
                                    if (lng < -82.25) {
                                        if (lat < 42.75) {
                                            return 239;
                                        } else {
                                            return 93;
                                        }
                                    } else {
                                        return 239;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -83.25) {
                            return 93;
                        } else {
                            if (lat < 44.0) {
                                if (lng < -82.25) {
                                    return 93;
                                } else {
                                    return 239;
                                }
                            } else {
                                return 93;
                            }
                        }
                    }
                } else {
                    if (lat < 43.25) {
                        if (lng < -80.25) {
                            if (lng < -81.0) {
                                if (lat < 42.5) {
                                    if (lng < -81.5) {
                                        return 239;
                                    } else {
                                        return 166;
                                    }
                                } else {
                                    return 239;
                                }
                            } else {
                                if (lat < 42.5) {
                                    return 166;
                                } else {
                                    return 239;
                                }
                            }
                        } else {
                            if (lng < -79.5) {
                                if (lat < 42.5) {
                                    return 166;
                                } else {
                                    if (lng < -79.75) {
                                        return 239;
                                    } else {
                                        if (lat < 42.75) {
                                            return 166;
                                        } else {
                                            return 239;
                                        }
                                    }
                                }
                            } else {
                                if (lat < 42.75) {
                                    return 166;
                                } else {
                                    if (lng < -79.0) {
                                        return 239;
                                    } else {
                                        return 166;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -80.25) {
                            return 239;
                        } else {
                            if (lat < 43.75) {
                                if (lng < -79.0) {
                                    return 239;
                                } else {
                                    return 166;
                                }
                            } else {
                                return 239;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup53(double lat, double lng)
    {
        if (lng < -78.75) {
            if (lat < 33.75) {
                if (lng < -84.5) {
                    if (lat < 28.0) {
                        return 0;
                    } else {
                        if (lat < 30.75) {
                            if (lng < -85.25) {
                                return 161;
                            } else {
                                if (lat < 29.25) {
                                    return 0;
                                } else {
                                    if (lat < 30.0) {
                                        return 166;
                                    } else {
                                        if (lng < -85.0) {
                                            return 161;
                                        } else {
                                            if (lat < 30.5) {
                                                return 166;
                                            } else {
                                                if (lng < -84.75) {
                                                    return 161;
                                                } else {
                                                    return 166;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -85.25) {
                                return 161;
                            } else {
                                if (lat < 32.25) {
                                    if (lat < 31.5) {
                                        if (lng < -85.0) {
                                            return 161;
                                        } else {
                                            if (lat < 31.0) {
                                                if (lng < -84.75) {
                                                    return 161;
                                                } else {
                                                    return 166;
                                                }
                                            } else {
                                                return 166;
                                            }
                                        }
                                    } else {
                                        if (lng < -85.0) {
                                            return 161;
                                        } else {
                                            return 166;
                                        }
                                    }
                                } else {
                                    if (lat < 33.0) {
                                        if (lng < -85.0) {
                                            return 161;
                                        } else {
                                            if (lat < 32.5) {
                                                if (lng < -84.75) {
                                                    return 161;
                                                } else {
                                                    return 166;
                                                }
                                            } else {
                                                if (lng < -84.75) {
                                                    if (lat < 32.75) {
                                                        return 161;
                                                    } else {
                                                        return 166;
                                                    }
                                                } else {
                                                    return 166;
                                                }
                                            }
                                        }
                                    } else {
                                        if (lng < -85.0) {
                                            if (lat < 33.25) {
                                                return 161;
                                            } else {
                                                return 166;
                                            }
                                        } else {
                                            return 166;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 28.0) {
                        if (lng < -81.75) {
                            if (lat < 25.25) {
                                return 377;
                            } else {
                                return 166;
                            }
                        } else {
                            if (lat < 25.25) {
                                if (lng < -80.25) {
                                    if (lat < 23.75) {
                                        return 377;
                                    } else {
                                        return 166;
                                    }
                                } else {
                                    return 377;
                                }
                            } else {
                                if (lng < -80.0) {
                                    return 166;
                                } else {
                                    if (lat < 26.5) {
                                        if (lng < -79.5) {
                                            return 166;
                                        } else {
                                            return 282;
                                        }
                                    } else {
                                        return 166;
                                    }
                                }
                            }
                        }
                    } else {
                        return 166;
                    }
                }
            } else {
                return kdLookup52(lat,lng);
            }
        } else {
            if (lat < 33.75) {
                if (lng < -73.25) {
                    if (lat < 28.0) {
                        if (lng < -76.0) {
                            if (lat < 25.25) {
                                if (lng < -77.75) {
                                    if (lat < 23.75) {
                                        return 377;
                                    } else {
                                        return 282;
                                    }
                                } else {
                                    return 282;
                                }
                            } else {
                                return 282;
                            }
                        } else {
                            return 282;
                        }
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } else {
                if (lng < -75.25) {
                    if (lat < 39.25) {
                        return 166;
                    } else {
                        if (lat < 43.5) {
                            return 166;
                        } else {
                            if (lng < -77.0) {
                                if (lng < -78.0) {
                                    if (lat < 43.75) {
                                        return 166;
                                    } else {
                                        return 239;
                                    }
                                } else {
                                    if (lat < 44.0) {
                                        if (lng < -77.5) {
                                            if (lng < -77.75) {
                                                if (lat < 43.75) {
                                                    return 166;
                                                } else {
                                                    return 239;
                                                }
                                            } else {
                                                return 166;
                                            }
                                        } else {
                                            if (lng < -77.25) {
                                                return 239;
                                            } else {
                                                if (lat < 43.75) {
                                                    return 166;
                                                } else {
                                                    return 239;
                                                }
                                            }
                                        }
                                    } else {
                                        return 239;
                                    }
                                }
                            } else {
                                if (lng < -76.25) {
                                    if (lat < 44.25) {
                                        if (lng < -76.75) {
                                            if (lat < 43.75) {
                                                return 166;
                                            } else {
                                                return 239;
                                            }
                                        } else {
                                            if (lat < 43.75) {
                                                return 166;
                                            } else {
                                                return 239;
                                            }
                                        }
                                    } else {
                                        return 239;
                                    }
                                } else {
                                    if (lat < 44.5) {
                                        return 166;
                                    } else {
                                        if (lng < -75.75) {
                                            return 239;
                                        } else {
                                            if (lng < -75.5) {
                                                if (lat < 44.75) {
                                                    return 166;
                                                } else {
                                                    return 239;
                                                }
                                            } else {
                                                if (lat < 44.75) {
                                                    return 166;
                                                } else {
                                                    return 239;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 166;
                }
            }
        }
    }

    public static int kdLookup54(double lat, double lng)
    {
        if (lng < -64.75) {
            if (lat < 2.75) {
                if (lng < -66.25) {
                    if (lat < 1.25) {
                        if (lng < -66.5) {
                            return 7;
                        } else {
                            if (lat < 1.0) {
                                return 7;
                            } else {
                                return 398;
                            }
                        }
                    } else {
                        if (lat < 2.0) {
                            if (lng < -67.0) {
                                return 7;
                            } else {
                                if (lng < -66.75) {
                                    if (lat < 1.75) {
                                        return 391;
                                    } else {
                                        return 398;
                                    }
                                } else {
                                    return 398;
                                }
                            }
                        } else {
                            if (lng < -67.0) {
                                if (lat < 2.25) {
                                    if (lng < -67.25) {
                                        return 7;
                                    } else {
                                        return 391;
                                    }
                                } else {
                                    if (lng < -67.25) {
                                        return 391;
                                    } else {
                                        if (lat < 2.5) {
                                            return 391;
                                        } else {
                                            return 398;
                                        }
                                    }
                                }
                            } else {
                                return 398;
                            }
                        }
                    }
                } else {
                    if (lat < 1.25) {
                        if (lng < -65.5) {
                            if (lat < 1.0) {
                                return 7;
                            } else {
                                return 398;
                            }
                        } else {
                            if (lat < 0.75) {
                                return 7;
                            } else {
                                if (lng < -65.25) {
                                    return 398;
                                } else {
                                    if (lng < -65.0) {
                                        if (lat < 1.0) {
                                            return 7;
                                        } else {
                                            return 398;
                                        }
                                    } else {
                                        return 7;
                                    }
                                }
                            }
                        }
                    } else {
                        return 398;
                    }
                }
            } else {
                if (lng < -67.25) {
                    if (lat < 5.25) {
                        if (lat < 5.0) {
                            if (lat < 4.75) {
                                if (lat < 4.5) {
                                    if (lat < 3.5) {
                                        if (lat < 3.25) {
                                            return 398;
                                        } else {
                                            return 391;
                                        }
                                    } else {
                                        if (lat < 3.75) {
                                            return 391;
                                        } else {
                                            return 398;
                                        }
                                    }
                                } else {
                                    return 398;
                                }
                            } else {
                                return 398;
                            }
                        } else {
                            return 398;
                        }
                    } else {
                        return 398;
                    }
                } else {
                    return 398;
                }
            }
        } else {
            if (lat < 2.75) {
                if (lng < -63.5) {
                    if (lat < 1.5) {
                        return 7;
                    } else {
                        if (lng < -64.0) {
                            return 398;
                        } else {
                            if (lat < 2.0) {
                                return 7;
                            } else {
                                if (lat < 2.25) {
                                    if (lng < -63.75) {
                                        return 398;
                                    } else {
                                        return 7;
                                    }
                                } else {
                                    if (lng < -63.75) {
                                        if (lat < 2.5) {
                                            return 398;
                                        } else {
                                            return 350;
                                        }
                                    } else {
                                        if (lat < 2.5) {
                                            return 398;
                                        } else {
                                            return 350;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 1.25) {
                        if (lng < -62.5) {
                            return 7;
                        } else {
                            if (lat < 0.5) {
                                return 350;
                            } else {
                                if (lat < 0.75) {
                                    if (lng < -62.25) {
                                        return 7;
                                    } else {
                                        return 350;
                                    }
                                } else {
                                    return 350;
                                }
                            }
                        }
                    } else {
                        if (lng < -62.75) {
                            if (lat < 2.25) {
                                return 7;
                            } else {
                                if (lng < -63.25) {
                                    if (lat < 2.5) {
                                        return 398;
                                    } else {
                                        return 350;
                                    }
                                } else {
                                    return 350;
                                }
                            }
                        } else {
                            if (lat < 2.0) {
                                if (lng < -62.5) {
                                    return 7;
                                } else {
                                    return 350;
                                }
                            } else {
                                return 350;
                            }
                        }
                    }
                }
            } else {
                if (lng < -63.5) {
                    if (lat < 4.0) {
                        if (lng < -64.25) {
                            return 398;
                        } else {
                            if (lat < 3.25) {
                                if (lng < -64.0) {
                                    return 398;
                                } else {
                                    if (lng < -63.75) {
                                        if (lat < 3.0) {
                                            return 398;
                                        } else {
                                            return 350;
                                        }
                                    } else {
                                        return 350;
                                    }
                                }
                            } else {
                                if (lng < -64.0) {
                                    if (lat < 3.75) {
                                        return 398;
                                    } else {
                                        return 350;
                                    }
                                } else {
                                    return 350;
                                }
                            }
                        }
                    } else {
                        if (lat < 4.5) {
                            if (lng < -64.25) {
                                if (lng < -64.5) {
                                    if (lat < 4.25) {
                                        return 398;
                                    } else {
                                        return 350;
                                    }
                                } else {
                                    if (lat < 4.25) {
                                        return 350;
                                    } else {
                                        return 398;
                                    }
                                }
                            } else {
                                if (lng < -64.0) {
                                    if (lat < 4.25) {
                                        return 350;
                                    } else {
                                        return 398;
                                    }
                                } else {
                                    return 398;
                                }
                            }
                        } else {
                            return 398;
                        }
                    }
                } else {
                    if (lat < 4.0) {
                        if (lng < -62.75) {
                            if (lat < 3.75) {
                                return 350;
                            } else {
                                if (lng < -63.0) {
                                    return 350;
                                } else {
                                    return 398;
                                }
                            }
                        } else {
                            if (lat < 3.75) {
                                return 350;
                            } else {
                                if (lng < -62.5) {
                                    return 398;
                                } else {
                                    return 350;
                                }
                            }
                        }
                    } else {
                        if (lng < -62.75) {
                            return 398;
                        } else {
                            if (lat < 4.25) {
                                return 350;
                            } else {
                                return 398;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup55(double lat, double lng)
    {
        if (lng < -59.25) {
            if (lat < 2.75) {
                if (lng < -60.0) {
                    return 350;
                } else {
                    if (lat < 1.25) {
                        if (lat < 0.25) {
                            return 7;
                        } else {
                            return 350;
                        }
                    } else {
                        if (lat < 2.0) {
                            if (lng < -59.5) {
                                return 350;
                            } else {
                                if (lat < 1.75) {
                                    return 350;
                                } else {
                                    return 299;
                                }
                            }
                        } else {
                            if (lng < -59.75) {
                                return 350;
                            } else {
                                if (lat < 2.25) {
                                    if (lng < -59.5) {
                                        return 350;
                                    } else {
                                        return 299;
                                    }
                                } else {
                                    if (lng < -59.5) {
                                        if (lat < 2.5) {
                                            return 350;
                                        } else {
                                            return 299;
                                        }
                                    } else {
                                        return 299;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -60.75) {
                    if (lat < 4.25) {
                        return 350;
                    } else {
                        if (lng < -61.5) {
                            return 398;
                        } else {
                            if (lat < 4.75) {
                                if (lng < -61.25) {
                                    if (lat < 4.5) {
                                        return 350;
                                    } else {
                                        return 398;
                                    }
                                } else {
                                    return 350;
                                }
                            } else {
                                return 398;
                            }
                        }
                    }
                } else {
                    if (lat < 4.0) {
                        if (lng < -59.75) {
                            return 350;
                        } else {
                            if (lat < 3.75) {
                                return 299;
                            } else {
                                if (lng < -59.5) {
                                    return 350;
                                } else {
                                    return 299;
                                }
                            }
                        }
                    } else {
                        if (lng < -60.0) {
                            if (lat < 5.0) {
                                return 350;
                            } else {
                                if (lng < -60.5) {
                                    if (lat < 5.25) {
                                        return 398;
                                    } else {
                                        return 299;
                                    }
                                } else {
                                    if (lng < -60.25) {
                                        if (lat < 5.25) {
                                            return 350;
                                        } else {
                                            return 299;
                                        }
                                    } else {
                                        if (lat < 5.25) {
                                            return 350;
                                        } else {
                                            return 299;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 4.5) {
                                if (lng < -59.5) {
                                    return 350;
                                } else {
                                    return 299;
                                }
                            } else {
                                if (lat < 5.0) {
                                    return 299;
                                } else {
                                    if (lng < -59.75) {
                                        if (lat < 5.25) {
                                            return 350;
                                        } else {
                                            return 299;
                                        }
                                    } else {
                                        return 299;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 2.75) {
                if (lng < -57.75) {
                    if (lat < 1.25) {
                        if (lng < -58.75) {
                            if (lat < 0.25) {
                                return 7;
                            } else {
                                return 350;
                            }
                        } else {
                            return 311;
                        }
                    } else {
                        if (lng < -58.5) {
                            if (lat < 1.5) {
                                if (lng < -58.75) {
                                    return 350;
                                } else {
                                    return 299;
                                }
                            } else {
                                return 299;
                            }
                        } else {
                            if (lat < 1.75) {
                                if (lng < -58.25) {
                                    if (lat < 1.5) {
                                        return 311;
                                    } else {
                                        return 299;
                                    }
                                } else {
                                    return 311;
                                }
                            } else {
                                return 299;
                            }
                        }
                    }
                } else {
                    if (lat < 1.75) {
                        return 311;
                    } else {
                        if (lng < -57.0) {
                            if (lat < 2.0) {
                                if (lng < -57.5) {
                                    return 299;
                                } else {
                                    return 311;
                                }
                            } else {
                                return 299;
                            }
                        } else {
                            if (lat < 2.25) {
                                if (lng < -56.75) {
                                    if (lat < 2.0) {
                                        return 311;
                                    } else {
                                        return 299;
                                    }
                                } else {
                                    if (lng < -56.5) {
                                        if (lat < 2.0) {
                                            return 311;
                                        } else {
                                            return 299;
                                        }
                                    } else {
                                        if (lat < 2.0) {
                                            return 311;
                                        } else {
                                            return 201;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -56.75) {
                                    return 299;
                                } else {
                                    return 201;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -57.75) {
                    if (lat < 4.0) {
                        return 299;
                    } else {
                        if (lng < -58.0) {
                            return 299;
                        } else {
                            if (lat < 4.5) {
                                return 201;
                            } else {
                                return 299;
                            }
                        }
                    }
                } else {
                    if (lat < 4.0) {
                        if (lng < -57.0) {
                            if (lat < 3.25) {
                                return 299;
                            } else {
                                if (lng < -57.5) {
                                    if (lat < 3.75) {
                                        return 299;
                                    } else {
                                        return 201;
                                    }
                                } else {
                                    if (lat < 3.5) {
                                        if (lng < -57.25) {
                                            return 299;
                                        } else {
                                            return 201;
                                        }
                                    } else {
                                        return 201;
                                    }
                                }
                            }
                        } else {
                            return 201;
                        }
                    } else {
                        if (lng < -57.25) {
                            if (lat < 5.0) {
                                return 201;
                            } else {
                                if (lng < -57.5) {
                                    return 299;
                                } else {
                                    if (lat < 5.25) {
                                        return 201;
                                    } else {
                                        return 299;
                                    }
                                }
                            }
                        } else {
                            return 201;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup56(double lat, double lng)
    {
        if (lng < -62.0) {
            if (lat < 5.5) {
                return kdLookup54(lat,lng);
            } else {
                if (lat < 10.5) {
                    if (lng < -67.25) {
                        if (lat < 10.25) {
                            if (lat < 10.0) {
                                if (lat < 9.75) {
                                    if (lat < 9.5) {
                                        if (lat < 9.25) {
                                            if (lat < 9.0) {
                                                if (lat < 8.75) {
                                                    if (lat < 8.5) {
                                                        if (lat < 8.25) {
                                                            if (lat < 8.0) {
                                                                if (lat < 7.75) {
                                                                    if (lat < 7.5) {
                                                                        if (lat < 7.25) {
                                                                            if (lat < 7.0) {
                                                                                if (lat < 6.75) {
                                                                                    if (lat < 6.0) {
                                                                                        return 398;
                                                                                    } else {
                                                                                        if (lat < 6.25) {
                                                                                            return 391;
                                                                                        } else {
                                                                                            return 398;
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    return 398;
                                                                                }
                                                                            } else {
                                                                                return 398;
                                                                            }
                                                                        } else {
                                                                            return 398;
                                                                        }
                                                                    } else {
                                                                        return 398;
                                                                    }
                                                                } else {
                                                                    return 398;
                                                                }
                                                            } else {
                                                                return 398;
                                                            }
                                                        } else {
                                                            return 398;
                                                        }
                                                    } else {
                                                        return 398;
                                                    }
                                                } else {
                                                    return 398;
                                                }
                                            } else {
                                                return 398;
                                            }
                                        } else {
                                            return 398;
                                        }
                                    } else {
                                        return 398;
                                    }
                                } else {
                                    return 398;
                                }
                            } else {
                                return 398;
                            }
                        } else {
                            return 398;
                        }
                    } else {
                        return 398;
                    }
                } else {
                    return 398;
                }
            }
        } else {
            if (lat < 5.5) {
                return kdLookup55(lat,lng);
            } else {
                if (lng < -59.25) {
                    if (lat < 8.25) {
                        if (lng < -60.75) {
                            if (lat < 6.75) {
                                if (lng < -61.25) {
                                    return 398;
                                } else {
                                    if (lat < 6.0) {
                                        if (lng < -61.0) {
                                            return 398;
                                        } else {
                                            return 299;
                                        }
                                    } else {
                                        if (lat < 6.25) {
                                            return 299;
                                        } else {
                                            if (lng < -61.0) {
                                                return 398;
                                            } else {
                                                return 299;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 398;
                            }
                        } else {
                            if (lat < 7.0) {
                                return 299;
                            } else {
                                if (lng < -60.0) {
                                    if (lat < 7.5) {
                                        if (lng < -60.5) {
                                            return 398;
                                        } else {
                                            if (lng < -60.25) {
                                                if (lat < 7.25) {
                                                    return 398;
                                                } else {
                                                    return 299;
                                                }
                                            } else {
                                                return 299;
                                            }
                                        }
                                    } else {
                                        if (lng < -60.5) {
                                            return 398;
                                        } else {
                                            if (lat < 8.0) {
                                                return 299;
                                            } else {
                                                return 398;
                                            }
                                        }
                                    }
                                } else {
                                    return 299;
                                }
                            }
                        }
                    } else {
                        if (lat < 9.75) {
                            if (lng < -60.5) {
                                return 398;
                            } else {
                                if (lat < 9.0) {
                                    if (lng < -59.75) {
                                        return 398;
                                    } else {
                                        return 299;
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        } else {
                            if (lng < -60.75) {
                                if (lat < 10.25) {
                                    if (lng < -61.25) {
                                        return 398;
                                    } else {
                                        if (lng < -61.0) {
                                            if (lat < 10.0) {
                                                return 398;
                                            } else {
                                                return 169;
                                            }
                                        } else {
                                            if (lat < 10.0) {
                                                return 398;
                                            } else {
                                                return 169;
                                            }
                                        }
                                    }
                                } else {
                                    return 169;
                                }
                            } else {
                                return 169;
                            }
                        }
                    }
                } else {
                    if (lat < 8.25) {
                        if (lng < -57.75) {
                            return 299;
                        } else {
                            if (lat < 6.75) {
                                if (lng < -57.0) {
                                    if (lat < 6.0) {
                                        if (lng < -57.25) {
                                            return 299;
                                        } else {
                                            if (lat < 5.75) {
                                                return 201;
                                            } else {
                                                return 299;
                                            }
                                        }
                                    } else {
                                        return 299;
                                    }
                                } else {
                                    return 201;
                                }
                            } else {
                                return 299;
                            }
                        }
                    } else {
                        return 299;
                    }
                }
            }
        }
    }

    public static int kdLookup57(double lat, double lng)
    {
        if (lng < -53.5) {
            if (lat < 2.75) {
                if (lng < -55.0) {
                    if (lat < 2.0) {
                        return 311;
                    } else {
                        if (lng < -55.75) {
                            if (lat < 2.25) {
                                return 201;
                            } else {
                                if (lng < -56.0) {
                                    return 201;
                                } else {
                                    if (lat < 2.5) {
                                        return 311;
                                    } else {
                                        return 201;
                                    }
                                }
                            }
                        } else {
                            if (lng < -55.5) {
                                if (lat < 2.5) {
                                    return 311;
                                } else {
                                    return 201;
                                }
                            } else {
                                if (lat < 2.5) {
                                    return 311;
                                } else {
                                    return 201;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 1.5) {
                        return 311;
                    } else {
                        if (lng < -54.25) {
                            if (lat < 2.0) {
                                if (lng < -54.5) {
                                    return 311;
                                } else {
                                    if (lat < 1.75) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                }
                            } else {
                                if (lng < -54.75) {
                                    return 311;
                                } else {
                                    if (lat < 2.25) {
                                        return 401;
                                    } else {
                                        if (lng < -54.5) {
                                            if (lat < 2.5) {
                                                return 311;
                                            } else {
                                                return 201;
                                            }
                                        } else {
                                            if (lat < 2.5) {
                                                return 401;
                                            } else {
                                                return 201;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 2.0) {
                                if (lng < -54.0) {
                                    if (lat < 1.75) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                } else {
                                    if (lng < -53.75) {
                                        if (lat < 1.75) {
                                            return 311;
                                        } else {
                                            return 401;
                                        }
                                    } else {
                                        return 401;
                                    }
                                }
                            } else {
                                if (lng < -54.0) {
                                    if (lat < 2.25) {
                                        return 401;
                                    } else {
                                        return 363;
                                    }
                                } else {
                                    if (lat < 2.25) {
                                        return 401;
                                    } else {
                                        if (lng < -53.75) {
                                            return 363;
                                        } else {
                                            if (lat < 2.5) {
                                                return 401;
                                            } else {
                                                return 363;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -54.25) {
                    return 201;
                } else {
                    if (lat < 4.0) {
                        if (lat < 3.25) {
                            if (lng < -54.0) {
                                return 201;
                            } else {
                                return 363;
                            }
                        } else {
                            if (lng < -54.0) {
                                return 201;
                            } else {
                                return 363;
                            }
                        }
                    } else {
                        return 363;
                    }
                }
            }
        } else {
            if (lat < 2.75) {
                if (lng < -52.5) {
                    if (lat < 1.25) {
                        if (lat < 0.5) {
                            if (lng < -53.0) {
                                return 311;
                            } else {
                                if (lng < -52.75) {
                                    if (lat < 0.25) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                } else {
                                    return 401;
                                }
                            }
                        } else {
                            if (lng < -53.0) {
                                if (lat < 1.0) {
                                    return 311;
                                } else {
                                    if (lng < -53.25) {
                                        return 311;
                                    } else {
                                        return 401;
                                    }
                                }
                            } else {
                                return 401;
                            }
                        }
                    } else {
                        if (lat < 2.0) {
                            if (lng < -53.25) {
                                if (lat < 1.5) {
                                    return 311;
                                } else {
                                    return 401;
                                }
                            } else {
                                return 401;
                            }
                        } else {
                            if (lng < -53.0) {
                                if (lat < 2.5) {
                                    return 401;
                                } else {
                                    return 363;
                                }
                            } else {
                                if (lat < 2.25) {
                                    return 401;
                                } else {
                                    if (lng < -52.75) {
                                        return 363;
                                    } else {
                                        if (lat < 2.5) {
                                            return 401;
                                        } else {
                                            return 363;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return 401;
                }
            } else {
                if (lng < -52.25) {
                    return 363;
                } else {
                    if (lat < 4.0) {
                        if (lng < -51.75) {
                            if (lat < 3.5) {
                                return 401;
                            } else {
                                if (lng < -52.0) {
                                    return 363;
                                } else {
                                    if (lat < 3.75) {
                                        return 401;
                                    } else {
                                        return 363;
                                    }
                                }
                            }
                        } else {
                            return 401;
                        }
                    } else {
                        if (lng < -51.5) {
                            return 363;
                        } else {
                            return 401;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup58(double lat, double lng)
    {
        if (lng < -67.5) {
            if (lat < 22.5) {
                if (lng < -78.75) {
                    if (lat < 11.25) {
                        if (lng < -84.5) {
                            if (lat < 5.5) {
                                return 0;
                            } else {
                                if (lat < 8.25) {
                                    return 0;
                                } else {
                                    if (lng < -87.25) {
                                        return 0;
                                    } else {
                                        if (lat < 9.75) {
                                            return 397;
                                        } else {
                                            if (lng < -86.0) {
                                                return 0;
                                            } else {
                                                if (lng < -85.5) {
                                                    return 397;
                                                } else {
                                                    if (lat < 11.0) {
                                                        return 397;
                                                    } else {
                                                        if (lng < -85.0) {
                                                            return 397;
                                                        } else {
                                                            if (lng < -84.75) {
                                                                return 260;
                                                            } else {
                                                                return 397;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 5.5) {
                                if (lng < -81.75) {
                                    return 0;
                                } else {
                                    if (lat < 2.75) {
                                        if (lng < -80.25) {
                                            return 0;
                                        } else {
                                            if (lat < 1.5) {
                                                return 382;
                                            } else {
                                                return 391;
                                            }
                                        }
                                    } else {
                                        return 0;
                                    }
                                }
                            } else {
                                if (lng < -81.75) {
                                    if (lat < 8.25) {
                                        return 22;
                                    } else {
                                        if (lat < 9.75) {
                                            if (lng < -82.75) {
                                                return 397;
                                            } else {
                                                return 22;
                                            }
                                        } else {
                                            if (lng < -83.25) {
                                                if (lat < 11.0) {
                                                    return 397;
                                                } else {
                                                    if (lng < -84.0) {
                                                        if (lng < -84.25) {
                                                            return 397;
                                                        } else {
                                                            return 260;
                                                        }
                                                    } else {
                                                        return 260;
                                                    }
                                                }
                                            } else {
                                                if (lng < -82.5) {
                                                    return 397;
                                                } else {
                                                    return 22;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 22;
                                }
                            }
                        }
                    } else {
                        if (lng < -84.5) {
                            return TimeZoneMapperMethodsTwo.kdLookup48(lat,lng);
                        } else {
                            if (lat < 16.75) {
                                if (lng < -81.75) {
                                    if (lat < 14.0) {
                                        return 260;
                                    } else {
                                        if (lng < -83.25) {
                                            if (lat < 15.25) {
                                                if (lng < -84.0) {
                                                    if (lat < 14.75) {
                                                        return 260;
                                                    } else {
                                                        return 146;
                                                    }
                                                } else {
                                                    if (lat < 14.75) {
                                                        return 260;
                                                    } else {
                                                        if (lng < -83.75) {
                                                            return 146;
                                                        } else {
                                                            if (lng < -83.5) {
                                                                if (lat < 15.0) {
                                                                    return 260;
                                                                } else {
                                                                    return 146;
                                                                }
                                                            } else {
                                                                return 260;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                return 146;
                                            }
                                        } else {
                                            if (lat < 15.25) {
                                                if (lng < -82.5) {
                                                    if (lat < 14.5) {
                                                        return 260;
                                                    } else {
                                                        if (lng < -83.0) {
                                                            if (lat < 14.75) {
                                                                return 260;
                                                            } else {
                                                                return 146;
                                                            }
                                                        } else {
                                                            if (lat < 14.75) {
                                                                return 260;
                                                            } else {
                                                                return 146;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    return 0;
                                                }
                                            } else {
                                                return 146;
                                            }
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            } else {
                                if (lng < -80.0) {
                                    return 377;
                                } else {
                                    if (lat < 19.5) {
                                        return 0;
                                    } else {
                                        if (lat < 21.0) {
                                            return 222;
                                        } else {
                                            return 377;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return kdLookup50(lat,lng);
                }
            } else {
                return kdLookup53(lat,lng);
            }
        } else {
            if (lat < 22.5) {
                if (lng < -56.25) {
                    if (lat < 11.25) {
                        return kdLookup56(lat,lng);
                    } else {
                        if (lng < -62.0) {
                            if (lat < 16.75) {
                                return 398;
                            } else {
                                if (lat < 19.5) {
                                    if (lng < -64.75) {
                                        if (lng < -66.25) {
                                            return 394;
                                        } else {
                                            if (lat < 18.0) {
                                                if (lng < -65.5) {
                                                    return 394;
                                                } else {
                                                    return 339;
                                                }
                                            } else {
                                                return 394;
                                            }
                                        }
                                    } else {
                                        if (lng < -63.5) {
                                            return 339;
                                        } else {
                                            return 177;
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        } else {
                            if (lat < 16.75) {
                                if (lng < -59.25) {
                                    if (lat < 14.0) {
                                        if (lng < -60.75) {
                                            if (lat < 12.5) {
                                                return 86;
                                            } else {
                                                if (lat < 13.25) {
                                                    return 2;
                                                } else {
                                                    if (lng < -61.5) {
                                                        return 0;
                                                    } else {
                                                        if (lng < -61.25) {
                                                            return 2;
                                                        } else {
                                                            if (lat < 13.5) {
                                                                return 2;
                                                            } else {
                                                                if (lng < -61.0) {
                                                                    if (lat < 13.75) {
                                                                        return 2;
                                                                    } else {
                                                                        return 387;
                                                                    }
                                                                } else {
                                                                    return 387;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            return 387;
                                        }
                                    } else {
                                        if (lng < -60.75) {
                                            if (lat < 15.25) {
                                                if (lng < -61.5) {
                                                    return 0;
                                                } else {
                                                    if (lat < 14.5) {
                                                        return 387;
                                                    } else {
                                                        return 329;
                                                    }
                                                }
                                            } else {
                                                return 278;
                                            }
                                        } else {
                                            if (lat < 15.25) {
                                                if (lng < -60.0) {
                                                    if (lat < 14.5) {
                                                        return 387;
                                                    } else {
                                                        return 329;
                                                    }
                                                } else {
                                                    return 0;
                                                }
                                            } else {
                                                return 0;
                                            }
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            } else {
                                return 205;
                            }
                        }
                    }
                } else {
                    if (lat < 11.25) {
                        if (lng < -50.75) {
                            if (lat < 5.5) {
                                return kdLookup57(lat,lng);
                            } else {
                                if (lat < 8.25) {
                                    if (lng < -53.5) {
                                        if (lng < -55.0) {
                                            return 201;
                                        } else {
                                            if (lat < 6.75) {
                                                if (lng < -54.25) {
                                                    return 201;
                                                } else {
                                                    if (lat < 6.0) {
                                                        if (lng < -54.0) {
                                                            return 201;
                                                        } else {
                                                            if (lng < -53.75) {
                                                                if (lat < 5.75) {
                                                                    return 363;
                                                                } else {
                                                                    return 201;
                                                                }
                                                            } else {
                                                                return 363;
                                                            }
                                                        }
                                                    } else {
                                                        return 201;
                                                    }
                                                }
                                            } else {
                                                return 0;
                                            }
                                        }
                                    } else {
                                        return 363;
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        } else {
                            return 401;
                        }
                    } else {
                        return 0;
                    }
                }
            } else {
                if (lng < -56.25) {
                    if (lat < 33.75) {
                        return 0;
                    } else {
                        if (lng < -62.0) {
                            if (lat < 39.25) {
                                return 0;
                            } else {
                                if (lat < 42.0) {
                                    return 0;
                                } else {
                                    if (lng < -65.75) {
                                        if (lat < 43.5) {
                                            return 0;
                                        } else {
                                            if (lng < -66.75) {
                                                return 166;
                                            } else {
                                                return 118;
                                            }
                                        }
                                    } else {
                                        return 118;
                                    }
                                }
                            }
                        } else {
                            return 0;
                        }
                    }
                } else {
                    return 0;
                }
            }
        }
    }

    public static int kdLookup59(double lat, double lng)
    {
        if (lng < -87.25) {
            if (lat < 47.75) {
                if (lng < -88.75) {
                    if (lat < 46.25) {
                        return 161;
                    } else {
                        if (lat < 47.0) {
                            if (lng < -89.5) {
                                if (lat < 46.5) {
                                    return 161;
                                } else {
                                    if (lng < -89.75) {
                                        return 94;
                                    } else {
                                        if (lat < 46.75) {
                                            return 94;
                                        } else {
                                            return 93;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -89.25) {
                                    if (lat < 46.75) {
                                        return 94;
                                    } else {
                                        return 93;
                                    }
                                } else {
                                    if (lat < 46.5) {
                                        return 94;
                                    } else {
                                        return 93;
                                    }
                                }
                            }
                        } else {
                            return 93;
                        }
                    }
                } else {
                    if (lat < 46.25) {
                        if (lng < -88.0) {
                            if (lat < 46.0) {
                                return 161;
                            } else {
                                if (lng < -88.5) {
                                    return 161;
                                } else {
                                    return 94;
                                }
                            }
                        } else {
                            if (lat < 45.5) {
                                if (lng < -87.5) {
                                    return 161;
                                } else {
                                    return 94;
                                }
                            } else {
                                if (lng < -87.75) {
                                    if (lat < 46.0) {
                                        return 161;
                                    } else {
                                        return 94;
                                    }
                                } else {
                                    if (lat < 46.0) {
                                        return 94;
                                    } else {
                                        if (lng < -87.5) {
                                            return 94;
                                        } else {
                                            return 93;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -88.0) {
                            if (lat < 46.5) {
                                return 94;
                            } else {
                                return 93;
                            }
                        } else {
                            return 93;
                        }
                    }
                }
            } else {
                if (lng < -88.75) {
                    if (lat < 48.75) {
                        if (lng < -89.5) {
                            if (lat < 48.25) {
                                return 161;
                            } else {
                                return 239;
                            }
                        } else {
                            if (lat < 48.25) {
                                return 93;
                            } else {
                                if (lng < -89.25) {
                                    return 239;
                                } else {
                                    return 359;
                                }
                            }
                        }
                    } else {
                        return 239;
                    }
                } else {
                    if (lat < 48.75) {
                        if (lng < -88.0) {
                            if (lat < 48.25) {
                                return 93;
                            } else {
                                return 239;
                            }
                        } else {
                            return 239;
                        }
                    } else {
                        if (lat < 50.25) {
                            if (lng < -87.5) {
                                if (lat < 50.0) {
                                    if (lng < -88.25) {
                                        return 239;
                                    } else {
                                        if (lat < 49.75) {
                                            if (lat < 49.5) {
                                                if (lng < -88.0) {
                                                    if (lat < 49.0) {
                                                        return 239;
                                                    } else {
                                                        if (lat < 49.25) {
                                                            return 336;
                                                        } else {
                                                            return 239;
                                                        }
                                                    }
                                                } else {
                                                    return 239;
                                                }
                                            } else {
                                                return 239;
                                            }
                                        } else {
                                            return 239;
                                        }
                                    }
                                } else {
                                    return 239;
                                }
                            } else {
                                return 239;
                            }
                        } else {
                            return 239;
                        }
                    }
                }
            }
        } else {
            if (lat < 47.75) {
                if (lng < -86.0) {
                    if (lat < 46.25) {
                        if (lng < -86.75) {
                            if (lat < 45.75) {
                                return 161;
                            } else {
                                return 93;
                            }
                        } else {
                            if (lat < 45.5) {
                                return 161;
                            } else {
                                return 93;
                            }
                        }
                    } else {
                        return 93;
                    }
                } else {
                    if (lat < 46.5) {
                        return 93;
                    } else {
                        if (lng < -85.25) {
                            if (lat < 47.0) {
                                return 93;
                            } else {
                                return 239;
                            }
                        } else {
                            if (lat < 47.0) {
                                return 93;
                            } else {
                                return 239;
                            }
                        }
                    }
                }
            } else {
                return 239;
            }
        }
    }

    public static int kdLookup60(double lat, double lng)
    {
        if (lng < -84.5) {
            if (lat < 50.5) {
                return kdLookup59(lat,lng);
            } else {
                if (lat < 53.25) {
                    if (lng < -89.0) {
                        if (lat < 52.75) {
                            return 239;
                        } else {
                            return 285;
                        }
                    } else {
                        return 239;
                    }
                } else {
                    if (lng < -87.25) {
                        if (lat < 54.0) {
                            if (lng < -88.5) {
                                return 285;
                            } else {
                                return 239;
                            }
                        } else {
                            return 239;
                        }
                    } else {
                        return 239;
                    }
                }
            }
        } else {
            if (lat < 50.5) {
                if (lng < -81.75) {
                    if (lat < 46.5) {
                        if (lng < -83.25) {
                            if (lat < 46.25) {
                                return 93;
                            } else {
                                if (lng < -84.0) {
                                    return 93;
                                } else {
                                    return 239;
                                }
                            }
                        } else {
                            if (lng < -83.0) {
                                if (lat < 45.75) {
                                    return 93;
                                } else {
                                    return 239;
                                }
                            } else {
                                return 239;
                            }
                        }
                    } else {
                        return 239;
                    }
                } else {
                    if (lat < 47.75) {
                        if (lng < -79.5) {
                            return 239;
                        } else {
                            if (lat < 46.75) {
                                return 239;
                            } else {
                                if (lat < 47.25) {
                                    if (lng < -79.25) {
                                        return 239;
                                    } else {
                                        if (lng < -79.0) {
                                            if (lat < 47.0) {
                                                return 239;
                                            } else {
                                                return 20;
                                            }
                                        } else {
                                            return 20;
                                        }
                                    }
                                } else {
                                    if (lng < -79.25) {
                                        if (lat < 47.5) {
                                            return 239;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -79.5) {
                            return 239;
                        } else {
                            return 20;
                        }
                    }
                }
            } else {
                if (lng < -81.75) {
                    if (lat < 53.25) {
                        if (lng < -82.25) {
                            return 239;
                        } else {
                            if (lat < 53.0) {
                                return 239;
                            } else {
                                return 138;
                            }
                        }
                    } else {
                        return 239;
                    }
                } else {
                    if (lat < 53.25) {
                        if (lng < -80.25) {
                            if (lat < 52.0) {
                                return 239;
                            } else {
                                if (lng < -81.0) {
                                    if (lat < 52.5) {
                                        return 239;
                                    } else {
                                        if (lng < -81.5) {
                                            if (lat < 52.75) {
                                                return 239;
                                            } else {
                                                return 138;
                                            }
                                        } else {
                                            if (lat < 52.75) {
                                                return 239;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 52.5) {
                                        return 239;
                                    } else {
                                        return 138;
                                    }
                                }
                            }
                        } else {
                            if (lat < 51.75) {
                                if (lng < -79.5) {
                                    if (lat < 51.25) {
                                        return 239;
                                    } else {
                                        if (lng < -80.0) {
                                            return 239;
                                        } else {
                                            if (lng < -79.75) {
                                                return 239;
                                            } else {
                                                return 20;
                                            }
                                        }
                                    }
                                } else {
                                    return 20;
                                }
                            } else {
                                if (lng < -79.5) {
                                    return 138;
                                } else {
                                    if (lat < 52.5) {
                                        if (lng < -79.25) {
                                            return 138;
                                        } else {
                                            if (lat < 52.0) {
                                                return 20;
                                            } else {
                                                if (lng < -79.0) {
                                                    return 138;
                                                } else {
                                                    return 20;
                                                }
                                            }
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -80.25) {
                            return 138;
                        } else {
                            if (lat < 54.75) {
                                if (lng < -79.5) {
                                    if (lat < 54.0) {
                                        return 138;
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    return 20;
                                }
                            } else {
                                if (lng < -79.5) {
                                    return 138;
                                } else {
                                    if (lat < 55.5) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup61(double lat, double lng)
    {
        if (lng < -84.5) {
            if (lat < 61.75) {
                if (lng < -87.25) {
                    if (lat < 59.0) {
                        if (lng < -88.75) {
                            if (lat < 57.5) {
                                if (lng < -89.5) {
                                    if (lat < 56.75) {
                                        if (lng < -89.75) {
                                            return 285;
                                        } else {
                                            if (lat < 56.5) {
                                                return 239;
                                            } else {
                                                return 285;
                                            }
                                        }
                                    } else {
                                        return 285;
                                    }
                                } else {
                                    if (lat < 56.75) {
                                        return 239;
                                    } else {
                                        if (lng < -89.25) {
                                            return 285;
                                        } else {
                                            if (lat < 57.0) {
                                                if (lng < -89.0) {
                                                    return 285;
                                                } else {
                                                    return 239;
                                                }
                                            } else {
                                                if (lng < -89.0) {
                                                    return 285;
                                                } else {
                                                    return 239;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 0;
                            }
                        } else {
                            return 239;
                        }
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } else {
                if (lat < 64.5) {
                    if (lng < -87.25) {
                        return 308;
                    } else {
                        return 64;
                    }
                } else {
                    if (lng < -87.25) {
                        if (lat < 67.0) {
                            return 308;
                        } else {
                            if (lng < -89.0) {
                                return 108;
                            } else {
                                return 308;
                            }
                        }
                    } else {
                        if (lat < 66.0) {
                            if (lng < -86.0) {
                                if (lat < 65.25) {
                                    if (lng < -86.75) {
                                        return 308;
                                    } else {
                                        return 64;
                                    }
                                } else {
                                    if (lng < -86.25) {
                                        return 308;
                                    } else {
                                        if (lat < 65.75) {
                                            return 64;
                                        } else {
                                            return 308;
                                        }
                                    }
                                }
                            } else {
                                return 64;
                            }
                        } else {
                            if (lng < -85.75) {
                                return 308;
                            } else {
                                if (lat < 66.75) {
                                    if (lng < -85.25) {
                                        if (lat < 66.25) {
                                            return 64;
                                        } else {
                                            return 308;
                                        }
                                    } else {
                                        if (lng < -85.0) {
                                            if (lat < 66.25) {
                                                return 64;
                                            } else {
                                                return 308;
                                            }
                                        } else {
                                            if (lat < 66.25) {
                                                return 64;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -85.0) {
                                        return 308;
                                    } else {
                                        return 138;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 61.75) {
                return 138;
            } else {
                if (lng < -81.75) {
                    if (lat < 64.5) {
                        return 64;
                    } else {
                        if (lat < 66.0) {
                            if (lng < -83.25) {
                                if (lat < 65.5) {
                                    return 64;
                                } else {
                                    if (lng < -84.0) {
                                        if (lng < -84.25) {
                                            return 64;
                                        } else {
                                            if (lat < 65.75) {
                                                return 64;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    } else {
                                        return 138;
                                    }
                                }
                            } else {
                                return 64;
                            }
                        } else {
                            return 138;
                        }
                    }
                } else {
                    if (lat < 64.5) {
                        if (lng < -80.25) {
                            if (lat < 63.0) {
                                if (lng < -81.0) {
                                    return 64;
                                } else {
                                    return 138;
                                }
                            } else {
                                return 64;
                            }
                        } else {
                            if (lat < 63.0) {
                                return 138;
                            } else {
                                return 64;
                            }
                        }
                    } else {
                        if (lng < -80.25) {
                            if (lat < 66.0) {
                                return 64;
                            } else {
                                return 138;
                            }
                        } else {
                            return 0;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup62(double lat, double lng)
    {
        if (lng < -73.25) {
            if (lat < 50.5) {
                if (lng < -76.0) {
                    if (lat < 46.5) {
                        if (lng < -77.5) {
                            if (lat < 46.25) {
                                return 239;
                            } else {
                                if (lng < -78.0) {
                                    return 239;
                                } else {
                                    return 20;
                                }
                            }
                        } else {
                            if (lng < -76.75) {
                                if (lat < 46.0) {
                                    return 239;
                                } else {
                                    if (lng < -77.25) {
                                        if (lat < 46.25) {
                                            return 239;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            } else {
                                if (lat < 45.75) {
                                    if (lng < -76.25) {
                                        return 239;
                                    } else {
                                        if (lat < 45.5) {
                                            return 239;
                                        } else {
                                            return 20;
                                        }
                                    }
                                } else {
                                    if (lng < -76.5) {
                                        if (lat < 46.0) {
                                            return 239;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        }
                    } else {
                        return 20;
                    }
                } else {
                    if (lat < 45.75) {
                        if (lng < -74.75) {
                            if (lng < -75.5) {
                                if (lat < 45.5) {
                                    return 239;
                                } else {
                                    return 20;
                                }
                            } else {
                                return 239;
                            }
                        } else {
                            if (lng < -74.25) {
                                if (lat < 45.25) {
                                    return 20;
                                } else {
                                    return 239;
                                }
                            } else {
                                if (lng < -73.75) {
                                    return 20;
                                } else {
                                    if (lat < 45.25) {
                                        if (lng < -73.5) {
                                            return 166;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        }
                    } else {
                        return 20;
                    }
                }
            } else {
                if (lat < 56.0) {
                    return 20;
                } else {
                    if (lng < -77.0) {
                        return 138;
                    } else {
                        return 20;
                    }
                }
            }
        } else {
            if (lat < 49.25) {
                if (lng < -70.5) {
                    if (lat < 45.5) {
                        if (lng < -72.0) {
                            if (lng < -72.75) {
                                if (lng < -73.0) {
                                    if (lat < 45.25) {
                                        return 166;
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    return 20;
                                }
                            } else {
                                if (lng < -72.25) {
                                    return 20;
                                } else {
                                    if (lat < 45.25) {
                                        return 166;
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        } else {
                            if (lng < -71.25) {
                                if (lng < -71.5) {
                                    return 20;
                                } else {
                                    if (lat < 45.25) {
                                        return 166;
                                    } else {
                                        return 20;
                                    }
                                }
                            } else {
                                return 166;
                            }
                        }
                    } else {
                        return 20;
                    }
                } else {
                    if (lat < 47.0) {
                        if (lng < -69.0) {
                            if (lat < 46.0) {
                                if (lng < -70.25) {
                                    if (lat < 45.75) {
                                        return 166;
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    return 166;
                                }
                            } else {
                                if (lng < -69.75) {
                                    if (lat < 46.5) {
                                        if (lng < -70.25) {
                                            return 20;
                                        } else {
                                            return 166;
                                        }
                                    } else {
                                        if (lng < -70.0) {
                                            return 20;
                                        } else {
                                            if (lat < 46.75) {
                                                return 166;
                                            } else {
                                                return 20;
                                            }
                                        }
                                    }
                                } else {
                                    return 166;
                                }
                            }
                        } else {
                            if (lat < 46.0) {
                                if (lng < -67.75) {
                                    return 166;
                                } else {
                                    if (lat < 45.75) {
                                        return 166;
                                    } else {
                                        return 77;
                                    }
                                }
                            } else {
                                if (lng < -67.75) {
                                    return 166;
                                } else {
                                    return 77;
                                }
                            }
                        }
                    } else {
                        if (lng < -69.0) {
                            if (lat < 47.5) {
                                if (lng < -69.5) {
                                    return 20;
                                } else {
                                    if (lng < -69.25) {
                                        if (lat < 47.25) {
                                            return 166;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 166;
                                    }
                                }
                            } else {
                                return 20;
                            }
                        } else {
                            if (lat < 48.0) {
                                if (lng < -68.25) {
                                    if (lat < 47.5) {
                                        if (lng < -68.75) {
                                            if (lat < 47.25) {
                                                return 166;
                                            } else {
                                                return 77;
                                            }
                                        } else {
                                            if (lng < -68.5) {
                                                if (lat < 47.25) {
                                                    return 166;
                                                } else {
                                                    return 77;
                                                }
                                            } else {
                                                return 166;
                                            }
                                        }
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    if (lat < 47.5) {
                                        if (lng < -68.0) {
                                            return 166;
                                        } else {
                                            if (lng < -67.75) {
                                                if (lat < 47.25) {
                                                    return 166;
                                                } else {
                                                    return 77;
                                                }
                                            } else {
                                                return 77;
                                            }
                                        }
                                    } else {
                                        return 77;
                                    }
                                }
                            } else {
                                if (lng < -68.0) {
                                    return 20;
                                } else {
                                    if (lat < 48.25) {
                                        if (lng < -67.75) {
                                            return 77;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 54.0) {
                    return 20;
                } else {
                    if (lng < -67.75) {
                        return 20;
                    } else {
                        if (lat < 54.25) {
                            return 183;
                        } else {
                            return 20;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup63(double lat, double lng)
    {
        if (lng < -78.75) {
            if (lat < 56.25) {
                return kdLookup60(lat,lng);
            } else {
                return kdLookup61(lat,lng);
            }
        } else {
            if (lat < 56.25) {
                return kdLookup62(lat,lng);
            } else {
                if (lng < -73.25) {
                    if (lat < 61.75) {
                        if (lng < -76.5) {
                            if (lat < 58.5) {
                                if (lng < -77.75) {
                                    if (lat < 57.25) {
                                        return 138;
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    if (lat < 57.25) {
                                        return 20;
                                    } else {
                                        if (lng < -77.25) {
                                            if (lat < 57.75) {
                                                return 0;
                                            } else {
                                                if (lat < 58.0) {
                                                    return 0;
                                                } else {
                                                    if (lng < -77.5) {
                                                        return 138;
                                                    } else {
                                                        return 20;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lat < 57.5) {
                                                return 138;
                                            } else {
                                                return 20;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 60.5) {
                                    return 20;
                                } else {
                                    if (lng < -78.0) {
                                        return 138;
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        } else {
                            return 20;
                        }
                    } else {
                        if (lat < 64.5) {
                            if (lng < -76.0) {
                                if (lng < -77.5) {
                                    if (lat < 63.0) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                } else {
                                    if (lat < 63.0) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                }
                            } else {
                                if (lng < -74.75) {
                                    if (lat < 63.0) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                } else {
                                    if (lat < 63.0) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                }
                            }
                        } else {
                            return 138;
                        }
                    }
                } else {
                    if (lat < 61.75) {
                        if (lng < -70.5) {
                            return 20;
                        } else {
                            if (lat < 59.0) {
                                return 20;
                            } else {
                                if (lng < -69.0) {
                                    return 20;
                                } else {
                                    if (lat < 60.25) {
                                        if (lng < -68.25) {
                                            if (lat < 59.5) {
                                                if (lng < -68.75) {
                                                    return 138;
                                                } else {
                                                    return 20;
                                                }
                                            } else {
                                                return 0;
                                            }
                                        } else {
                                            return 138;
                                        }
                                    } else {
                                        if (lng < -68.25) {
                                            return 138;
                                        } else {
                                            if (lat < 61.0) {
                                                if (lng < -68.0) {
                                                    return 138;
                                                } else {
                                                    return 167;
                                                }
                                            } else {
                                                return 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -70.5) {
                            if (lat < 63.75) {
                                if (lng < -72.0) {
                                    if (lat < 62.75) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                } else {
                                    if (lat < 62.75) {
                                        return 20;
                                    } else {
                                        return 138;
                                    }
                                }
                            } else {
                                return 138;
                            }
                        } else {
                            if (lat < 64.5) {
                                if (lng < -68.25) {
                                    return 138;
                                } else {
                                    if (lat < 63.0) {
                                        if (lat < 62.25) {
                                            if (lng < -68.0) {
                                                return 138;
                                            } else {
                                                return 167;
                                            }
                                        } else {
                                            if (lng < -68.0) {
                                                return 138;
                                            } else {
                                                return 167;
                                            }
                                        }
                                    } else {
                                        if (lat < 63.75) {
                                            if (lng < -68.0) {
                                                return 138;
                                            } else {
                                                return 167;
                                            }
                                        } else {
                                            if (lng < -68.0) {
                                                return 138;
                                            } else {
                                                return 167;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -68.0) {
                                    return 138;
                                } else {
                                    return 167;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup64(double lat, double lng)
    {
        if (lat < 78.75) {
            if (lng < -84.5) {
                if (lat < 73.0) {
                    if (lng < -87.25) {
                        if (lat < 70.25) {
                            if (lng < -88.75) {
                                if (lat < 68.75) {
                                    if (lng < -89.0) {
                                        return 108;
                                    } else {
                                        return 308;
                                    }
                                } else {
                                    if (lat < 69.5) {
                                        if (lng < -89.5) {
                                            return 108;
                                        } else {
                                            if (lng < -89.0) {
                                                return 108;
                                            } else {
                                                return 308;
                                            }
                                        }
                                    } else {
                                        if (lng < -89.5) {
                                            return 0;
                                        } else {
                                            if (lng < -89.25) {
                                                return 0;
                                            } else {
                                                if (lat < 69.75) {
                                                    if (lng < -89.0) {
                                                        return 108;
                                                    } else {
                                                        return 308;
                                                    }
                                                } else {
                                                    return 0;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 308;
                            }
                        } else {
                            return 308;
                        }
                    } else {
                        if (lat < 70.25) {
                            if (lng < -86.0) {
                                return 308;
                            } else {
                                if (lat < 68.75) {
                                    if (lng < -85.25) {
                                        return 308;
                                    } else {
                                        if (lat < 68.0) {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        } else {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                } else {
                                    if (lng < -85.0) {
                                        return 308;
                                    } else {
                                        return 138;
                                    }
                                }
                            }
                        } else {
                            if (lng < -86.0) {
                                return 308;
                            } else {
                                if (lat < 71.5) {
                                    if (lng < -85.0) {
                                        return 308;
                                    } else {
                                        return 138;
                                    }
                                } else {
                                    if (lng < -85.25) {
                                        return 308;
                                    } else {
                                        if (lat < 72.25) {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        } else {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 75.75) {
                        if (lng < -86.25) {
                            return 308;
                        } else {
                            if (lat < 74.25) {
                                if (lng < -85.5) {
                                    return 308;
                                } else {
                                    if (lat < 73.5) {
                                        if (lng < -85.25) {
                                            return 308;
                                        } else {
                                            return 138;
                                        }
                                    } else {
                                        if (lng < -85.0) {
                                            return 308;
                                        } else {
                                            return 138;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -85.0) {
                                    return 308;
                                } else {
                                    return 138;
                                }
                            }
                        }
                    } else {
                        if (lng < -87.25) {
                            return 308;
                        } else {
                            if (lat < 77.25) {
                                if (lng < -86.0) {
                                    return 308;
                                } else {
                                    if (lng < -85.25) {
                                        return 308;
                                    } else {
                                        if (lat < 76.5) {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        } else {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -86.0) {
                                    return 308;
                                } else {
                                    if (lng < -85.25) {
                                        return 308;
                                    } else {
                                        if (lat < 78.0) {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        } else {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                return 138;
            }
        } else {
            if (lng < -84.5) {
                if (lat < 84.25) {
                    if (lng < -87.25) {
                        return 308;
                    } else {
                        if (lat < 81.5) {
                            if (lng < -85.25) {
                                return 308;
                            } else {
                                if (lat < 80.0) {
                                    if (lat < 79.25) {
                                        if (lng < -85.0) {
                                            return 308;
                                        } else {
                                            return 138;
                                        }
                                    } else {
                                        if (lng < -85.0) {
                                            return 308;
                                        } else {
                                            if (lat < 79.5) {
                                                if (lng < -84.75) {
                                                    return 308;
                                                } else {
                                                    return 138;
                                                }
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 80.75) {
                                        if (lng < -85.0) {
                                            return 308;
                                        } else {
                                            return 138;
                                        }
                                    } else {
                                        if (lng < -85.0) {
                                            return 308;
                                        } else {
                                            return 138;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -86.0) {
                                return 308;
                            } else {
                                if (lat < 82.75) {
                                    if (lng < -85.25) {
                                        return 308;
                                    } else {
                                        if (lat < 82.0) {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        } else {
                                            if (lng < -85.0) {
                                                return 308;
                                            } else {
                                                return 138;
                                            }
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        }
                    }
                } else {
                    return 0;
                }
            } else {
                return 138;
            }
        }
    }

    public static int kdLookup65(double lat, double lng)
    {
        if (lat < 53.25) {
            if (lng < -64.75) {
                if (lng < -66.25) {
                    if (lat < 53.0) {
                        return 20;
                    } else {
                        if (lng < -67.25) {
                            return 20;
                        } else {
                            return 183;
                        }
                    }
                } else {
                    if (lat < 51.75) {
                        return 20;
                    } else {
                        if (lng < -65.5) {
                            if (lat < 52.5) {
                                if (lng < -66.0) {
                                    return 20;
                                } else {
                                    if (lat < 52.25) {
                                        return 20;
                                    } else {
                                        return 183;
                                    }
                                }
                            } else {
                                return 183;
                            }
                        } else {
                            if (lat < 52.25) {
                                if (lng < -65.25) {
                                    return 20;
                                } else {
                                    if (lng < -65.0) {
                                        if (lat < 52.0) {
                                            return 20;
                                        } else {
                                            return 183;
                                        }
                                    } else {
                                        return 183;
                                    }
                                }
                            } else {
                                return 183;
                            }
                        }
                    }
                }
            } else {
                if (lng < -63.5) {
                    if (lat < 51.75) {
                        return 20;
                    } else {
                        if (lat < 52.5) {
                            if (lng < -64.25) {
                                if (lat < 52.0) {
                                    if (lng < -64.5) {
                                        return 20;
                                    } else {
                                        return 183;
                                    }
                                } else {
                                    return 183;
                                }
                            } else {
                                if (lng < -64.0) {
                                    if (lat < 52.25) {
                                        return 20;
                                    } else {
                                        return 183;
                                    }
                                } else {
                                    if (lat < 52.0) {
                                        return 20;
                                    } else {
                                        if (lng < -63.75) {
                                            return 20;
                                        } else {
                                            return 183;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -63.75) {
                                return 183;
                            } else {
                                if (lat < 52.75) {
                                    return 183;
                                } else {
                                    if (lat < 53.0) {
                                        return 20;
                                    } else {
                                        return 183;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 52.0) {
                        return 20;
                    } else {
                        return 183;
                    }
                }
            }
        } else {
            if (lng < -64.75) {
                if (lat < 54.75) {
                    if (lng < -66.75) {
                        if (lat < 54.0) {
                            if (lng < -67.25) {
                                if (lat < 53.75) {
                                    return 20;
                                } else {
                                    return 183;
                                }
                            } else {
                                if (lat < 53.5) {
                                    return 20;
                                } else {
                                    if (lng < -67.0) {
                                        if (lat < 53.75) {
                                            return 20;
                                        } else {
                                            return 183;
                                        }
                                    } else {
                                        return 183;
                                    }
                                }
                            }
                        } else {
                            if (lng < -67.25) {
                                if (lat < 54.5) {
                                    return 183;
                                } else {
                                    return 20;
                                }
                            } else {
                                return 183;
                            }
                        }
                    } else {
                        return 183;
                    }
                } else {
                    if (lng < -66.25) {
                        if (lat < 55.25) {
                            if (lng < -67.0) {
                                if (lng < -67.25) {
                                    return 20;
                                } else {
                                    if (lat < 55.0) {
                                        return 20;
                                    } else {
                                        return 183;
                                    }
                                }
                            } else {
                                if (lng < -66.75) {
                                    if (lat < 55.0) {
                                        return 183;
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    return 183;
                                }
                            }
                        } else {
                            return 20;
                        }
                    } else {
                        if (lng < -65.5) {
                            if (lat < 55.0) {
                                return 183;
                            } else {
                                return 20;
                            }
                        } else {
                            if (lat < 55.0) {
                                if (lng < -65.25) {
                                    return 20;
                                } else {
                                    return 183;
                                }
                            } else {
                                return 20;
                            }
                        }
                    }
                }
            } else {
                if (lat < 54.75) {
                    return 183;
                } else {
                    if (lng < -63.5) {
                        if (lat < 55.0) {
                            if (lng < -64.5) {
                                return 20;
                            } else {
                                if (lng < -64.0) {
                                    if (lng < -64.25) {
                                        return 183;
                                    } else {
                                        return 20;
                                    }
                                } else {
                                    if (lng < -63.75) {
                                        return 20;
                                    } else {
                                        return 183;
                                    }
                                }
                            }
                        } else {
                            return 20;
                        }
                    } else {
                        if (lng < -63.25) {
                            if (lat < 56.0) {
                                return 183;
                            } else {
                                return 20;
                            }
                        } else {
                            return 183;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup66(double lat, double lng)
    {
        if (lat < 50.5) {
            if (lng < -59.25) {
                if (lat < 47.75) {
                    if (lng < -60.75) {
                        return 118;
                    } else {
                        if (lat < 46.25) {
                            if (lng < -60.0) {
                                if (lat < 45.5) {
                                    return 0;
                                } else {
                                    if (lng < -60.5) {
                                        return 118;
                                    } else {
                                        if (lat < 45.75) {
                                            return 118;
                                        } else {
                                            if (lng < -60.25) {
                                                if (lat < 46.0) {
                                                    return 118;
                                                } else {
                                                    return 81;
                                                }
                                            } else {
                                                return 81;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 81;
                            }
                        } else {
                            if (lng < -60.0) {
                                if (lat < 47.0) {
                                    if (lng < -60.25) {
                                        return 118;
                                    } else {
                                        if (lat < 46.5) {
                                            return 81;
                                        } else {
                                            return 118;
                                        }
                                    }
                                } else {
                                    return 118;
                                }
                            } else {
                                return 81;
                            }
                        }
                    }
                } else {
                    if (lng < -60.75) {
                        if (lat < 49.0) {
                            return 0;
                        } else {
                            if (lat < 49.75) {
                                return 20;
                            } else {
                                if (lng < -61.25) {
                                    return 20;
                                } else {
                                    return 244;
                                }
                            }
                        }
                    } else {
                        if (lat < 49.0) {
                            return 291;
                        } else {
                            return 244;
                        }
                    }
                }
            } else {
                return 291;
            }
        } else {
            if (lng < -59.25) {
                if (lat < 53.25) {
                    if (lng < -60.75) {
                        if (lat < 52.0) {
                            return 20;
                        } else {
                            return 183;
                        }
                    } else {
                        if (lat < 51.75) {
                            if (lng < -59.75) {
                                return 20;
                            } else {
                                if (lat < 50.75) {
                                    return 244;
                                } else {
                                    return 20;
                                }
                            }
                        } else {
                            if (lng < -60.0) {
                                if (lat < 52.0) {
                                    return 20;
                                } else {
                                    return 183;
                                }
                            } else {
                                if (lat < 52.0) {
                                    return 20;
                                } else {
                                    return 183;
                                }
                            }
                        }
                    }
                } else {
                    return 183;
                }
            } else {
                if (lat < 53.25) {
                    if (lng < -57.75) {
                        if (lat < 51.75) {
                            if (lng < -58.5) {
                                if (lat < 51.0) {
                                    if (lng < -59.0) {
                                        return 20;
                                    } else {
                                        return 244;
                                    }
                                } else {
                                    if (lng < -59.0) {
                                        return 20;
                                    } else {
                                        if (lat < 51.25) {
                                            return 244;
                                        } else {
                                            if (lng < -58.75) {
                                                return 20;
                                            } else {
                                                if (lat < 51.5) {
                                                    return 244;
                                                } else {
                                                    return 20;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 51.0) {
                                    return 0;
                                } else {
                                    if (lng < -58.25) {
                                        if (lat < 51.5) {
                                            return 244;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        return 20;
                                    }
                                }
                            }
                        } else {
                            if (lng < -58.5) {
                                if (lat < 52.0) {
                                    return 20;
                                } else {
                                    return 183;
                                }
                            } else {
                                if (lat < 52.0) {
                                    return 20;
                                } else {
                                    return 183;
                                }
                            }
                        }
                    } else {
                        if (lat < 51.75) {
                            if (lng < -57.0) {
                                if (lat < 51.0) {
                                    return 291;
                                } else {
                                    return 244;
                                }
                            } else {
                                return 291;
                            }
                        } else {
                            if (lng < -57.0) {
                                if (lat < 52.5) {
                                    if (lng < -57.5) {
                                        if (lat < 52.0) {
                                            return 20;
                                        } else {
                                            return 183;
                                        }
                                    } else {
                                        if (lat < 52.0) {
                                            return 20;
                                        } else {
                                            return 183;
                                        }
                                    }
                                } else {
                                    if (lng < -57.25) {
                                        return 183;
                                    } else {
                                        if (lat < 53.0) {
                                            return 183;
                                        } else {
                                            return 291;
                                        }
                                    }
                                }
                            } else {
                                return 291;
                            }
                        }
                    }
                } else {
                    if (lng < -57.75) {
                        return 183;
                    } else {
                        if (lat < 54.75) {
                            if (lng < -57.0) {
                                if (lat < 53.75) {
                                    if (lng < -57.5) {
                                        return 183;
                                    } else {
                                        if (lng < -57.25) {
                                            if (lat < 53.5) {
                                                return 291;
                                            } else {
                                                return 183;
                                            }
                                        } else {
                                            return 291;
                                        }
                                    }
                                } else {
                                    return 183;
                                }
                            } else {
                                if (lat < 54.0) {
                                    if (lng < -56.75) {
                                        return 291;
                                    } else {
                                        if (lat < 53.75) {
                                            return 291;
                                        } else {
                                            return 183;
                                        }
                                    }
                                } else {
                                    return 183;
                                }
                            }
                        } else {
                            return 183;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup67(double lat, double lng)
    {
        if (lat < 56.25) {
            if (lng < -62.0) {
                if (lat < 50.5) {
                    if (lng < -64.75) {
                        if (lat < 47.75) {
                            if (lng < -66.25) {
                                if (lat < 45.75) {
                                    if (lng < -67.0) {
                                        if (lat < 45.25) {
                                            return 166;
                                        } else {
                                            if (lng < -67.25) {
                                                return 166;
                                            } else {
                                                return 77;
                                            }
                                        }
                                    } else {
                                        return 77;
                                    }
                                } else {
                                    return 77;
                                }
                            } else {
                                if (lat < 45.75) {
                                    if (lng < -65.5) {
                                        return 77;
                                    } else {
                                        if (lng < -65.25) {
                                            if (lat < 45.25) {
                                                return 118;
                                            } else {
                                                return 77;
                                            }
                                        } else {
                                            if (lat < 45.5) {
                                                return 118;
                                            } else {
                                                return 77;
                                            }
                                        }
                                    }
                                } else {
                                    return 77;
                                }
                            }
                        } else {
                            if (lng < -66.25) {
                                if (lat < 48.25) {
                                    if (lng < -67.0) {
                                        if (lng < -67.25) {
                                            if (lat < 48.0) {
                                                return 77;
                                            } else {
                                                return 20;
                                            }
                                        } else {
                                            if (lat < 48.0) {
                                                return 77;
                                            } else {
                                                return 20;
                                            }
                                        }
                                    } else {
                                        if (lng < -66.75) {
                                            if (lat < 48.0) {
                                                return 77;
                                            } else {
                                                return 20;
                                            }
                                        } else {
                                            return 77;
                                        }
                                    }
                                } else {
                                    return 20;
                                }
                            } else {
                                if (lat < 48.25) {
                                    if (lng < -65.5) {
                                        return 77;
                                    } else {
                                        if (lng < -65.25) {
                                            if (lat < 48.0) {
                                                return 77;
                                            } else {
                                                return 20;
                                            }
                                        } else {
                                            return 77;
                                        }
                                    }
                                } else {
                                    return 20;
                                }
                            }
                        }
                    } else {
                        if (lat < 47.75) {
                            if (lng < -63.5) {
                                if (lat < 46.25) {
                                    if (lng < -64.25) {
                                        if (lat < 45.75) {
                                            return 118;
                                        } else {
                                            return 77;
                                        }
                                    } else {
                                        if (lat < 46.0) {
                                            return 118;
                                        } else {
                                            if (lng < -64.0) {
                                                return 77;
                                            } else {
                                                return 118;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 47.0) {
                                        if (lng < -64.25) {
                                            if (lat < 46.75) {
                                                return 77;
                                            } else {
                                                if (lng < -64.5) {
                                                    return 77;
                                                } else {
                                                    return 118;
                                                }
                                            }
                                        } else {
                                            if (lng < -64.0) {
                                                if (lat < 46.5) {
                                                    return 77;
                                                } else {
                                                    return 118;
                                                }
                                            } else {
                                                return 118;
                                            }
                                        }
                                    } else {
                                        if (lng < -64.25) {
                                            return 77;
                                        } else {
                                            return 118;
                                        }
                                    }
                                }
                            } else {
                                return 118;
                            }
                        } else {
                            if (lng < -63.5) {
                                if (lat < 49.0) {
                                    if (lng < -64.25) {
                                        if (lat < 48.25) {
                                            return 77;
                                        } else {
                                            return 20;
                                        }
                                    } else {
                                        if (lat < 48.25) {
                                            return 77;
                                        } else {
                                            return 20;
                                        }
                                    }
                                } else {
                                    return 20;
                                }
                            } else {
                                return 20;
                            }
                        }
                    }
                } else {
                    return kdLookup65(lat,lng);
                }
            } else {
                return kdLookup66(lat,lng);
            }
        } else {
            if (lng < -62.0) {
                if (lat < 61.75) {
                    if (lng < -64.75) {
                        if (lat < 60.25) {
                            return 20;
                        } else {
                            if (lng < -66.25) {
                                return 0;
                            } else {
                                if (lng < -65.5) {
                                    return 0;
                                } else {
                                    if (lat < 61.0) {
                                        return 20;
                                    } else {
                                        return 167;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 59.0) {
                            if (lng < -63.5) {
                                if (lat < 57.5) {
                                    if (lng < -64.0) {
                                        return 20;
                                    } else {
                                        if (lat < 56.75) {
                                            if (lng < -63.75) {
                                                return 20;
                                            } else {
                                                return 183;
                                            }
                                        } else {
                                            if (lat < 57.0) {
                                                return 183;
                                            } else {
                                                if (lng < -63.75) {
                                                    return 20;
                                                } else {
                                                    return 183;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 58.25) {
                                        if (lng < -64.0) {
                                            return 20;
                                        } else {
                                            if (lat < 57.75) {
                                                if (lng < -63.75) {
                                                    return 20;
                                                } else {
                                                    return 183;
                                                }
                                            } else {
                                                if (lng < -63.75) {
                                                    if (lat < 58.0) {
                                                        return 20;
                                                    } else {
                                                        return 183;
                                                    }
                                                } else {
                                                    return 183;
                                                }
                                            }
                                        }
                                    } else {
                                        if (lng < -64.0) {
                                            return 20;
                                        } else {
                                            if (lat < 58.5) {
                                                return 183;
                                            } else {
                                                if (lng < -63.75) {
                                                    return 20;
                                                } else {
                                                    if (lat < 58.75) {
                                                        return 183;
                                                    } else {
                                                        return 20;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 58.25) {
                                    return 183;
                                } else {
                                    if (lng < -62.75) {
                                        if (lng < -63.25) {
                                            if (lat < 58.75) {
                                                return 183;
                                            } else {
                                                return 20;
                                            }
                                        } else {
                                            return 183;
                                        }
                                    } else {
                                        return 183;
                                    }
                                }
                            }
                        } else {
                            if (lng < -63.5) {
                                if (lat < 60.25) {
                                    if (lng < -64.25) {
                                        if (lat < 59.5) {
                                            if (lng < -64.5) {
                                                if (lat < 59.25) {
                                                    return 183;
                                                } else {
                                                    return 20;
                                                }
                                            } else {
                                                return 183;
                                            }
                                        } else {
                                            if (lat < 59.75) {
                                                if (lng < -64.5) {
                                                    return 183;
                                                } else {
                                                    return 20;
                                                }
                                            } else {
                                                return 183;
                                            }
                                        }
                                    } else {
                                        return 183;
                                    }
                                } else {
                                    if (lat < 61.0) {
                                        if (lng < -64.25) {
                                            if (lat < 60.5) {
                                                return 183;
                                            } else {
                                                return 20;
                                            }
                                        } else {
                                            return 183;
                                        }
                                    } else {
                                        return 167;
                                    }
                                }
                            } else {
                                return 183;
                            }
                        }
                    }
                } else {
                    return 167;
                }
            } else {
                if (lat < 61.75) {
                    return 183;
                } else {
                    return 167;
                }
            }
        }
    }

    public static int kdLookup68(double lat, double lng)
    {
        if (lat < 78.75) {
            if (lng < -62.75) {
                if (lat < 73.0) {
                    return 167;
                } else {
                    if (lat < 75.75) {
                        return 0;
                    } else {
                        if (lng < -65.25) {
                            if (lat < 77.25) {
                                return 307;
                            } else {
                                if (lng < -65.75) {
                                    return 307;
                                } else {
                                    if (lat < 78.25) {
                                        return 307;
                                    } else {
                                        if (lng < -65.5) {
                                            if (lat < 78.5) {
                                                return 307;
                                            } else {
                                                return 16;
                                            }
                                        } else {
                                            return 16;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 77.25) {
                                if (lng < -64.0) {
                                    if (lat < 76.5) {
                                        return 307;
                                    } else {
                                        if (lng < -64.25) {
                                            return 307;
                                        } else {
                                            if (lat < 76.75) {
                                                return 307;
                                            } else {
                                                return 16;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 76.5) {
                                        if (lng < -63.75) {
                                            return 307;
                                        } else {
                                            return 16;
                                        }
                                    } else {
                                        return 16;
                                    }
                                }
                            } else {
                                if (lng < -64.5) {
                                    if (lat < 78.0) {
                                        if (lng < -65.0) {
                                            return 307;
                                        } else {
                                            if (lat < 77.5) {
                                                return 307;
                                            } else {
                                                if (lng < -64.75) {
                                                    if (lat < 77.75) {
                                                        return 307;
                                                    } else {
                                                        return 16;
                                                    }
                                                } else {
                                                    return 16;
                                                }
                                            }
                                        }
                                    } else {
                                        return 16;
                                    }
                                } else {
                                    return 16;
                                }
                            }
                        }
                    }
                }
            } else {
                return 16;
            }
        } else {
            if (lng < -62.0) {
                if (lat < 84.25) {
                    if (lng < -64.75) {
                        if (lat < 81.5) {
                            if (lng < -66.25) {
                                if (lat < 80.0) {
                                    return 307;
                                } else {
                                    if (lat < 80.75) {
                                        return 16;
                                    } else {
                                        if (lng < -66.5) {
                                            return 167;
                                        } else {
                                            if (lat < 81.0) {
                                                return 16;
                                            } else {
                                                return 167;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lat < 80.0) {
                                    if (lng < -65.5) {
                                        if (lat < 79.0) {
                                            if (lng < -66.0) {
                                                return 307;
                                            } else {
                                                return 16;
                                            }
                                        } else {
                                            return 16;
                                        }
                                    } else {
                                        return 16;
                                    }
                                } else {
                                    if (lng < -65.5) {
                                        if (lat < 81.0) {
                                            return 16;
                                        } else {
                                            return 167;
                                        }
                                    } else {
                                        if (lat < 81.25) {
                                            return 16;
                                        } else {
                                            return 167;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 167;
                        }
                    } else {
                        if (lat < 81.5) {
                            if (lng < -64.25) {
                                if (lat < 81.25) {
                                    return 16;
                                } else {
                                    return 167;
                                }
                            } else {
                                return 16;
                            }
                        } else {
                            if (lng < -63.5) {
                                return 167;
                            } else {
                                if (lat < 82.75) {
                                    if (lng < -62.25) {
                                        if (lng < -63.0) {
                                            return 167;
                                        } else {
                                            if (lat < 82.0) {
                                                if (lng < -62.75) {
                                                    if (lat < 81.75) {
                                                        return 16;
                                                    } else {
                                                        return 167;
                                                    }
                                                } else {
                                                    return 167;
                                                }
                                            } else {
                                                return 167;
                                            }
                                        }
                                    } else {
                                        return 167;
                                    }
                                } else {
                                    return 167;
                                }
                            }
                        }
                    }
                } else {
                    return 0;
                }
            } else {
                if (lat < 84.25) {
                    if (lng < -59.25) {
                        if (lat < 82.0) {
                            return 16;
                        } else {
                            if (lng < -60.75) {
                                if (lat < 83.0) {
                                    if (lng < -61.5) {
                                        return 167;
                                    } else {
                                        if (lat < 82.5) {
                                            if (lng < -61.0) {
                                                return 167;
                                            } else {
                                                if (lat < 82.25) {
                                                    return 16;
                                                } else {
                                                    return 167;
                                                }
                                            }
                                        } else {
                                            return 167;
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            } else {
                                return 16;
                            }
                        }
                    } else {
                        return 16;
                    }
                } else {
                    return 0;
                }
            }
        }
    }

    public static int kdLookup69(double lat, double lng)
    {
        if (lat < 45.0) {
            return kdLookup58(lat,lng);
        } else {
            if (lng < -67.5) {
                if (lat < 67.5) {
                    return kdLookup63(lat,lng);
                } else {
                    if (lng < -78.75) {
                        return kdLookup64(lat,lng);
                    } else {
                        if (lat < 78.75) {
                            if (lng < -73.25) {
                                return 138;
                            } else {
                                if (lat < 73.0) {
                                    if (lng < -70.5) {
                                        return 138;
                                    } else {
                                        if (lat < 70.25) {
                                            if (lng < -68.25) {
                                                return 138;
                                            } else {
                                                if (lat < 68.75) {
                                                    if (lat < 68.0) {
                                                        if (lng < -68.0) {
                                                            return 138;
                                                        } else {
                                                            return 167;
                                                        }
                                                    } else {
                                                        if (lng < -68.0) {
                                                            return 138;
                                                        } else {
                                                            return 167;
                                                        }
                                                    }
                                                } else {
                                                    if (lat < 69.5) {
                                                        if (lng < -68.0) {
                                                            return 138;
                                                        } else {
                                                            return 167;
                                                        }
                                                    } else {
                                                        if (lng < -68.0) {
                                                            return 138;
                                                        } else {
                                                            if (lat < 69.75) {
                                                                if (lng < -67.75) {
                                                                    return 138;
                                                                } else {
                                                                    return 167;
                                                                }
                                                            } else {
                                                                return 167;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lng < -69.0) {
                                                return 138;
                                            } else {
                                                if (lat < 71.5) {
                                                    if (lng < -68.25) {
                                                        return 138;
                                                    } else {
                                                        if (lat < 70.75) {
                                                            if (lng < -68.0) {
                                                                return 138;
                                                            } else {
                                                                return 167;
                                                            }
                                                        } else {
                                                            return 0;
                                                        }
                                                    }
                                                } else {
                                                    return 0;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 307;
                                }
                            }
                        } else {
                            if (lng < -73.25) {
                                return 138;
                            } else {
                                if (lat < 84.25) {
                                    if (lng < -70.5) {
                                        if (lat < 81.5) {
                                            if (lng < -72.0) {
                                                if (lat < 79.75) {
                                                    if (lng < -72.5) {
                                                        return 138;
                                                    } else {
                                                        if (lat < 79.25) {
                                                            return 307;
                                                        } else {
                                                            return 138;
                                                        }
                                                    }
                                                } else {
                                                    return 138;
                                                }
                                            } else {
                                                if (lat < 79.75) {
                                                    if (lng < -71.25) {
                                                        if (lat < 79.25) {
                                                            return 307;
                                                        } else {
                                                            return 138;
                                                        }
                                                    } else {
                                                        return 307;
                                                    }
                                                } else {
                                                    return 138;
                                                }
                                            }
                                        } else {
                                            return 138;
                                        }
                                    } else {
                                        if (lat < 81.5) {
                                            if (lng < -69.0) {
                                                if (lat < 80.0) {
                                                    return 307;
                                                } else {
                                                    return 138;
                                                }
                                            } else {
                                                if (lat < 80.0) {
                                                    return 307;
                                                } else {
                                                    if (lng < -68.0) {
                                                        return 138;
                                                    } else {
                                                        return 167;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lng < -69.0) {
                                                return 138;
                                            } else {
                                                if (lat < 82.75) {
                                                    if (lng < -68.0) {
                                                        return 138;
                                                    } else {
                                                        return 167;
                                                    }
                                                } else {
                                                    if (lng < -68.25) {
                                                        return 138;
                                                    } else {
                                                        if (lat < 83.5) {
                                                            if (lng < -68.0) {
                                                                return 138;
                                                            } else {
                                                                return 167;
                                                            }
                                                        } else {
                                                            return 0;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 0;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 67.5) {
                    if (lng < -56.25) {
                        return kdLookup67(lat,lng);
                    } else {
                        if (lat < 56.25) {
                            return 291;
                        } else {
                            return 16;
                        }
                    }
                } else {
                    if (lng < -56.25) {
                        return kdLookup68(lat,lng);
                    } else {
                        return 16;
                    }
                }
            }
        }
    }

    public static int kdLookup70(double lat, double lng)
    {
        if (lng < -14.25) {
            if (lat < 14.0) {
                if (lng < -15.75) {
                    if (lat < 12.5) {
                        return 38;
                    } else {
                        if (lat < 13.25) {
                            return 226;
                        } else {
                            if (lng < -16.5) {
                                if (lat < 13.75) {
                                    return 253;
                                } else {
                                    return 226;
                                }
                            } else {
                                if (lng < -16.25) {
                                    if (lat < 13.75) {
                                        return 253;
                                    } else {
                                        return 226;
                                    }
                                } else {
                                    if (lat < 13.75) {
                                        return 253;
                                    } else {
                                        return 226;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 12.5) {
                        if (lng < -14.75) {
                            return 38;
                        } else {
                            if (lat < 11.75) {
                                if (lng < -14.5) {
                                    if (lat < 11.5) {
                                        return 185;
                                    } else {
                                        return 38;
                                    }
                                } else {
                                    return 185;
                                }
                            } else {
                                return 38;
                            }
                        }
                    } else {
                        if (lng < -15.0) {
                            if (lat < 13.25) {
                                if (lng < -15.5) {
                                    return 226;
                                } else {
                                    if (lat < 12.75) {
                                        return 38;
                                    } else {
                                        return 226;
                                    }
                                }
                            } else {
                                if (lng < -15.5) {
                                    if (lat < 13.5) {
                                        return 226;
                                    } else {
                                        if (lat < 13.75) {
                                            return 253;
                                        } else {
                                            return 226;
                                        }
                                    }
                                } else {
                                    if (lat < 13.5) {
                                        return 226;
                                    } else {
                                        if (lng < -15.25) {
                                            if (lat < 13.75) {
                                                return 253;
                                            } else {
                                                return 226;
                                            }
                                        } else {
                                            return 253;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 13.25) {
                                if (lng < -14.75) {
                                    if (lat < 12.75) {
                                        return 38;
                                    } else {
                                        return 226;
                                    }
                                } else {
                                    if (lat < 12.75) {
                                        return 38;
                                    } else {
                                        return 226;
                                    }
                                }
                            } else {
                                if (lng < -14.75) {
                                    if (lat < 13.5) {
                                        return 226;
                                    } else {
                                        return 253;
                                    }
                                } else {
                                    if (lat < 13.5) {
                                        return 226;
                                    } else {
                                        if (lng < -14.5) {
                                            if (lat < 13.75) {
                                                return 253;
                                            } else {
                                                return 226;
                                            }
                                        } else {
                                            if (lat < 13.75) {
                                                return 253;
                                            } else {
                                                return 226;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -16.25) {
                    if (lat < 16.25) {
                        return 226;
                    } else {
                        return 33;
                    }
                } else {
                    if (lat < 16.5) {
                        return 226;
                    } else {
                        if (lng < -14.5) {
                            if (lng < -14.75) {
                                if (lng < -15.0) {
                                    if (lng < -15.75) {
                                        return 226;
                                    } else {
                                        if (lng < -15.5) {
                                            return 33;
                                        } else {
                                            return 226;
                                        }
                                    }
                                } else {
                                    return 226;
                                }
                            } else {
                                return 226;
                            }
                        } else {
                            return 226;
                        }
                    }
                }
            }
        } else {
            if (lat < 14.0) {
                if (lng < -12.75) {
                    if (lat < 12.5) {
                        if (lng < -13.5) {
                            if (lat < 11.75) {
                                return 185;
                            } else {
                                if (lng < -13.75) {
                                    return 38;
                                } else {
                                    if (lat < 12.25) {
                                        return 38;
                                    } else {
                                        return 185;
                                    }
                                }
                            }
                        } else {
                            return 185;
                        }
                    } else {
                        if (lng < -13.5) {
                            if (lat < 12.75) {
                                return 38;
                            } else {
                                if (lat < 13.25) {
                                    return 226;
                                } else {
                                    if (lng < -14.0) {
                                        if (lat < 13.5) {
                                            return 253;
                                        } else {
                                            return 226;
                                        }
                                    } else {
                                        if (lat < 13.5) {
                                            return 226;
                                        } else {
                                            if (lng < -13.75) {
                                                if (lat < 13.75) {
                                                    return 253;
                                                } else {
                                                    return 226;
                                                }
                                            } else {
                                                return 226;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lat < 12.75) {
                                if (lng < -13.0) {
                                    return 185;
                                } else {
                                    return 226;
                                }
                            } else {
                                return 226;
                            }
                        }
                    }
                } else {
                    if (lat < 12.5) {
                        return 185;
                    } else {
                        if (lng < -12.0) {
                            return 226;
                        } else {
                            if (lat < 13.25) {
                                return 226;
                            } else {
                                if (lng < -11.75) {
                                    if (lat < 13.75) {
                                        return 226;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    if (lat < 13.5) {
                                        if (lng < -11.5) {
                                            return 226;
                                        } else {
                                            return 182;
                                        }
                                    } else {
                                        return 182;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -12.75) {
                    if (lat < 15.5) {
                        return 226;
                    } else {
                        if (lng < -13.5) {
                            if (lat < 16.25) {
                                return 226;
                            } else {
                                if (lng < -14.0) {
                                    return 226;
                                } else {
                                    if (lng < -13.75) {
                                        if (lat < 16.5) {
                                            return 226;
                                        } else {
                                            return 33;
                                        }
                                    } else {
                                        return 33;
                                    }
                                }
                            }
                        } else {
                            if (lat < 16.0) {
                                if (lng < -13.25) {
                                    return 226;
                                } else {
                                    if (lng < -13.0) {
                                        if (lat < 15.75) {
                                            return 226;
                                        } else {
                                            return 33;
                                        }
                                    } else {
                                        return 33;
                                    }
                                }
                            } else {
                                if (lng < -13.25) {
                                    if (lat < 16.25) {
                                        return 226;
                                    } else {
                                        return 33;
                                    }
                                } else {
                                    return 33;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 15.25) {
                        if (lng < -12.0) {
                            if (lat < 15.0) {
                                return 226;
                            } else {
                                if (lng < -12.25) {
                                    return 226;
                                } else {
                                    return 33;
                                }
                            }
                        } else {
                            if (lat < 15.0) {
                                return 182;
                            } else {
                                if (lng < -11.75) {
                                    return 33;
                                } else {
                                    return 182;
                                }
                            }
                        }
                    } else {
                        if (lng < -11.75) {
                            return 33;
                        } else {
                            if (lat < 15.75) {
                                if (lng < -11.5) {
                                    if (lat < 15.5) {
                                        return 182;
                                    } else {
                                        return 33;
                                    }
                                } else {
                                    return 182;
                                }
                            } else {
                                return 33;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup71(double lat, double lng)
    {
        if (lat < 8.25) {
            if (lng < -8.5) {
                if (lng < -10.0) {
                    if (lat < 6.75) {
                        return 357;
                    } else {
                        if (lat < 7.5) {
                            if (lng < -11.0) {
                                if (lat < 7.25) {
                                    return 357;
                                } else {
                                    return 68;
                                }
                            } else {
                                return 357;
                            }
                        } else {
                            if (lng < -10.75) {
                                return 68;
                            } else {
                                if (lng < -10.5) {
                                    if (lat < 7.75) {
                                        return 357;
                                    } else {
                                        return 68;
                                    }
                                } else {
                                    return 357;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 7.25) {
                        return 357;
                    } else {
                        if (lng < -9.25) {
                            return 357;
                        } else {
                            if (lat < 7.5) {
                                if (lng < -9.0) {
                                    return 357;
                                } else {
                                    if (lng < -8.75) {
                                        return 185;
                                    } else {
                                        return 357;
                                    }
                                }
                            } else {
                                return 185;
                            }
                        }
                    }
                }
            } else {
                if (lng < -7.25) {
                    if (lat < 6.75) {
                        if (lng < -8.0) {
                            if (lat < 6.5) {
                                return 357;
                            } else {
                                return 301;
                            }
                        } else {
                            if (lat < 6.0) {
                                return 357;
                            } else {
                                if (lng < -7.75) {
                                    if (lat < 6.5) {
                                        return 357;
                                    } else {
                                        return 301;
                                    }
                                } else {
                                    return 301;
                                }
                            }
                        }
                    } else {
                        if (lat < 7.5) {
                            if (lng < -8.25) {
                                return 357;
                            } else {
                                return 301;
                            }
                        } else {
                            if (lng < -8.0) {
                                if (lat < 7.75) {
                                    if (lng < -8.25) {
                                        return 357;
                                    } else {
                                        return 301;
                                    }
                                } else {
                                    return 185;
                                }
                            } else {
                                return 301;
                            }
                        }
                    }
                } else {
                    return 301;
                }
            }
        } else {
            if (lng < -8.5) {
                if (lat < 9.75) {
                    if (lng < -10.0) {
                        if (lat < 9.0) {
                            if (lng < -10.5) {
                                return 68;
                            } else {
                                if (lat < 8.5) {
                                    if (lng < -10.25) {
                                        return 68;
                                    } else {
                                        return 357;
                                    }
                                } else {
                                    return 185;
                                }
                            }
                        } else {
                            if (lng < -10.75) {
                                return 68;
                            } else {
                                if (lng < -10.5) {
                                    if (lat < 9.5) {
                                        return 68;
                                    } else {
                                        return 185;
                                    }
                                } else {
                                    return 185;
                                }
                            }
                        }
                    } else {
                        if (lng < -9.5) {
                            if (lat < 8.75) {
                                if (lng < -9.75) {
                                    if (lat < 8.5) {
                                        return 357;
                                    } else {
                                        return 185;
                                    }
                                } else {
                                    return 357;
                                }
                            } else {
                                return 185;
                            }
                        } else {
                            return 185;
                        }
                    }
                } else {
                    if (lng < -11.0) {
                        if (lat < 10.0) {
                            return 68;
                        } else {
                            return 185;
                        }
                    } else {
                        return 185;
                    }
                }
            } else {
                if (lat < 9.75) {
                    if (lng < -7.5) {
                        if (lat < 9.0) {
                            if (lng < -8.0) {
                                return 185;
                            } else {
                                if (lat < 8.5) {
                                    return 301;
                                } else {
                                    if (lng < -7.75) {
                                        return 185;
                                    } else {
                                        if (lat < 8.75) {
                                            return 185;
                                        } else {
                                            return 301;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -8.0) {
                                return 185;
                            } else {
                                if (lat < 9.25) {
                                    if (lng < -7.75) {
                                        return 185;
                                    } else {
                                        return 301;
                                    }
                                } else {
                                    if (lng < -7.75) {
                                        if (lat < 9.5) {
                                            return 185;
                                        } else {
                                            return 301;
                                        }
                                    } else {
                                        return 301;
                                    }
                                }
                            }
                        }
                    } else {
                        return 301;
                    }
                } else {
                    if (lng < -7.25) {
                        if (lat < 10.5) {
                            if (lng < -8.0) {
                                return 185;
                            } else {
                                if (lng < -7.75) {
                                    if (lat < 10.25) {
                                        return 301;
                                    } else {
                                        return 185;
                                    }
                                } else {
                                    return 301;
                                }
                            }
                        } else {
                            if (lng < -8.25) {
                                return 185;
                            } else {
                                return 182;
                            }
                        }
                    } else {
                        if (lng < -6.5) {
                            if (lat < 10.5) {
                                if (lng < -7.0) {
                                    return 301;
                                } else {
                                    if (lat < 10.25) {
                                        return 301;
                                    } else {
                                        if (lng < -6.75) {
                                            return 182;
                                        } else {
                                            return 301;
                                        }
                                    }
                                }
                            } else {
                                return 182;
                            }
                        } else {
                            if (lat < 10.5) {
                                if (lng < -6.0) {
                                    return 301;
                                } else {
                                    if (lat < 10.25) {
                                        return 301;
                                    } else {
                                        return 182;
                                    }
                                }
                            } else {
                                if (lng < -6.25) {
                                    if (lat < 10.75) {
                                        return 301;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    if (lat < 10.75) {
                                        if (lng < -6.0) {
                                            return 301;
                                        } else {
                                            return 182;
                                        }
                                    } else {
                                        return 182;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup72(double lat, double lng)
    {
        if (lng < -5.75) {
            if (lat < 5.5) {
                if (lng < -8.5) {
                    return 357;
                } else {
                    if (lat < 2.75) {
                        return 0;
                    } else {
                        if (lng < -7.25) {
                            if (lat < 4.0) {
                                return 0;
                            } else {
                                if (lat < 4.75) {
                                    if (lng < -7.5) {
                                        return 357;
                                    } else {
                                        return 301;
                                    }
                                } else {
                                    if (lng < -7.5) {
                                        return 357;
                                    } else {
                                        if (lat < 5.25) {
                                            return 301;
                                        } else {
                                            return 357;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 301;
                        }
                    }
                }
            } else {
                return kdLookup71(lat,lng);
            }
        } else {
            if (lat < 5.5) {
                if (lng < -2.75) {
                    return 301;
                } else {
                    return 70;
                }
            } else {
                if (lng < -3.0) {
                    if (lat < 9.75) {
                        return 301;
                    } else {
                        if (lng < -4.5) {
                            if (lat < 10.5) {
                                if (lng < -5.0) {
                                    return 301;
                                } else {
                                    if (lat < 10.0) {
                                        return 301;
                                    } else {
                                        if (lng < -4.75) {
                                            if (lat < 10.25) {
                                                return 301;
                                            } else {
                                                return 263;
                                            }
                                        } else {
                                            return 263;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -5.25) {
                                    return 182;
                                } else {
                                    return 263;
                                }
                            }
                        } else {
                            if (lng < -3.75) {
                                if (lat < 10.0) {
                                    if (lng < -4.25) {
                                        return 263;
                                    } else {
                                        return 301;
                                    }
                                } else {
                                    return 263;
                                }
                            } else {
                                if (lat < 10.0) {
                                    return 301;
                                } else {
                                    return 263;
                                }
                            }
                        }
                    }
                } else {
                    if (lat < 8.25) {
                        if (lng < -2.5) {
                            if (lat < 6.75) {
                                if (lat < 5.75) {
                                    if (lng < -2.75) {
                                        return 301;
                                    } else {
                                        return 70;
                                    }
                                } else {
                                    return 70;
                                }
                            } else {
                                if (lat < 7.5) {
                                    if (lat < 7.25) {
                                        return 70;
                                    } else {
                                        if (lng < -2.75) {
                                            return 301;
                                        } else {
                                            return 70;
                                        }
                                    }
                                } else {
                                    if (lat < 7.75) {
                                        if (lng < -2.75) {
                                            return 301;
                                        } else {
                                            return 70;
                                        }
                                    } else {
                                        if (lng < -2.75) {
                                            return 301;
                                        } else {
                                            if (lat < 8.0) {
                                                return 70;
                                            } else {
                                                return 301;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            return 70;
                        }
                    } else {
                        if (lng < -1.5) {
                            if (lat < 9.75) {
                                if (lng < -2.25) {
                                    if (lat < 9.0) {
                                        if (lng < -2.5) {
                                            return 301;
                                        } else {
                                            if (lat < 8.5) {
                                                return 301;
                                            } else {
                                                return 70;
                                            }
                                        }
                                    } else {
                                        if (lng < -2.75) {
                                            return 301;
                                        } else {
                                            if (lat < 9.25) {
                                                if (lng < -2.5) {
                                                    return 301;
                                                } else {
                                                    return 70;
                                                }
                                            } else {
                                                if (lng < -2.5) {
                                                    if (lat < 9.5) {
                                                        return 301;
                                                    } else {
                                                        return 263;
                                                    }
                                                } else {
                                                    return 70;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 70;
                                }
                            } else {
                                if (lng < -2.25) {
                                    if (lat < 10.5) {
                                        if (lng < -2.75) {
                                            return 263;
                                        } else {
                                            return 70;
                                        }
                                    } else {
                                        if (lng < -2.75) {
                                            return 263;
                                        } else {
                                            if (lat < 11.0) {
                                                return 70;
                                            } else {
                                                return 263;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 11.0) {
                                        return 70;
                                    } else {
                                        return 263;
                                    }
                                }
                            }
                        } else {
                            if (lat < 11.0) {
                                return 70;
                            } else {
                                if (lng < -0.75) {
                                    if (lng < -1.25) {
                                        return 70;
                                    } else {
                                        return 263;
                                    }
                                } else {
                                    if (lng < -0.5) {
                                        return 263;
                                    } else {
                                        return 70;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup73(double lat, double lng)
    {
        if (lng < -3.0) {
            if (lat < 14.0) {
                if (lng < -4.5) {
                    if (lat < 12.25) {
                        if (lng < -5.25) {
                            return 182;
                        } else {
                            if (lat < 12.0) {
                                if (lng < -5.0) {
                                    if (lat < 11.5) {
                                        return 263;
                                    } else {
                                        if (lat < 11.75) {
                                            return 182;
                                        } else {
                                            return 263;
                                        }
                                    }
                                } else {
                                    return 263;
                                }
                            } else {
                                if (lng < -4.75) {
                                    return 182;
                                } else {
                                    return 263;
                                }
                            }
                        }
                    } else {
                        return 182;
                    }
                } else {
                    if (lat < 12.5) {
                        return 263;
                    } else {
                        if (lng < -3.75) {
                            if (lat < 13.25) {
                                if (lng < -4.25) {
                                    return 182;
                                } else {
                                    if (lat < 12.75) {
                                        return 263;
                                    } else {
                                        if (lng < -4.0) {
                                            if (lat < 13.0) {
                                                return 182;
                                            } else {
                                                return 263;
                                            }
                                        } else {
                                            return 263;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -4.0) {
                                    return 182;
                                } else {
                                    if (lat < 13.5) {
                                        return 263;
                                    } else {
                                        return 182;
                                    }
                                }
                            }
                        } else {
                            if (lat < 13.25) {
                                return 263;
                            } else {
                                if (lng < -3.5) {
                                    if (lat < 13.5) {
                                        return 263;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    if (lat < 13.5) {
                                        if (lng < -3.25) {
                                            return 182;
                                        } else {
                                            return 263;
                                        }
                                    } else {
                                        if (lng < -3.25) {
                                            return 182;
                                        } else {
                                            if (lat < 13.75) {
                                                return 263;
                                            } else {
                                                return 182;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if (lng < -5.25) {
                    if (lat < 15.5) {
                        return 182;
                    } else {
                        if (lat < 16.0) {
                            if (lng < -5.5) {
                                return 33;
                            } else {
                                if (lat < 15.75) {
                                    return 182;
                                } else {
                                    return 33;
                                }
                            }
                        } else {
                            if (lat < 16.5) {
                                return 33;
                            } else {
                                if (lng < -5.5) {
                                    return 33;
                                } else {
                                    return 182;
                                }
                            }
                        }
                    }
                } else {
                    return 182;
                }
            }
        } else {
            if (lat < 14.0) {
                if (lng < -2.75) {
                    if (lat < 13.75) {
                        return 263;
                    } else {
                        return 182;
                    }
                } else {
                    return 263;
                }
            } else {
                if (lng < -1.5) {
                    if (lat < 14.5) {
                        if (lng < -2.5) {
                            if (lng < -2.75) {
                                return 182;
                            } else {
                                if (lat < 14.25) {
                                    return 263;
                                } else {
                                    return 182;
                                }
                            }
                        } else {
                            if (lng < -2.0) {
                                if (lng < -2.25) {
                                    return 263;
                                } else {
                                    if (lat < 14.25) {
                                        return 263;
                                    } else {
                                        return 182;
                                    }
                                }
                            } else {
                                if (lng < -1.75) {
                                    if (lat < 14.25) {
                                        return 263;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    return 263;
                                }
                            }
                        }
                    } else {
                        return 182;
                    }
                } else {
                    if (lat < 15.25) {
                        if (lng < -0.75) {
                            if (lat < 14.75) {
                                return 263;
                            } else {
                                if (lng < -1.0) {
                                    return 182;
                                } else {
                                    if (lat < 15.0) {
                                        return 263;
                                    } else {
                                        return 182;
                                    }
                                }
                            }
                        } else {
                            return 263;
                        }
                    } else {
                        return 182;
                    }
                }
            }
        }
    }

    public static int kdLookup74(double lat, double lng)
    {
        if (lng < -5.75) {
            if (lat < 16.75) {
                if (lng < -8.5) {
                    if (lat < 14.0) {
                        if (lng < -10.0) {
                            if (lat < 12.25) {
                                if (lng < -11.0) {
                                    if (lat < 12.0) {
                                        return 185;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    if (lng < -10.25) {
                                        if (lat < 12.0) {
                                            return 185;
                                        } else {
                                            if (lng < -10.75) {
                                                return 185;
                                            } else {
                                                if (lng < -10.5) {
                                                    return 182;
                                                } else {
                                                    return 185;
                                                }
                                            }
                                        }
                                    } else {
                                        return 185;
                                    }
                                }
                            } else {
                                return 182;
                            }
                        } else {
                            if (lat < 12.5) {
                                if (lng < -9.25) {
                                    if (lat < 12.25) {
                                        return 185;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    if (lat < 11.75) {
                                        return 185;
                                    } else {
                                        if (lng < -8.75) {
                                            return 185;
                                        } else {
                                            return 182;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -9.25) {
                                    return 182;
                                } else {
                                    if (lat < 12.75) {
                                        if (lng < -9.0) {
                                            return 185;
                                        } else {
                                            return 182;
                                        }
                                    } else {
                                        return 182;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -10.0) {
                            if (lat < 15.25) {
                                return 182;
                            } else {
                                if (lat < 15.5) {
                                    if (lng < -10.25) {
                                        if (lng < -10.5) {
                                            if (lng < -11.0) {
                                                return 182;
                                            } else {
                                                if (lng < -10.75) {
                                                    return 33;
                                                } else {
                                                    return 182;
                                                }
                                            }
                                        } else {
                                            return 182;
                                        }
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    return 33;
                                }
                            }
                        } else {
                            if (lat < 15.5) {
                                return 182;
                            } else {
                                return 33;
                            }
                        }
                    }
                } else {
                    if (lat < 14.0) {
                        if (lng < -8.25) {
                            if (lat < 11.5) {
                                return 185;
                            } else {
                                return 182;
                            }
                        } else {
                            return 182;
                        }
                    } else {
                        if (lng < -7.25) {
                            if (lat < 15.5) {
                                return 182;
                            } else {
                                return 33;
                            }
                        } else {
                            if (lat < 15.5) {
                                return 182;
                            } else {
                                return 33;
                            }
                        }
                    }
                }
            } else {
                if (lat < 20.25) {
                    return 33;
                } else {
                    if (lng < -6.25) {
                        return 33;
                    } else {
                        if (lat < 21.25) {
                            if (lat < 20.75) {
                                if (lng < -6.0) {
                                    return 33;
                                } else {
                                    return 182;
                                }
                            } else {
                                if (lng < -6.0) {
                                    return 33;
                                } else {
                                    return 182;
                                }
                            }
                        } else {
                            if (lat < 21.75) {
                                if (lng < -6.0) {
                                    return 33;
                                } else {
                                    return 182;
                                }
                            } else {
                                if (lat < 22.0) {
                                    if (lng < -6.0) {
                                        return 33;
                                    } else {
                                        return 182;
                                    }
                                } else {
                                    if (lng < -6.0) {
                                        if (lat < 22.25) {
                                            return 33;
                                        } else {
                                            return 182;
                                        }
                                    } else {
                                        return 182;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 16.75) {
                return kdLookup73(lat,lng);
            } else {
                if (lng < -3.0) {
                    if (lat < 18.0) {
                        if (lng < -5.5) {
                            return 33;
                        } else {
                            return 182;
                        }
                    } else {
                        return 182;
                    }
                } else {
                    if (lat < 22.25) {
                        return 182;
                    } else {
                        if (lng < -0.5) {
                            return 182;
                        } else {
                            return 381;
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup75(double lat, double lng)
    {
        if (lat < 28.0) {
            if (lng < -3.0) {
                if (lat < 25.25) {
                    if (lng < -4.5) {
                        if (lat < 25.0) {
                            return 182;
                        } else {
                            if (lng < -5.0) {
                                return 33;
                            } else {
                                if (lng < -4.75) {
                                    return 182;
                                } else {
                                    return 381;
                                }
                            }
                        }
                    } else {
                        if (lat < 24.25) {
                            return 182;
                        } else {
                            if (lng < -3.75) {
                                if (lat < 24.75) {
                                    if (lng < -4.0) {
                                        return 182;
                                    } else {
                                        if (lat < 24.5) {
                                            return 182;
                                        } else {
                                            return 381;
                                        }
                                    }
                                } else {
                                    if (lng < -4.25) {
                                        if (lat < 25.0) {
                                            return 182;
                                        } else {
                                            return 381;
                                        }
                                    } else {
                                        return 381;
                                    }
                                }
                            } else {
                                if (lat < 24.5) {
                                    if (lng < -3.5) {
                                        return 182;
                                    } else {
                                        return 381;
                                    }
                                } else {
                                    return 381;
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -5.0) {
                        if (lat < 25.75) {
                            if (lng < -5.5) {
                                return 33;
                            } else {
                                if (lng < -5.25) {
                                    if (lat < 25.5) {
                                        return 33;
                                    } else {
                                        return 381;
                                    }
                                } else {
                                    if (lat < 25.5) {
                                        return 33;
                                    } else {
                                        return 381;
                                    }
                                }
                            }
                        } else {
                            return 381;
                        }
                    } else {
                        return 381;
                    }
                }
            } else {
                if (lat < 24.0) {
                    if (lng < -1.5) {
                        if (lng < -2.25) {
                            if (lat < 23.75) {
                                return 182;
                            } else {
                                if (lng < -2.75) {
                                    return 182;
                                } else {
                                    return 381;
                                }
                            }
                        } else {
                            if (lat < 23.25) {
                                return 182;
                            } else {
                                if (lng < -2.0) {
                                    if (lat < 23.5) {
                                        return 182;
                                    } else {
                                        return 381;
                                    }
                                } else {
                                    return 381;
                                }
                            }
                        }
                    } else {
                        if (lng < -0.75) {
                            if (lat < 23.0) {
                                if (lng < -1.25) {
                                    return 182;
                                } else {
                                    if (lng < -1.0) {
                                        if (lat < 22.75) {
                                            return 182;
                                        } else {
                                            return 381;
                                        }
                                    } else {
                                        if (lat < 22.75) {
                                            return 182;
                                        } else {
                                            return 381;
                                        }
                                    }
                                }
                            } else {
                                return 381;
                            }
                        } else {
                            return 381;
                        }
                    }
                } else {
                    return 381;
                }
            }
        } else {
            if (lng < -3.0) {
                if (lat < 30.75) {
                    if (lng < -4.75) {
                        if (lat < 30.0) {
                            return 381;
                        } else {
                            if (lng < -5.0) {
                                return 344;
                            } else {
                                if (lat < 30.25) {
                                    return 381;
                                } else {
                                    return 344;
                                }
                            }
                        }
                    } else {
                        return 381;
                    }
                } else {
                    if (lat < 31.75) {
                        if (lng < -4.0) {
                            return 344;
                        } else {
                            if (lng < -3.5) {
                                if (lat < 31.0) {
                                    return 381;
                                } else {
                                    if (lat < 31.25) {
                                        return 344;
                                    } else {
                                        if (lng < -3.75) {
                                            return 344;
                                        } else {
                                            if (lat < 31.5) {
                                                return 381;
                                            } else {
                                                return 344;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return 381;
                            }
                        }
                    } else {
                        return 344;
                    }
                }
            } else {
                if (lat < 31.75) {
                    return 381;
                } else {
                    if (lng < -1.5) {
                        if (lat < 32.25) {
                            if (lng < -2.75) {
                                return 344;
                            } else {
                                return 381;
                            }
                        } else {
                            return 344;
                        }
                    } else {
                        if (lat < 32.75) {
                            if (lng < -1.0) {
                                if (lat < 32.25) {
                                    return 381;
                                } else {
                                    if (lng < -1.25) {
                                        return 344;
                                    } else {
                                        if (lat < 32.5) {
                                            return 381;
                                        } else {
                                            return 344;
                                        }
                                    }
                                }
                            } else {
                                return 381;
                            }
                        } else {
                            if (lng < -1.25) {
                                if (lat < 33.25) {
                                    return 344;
                                } else {
                                    return 381;
                                }
                            } else {
                                return 381;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup76(double lat, double lng)
    {
        if (lng < -5.75) {
            if (lat < 39.25) {
                if (lng < -8.5) {
                    return 57;
                } else {
                    if (lat < 36.5) {
                        if (lng < -7.25) {
                            return 344;
                        } else {
                            if (lat < 36.0) {
                                return 344;
                            } else {
                                return 335;
                            }
                        }
                    } else {
                        if (lng < -7.25) {
                            return 57;
                        } else {
                            if (lat < 38.0) {
                                return 335;
                            } else {
                                if (lng < -6.75) {
                                    if (lat < 38.5) {
                                        if (lng < -7.0) {
                                            return 57;
                                        } else {
                                            return 335;
                                        }
                                    } else {
                                        if (lat < 38.75) {
                                            return 335;
                                        } else {
                                            if (lng < -7.0) {
                                                return 57;
                                            } else {
                                                if (lat < 39.0) {
                                                    return 335;
                                                } else {
                                                    return 57;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return 335;
                                }
                            }
                        }
                    }
                }
            } else {
                if (lat < 42.0) {
                    if (lng < -8.5) {
                        return 57;
                    } else {
                        if (lng < -7.25) {
                            return 57;
                        } else {
                            if (lat < 40.5) {
                                if (lng < -6.75) {
                                    if (lat < 39.75) {
                                        if (lng < -7.0) {
                                            if (lat < 39.5) {
                                                return 57;
                                            } else {
                                                return 335;
                                            }
                                        } else {
                                            return 335;
                                        }
                                    } else {
                                        return 57;
                                    }
                                } else {
                                    return 335;
                                }
                            } else {
                                if (lng < -6.5) {
                                    if (lat < 41.25) {
                                        if (lng < -6.75) {
                                            return 57;
                                        } else {
                                            return 335;
                                        }
                                    } else {
                                        return 57;
                                    }
                                } else {
                                    if (lat < 41.5) {
                                        return 335;
                                    } else {
                                        if (lng < -6.25) {
                                            if (lat < 41.75) {
                                                return 57;
                                            } else {
                                                return 335;
                                            }
                                        } else {
                                            return 335;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (lng < -8.5) {
                        return 335;
                    } else {
                        if (lat < 43.5) {
                            if (lng < -8.0) {
                                if (lat < 42.25) {
                                    return 57;
                                } else {
                                    return 335;
                                }
                            } else {
                                return 335;
                            }
                        } else {
                            return 335;
                        }
                    }
                }
            }
        } else {
            if (lat < 39.25) {
                if (lng < -3.0) {
                    if (lat < 36.5) {
                        if (lng < -4.5) {
                            if (lat < 35.5) {
                                return 344;
                            } else {
                                if (lng < -5.25) {
                                    if (lat < 36.0) {
                                        return 344;
                                    } else {
                                        return 335;
                                    }
                                } else {
                                    if (lat < 36.0) {
                                        return 344;
                                    } else {
                                        return 335;
                                    }
                                }
                            }
                        } else {
                            return 344;
                        }
                    } else {
                        return 335;
                    }
                } else {
                    if (lat < 36.5) {
                        if (lng < -1.5) {
                            if (lat < 35.0) {
                                return 344;
                            } else {
                                if (lng < -2.25) {
                                    return 344;
                                } else {
                                    if (lat < 35.75) {
                                        if (lng < -2.0) {
                                            return 344;
                                        } else {
                                            return 381;
                                        }
                                    } else {
                                        return 0;
                                    }
                                }
                            }
                        } else {
                            return 381;
                        }
                    } else {
                        return 335;
                    }
                }
            } else {
                if (lng < -3.0) {
                    return 335;
                } else {
                    if (lat < 43.0) {
                        return 335;
                    } else {
                        if (lng < -1.5) {
                            return 335;
                        } else {
                            if (lat < 43.5) {
                                if (lng < -1.0) {
                                    if (lng < -1.25) {
                                        return 335;
                                    } else {
                                        if (lat < 43.25) {
                                            return 335;
                                        } else {
                                            return 298;
                                        }
                                    }
                                } else {
                                    return 298;
                                }
                            } else {
                                return 298;
                            }
                        }
                    }
                }
            }
        }
    }

    public static int kdLookup77(double lat, double lng)
    {
        if (lng < -11.25) {
            if (lat < 33.75) {
                if (lng < -17.0) {
                    if (lat < 28.0) {
                        return 265;
                    } else {
                        if (lat < 30.75) {
                            return 265;
                        } else {
                            return 63;
                        }
                    }
                } else {
                    if (lat < 28.0) {
                        if (lng < -14.25) {
                            if (lat < 26.5) {
                                return 29;
                            } else {
                                return 265;
                            }
                        } else {
                            if (lat < 25.25) {
                                if (lng < -12.75) {
                                    if (lat < 23.25) {
                                        if (lng < -13.0) {
                                            return 29;
                                        } else {
                                            return 33;
                                        }
                                    } else {
                                        return 29;
                                    }
                                } else {
                                    if (lat < 23.75) {
                                        if (lng < -12.0) {
                                            if (lat < 23.5) {
                                                return 33;
                                            } else {
                                                return 29;
                                            }
                                        } else {
                                            return 33;
                                        }
                                    } else {
                                        if (lng < -12.0) {
                                            return 29;
                                        } else {
                                            return 33;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -12.75) {
                                    if (lat < 27.75) {
                                        return 29;
                                    } else {
                                        return 344;
                                    }
                                } else {
                                    if (lat < 26.5) {
                                        if (lng < -12.0) {
                                            return 29;
                                        } else {
                                            if (lat < 26.0) {
                                                return 33;
                                            } else {
                                                return 29;
                                            }
                                        }
                                    } else {
                                        if (lng < -12.0) {
                                            if (lat < 27.75) {
                                                return 29;
                                            } else {
                                                return 344;
                                            }
                                        } else {
                                            if (lat < 27.75) {
                                                return 29;
                                            } else {
                                                return 344;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lng < -14.25) {
                            if (lat < 30.75) {
                                return 265;
                            } else {
                                return 63;
                            }
                        } else {
                            if (lat < 30.75) {
                                if (lng < -12.75) {
                                    if (lat < 29.25) {
                                        if (lng < -13.5) {
                                            return 265;
                                        } else {
                                            if (lat < 28.5) {
                                                return 344;
                                            } else {
                                                return 265;
                                            }
                                        }
                                    } else {
                                        return 265;
                                    }
                                } else {
                                    return 344;
                                }
                            } else {
                                return 0;
                            }
                        }
                    }
                }
            } else {
                return 0;
            }
        } else {
            if (lat < 33.75) {
                if (lng < -5.75) {
                    if (lat < 28.0) {
                        if (lng < -8.5) {
                            if (lat < 26.0) {
                                return 33;
                            } else {
                                if (lng < -10.0) {
                                    if (lat < 27.75) {
                                        return 29;
                                    } else {
                                        return 344;
                                    }
                                } else {
                                    if (lat < 27.75) {
                                        return 29;
                                    } else {
                                        return 344;
                                    }
                                }
                            }
                        } else {
                            if (lat < 25.25) {
                                if (lng < -6.5) {
                                    return 33;
                                } else {
                                    if (lat < 23.75) {
                                        if (lat < 23.0) {
                                            if (lng < -6.25) {
                                                return 33;
                                            } else {
                                                return 182;
                                            }
                                        } else {
                                            if (lng < -6.25) {
                                                return 33;
                                            } else {
                                                return 182;
                                            }
                                        }
                                    } else {
                                        if (lat < 24.5) {
                                            if (lng < -6.25) {
                                                return 33;
                                            } else {
                                                return 182;
                                            }
                                        } else {
                                            if (lng < -6.25) {
                                                if (lat < 25.0) {
                                                    return 182;
                                                } else {
                                                    return 33;
                                                }
                                            } else {
                                                if (lat < 25.0) {
                                                    return 182;
                                                } else {
                                                    return 33;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (lng < -7.25) {
                                    if (lat < 26.75) {
                                        return 33;
                                    } else {
                                        if (lng < -8.0) {
                                            if (lat < 27.25) {
                                                return 33;
                                            } else {
                                                return 381;
                                            }
                                        } else {
                                            if (lat < 27.0) {
                                                if (lng < -7.5) {
                                                    return 33;
                                                } else {
                                                    return 381;
                                                }
                                            } else {
                                                return 381;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 26.5) {
                                        if (lng < -6.5) {
                                            if (lat < 26.25) {
                                                return 33;
                                            } else {
                                                if (lng < -6.75) {
                                                    return 33;
                                                } else {
                                                    return 381;
                                                }
                                            }
                                        } else {
                                            if (lat < 25.75) {
                                                return 33;
                                            } else {
                                                if (lng < -6.25) {
                                                    if (lat < 26.25) {
                                                        return 33;
                                                    } else {
                                                        return 381;
                                                    }
                                                } else {
                                                    if (lat < 26.0) {
                                                        if (lng < -6.0) {
                                                            return 33;
                                                        } else {
                                                            return 381;
                                                        }
                                                    } else {
                                                        return 381;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        return 381;
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 30.0) {
                            if (lng < -8.5) {
                                return 344;
                            } else {
                                if (lng < -7.25) {
                                    if (lat < 29.0) {
                                        return 381;
                                    } else {
                                        if (lng < -8.0) {
                                            return 344;
                                        } else {
                                            if (lat < 29.5) {
                                                if (lng < -7.75) {
                                                    if (lat < 29.25) {
                                                        return 381;
                                                    } else {
                                                        return 344;
                                                    }
                                                } else {
                                                    return 381;
                                                }
                                            } else {
                                                return 344;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 29.75) {
                                        return 381;
                                    } else {
                                        if (lng < -6.0) {
                                            return 344;
                                        } else {
                                            return 381;
                                        }
                                    }
                                }
                            }
                        } else {
                            return 344;
                        }
                    }
                } else {
                    return kdLookup75(lat,lng);
                }
            } else {
                return kdLookup76(lat,lng);
            }
        }
    }

    public static int kdLookup78(double lat, double lng)
    {
        if (lng < -5.75) {
            if (lat < 50.5) {
                return 0;
            } else {
                if (lat < 53.25) {
                    return 286;
                } else {
                    if (lng < -8.5) {
                        return 286;
                    } else {
                        if (lat < 54.75) {
                            if (lng < -7.25) {
                                if (lat < 54.25) {
                                    return 286;
                                } else {
                                    if (lng < -8.0) {
                                        return 286;
                                    } else {
                                        if (lng < -7.75) {
                                            if (lat < 54.5) {
                                                return 286;
                                            } else {
                                                return 304;
                                            }
                                        } else {
                                            return 304;
                                        }
                                    }
                                }
                            } else {
                                if (lng < -6.5) {
                                    if (lat < 54.25) {
                                        return 286;
                                    } else {
                                        if (lng < -7.0) {
                                            return 304;
                                        } else {
                                            if (lng < -6.75) {
                                                if (lat < 54.5) {
                                                    return 286;
                                                } else {
                                                    return 304;
                                                }
                                            } else {
                                                return 304;
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 54.0) {
                                        return 286;
                                    } else {
                                        if (lng < -6.25) {
                                            if (lat < 54.25) {
                                                return 286;
                                            } else {
                                                return 304;
                                            }
                                        } else {
                                            if (lat < 54.25) {
                                                return 286;
                                            } else {
                                                return 304;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -7.25) {
                                if (lat < 55.5) {
                                    if (lng < -8.0) {
                                        return 286;
                                    } else {
                                        if (lng < -7.5) {
                                            return 286;
                                        } else {
                                            if (lat < 55.0) {
                                                return 304;
                                            } else {
                                                return 286;
                                            }
                                        }
                                    }
                                } else {
                                    return 286;
                                }
                            } else {
                                if (lng < -6.5) {
                                    if (lat < 55.5) {
                                        if (lng < -7.0) {
                                            if (lat < 55.25) {
                                                return 304;
                                            } else {
                                                return 286;
                                            }
                                        } else {
                                            if (lat < 55.25) {
                                                return 304;
                                            } else {
                                                return 286;
                                            }
                                        }
                                    } else {
                                        return 286;
                                    }
                                } else {
                                    return 304;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (lat < 50.5) {
                if (lng < -3.0) {
                    if (lat < 47.75) {
                        return 298;
                    } else {
                        if (lng < -4.5) {
                            if (lat < 49.0) {
                                return 298;
                            } else {
                                return 304;
                            }
                        } else {
                            if (lat < 49.25) {
                                return 298;
                            } else {
                                return 304;
                            }
                        }
                    }
                } else {
                    if (lat < 49.0) {
                        return 298;
                    } else {
                        if (lng < -1.5) {
                            if (lng < -2.25) {
                                if (lat < 49.75) {
                                    if (lng < -2.75) {
                                        return 298;
                                    } else {
                                        return 140;
                                    }
                                } else {
                                    return 0;
                                }
                            } else {
                                if (lat < 49.75) {
                                    if (lng < -2.0) {
                                        return 140;
                                    } else {
                                        if (lat < 49.25) {
                                            return 298;
                                        } else {
                                            if (lng < -1.75) {
                                                if (lat < 49.5) {
                                                    return 140;
                                                } else {
                                                    return 298;
                                                }
                                            } else {
                                                return 298;
                                            }
                                        }
                                    }
                                } else {
                                    return 298;
                                }
                            }
                        } else {
                            return 298;
                        }
                    }
                }
            } else {
                if (lng < -3.0) {
                    if (lat < 54.0) {
                        return 304;
                    } else {
                        if (lng < -4.5) {
                            if (lat < 55.0) {
                                if (lng < -5.25) {
                                    return 304;
                                } else {
                                    if (lat < 54.5) {
                                        return 243;
                                    } else {
                                        return 304;
                                    }
                                }
                            } else {
                                return 304;
                            }
                        } else {
                            if (lat < 54.5) {
                                if (lng < -3.75) {
                                    return 243;
                                } else {
                                    return 304;
                                }
                            } else {
                                return 304;
                            }
                        }
                    }
                } else {
                    return 304;
                }
            }
        }
    }

    public static int kdLookup79(double lat, double lng)
    {
        if (lat < 78.75) {
            if (lng < -17.0) {
                if (lat < 73.0) {
                    if (lng < -19.75) {
                        if (lat < 70.25) {
                            return 16;
                        } else {
                            if (lng < -21.25) {
                                if (lat < 71.5) {
                                    if (lng < -22.0) {
                                        if (lat < 70.5) {
                                            if (lng < -22.25) {
                                                return 16;
                                            } else {
                                                return 73;
                                            }
                                        } else {
                                            return 73;
                                        }
                                    } else {
                                        return 73;
                                    }
                                } else {
                                    if (lat < 72.25) {
                                        if (lng < -22.0) {
                                            if (lat < 72.0) {
                                                return 73;
                                            } else {
                                                return 16;
                                            }
                                        } else {
                                            return 73;
                                        }
                                    } else {
                                        return 16;
                                    }
                                }
                            } else {
                                return 0;
                            }
                        }
                    } else {
                        return 0;
                    }
                } else {
                    if (lat < 75.75) {
                        return 16;
                    } else {
                        if (lng < -19.75) {
                            if (lat < 77.25) {
                                if (lng < -21.25) {
                                    if (lat < 76.0) {
                                        return 16;
                                    } else {
                                        return 25;
                                    }
                                } else {
                                    if (lng < -20.5) {
                                        if (lat < 76.0) {
                                            return 16;
                                        } else {
                                            return 25;
                                        }
                                    } else {
                                        if (lat < 76.25) {
                                            if (lng < -20.25) {
                                                if (lat < 76.0) {
                                                    return 16;
                                                } else {
                                                    return 25;
                                                }
                                            } else {
                                                return 16;
                                            }
                                        } else {
                                            return 25;
                                        }
                                    }
                                }
                            } else {
                                return 25;
                            }
                        } else {
                            if (lat < 77.25) {
                                if (lng < -18.5) {
                                    if (lat < 76.5) {
                                        if (lng < -19.25) {
                                            if (lat < 76.0) {
                                                return 16;
                                            } else {
                                                if (lng < -19.5) {
                                                    if (lat < 76.25) {
                                                        return 16;
                                                    } else {
                                                        return 25;
                                                    }
                                                } else {
                                                    return 0;
                                                }
                                            }
                                        } else {
                                            return 25;
                                        }
                                    } else {
                                        return 25;
                                    }
                                } else {
                                    return 25;
                                }
                            } else {
                                return 25;
                            }
                        }
                    }
                }
            } else {
                return 0;
            }
        } else {
            if (lng < -17.0) {
                if (lat < 84.25) {
                    if (lng < -19.75) {
                        if (lat < 81.5) {
                            if (lng < -21.25) {
                                if (lat < 79.75) {
                                    return 25;
                                } else {
                                    return 16;
                                }
                            } else {
                                if (lat < 80.0) {
                                    if (lng < -20.5) {
                                        if (lat < 79.75) {
                                            return 25;
                                        } else {
                                            return 16;
                                        }
                                    } else {
                                        if (lat < 79.75) {
                                            return 25;
                                        } else {
                                            return 16;
                                        }
                                    }
                                } else {
                                    return 16;
                                }
                            }
                        } else {
                            return 16;
                        }
                    } else {
                        if (lat < 81.5) {
                            if (lng < -18.5) {
                                if (lat < 79.75) {
                                    if (lng < -19.25) {
                                        return 25;
                                    } else {
                                        if (lat < 79.5) {
                                            return 25;
                                        } else {
                                            return 16;
                                        }
                                    }
                                } else {
                                    return 16;
                                }
                            } else {
                                if (lat < 79.75) {
                                    if (lng < -17.75) {
                                        if (lat < 79.25) {
                                            return 25;
                                        } else {
                                            return 16;
                                        }
                                    } else {
                                        if (lat < 79.25) {
                                            return 25;
                                        } else {
                                            if (lng < -17.5) {
                                                if (lat < 79.5) {
                                                    return 25;
                                                } else {
                                                    return 16;
                                                }
                                            } else {
                                                return 0;
                                            }
                                        }
                                    }
                                } else {
                                    return 16;
                                }
                            }
                        } else {
                            return 16;
                        }
                    }
                } else {
                    return 0;
                }
            } else {
                return 16;
            }
        }
    }

    public static int kdLookup80(double lat, double lng)
    {
        if (lat < 45.0) {
            if (lng < -22.5) {
                if (lat < 22.5) {
                    return 354;
                } else {
                    return 284;
                }
            } else {
                if (lat < 22.5) {
                    if (lng < -11.25) {
                        if (lat < 11.25) {
                            if (lng < -17.0) {
                                return 0;
                            } else {
                                if (lat < 5.5) {
                                    return 0;
                                } else {
                                    if (lng < -14.25) {
                                        if (lat < 8.25) {
                                            return 0;
                                        } else {
                                            if (lat < 9.75) {
                                                return 0;
                                            } else {
                                                if (lng < -15.75) {
                                                    return 38;
                                                } else {
                                                    if (lng < -14.75) {
                                                        return 38;
                                                    } else {
                                                        return 185;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (lat < 8.25) {
                                            return 68;
                                        } else {
                                            if (lng < -12.75) {
                                                if (lat < 9.25) {
                                                    return 68;
                                                } else {
                                                    return 185;
                                                }
                                            } else {
                                                if (lat < 9.75) {
                                                    if (lng < -12.5) {
                                                        if (lat < 9.5) {
                                                            return 68;
                                                        } else {
                                                            return 185;
                                                        }
                                                    } else {
                                                        return 68;
                                                    }
                                                } else {
                                                    if (lng < -12.0) {
                                                        if (lat < 10.0) {
                                                            if (lng < -12.5) {
                                                                return 185;
                                                            } else {
                                                                return 68;
                                                            }
                                                        } else {
                                                            return 185;
                                                        }
                                                    } else {
                                                        if (lat < 10.0) {
                                                            return 68;
                                                        } else {
                                                            return 185;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (lng < -17.0) {
                                if (lat < 16.75) {
                                    return 226;
                                } else {
                                    return 29;
                                }
                            } else {
                                if (lat < 16.75) {
                                    return kdLookup70(lat,lng);
                                } else {
                                    if (lng < -14.25) {
                                        if (lat < 21.25) {
                                            return 33;
                                        } else {
                                            if (lng < -15.75) {
                                                if (lng < -16.5) {
                                                    if (lat < 21.5) {
                                                        if (lng < -16.75) {
                                                            return 29;
                                                        } else {
                                                            return 33;
                                                        }
                                                    } else {
                                                        return 29;
                                                    }
                                                } else {
                                                    if (lat < 21.5) {
                                                        return 33;
                                                    } else {
                                                        return 29;
                                                    }
                                                }
                                            } else {
                                                if (lng < -15.0) {
                                                    if (lat < 21.5) {
                                                        return 33;
                                                    } else {
                                                        return 29;
                                                    }
                                                } else {
                                                    if (lat < 21.5) {
                                                        return 33;
                                                    } else {
                                                        return 29;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (lat < 21.5) {
                                            return 33;
                                        } else {
                                            if (lng < -13.0) {
                                                return 29;
                                            } else {
                                                return 33;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (lat < 11.25) {
                            return kdLookup72(lat,lng);
                        } else {
                            return kdLookup74(lat,lng);
                        }
                    }
                } else {
                    return kdLookup77(lat,lng);
                }
            }
        } else {
            if (lng < -22.5) {
                if (lat < 67.5) {
                    if (lng < -33.0) {
                        return 16;
                    } else {
                        return 21;
                    }
                } else {
                    if (lng < -33.75) {
                        return 16;
                    } else {
                        if (lat < 78.75) {
                            if (lng < -25.0) {
                                return 16;
                            } else {
                                if (lat < 72.75) {
                                    if (lat < 70.0) {
                                        return 16;
                                    } else {
                                        if (lat < 71.25) {
                                            if (lng < -23.75) {
                                                if (lng < -24.5) {
                                                    return 16;
                                                } else {
                                                    if (lat < 70.5) {
                                                        return 16;
                                                    } else {
                                                        return 73;
                                                    }
                                                }
                                            } else {
                                                if (lng < -23.25) {
                                                    if (lat < 70.5) {
                                                        return 16;
                                                    } else {
                                                        return 73;
                                                    }
                                                } else {
                                                    if (lat < 70.5) {
                                                        return 16;
                                                    } else {
                                                        return 73;
                                                    }
                                                }
                                            }
                                        } else {
                                            if (lng < -23.75) {
                                                if (lat < 72.0) {
                                                    if (lng < -24.5) {
                                                        return 16;
                                                    } else {
                                                        if (lng < -24.25) {
                                                            if (lat < 71.5) {
                                                                return 73;
                                                            } else {
                                                                return 16;
                                                            }
                                                        } else {
                                                            return 73;
                                                        }
                                                    }
                                                } else {
                                                    if (lng < -24.25) {
                                                        return 16;
                                                    } else {
                                                        if (lat < 72.5) {
                                                            return 73;
                                                        } else {
                                                            return 16;
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (lat < 72.0) {
                                                    return 73;
                                                } else {
                                                    if (lng < -23.25) {
                                                        if (lat < 72.25) {
                                                            return 73;
                                                        } else {
                                                            return 16;
                                                        }
                                                    } else {
                                                        if (lng < -23.0) {
                                                            if (lat < 72.25) {
                                                                return 73;
                                                            } else {
                                                                return 16;
                                                            }
                                                        } else {
                                                            if (lat < 72.25) {
                                                                return 73;
                                                            } else {
                                                                return 16;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (lat < 76.0) {
                                        return 16;
                                    } else {
                                        if (lat < 77.25) {
                                            if (lng < -22.75) {
                                                return 16;
                                            } else {
                                                return 25;
                                            }
                                        } else {
                                            if (lng < -22.75) {
                                                return 16;
                                            } else {
                                                if (lat < 78.5) {
                                                    return 25;
                                                } else {
                                                    return 16;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            return 16;
                        }
                    }
                }
            } else {
                if (lat < 67.5) {
                    if (lng < -11.25) {
                        return 21;
                    } else {
                        if (lat < 56.25) {
                            return kdLookup78(lat,lng);
                        } else {
                            if (lng < -5.75) {
                                if (lat < 61.75) {
                                    if (lng < -8.5) {
                                        return 0;
                                    } else {
                                        if (lat < 59.0) {
                                            return 304;
                                        } else {
                                            return 362;
                                        }
                                    }
                                } else {
                                    return 362;
                                }
                            } else {
                                return 304;
                            }
                        }
                    }
                } else {
                    if (lng < -11.25) {
                        return kdLookup79(lat,lng);
                    } else {
                        return 111;
                    }
                }
            }
        }
    }
}
