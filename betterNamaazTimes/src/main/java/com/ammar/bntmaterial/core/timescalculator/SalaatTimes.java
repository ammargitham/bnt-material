package com.ammar.bntmaterial.core.timescalculator;

/**
 * Created by agitham on 10/9/2015.
 */
public class SalaatTimes {

	public static final int   KEY_SIHORI_END  = 1;
	public static final int   KEY_SUNRISE     = 2;
	public static final int   KEY_FAJR_START  = 3;
	public static final int   KEY_FAJR_END    = 4;
	public static final int   KEY_ZAWAL       = 5;
	public static final int   KEY_ZOHR_END    = 6;
	public static final int   KEY_ASR_END     = 7;
	public static final int   KEY_MAGHRIB     = 8;
	public static final int   KEY_NISFUL_LAYL = 9;
	public static final int[] SALAAT_KEYS     = {KEY_SIHORI_END, KEY_FAJR_START, KEY_FAJR_END,
			KEY_ZAWAL, KEY_ZOHR_END, KEY_ASR_END, KEY_MAGHRIB, KEY_NISFUL_LAYL};
}
