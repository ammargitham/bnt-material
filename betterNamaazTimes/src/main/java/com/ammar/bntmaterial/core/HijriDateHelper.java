package com.ammar.bntmaterial.core;

import com.ammar.bntmaterial.core.joda.HijriChronology;
import com.ammar.bntmaterial.util.Utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Created by agitham on 29/6/2015.
 */
public class HijriDateHelper {

	/**
	 * @return DateTime object of the current Hijri date.
	 */
	public static DateTime getHijriDate() {
		return getHijriDate(new DateTime());
	}

	public static DateTime getHijriDate(DateTime gregorianDateTime) {
		return gregorianDateTime.withChronology(HijriChronology.getInstance(DateTimeZone.getDefault(), HijriChronology.LEAP_YEAR_INDIAN));
	}

	public static String dateTimeToString(DateTime dateTime) {

		if (dateTime == null) {
			return "null";
		}

		return dateTime.toString(dateTime.getYear() + "-" + dateTime.getMonthOfYear() + "-" + dateTime.getDayOfMonth());
	}

	public static DateTime stringToDateTime(String string) {

		if (string == null) {
			return null;
		}

		String[] dateParts = string.split("-");

		return getHijriDate().withYear(Integer.parseInt(dateParts[0])).withMonthOfYear(Integer.parseInt(dateParts[1])).withDayOfMonth(Integer.parseInt(dateParts[2]));
	}

	public static boolean isHijriDateStringToday(String string) {

		if (Utils.isStringNullOrWhitespace(string)) {
			return false;
		}

		DateTime hijriDateTimeFromString = stringToDateTime(string);
		DateTime todayHijriDateTime = getHijriDate();

		return hijriDateTimeFromString.getYear() == todayHijriDateTime.getYear() &&
				hijriDateTimeFromString.getMonthOfYear() == todayHijriDateTime.getMonthOfYear() &&
				hijriDateTimeFromString.getDayOfMonth() == todayHijriDateTime.getDayOfMonth();
	}
}
