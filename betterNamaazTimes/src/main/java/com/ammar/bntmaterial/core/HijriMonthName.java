package com.ammar.bntmaterial.core;

public class HijriMonthName {

    public static final int MUHARRAM = 1;
    public static final int SAFAR = 2;
    public static final int RABI_AL_AWAL = 3;
    public static final int RABI_AL_AKHAR = 4;
    public static final int JUMADA_AL_ULA = 5;
    public static final int JUMADA_AL_UKHRA = 6;
    public static final int RAJAB = 7;
    public static final int SHABAAN = 8;
    public static final int RAMADAN = 9;
    public static final int SHAWWAL = 10;
    public static final int ZILQADATIL_HARAM = 11;
    public static final int ZILHAJJATIL_HARAM = 12;

    public static String getLongNamefromNumber(int num) {
        switch (num) {
            case MUHARRAM:
                return "Moharrum-ul-Haram";
            case SAFAR:
                return "Safar-ul-Muzaffar";
            case RABI_AL_AWAL:
                return "Rabi-ul-Awwal";
            case RABI_AL_AKHAR:
                return "Rabi-ul-Aakhar";
            case JUMADA_AL_ULA:
                return "Jamadal-Ula";
            case JUMADA_AL_UKHRA:
                return "Jumadal-Ukhra";
            case RAJAB:
                return "Rajab-ul-Asab";
            case SHABAAN:
                return "Shaban-al-Karim";
            case RAMADAN:
                return "Ramadan-al-Moazzam";
            case SHAWWAL:
                return "Shawwal-al-Mukarram";
            case ZILQADATIL_HARAM:
                return "Zilqadatil-Haram";
            case ZILHAJJATIL_HARAM:
                return "Zilhajjatil-Haram";
        }
        return "";
    }

    public static String getShortNameFromNumber(int num) {
        switch (num) {
            case MUHARRAM:
                return "Moharrum";
            case SAFAR:
                return "Safar";
            case RABI_AL_AWAL:
                return "Rabi-ul-Awwal";
            case RABI_AL_AKHAR:
                return "Rabi-ul-Aakhar";
            case JUMADA_AL_ULA:
                return "Jamadal-Ula";
            case JUMADA_AL_UKHRA:
                return "Jumadal-Ukhra";
            case RAJAB:
                return "Rajab";
            case SHABAAN:
                return "Shaban";
            case RAMADAN:
                return "Ramadan";
            case SHAWWAL:
                return "Shawwal";
            case ZILQADATIL_HARAM:
                return "Zilqad";
            case ZILHAJJATIL_HARAM:
                return "Zilhaj";
        }
        return "";
    }
}
