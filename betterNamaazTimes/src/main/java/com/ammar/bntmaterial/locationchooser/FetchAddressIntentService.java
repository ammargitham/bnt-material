package com.ammar.bntmaterial.locationchooser;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.util.Constants;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class FetchAddressIntentService extends IntentService {

	private static final String TAG = "FetchAddressIntentService";
	protected ResultReceiver mReceiver;

	public FetchAddressIntentService() {
		super("FetchAddressIntentService");
	}

	/**
	 * Creates an IntentService.  Invoked by your subclass's constructor.
	 *
	 * @param name Used to name the worker thread, important only for debugging.
	 */
	public FetchAddressIntentService(String name) {
		super(name);
	}

	/**
	 * This method is invoked on the worker thread with a request to process.
	 * Only one Intent is processed at a time, but the processing happens on a
	 * worker thread that runs independently from other application logic.
	 * So, if this code takes a long time, it will hold up other requests to
	 * the same IntentService, but it will not hold up anything else.
	 * When all requests have been handled, the IntentService stops itself,
	 * so you should not call {@link #stopSelf}.
	 *
	 * @param intent The value passed to {@link
	 *               Context#startService(Intent)}.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {

		Geocoder geocoder = new Geocoder(this, Locale.getDefault());
		String requestId = intent.getStringExtra(Constants.GEOCODER_REQUEST_ID);
		mReceiver = intent.getParcelableExtra(Constants.GEOCODER_RECEIVER);
		double latitude = intent.getDoubleExtra(Constants.LOCATION_LATITUDE, 0);
		double longitude = intent.getDoubleExtra(Constants.LOCATION_LONGITUDE, 0);

		String errorMessage = "";
		List<Address> addresses = null;

		try {
			addresses = geocoder.getFromLocation(latitude, longitude, 1);
		}
		catch (IOException ioException) {
			// Catch network or other I/O problems.
			errorMessage = getString(R.string.service_not_available);
			Log.e(TAG, errorMessage, ioException);
		}
		catch (IllegalArgumentException illegalArgumentException) {
			// Catch invalid latitude or longitude values.
			errorMessage = getString(R.string.invalid_lat_long_used);
			Log.e(TAG, errorMessage + ". " + "Latitude = " + latitude + ", Longitude = " + longitude, illegalArgumentException);
		}

		// Handle case where no address was found.
		if (addresses == null || addresses.size() == 0) {
			if (errorMessage.isEmpty()) {
				errorMessage = getString(R.string.no_address_found);
				Log.e(TAG, errorMessage);
			}

			deliverResultToReceiver(requestId, Constants.GEOCODER_FAILURE_RESULT, errorMessage);
		}
		else {
			Address address = addresses.get(0);
			String addressString;

			if (address.getMaxAddressLineIndex() == 0) {
				addressString = "";
			}
			else {
				addressString = address.getAddressLine(address.getMaxAddressLineIndex() - 1).trim();
			}
			deliverResultToReceiver(requestId, Constants.GEOCODER_SUCCESS_RESULT, addressString);
		}
	}

	private void deliverResultToReceiver(String requestId, int resultCode, String message) {
		Bundle bundle = new Bundle();
		bundle.putString(Constants.GEOCODER_RESULT_REQUEST_ID, requestId);
		bundle.putString(Constants.GEOCODER_RESULT_DATA_KEY, message);
		mReceiver.send(resultCode, bundle);
	}
}
