package com.ammar.bntmaterial.locationchooser;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.transition.Scene;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.util.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.UUID;

public class MapLocationChooserActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private double mLatitude;
    private double mLongitude;
    private String mAddress;
    private ViewGroup mSceneRoot;
    private Scene mMapClickScene;
    private boolean mChosen;
    private LatLng mChosenLatLng;
    private Scene mMainScene;
    private TextView mLatLngTextView;
    private Button mNegativeButton;
    private AddressResultReceiver mResultReceiver;
    private String lastRequestId;
    private TextView mAddressTextView;
    private int defaultTextColor;
    private ProgressBar mProgressBar;
    private Button mPositiveButton;
    private String mAddressOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_MapLocationChooserTheme);
        setContentView(R.layout.activity_map_location_chooser);

        mLatitude = getIntent().getDoubleExtra("latitude", 0);
        mLongitude = getIntent().getDoubleExtra("longitude", 0);
        mAddress = getIntent().getStringExtra("address");
        mAddress = mAddress == null ? "" : mAddress;

        mResultReceiver = new AddressResultReceiver(new Handler());

        mSceneRoot = (ViewGroup) findViewById(R.id.scene_root);
        mMainScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_map_chooser_tap, this);
        mMapClickScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_map_chooser_confirm, this);

        mMapClickScene.setEnterAction(new Runnable() {
            @Override
            public void run() {
                mProgressBar = (ProgressBar) mSceneRoot.findViewById(R.id.progressbar);
                mAddressTextView = (TextView) mSceneRoot.findViewById(R.id.address_tv);
                mLatLngTextView = (TextView) mSceneRoot.findViewById(R.id.textView);

                mLatLngTextView.setText(String.format(getResources().getString(R.string.location_chooser_latlong),
                        mChosenLatLng.latitude,
                        mChosenLatLng.longitude));

                mPositiveButton = (Button) mSceneRoot.findViewById(R.id.yes_btn);

                mPositiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.putExtra("address", mAddressOutput);
                        intent.putExtra("latitude", mChosenLatLng.latitude);
                        intent.putExtra("longitude", mChosenLatLng.longitude);
                        setResult(Constants.MAP_CHOOSER_SUCCESS_RESULT, intent);
                        finish();
                    }
                });
                mPositiveButton.setEnabled(false);

                mNegativeButton = (Button) mSceneRoot.findViewById(R.id.no_btn);

                mNegativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clearMap();
                    }
                });
                mNegativeButton.setEnabled(false);
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setMapToolbarEnabled(false);
        LatLng latLng = new LatLng(mLatitude, mLongitude);
        //mMap.addMarker(new MarkerOptions().position(latLng).title(mAddress));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        mChosenLatLng = latLng;
        mMap.addMarker(new MarkerOptions().position(latLng));
        if (!mChosen) {
            TransitionManager.go(mMapClickScene);
        }
        mChosen = true;
        startFetchAddressIntentService(latLng);
    }

    @Override
    public void onBackPressed() {

        if (mChosen) {
            clearMap();
        }
        else {
            super.onBackPressed();
        }
    }

    private void clearMap() {
        mMap.clear();
        mChosen = false;
        TransitionManager.go(mMainScene);
    }

    private void startFetchAddressIntentService(LatLng latLng) {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        lastRequestId = UUID.randomUUID().toString();
        intent.putExtra(Constants.GEOCODER_REQUEST_ID, lastRequestId);
        intent.putExtra(Constants.GEOCODER_RECEIVER, mResultReceiver);
        intent.putExtra(Constants.LOCATION_LATITUDE, latLng.latitude);
        intent.putExtra(Constants.LOCATION_LONGITUDE, latLng.longitude);
        startService(intent);
    }

    class AddressResultReceiver extends ResultReceiver {

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            String requestId = resultData.getString(Constants.GEOCODER_RESULT_REQUEST_ID);

            if (requestId != null && requestId.equals(lastRequestId)) {

                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }

                if (resultCode == Constants.GEOCODER_SUCCESS_RESULT) {

                    if (mAddressTextView != null) {
                        mAddressOutput = resultData.getString(Constants.GEOCODER_RESULT_DATA_KEY);
                        mAddressTextView.setText(mAddressOutput);
                        mAddressTextView.setClickable(false);
                    }


                    if (mLatLngTextView != null) {
                        mLatLngTextView.setText(String.format(getResources().getString(R.string.location_chooser_latlong),
                                mChosenLatLng.latitude,
                                mChosenLatLng.longitude));
                        mLatLngTextView.setVisibility(View.VISIBLE);
                    }

                    if (mPositiveButton != null && mNegativeButton != null) {
                        mPositiveButton.setEnabled(true);
                        mNegativeButton.setEnabled(true);
                    }
                }
                else {

                    if (mAddressTextView != null) {
                        mAddressTextView.setText(getString(R.string.location_chooser_failed));
                        mAddressTextView.setClickable(true);
                        defaultTextColor = mAddressTextView.getCurrentTextColor();
                        mAddressTextView.setTextColor(getResources().getColor(R.color.red));

                        mAddressTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAddressTextView.setText("Getting address...");
                                mAddressTextView.setTextColor(defaultTextColor);
                                mAddressTextView.setOnClickListener(null);
                                mProgressBar.setVisibility(View.VISIBLE);

                                if (mPositiveButton != null && mNegativeButton != null) {
                                    mPositiveButton.setEnabled(false);
                                    mNegativeButton.setEnabled(false);
                                }

                                startFetchAddressIntentService(mChosenLatLng);
                            }
                        });
                    }
                }
            }
        }
    }
}
