package com.ammar.bntmaterial.locationchooser;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by agitham on 18/11/2015.
 */
public class AddressResultReceiver extends ResultReceiver {

	private Creator        CREATOR;
	private ResultListener resultListener;

	public AddressResultReceiver(Handler handler) {
		super(handler);
	}

	public void setResultListener(ResultListener resultListener) {
		this.resultListener = resultListener;
	}

	public ResultListener getResultListener() {
		return this.resultListener;
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {

		if (resultListener != null) {
			resultListener.onReceiveResult(resultCode, resultData);
		}
	}

	interface ResultListener {

		void onReceiveResult(int resultCode, Bundle resultData);
	}
}