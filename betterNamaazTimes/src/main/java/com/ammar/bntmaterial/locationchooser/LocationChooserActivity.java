package com.ammar.bntmaterial.locationchooser;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ammar.bntmaterial.R;
import com.ammar.bntmaterial.shared.MainActivity;
import com.ammar.bntmaterial.shared.ui.transitions.EnterFromBottomTransition;
import com.ammar.bntmaterial.util.Constants;
import com.ammar.bntmaterial.util.PreferenceKeys;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Date;
import java.util.UUID;

public class LocationChooserActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, AddressResultReceiver.ResultListener {

	private static final String  TAG                                           = "LocationChooserActivity";
	private static final int     REQUEST_RESOLVE_ERROR                         = 1001;
	private static final String  DIALOG_ERROR                                  = "dialog_error";
	private static final int     MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 101;
	private              boolean mResolvingError                               = false;
	private GoogleApiClient       mGoogleApiClient;
	private Location              mLastLocation;
	private AddressResultReceiver mResultReceiver;
	private ViewGroup             mSceneRoot;
	private Scene                 mLocationFoundScene;
	private TextView              mLocatingTextView;
	private ProgressBar           mProgressBar;
	private int                   defaultTextColor;
	private String                mAddressOutput;
	private TextView              mAddressView;
	private TextView              mLatLongTextView;
	private int                   mLocationAutoUpdatePreference;
	private LocationRequest       mLocationRequest;
	private boolean               mLocationUpdateRequested;
	private boolean               mLocationAlreadySaved;
	private Scene                 mMainScene;
	private double                mLatitude;
	private double                mLongitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(R.style.Theme_LocationChooserTheme);
		setContentView(R.layout.activity_location_chooser);

		SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
		String savedLat = sharedPreferences.getString(PreferenceKeys.LATITUDE, "");
		String savedLong = sharedPreferences.getString(PreferenceKeys.LONGITUDE, "");
		mAddressOutput = sharedPreferences.getString(PreferenceKeys.ADDRESS, "");
		mLatitude = 0;
		mLongitude = 0;

		try {
			mLatitude = Double.parseDouble(savedLat);
			mLongitude = Double.parseDouble(savedLong);
			mLocationAlreadySaved = true;
		}
		catch (NumberFormatException e) {
			mLocationAlreadySaved = false;
		}

		mSceneRoot = (ViewGroup) findViewById(R.id.scene_root);
		mLocationFoundScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_location_chooser_found, this);
		mMainScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_location_chooser_main, LocationChooserActivity.this);
		mResultReceiver = new AddressResultReceiver(new Handler());

		setUpScenes();

		if (mLocationAlreadySaved) {
			mLocationFoundScene.enter();
		}

		if (!mLocationAlreadySaved) {
			mMainScene.enter();
		}
	}

	private void setUpScenes() {
		mMainScene.setEnterAction(new Runnable() {
			@Override
			public void run() {
				mLocationAlreadySaved = false;
				mLocatingTextView = (TextView) mSceneRoot.findViewById(R.id.locating_tv);
				mProgressBar = (ProgressBar) mSceneRoot.findViewById(R.id.progressbar);
				ImageView mPinImageView = (ImageView) mSceneRoot.findViewById(R.id.pin);
				Animation animation = AnimationUtils.loadAnimation(LocationChooserActivity.this, R.anim.scale_from_0_anim);
				animation.setInterpolator(new BounceInterpolator());
				animation.setAnimationListener(new Animation.AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) { }

					@Override
					public void onAnimationEnd(Animation animation) {
						if (mGoogleApiClient != null) {
							mGoogleApiClient.connect();
						}
					}

					@Override
					public void onAnimationRepeat(Animation animation) { }
				});

				mPinImageView.startAnimation(animation);

				mLocatingTextView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mLocatingTextView.setText(getString(R.string.location_chooser_locating));
						mLocatingTextView.setTextColor(defaultTextColor);
						mLocatingTextView.setClickable(false);
						mProgressBar.setVisibility(View.VISIBLE);
						if (mGoogleApiClient != null) {
							mGoogleApiClient.connect();
						}
					}
				});
				buildGoogleApiClient();
			}
		});

		mLocationFoundScene.setEnterAction(new Runnable() {
			@Override
			public void run() {
				mAddressView = (TextView) mSceneRoot.findViewById(R.id.address_tv);
				mLatLongTextView = (TextView) mSceneRoot.findViewById(R.id.latlong_tv);
				mAddressView.setText(mAddressOutput);
				Button positiveButton = (Button) mSceneRoot.findViewById(R.id.yes_btn);
				Button negativeButton = (Button) mSceneRoot.findViewById(R.id.no_btn);

				if (mLocationAlreadySaved) {
					mLatLongTextView.setText(
							String.format(getResources().getString(R.string.location_chooser_latlong),
									mLatitude,
									mLongitude));

					negativeButton.setText(getString(R.string.location_chooser_btn_negative_retry));

					positiveButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							// Location is correct and already saved in preferences. Just exit the activity.
							finish();
						}
					});

					negativeButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							// Locate again from FusedLocation. Scene transition.
							TransitionSet transitionSet = new TransitionSet();
							Transition foundTransition = TransitionInflater.from(LocationChooserActivity.this).inflateTransition(R.transition.location_main_scene_transitions);
							transitionSet.addTransition(foundTransition);
							TransitionManager transitionManager = new TransitionManager();
							transitionManager.setTransition(mLocationFoundScene, mMainScene, transitionSet);
							TransitionManager.go(mMainScene);
						}
					});
				}
				else {

					mLatLongTextView.setText(String.format(getResources().getString(R.string.location_chooser_latlong),
							mLastLocation.getLatitude(),
							mLastLocation.getLongitude()));

					negativeButton.setText(getString(R.string.location_chooser_btn_negative));

					positiveButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {

							LocationChooserActivity.LocationAutoUpdateDialog locationAutoUpdateDialog = LocationChooserActivity.LocationAutoUpdateDialog.newInstance(mLocationAutoUpdatePreference);
							locationAutoUpdateDialog.setOnDialogResultListener(new LocationChooserActivity.OnDialogResultListener() {
								@Override
								public void onResult(int resultOption) {
									mLocationAutoUpdatePreference = resultOption;
									SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
									editor.putString(PreferenceKeys.LATITUDE, String.valueOf(mLastLocation.getLatitude()));
									editor.putString(PreferenceKeys.LONGITUDE, String.valueOf(mLastLocation.getLongitude()));
									editor.putString(PreferenceKeys.ADDRESS, mAddressOutput);
									editor.putInt(PreferenceKeys.LOCATION_AUTO_UPDATE, mLocationAutoUpdatePreference);
									editor.putString(PreferenceKeys.LOCATION_LAST_UPDATED, new Date().toString());
									editor.apply();

									Intent intent = new Intent(LocationChooserActivity.this, MainActivity.class);
									startActivity(intent);
									finish();
								}
							});

							locationAutoUpdateDialog.show(getSupportFragmentManager(), "LocationAutoUpdateDialog");
						}
					});

					negativeButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(LocationChooserActivity.this, MapLocationChooserActivity.class);
							intent.putExtra("latitude", mLastLocation.getLatitude());
							intent.putExtra("longitude", mLastLocation.getLongitude());
							intent.putExtra("address", mAddressOutput);
							startActivityForResult(intent, Constants.MAP_CHOOSER_REQUEST_CODE);
						}
					});
				}
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient != null) {
			mGoogleApiClient.disconnect();
		}
	}

	private void buildGoogleApiClient() {
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();
		}
		if (mLocationRequest == null) {
			mLocationRequest = LocationRequest.create()
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
					.setNumUpdates(1)
					.setInterval(10 * 1000)        // 10 seconds, in milliseconds
					.setFastestInterval(1000); // 1 second, in milliseconds
		}
	}

	@Override
	public void onConnected(Bundle bundle) {
		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

		if (mLastLocation == null) {
			Log.d(TAG, "onConnected: mLastLocation is null!");
			mLocationUpdateRequested = true;
			int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
			if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
			}
			else {
				Log.d(TAG, "onConnected: Requesting Location Updates!");
				LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
			}
		}
		else {
			handleLocation(mLastLocation);
		}

		if (mLastLocation == null && !mLocationUpdateRequested) {
			connectionFailed();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d(TAG, "onConnected: Requesting Location Updates!");
					LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
				}
				else {
					connectionFailed();
				}
			}
		}
	}

	private void handleLocation(Location lastLocation) {
		// Determine whether a Geocoder is available.
		if (!Geocoder.isPresent()) {
			Toast.makeText(this, R.string.no_geocoder_available, Toast.LENGTH_LONG).show();
			return;
		}
		startFetchAddressIntentService(lastLocation);
	}

	private void connectionFailed() {
		mLocatingTextView.setText(getString(R.string.location_chooser_failed));
		mLocatingTextView.setClickable(true);
		defaultTextColor = mLocatingTextView.getCurrentTextColor();
		mLocatingTextView.setTextColor(ContextCompat.getColor(this, R.color.red));
		mProgressBar.setVisibility(View.GONE);
	}

	@Override
	public void onConnectionSuspended(int i) {
		Log.d(TAG, "onConnectionSuspended(): " + i);
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.d(TAG, "onConnectionFailed(): result: " + connectionResult.toString());
		connectionFailed();
		mGoogleApiClient.disconnect();
		if (!mResolvingError) {
			if (connectionResult.hasResolution()) {
				try {
					mResolvingError = true;
					connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
				}
				catch (IntentSender.SendIntentException e) {
					// There was an error with the resolution intent. Try again.
					mGoogleApiClient.connect();
				}
			}
			else {
				// Show dialog using GoogleApiAvailability.getErrorDialog()
				showErrorDialog(connectionResult.getErrorCode());
				mResolvingError = true;
			}
		}
	}

	/* Creates a dialog for an error message */
	private void showErrorDialog(int errorCode) {
		// Create a fragment for the error dialog
		ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
		// Pass the error that should be displayed
		Bundle args = new Bundle();
		args.putInt(DIALOG_ERROR, errorCode);
		dialogFragment.setArguments(args);
		dialogFragment.show(getSupportFragmentManager(), "errordialog");
	}

	/* Called from ErrorDialogFragment when the dialog is dismissed. */
	public void onDialogDismissed() {
		mResolvingError = false;
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			Log.d(TAG, "onLocationChanged: Found location via locationUpdates");
			mLastLocation = location;
			handleLocation(location);
		}
		else {
			connectionFailed();
		}
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
		// Display the address string
		// or an error message sent from the intent service.
		mAddressOutput = resultData.getString(Constants.GEOCODER_RESULT_DATA_KEY);

		if (resultCode == Constants.GEOCODER_SUCCESS_RESULT) {

			TransitionSet transitionSet = new TransitionSet();
			Transition foundTransition = TransitionInflater.from(LocationChooserActivity.this).inflateTransition(R.transition.found_scene_transitions);
			transitionSet.addTransition(foundTransition);
			EnterFromBottomTransition enterFromBottomTransition = new EnterFromBottomTransition();
			enterFromBottomTransition
					.addTarget(R.id.yes_btn)
					.addTarget(R.id.no_btn);
			transitionSet.addTransition(enterFromBottomTransition);

			TransitionManager transitionManager = new TransitionManager();
			transitionManager.setTransition(mMainScene, mLocationFoundScene, transitionSet);
			TransitionManager.go(mLocationFoundScene);
		}
		else {
			connectionFailed();
		}
	}

	/* A fragment to display an error dialog */
	public static class ErrorDialogFragment extends DialogFragment {

		public ErrorDialogFragment() { }

		@NonNull
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Get the error code and retrieve the appropriate dialog
			int errorCode = this.getArguments().getInt(DIALOG_ERROR);
			return GoogleApiAvailability.getInstance().getErrorDialog(this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			((LocationChooserActivity) getActivity()).onDialogDismissed();
		}
	}

	private void startFetchAddressIntentService(Location lastLocation) {
		Intent intent = new Intent(this, FetchAddressIntentService.class);
		String lastRequestId = UUID.randomUUID().toString();
		intent.putExtra(Constants.GEOCODER_REQUEST_ID, lastRequestId);
		intent.putExtra(Constants.GEOCODER_RECEIVER, mResultReceiver);
		intent.putExtra(Constants.LOCATION_LATITUDE, lastLocation.getLatitude());
		intent.putExtra(Constants.LOCATION_LONGITUDE, lastLocation.getLongitude());
		startService(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
			case Constants.MAP_CHOOSER_REQUEST_CODE:
				if (data != null && mAddressView != null && mLatLongTextView != null && resultCode == Constants.MAP_CHOOSER_SUCCESS_RESULT) {
					mAddressOutput = data.getStringExtra("address");
					mAddressOutput = mAddressOutput == null ? "" : mAddressOutput;
					double latitude = data.getDoubleExtra("latitude", 0);
					double longitude = data.getDoubleExtra("longitude", 0);
					mAddressView.setText(mAddressOutput);
					mLatLongTextView.setText(String.format(getResources().getString(R.string.location_chooser_latlong),
							latitude,
							longitude));
				}
				break;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mResultReceiver != null) {
			mResultReceiver.setResultListener(this);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mResultReceiver != null) {
			mResultReceiver.setResultListener(null);
		}
	}

	public interface OnDialogResultListener {

		void onResult(int resultOption);
	}

	public static class LocationAutoUpdateDialog extends DialogFragment {

		private static String initialOptionArgKey = "initialOption";
		private int                    locationAutoUpdatePreference;
		private OnDialogResultListener onDialogResultListener;

		public LocationAutoUpdateDialog() {

		}

		public static LocationAutoUpdateDialog newInstance(int initialOption) {

			Bundle args = new Bundle();
			args.putInt(initialOptionArgKey, initialOption);
			LocationAutoUpdateDialog fragment = new LocationAutoUpdateDialog();
			fragment.setArguments(args);
			return fragment;
		}

		@NonNull
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			locationAutoUpdatePreference = getArguments().getInt(initialOptionArgKey);

			return new AlertDialog.Builder(getActivity())
					.setTitle(R.string.location_auto_update_dialog_title)
					.setSingleChoiceItems(R.array.location_auto_update_options, locationAutoUpdatePreference, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							locationAutoUpdatePreference = which;
						}
					})
					.setPositiveButton(R.string.location_auto_update_dialog_ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
									if (onDialogResultListener != null) {
										onDialogResultListener.onResult(locationAutoUpdatePreference);
									}
								}
							}
					)
					.create();
		}

		public void setOnDialogResultListener(OnDialogResultListener onDialogResultListener) {
			this.onDialogResultListener = onDialogResultListener;
		}
	}
}
